<?php

use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesAuthoritiesController ;
use com\ooopener\controllers\EdgesSingleController ;
use com\ooopener\controllers\MediaObjectsOrderController ;
use com\ooopener\controllers\WebsitesController ;

use com\ooopener\controllers\applications\ApplicationsController ;
use com\ooopener\controllers\applications\ApplicationTranslationController ;

$container['applicationsController'] = function( $container )
{
    return new ApplicationsController( $container , $container->applications , 'applications' ) ;
};

$container['applicationApplicationsTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->applicationsTypes,
        $container->applications,
        $container->applicationApplicationsTypes
    );
};

$container['applicationAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->applications,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['applicationAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->applications,
        $container->mediaObjectsApplicationsAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ) ;
};

$container['applicationImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->applications,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['applicationPhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->applications,
        $container->mediaObjectsApplicationsPhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ) ;
};

$container['applicationProducerController'] = function( $container )
{
    return new EdgesAuthoritiesController
    (
        $container ,
        $container->people,
        $container->organizations,
        $container->applications,
        $container->applicationProducerPeople,
        $container->applicationProducerOrganizations,
        'producer'
    ) ;
};

$container['applicationProducerOrganizationsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->applicationProducerOrganizations
    );
};

$container['applicationProducerPeopleController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->applicationProducerPeople
    );
};

$container['applicationPublisherController'] = function( $container )
{
    return new EdgesAuthoritiesController
    (
        $container ,
        $container->people,
        $container->organizations,
        $container->applications,
        $container->applicationPublisherPeople,
        $container->applicationPublisherOrganizations,
        'publisher'
    ) ;
};

$container['applicationPublisherOrganizationsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->applicationPublisherOrganizations
    );
};

$container['applicationPublisherPeopleController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->applicationPublisherPeople
    );
};

$container['applicationSponsorController'] = function( $container )
{
    return new EdgesAuthoritiesController
    (
        $container ,
        $container->people,
        $container->organizations,
        $container->applications,
        $container->applicationSponsorPeople,
        $container->applicationSponsorOrganizations,
        'sponsor'
    ) ;
};

$container['applicationSponsorOrganizationsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->applicationSponsorOrganizations
    );
};

$container['applicationSponsorPeopleController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->applicationSponsorPeople
    );
};

$container['applicationTranslationController'] = function( $container )
{
    return new ApplicationTranslationController
    (
        $container,
        $container->applications,
        'applications',
        'alternativeHeadline,headline,description,notes,text'
    ) ;
};

$container['applicationUsersController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->users,
        $container->applications,
        $container->applicationUsers
    );
};

$container['applicationVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->applications,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['applicationVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->applications,
        $container->mediaObjectsApplicationsVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ) ;
};

$container['applicationWebsitesController'] = function( $container )
{
    return new WebsitesController
    (
        $container ,
        $container->applicationWebsites,
        $container->applications,
        $container->applicationApplicationsWebsites,
        'websites'
    ) ;
};

$container['applicationApplicationsWebsitesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->applicationApplicationsWebsites
    );
};
