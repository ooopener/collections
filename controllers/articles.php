<?php

use com\ooopener\controllers\ActiveController;
use com\ooopener\controllers\EdgesController;
use com\ooopener\controllers\EdgesFieldController;
use com\ooopener\controllers\EdgesSingleController;
use com\ooopener\controllers\EdgesSubController;
use com\ooopener\controllers\MediaObjectsOrderController;
use com\ooopener\controllers\WebsitesController;

use com\ooopener\controllers\articles\ArticlesController;
use com\ooopener\controllers\articles\ArticleTranslationController;

$container['articlesController'] = function( $container )
{
    return new ArticlesController( $container , $container->articles , 'articles' )  ;
};

$container['articleActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->articles , 'articles' )  ;
};

$container['articleArticlesTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->articlesTypes,
        $container->articles,
        $container->articleArticlesTypes
    );
};

$container['articleAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->articles,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['articleAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->articles,
        $container->mediaObjectsArticlesAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ) ;
};

$container['articleHasPartController'] = function( $container )
{
    return new EdgesSubController
    (
        $container,
        $container->articles,
        $container->articles,
        $container->articleHasPart
    ) ;
};

$container['articleImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->articles,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['articleIsRelatedToController'] = function( $container )
{
    return new EdgesSubController
    (
        $container,
        $container->articles,
        $container->articles,
        $container->articleIsRelatedTo
    ) ;
};

$container['articleIsSimilarToController'] = function( $container )
{
    return new EdgesSubController
    (
        $container,
        $container->articles,
        $container->articles,
        $container->articleIsSimilarTo
    ) ;
};

$container['articlePhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->articles,
        $container->mediaObjectsArticlesPhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ) ;
};

$container['articleTranslationController'] = function( $container )
{
    return new ArticleTranslationController
    (
        $container,
        $container->articles,
        'articles',
        'alternateHeadline,description,headline,text,notes'
    ) ;
};

$container['articleTypeController'] = function( $container )
{
    return new EdgesFieldController
    (
        $container,
        $container->articles,
        'additionalType',
        TRUE
    ) ;
};

$container['articleVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->articles,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['articleVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->articles,
        $container->mediaObjectsArticlesVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ) ;
};

$container['articleWebsitesController'] = function( $container )
{
    return new WebsitesController
    (
        $container ,
        $container->articleWebsites,
        $container->articles,
        $container->articleArticlesWebsites,
        'websites'
    ) ;
};

$container['articleArticlesWebsitesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->articleArticlesWebsites
    );
};
