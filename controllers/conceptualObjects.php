<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesAuthoritiesController ;
use com\ooopener\controllers\EdgesSingleController ;
use com\ooopener\controllers\KeywordsController ;
use com\ooopener\controllers\MediaObjectsOrderController ;
use com\ooopener\controllers\ThingStatusController ;
use com\ooopener\controllers\WebsitesController ;

use com\ooopener\controllers\conceptualObjects\ConceptualObjectsController ;
use com\ooopener\controllers\conceptualObjects\ConceptualObjectMarksController ;
use com\ooopener\controllers\conceptualObjects\ConceptualObjectMaterialsController ;
use com\ooopener\controllers\conceptualObjects\ConceptualObjectMeasurementsController ;
use com\ooopener\controllers\conceptualObjects\ConceptualObjectNumbersController ;
use com\ooopener\controllers\conceptualObjects\ConceptualObjectTranslationController ;

$container['conceptualObjectsController'] = function( $container )
{
    return new ConceptualObjectsController( $container , $container->conceptualObjects , 'conceptualObjects' )  ;
};

$container['conceptualObjectActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->conceptualObjects , 'conceptualObjects' )  ;
};

$container['conceptualObjectWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->conceptualObjects , 'conceptualObjects' )  ;
};

$container['conceptualObjectAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->conceptualObjects,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['conceptualObjectAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->conceptualObjects,
        $container->mediaObjectsConceptualObjectsAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    );
};

$container['conceptualObjectArtMovementsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->artMovements,
        $container->conceptualObjects,
        $container->conceptualObjectArtMovements
    );
};

$container['conceptualObjectArtTemporalEntitiesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->artTemporalEntities,
        $container->conceptualObjects,
        $container->conceptualObjectArtTemporalEntities
    );
};

$container['conceptualObjectConceptualObjectsCategoriesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->conceptualObjectCategories,
        $container->conceptualObjects,
        $container->conceptualObjectConceptualObjectsCategories
    );
};

$container['conceptualObjectConceptualObjectsMarksController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->conceptualObjectConceptualObjectsMarks
    );
};

$container['conceptualObjectConceptualObjectsMaterialsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->conceptualObjectConceptualObjectsMaterials
    );
};

$container['conceptualObjectConceptualObjectsMeasurementsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->conceptualObjectConceptualObjectsMeasurements
    );
};

$container['conceptualObjectConceptualObjectsNumbersController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->conceptualObjectConceptualObjectsNumbers
    );
};

$container['conceptualObjectImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->conceptualObjects,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['conceptualObjectKeywordsController'] = function( $container )
{
    return new KeywordsController
    (
        $container ,
        $container->conceptualObjectKeywords,
        $container->conceptualObjects,
        $container->conceptualObjectConceptualObjectsKeywords,
        'keywords'
    ) ;
};

$container['conceptualObjectConceptualObjectsKeywordsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->conceptualObjectConceptualObjectsKeywords
    );
};

$container['conceptualObjectPlacesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->places ,
        $container->conceptualObjects ,
        $container->conceptualObjectPlaces
    )  ;
};

$container['conceptualObjectProductionsOrganizationsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->organizations ,
        $container->conceptualObjects ,
        $container->conceptualObjectProductionsOrganizations
    )  ;
};

$container['conceptualObjectProductionsPeopleController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->people ,
        $container->conceptualObjects ,
        $container->conceptualObjectProductionsPeople
    )  ;
};

$container['conceptualObjectMarksController'] = function( $container )
{
    return new ConceptualObjectMarksController
    (
        $container ,
        $container->conceptualObjectMarks,
        $container->conceptualObjects,
        $container->conceptualObjectConceptualObjectsMarks,
        'marks'
    ) ;
};

$container['conceptualObjectMaterialsController'] = function( $container )
{
    return new ConceptualObjectMaterialsController
    (
        $container ,
        $container->conceptualObjectMaterials,
        $container->conceptualObjects,
        $container->conceptualObjectConceptualObjectsMaterials,
        'materials'
    ) ;
};

$container['conceptualObjectMeasurementsController'] = function( $container )
{
    return new ConceptualObjectMeasurementsController
    (
        $container ,
        $container->conceptualObjectMeasurements,
        $container->conceptualObjects,
        $container->conceptualObjectConceptualObjectsMeasurements,
        'measurements'
    ) ;
};

$container['conceptualObjectNumbersController'] = function( $container )
{
    return new ConceptualObjectNumbersController
    (
        $container ,
        $container->conceptualObjectNumbers,
        $container->conceptualObjects,
        $container->conceptualObjectConceptualObjectsNumbers,
        'numbers'
    ) ;
};

$container['conceptualObjectPhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->conceptualObjects,
        $container->mediaObjectsConceptualObjectsPhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    );
};

$container['conceptualObjectProductionsController'] = function( $container )
{
    return new EdgesAuthoritiesController
    (
        $container ,
        $container->people,
        $container->organizations,
        $container->conceptualObjects,
        $container->conceptualObjectProductionsPeople,
        $container->conceptualObjectProductionsOrganizations,
        'productions'
    ) ;
};

$container['conceptualObjectTranslationController'] = function( $container )
{
    return new ConceptualObjectTranslationController
    (
        $container,
        $container->conceptualObjects,
        'conceptualObjects',
        'about,alternateName,description,remarks'
    ) ;
};

$container['conceptualObjectVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->conceptualObjects,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['conceptualObjectVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->conceptualObjects,
        $container->mediaObjectsConceptualObjectsVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    );
};

$container['conceptualObjectWebsitesController'] = function( $container )
{
    return new WebsitesController
    (
        $container ,
        $container->conceptualObjectWebsites,
        $container->conceptualObjects,
        $container->conceptualObjectConceptualObjectsWebsites,
        'websites'
    ) ;
};

$container['conceptualObjectConceptualObjectsWebsitesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->conceptualObjectConceptualObjectsWebsites
    );
};
