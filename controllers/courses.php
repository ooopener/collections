<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesFieldController ;
use com\ooopener\controllers\EdgesSingleController ;
use com\ooopener\controllers\MediaObjectsOrderController ;
use com\ooopener\controllers\OpeningHoursController ;
use com\ooopener\controllers\ThingStatusController ;

use com\ooopener\controllers\courses\CoursesController ;
use com\ooopener\controllers\courses\CourseDiscoverController;
use com\ooopener\controllers\courses\CourseTranslationController ;
use com\ooopener\controllers\courses\CourseTransportationsController ;

$container['coursesController'] = function( $container )
{
    return new CoursesController( $container , $container->courses , 'courses' ) ;
};

$container['courseActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->courses , 'courses' )  ;
};

$container['courseWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->courses , 'courses' )  ;
};

$container['courseAdditionalTypeController'] = function( $container )
{
    return new EdgesFieldController
    (
        $container,
        $container->courses,
        'additionalType',
        TRUE
    ) ;
};

$container['courseAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->courses,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['courseAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->courses,
        $container->mediaObjectsCoursesAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ) ;
};

$container['courseCoursesAudiencesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->coursesAudiences,
        $container->courses,
        $container->courseCoursesAudiences
    );
};

$container['courseCoursesLevelsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->coursesLevels,
        $container->courses,
        $container->courseCoursesLevels
    );
};

$container['courseCoursesPathsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->coursesPaths,
        $container->courses,
        $container->courseCoursesPaths
    );
};

$container['courseCoursesStatusController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->coursesStatus,
        $container->courses,
        $container->courseCoursesStatus
    );
};

$container['courseCoursesTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->coursesTypes,
        $container->courses,
        $container->courseCoursesTypes
    );
};

$container['courseCoursesOpeningHoursController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->courseCoursesOpeningHours
    );
};

$container['courseCoursesTransportationsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->courseCoursesTransportations
    );
};

$container['courseDiscoverController'] = function( $container )
{
    return new CourseDiscoverController( $container , $container->courses , 'discover' ) ;
};

$container['courseImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->courses,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['courseOpeningHoursController'] = function( $container )
{
    return new OpeningHoursController
    (
        $container ,
        $container->courseOpeningHours,
        $container->courses,
        $container->coursePlacesOpeningHours,
        'openingHoursSpecification'
    ) ;
};

$container['coursePhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->courses,
        $container->mediaObjectsCoursesPhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ) ;
};

$container['courseTranslationController'] = function( $container )
{
    return new CourseTranslationController
    (
        $container,
        $container->courses,
        'courses',
        'about,alternativeHeadline,cautions,headline,description'
    ) ;
};

$container['courseTransportationsController'] = function( $container )
{
    return new CourseTransportationsController
    (
        $container,
        $container->courseTransportations,
        $container->courses,
        $container->courseCoursesTransportations,
        'transportations'
    );
};

$container['courseVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->courses,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['courseVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->courses,
        $container->mediaObjectsCoursesVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ) ;
};

/// discover

$container['courseArticlesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->articles ,
        $container->courses ,
        $container->courseArticles
    )  ;
};

$container['courseConceptualObjectsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->conceptualObjects ,
        $container->courses ,
        $container->courseConceptualObjects
    )  ;
};

$container['courseCoursesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->courses ,
        $container->courses ,
        $container->courseCourses
    )  ;
};

$container['courseEventsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->events ,
        $container->courses ,
        $container->courseEvents
    )  ;
};

$container['courseOrganizationsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->organizations ,
        $container->courses ,
        $container->courseOrganizations
    )  ;
};

$container['coursePeopleController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->people ,
        $container->courses ,
        $container->coursePeople
    )  ;
};

$container['coursePlacesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->places ,
        $container->courses ,
        $container->coursePlaces
    )  ;
};

$container['courseStagesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->stages ,
        $container->courses ,
        $container->courseStages
    )  ;
};
