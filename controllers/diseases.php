<?php

use com\ooopener\controllers\ActiveController;
use com\ooopener\controllers\EdgesController;
use com\ooopener\controllers\MultiEdgeFieldController;
use com\ooopener\controllers\ThingStatusController;

use com\ooopener\controllers\diseases\DiseasesController;
use com\ooopener\controllers\diseases\DiseasesTranslationController;
use com\ooopener\controllers\diseases\AnalysisMethodController;
use com\ooopener\controllers\diseases\AnalysisSamplingController;

$container['diseasesController'] = function( $container )
{
    return new DiseasesController( $container , $container->diseases , 'diseases' ) ;
};

$container['diseaseActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->diseases , 'diseases' )  ;
};

$container['diseaseWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->diseases , 'diseases' )  ;
};

$container['diseaseAnalysisMethodController'] = function( $container )
{
    return new AnalysisMethodController
    (
        $container ,
        $container->analysisMethod,
        $container->diseases,
        $container->diseaseDiseasesAnalysisMethod,
        'analysisMethod'
    ) ;
};

$container['diseaseDiseasesAnalysisMethodController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->diseaseDiseasesAnalysisMethod
    );
};

$container['diseaseAnalysisSamplingController'] = function( $container )
{
    return new AnalysisSamplingController
    (
        $container ,
        $container->analysisSampling,
        $container->diseases,
        $container->diseaseDiseasesAnalysisSampling,
        'analysisSampling'
    ) ;
};

$container['diseaseDiseasesAnalysisSamplingController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->diseaseDiseasesAnalysisSampling
    );
};

$container['diseaseDiseasesLevelsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->diseasesLevels,
        $container->diseases,
        $container->diseaseDiseasesLevels
    );
};

$container['diseaseDiseasesTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->diseasesTypes,
        $container->diseases,
        $container->diseaseDiseasesTypes
    );
};

$container['diseaseTransmissionsMethodsController'] = function( $container )
{
    return new MultiEdgeFieldController
    (
        $container ,
        'transmissionMethod',
        $container->transmissionsMethods,
        $container->diseases,
        $container->diseaseTransmissionsMethods,
        'diseases/transmissionMethod'
    );
};

$container['diseaseTranslationController'] = function( $container )
{
    return new DiseasesTranslationController
    (
        $container,
        $container->diseases,
        'diseases',
        'alternateName,description,notes,text'
    ) ;
};
