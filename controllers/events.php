<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesAuthoritiesController ;
use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesFieldController ;
use com\ooopener\controllers\EdgesSingleController ;
use com\ooopener\controllers\EdgesSubController ;
use com\ooopener\controllers\KeywordsController ;
use com\ooopener\controllers\MediaObjectsOrderController ;
use com\ooopener\controllers\OffersController ;
use com\ooopener\controllers\ThingStatusController ;
use com\ooopener\controllers\WebsitesController ;

use com\ooopener\controllers\events\EventsController ;
use com\ooopener\controllers\events\EventTranslationController ;

$container['eventsController'] = function( $container )
{
    return new EventsController( $container , $container->events , 'events' )  ;
};

$container['eventActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->events , 'events' )  ;
};

$container['eventAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->events,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['eventAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container ,
        $container->mediaObjects,
        $container->events,
        $container->mediaObjectsEventsAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ) ;
};

$container['eventWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->events , 'events' )  ;
};

$container['eventImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->events,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['eventKeywordsController'] = function( $container )
{
    return new KeywordsController
    (
        $container ,
        $container->eventKeywords,
        $container->events,
        $container->eventEventsKeywords,
        'keywords'
    ) ;
};

$container['eventEventsKeywordsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->eventEventsKeywords
    );
};

$container['eventEventsOffersController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->eventEventsOffers
    );
};

$container['eventEventsTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->eventsTypes,
        $container->events,
        $container->eventEventsTypes
    );
};

$container['eventOffersController'] = function( $container )
{
    return new OffersController
    (
        $container ,
        $container->eventOffers,
        $container->events,
        $container->eventEventsOffers,
        'offers'
    ) ;
};

$container['eventOrganizersController'] = function( $container )
{
    return new EdgesAuthoritiesController
    (
        $container,
        $container->people,
        $container->organizations,
        $container->events,
        $container->eventOrganizersPeople,
        $container->eventOrganizersOrganizations,
        'organizer'
    ) ;
};

$container['eventPlacesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->places ,
        $container->events ,
        $container->eventPlaces
    )  ;
};

$container['eventPhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container ,
        $container->mediaObjects,
        $container->events,
        $container->mediaObjectsEventsPhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ) ;
};

$container['eventStatusTypesController'] = function( $container )
{
    return new EdgesFieldController
    (
        $container,
        $container->events,
        'eventStatus',
        TRUE
    ) ;
};

$container['eventSubEventsController'] = function( $container )
{
    return new EdgesSubController
    (
        $container,
        $container->events,
        $container->events,
        $container->eventSubEvents
    ) ;
};

$container['eventTranslationController'] = function( $container )
{
    return new EventTranslationController
    (
        $container,
        $container->events ,
        'events' ,
        'alternateName,alternativeHeadline,description,headline,notes,text'
    ) ;
};

$container['eventTypeController'] = function( $container )
{
    return new EdgesFieldController
    (
        $container,
        $container->events,
        'additionalType',
        TRUE
    ) ;
};

$container['eventVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->events,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['eventVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container ,
        $container->mediaObjects,
        $container->events,
        $container->mediaObjectsEventsVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ) ;
};

$container['eventWebsitesController'] = function( $container )
{
    return new WebsitesController
    (
        $container ,
        $container->eventWebsites,
        $container->events,
        $container->eventEventsWebsites,
        'websites'
    ) ;
};

$container['eventEventsWebsitesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->eventEventsWebsites
    );
};

$container['eventWorksFeaturedController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->conceptualObjects,
        $container->events,
        $container->eventWorksFeatured
    ) ;
};
