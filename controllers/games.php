<?php

use com\ooopener\controllers\games\ApplicationGamesController;
use com\ooopener\controllers\games\BadgeItemController;
use com\ooopener\controllers\games\CourseGamesController;
use com\ooopener\controllers\games\QuestionGamesController;
use com\ooopener\controllers\games\GameTranslationController;

use com\ooopener\controllers\EdgesController;

$container['applicationsGamesController'] = function( $container )
{
    return new ApplicationGamesController( $container , $container->applicationsGames , 'games' ) ;
};

$container['applicationsGamesCoursesGamesController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->coursesGames,
        $container->applicationsGames,
        $container->applicationsGamesCoursesGames
    );
};

$container['applicationsGamesGamesTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->gamesTypes,
        $container->applicationsGames,
        $container->applicationsGamesGamesTypes
    );
};

$container['applicationGamesTranslationController'] = function( $container )
{
    return new GameTranslationController
    (
        $container,
        $container->applicationsGames,
        'games',
        'alternateName,description,notes,text'
    ) ;
};

$container['applicationBadgeItemController'] = function( $container )
{
    return new BadgeItemController
    (
        $container ,
        $container->applicationsGames ,
        $container->applicationsGamesController
    );
};

$container['coursesGamesController'] = function( $container )
{
    return new CourseGamesController
    (
        $container ,
        $container->coursesGames ,
        $container->applicationsGames ,
        $container->applicationsGamesCoursesGames ,
        $container->applicationsGamesController,
        'games/courses'
    ) ;
};

$container['coursesGamesGamesTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->gamesTypes,
        $container->coursesGames,
        $container->coursesGamesGamesTypes
    );
};

$container['coursesGamesQuestionsGamesController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->questionsGames,
        $container->coursesGames,
        $container->coursesGamesQuestionsGames
    );
};

$container['courseGamesTranslationController'] = function( $container )
{
    return new GameTranslationController
    (
        $container,
        $container->coursesGames,
        'games/courses',
        'alternateName,description,notes,text'
    ) ;
};

$container['courseBadgeItemController'] = function( $container )
{
    return new BadgeItemController
    (
        $container ,
        $container->coursesGames ,
        $container->coursesGamesController
    );
};

$container['questionsGamesController'] = function( $container )
{
    return new QuestionGamesController
    (
        $container ,
        $container->questionsGames ,
        $container->coursesGames ,
        $container->coursesGamesQuestionsGames ,
        $container->coursesGamesController,
        'games/questions'
    ) ;
};

$container['questionsGamesGamesTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->gamesTypes,
        $container->questionsGames,
        $container->questionsGamesGamesTypes
    );
};

$container['questionGamesTranslationController'] = function( $container )
{
    return new GameTranslationController
    (
        $container,
        $container->questionsGames,
        'games/questions',
        'alternateName,description,notes,text'
    ) ;
};

$container['questionBadgeItemController'] = function( $container )
{
    return new BadgeItemController
    (
        $container ,
        $container->questionsGames ,
        $container->questionsGamesController
    );
};