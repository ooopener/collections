<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesMedicalAuthoritiesController ;
use com\ooopener\controllers\ThingStatusController ;

use com\ooopener\controllers\livestocks\LivestockNumbersController ;
use com\ooopener\controllers\livestocks\LivestocksController ;
use com\ooopener\controllers\livestocks\observations\ObservationsController ;
use com\ooopener\controllers\livestocks\observations\ObservationTranslationController ;
use com\ooopener\controllers\livestocks\SectorsController ;
use com\ooopener\controllers\livestocks\WorkplacesController ;
use com\ooopener\controllers\livestocks\WorkshopsController ;

$container['livestocksController'] = function( $container )
{
    return new LivestocksController( $container , $container->livestocks , 'livestocks' ) ;
};

$container['livestockActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->livestocks , 'livestocks' )  ;
};

$container['livestockWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->livestocks , 'livestocks' )  ;
};

$container['livestocksLivestocksNumbersController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->livestocksLivestocksNumbers
    );
};

$container['livestockNumbersController'] = function( $container )
{
    return new LivestockNumbersController
    (
        $container ,
        $container->livestockNumbers ,
        $container->livestocks,
        $container->livestocksLivestocksNumbers,
        'numbers'
    ) ;
};

$container['livestockOrganizationsController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->organizations,
        $container->livestocks,
        $container->livestockOrganizations
    );
};

$container['livestockTechniciansController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->technicians,
        $container->livestocks,
        $container->livestockTechnicians
    );
};

$container['livestockVeterinariansController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->veterinarians,
        $container->livestocks,
        $container->livestockVeterinarians
    );
};

$container['livestocksWorkshopsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null ,
        null ,
        $container->livestocksWorkshops
    );
};

$container['observationsController'] = function( $container )
{
    return new ObservationsController( $container , $container->observations , 'observations' );
};

$container['observationActorsController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->people,
        $container->observations,
        $container->observationActors
    ) ;
};

$container['observationAttendeesController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->people,
        $container->observations,
        $container->observationAttendees
    ) ;
};

$container['observationAuthorityController'] = function( $container )
{
    return new EdgesMedicalAuthoritiesController
    (
        $container ,
        $container->veterinarians,
        $container->technicians,
        $container->observations,
        $container->observationAuthorityVeterinarians,
        $container->observationAuthorityTechnicians,
        'authority'
    ) ;
};

$container['observationAuthorityTechniciansController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->observationAuthorityTechnicians
    );
};

$container['observationAuthorityVeterinariansController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->observationAuthorityVeterinarians
    );
};

$container['observationLivestocksController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->livestocks,
        $container->observations,
        $container->observationLivestocks
    );
};

$container['observationObservationsStatusController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->observationsStatus,
        $container->observations,
        $container->observationObservationsStatus
    );
};

$container['observationObservationsTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->observationsTypes,
        $container->observations,
        $container->observationObservationsTypes
    );
};

$container['observationOwnersController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->users,
        $container->observations,
        $container->observationOwners
    );
};

$container['observationPeopleController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->people,
        $container->observations,
        $container->observationPeople
    );
};

$container['observationPlacesController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->places,
        $container->observations,
        $container->observationPlaces
    );
};

$container['observationTranslationController'] = function( $container )
{
    return new ObservationTranslationController
    (
        $container,
        $container->observations,
        'observations',
        'alternateName,description,notes'
    ) ;
};

$container['observationWorkshopsController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->workshops,
        $container->observations,
        $container->observationWorkshops
    );
};

$container['sectorsController'] = function( $container )
{
    return new SectorsController
    (
        $container ,
        $container->sectors ,
        $container->workplaces,
        $container->workplacesSectors,
        'sectors'
    ) ;
};

$container['sectorSectorsTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->sectorsTypes ,
        $container->sectors ,
        $container->sectorsSectorsTypes
    );
};

$container['workplacesController'] = function( $container )
{
    return new WorkplacesController
    (
        $container ,
        $container->workplaces ,
        $container->workshops,
        $container->workshopsWorkplaces,
        'workplaces'
    ) ;
};

$container['workplacesSectorsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->sectors ,
        $container->workplaces ,
        $container->workplacesSectors
    );
};

$container['workshopsBreedingsTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->breedingsTypes ,
        $container->workshops ,
        $container->workshopsBreedingsTypes
    );
};

$container['workshopsProductionsTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->productionsTypes ,
        $container->workshops ,
        $container->workshopsProductionsTypes
    );
};

$container['workshopsWaterOriginsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->waterOrigins ,
        $container->workshops ,
        $container->workshopsWaterOrigins
    );
};

$container['workshopsWorkplacesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->workplaces ,
        $container->workshops ,
        $container->workshopsWorkplaces
    );
};

$container['workshopsController'] = function( $container )
{
    return new WorkshopsController
    (
        $container ,
        $container->workshops ,
        $container->livestocks,
        $container->livestocksWorkshops,
        'workshops'
    ) ;
};
