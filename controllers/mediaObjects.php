<?php

use com\ooopener\controllers\EdgesController ;

use com\ooopener\controllers\creativeWork\MediaObjectsController ;
use com\ooopener\controllers\creativeWork\mediaObject\AudioObjectsController ;
use com\ooopener\controllers\creativeWork\mediaObject\ImageObjectsController ;
use com\ooopener\controllers\creativeWork\mediaObject\VideoObjectsController ;

$container['audioObjectsController'] = function( $container )
{
    return new AudioObjectsController( $container , $container->mediaObjects , 'audioObjects' ) ;
};

$container['imageObjectsController'] = function( $container )
{
    return new ImageObjectsController( $container , $container->mediaObjects , 'imageObjects' ) ;
};

$container['videoObjectsController'] = function( $container )
{
    return new VideoObjectsController( $container , $container->mediaObjects , 'videoObjects' ) ;
};

$container['mediaObjectsController'] = function( $container )
{
    return new MediaObjectsController( $container , $container->mediaObjects , 'mediaObjects' ) ;
};

$container['mediaObjectsThingsAudioController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->mediaObjects,
        null,//$container->things,
        $container->mediaObjectsThingsAudio
    );
};

$container['mediaObjectsThingsAudiosController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->mediaObjects,
        null,//$container->things,
        $container->mediaObjectsThingsAudios
    );
};

$container['mediaObjectsThingsImageController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->mediaObjects,
        null,//$container->things,
        $container->mediaObjectsThingsImage
    );
};

$container['mediaObjectsThingsLogoController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->mediaObjects,
        null,//$container->things,
        $container->mediaObjectsThingsLogo
    );
};

$container['mediaObjectsThingsPhotosController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->mediaObjects,
        null,//$container->things,
        $container->mediaObjectsThingsPhotos
    );
};

$container['mediaObjectsThingsVideoController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->mediaObjects,
        null,//$container->things,
        $container->mediaObjectsThingsVideo
    );
};

$container['mediaObjectsThingsVideosController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->mediaObjects,
        null,//$container->things,
        $container->mediaObjectsThingsVideos
    );
};
