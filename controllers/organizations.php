<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesAuthoritiesController ;
use com\ooopener\controllers\EdgesFieldController ;
use com\ooopener\controllers\EdgesSingleController ;
use com\ooopener\controllers\EdgesSubController ;
use com\ooopener\controllers\EmailsController ;
use com\ooopener\controllers\GeoCoordinatesController ;
use com\ooopener\controllers\KeywordsController ;
use com\ooopener\controllers\MediaObjectsOrderController ;
use com\ooopener\controllers\PhoneNumbersController ;
use com\ooopener\controllers\PostalAddressController ;
use com\ooopener\controllers\ThingStatusController ;
use com\ooopener\controllers\WebsitesController ;

use com\ooopener\controllers\organizations\OrganizationNumbersController ;
use com\ooopener\controllers\organizations\OrganizationsController ;
use com\ooopener\controllers\organizations\OrganizationTranslationController ;

$container['organizationsController'] = function( $container )
{
    return new OrganizationsController( $container , $container->organizations , 'organizations' )  ;
};

$container['organizationActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->organizations , 'organizations' )  ;
};

$container['organizationApeController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->organizationsNaf ,
        $container->organizations ,
        $container->organizationApe
    )  ;
};

$container['organizationAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->organizations,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['organizationAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->organizations,
        $container->mediaObjectsOrganizationsAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ) ;
};

$container['organizationWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->organizations , 'organizations' )  ;
};

$container['organizationDepartmentController'] = function( $container )
{
    return new EdgesSubController
    (
        $container,
        $container->organizations,
        $container->organizations,
        $container->organizationDepartment
    ) ;
};

$container['organizationEmailsController'] = function( $container )
{
    return new EmailsController
    (
        $container,
        $container->organizationEmails,
        $container->organizations,
        $container->organizationOrganizationsEmails,
        'email'
    ) ;
};

$container['organizationEmployeesController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->people,
        $container->organizations,
        $container->organizationEmployees
    ) ;
};

$container['organizationFounderController'] = function( $container )
{
    return new EdgesController
    (
        $container,
        $container->people,
        $container->organizations,
        $container->organizationFounder
    ) ;
};

$container['organizationFoundingLocationController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->places ,
        $container->organizations ,
        $container->organizationFoundingLocation
    )  ;
};

$container['organizationGeoCoordinatesController'] = function( $container )
{
    return new GeoCoordinatesController( $container , $container->organizations );
};

$container['organizationImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->organizations,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['organizationKeywordsController'] = function( $container )
{
    return new KeywordsController
    (
        $container,
        $container->organizationKeywords,
        $container->organizations,
        $container->organizationOrganizationsKeywords,
        'keywords'
    ) ;
};

$container['organizationLegalFormController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->businessEntityTypes ,
        $container->organizations ,
        $container->organizationLegalForm
    )  ;
};

$container['organizationLogoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->organizations,
        $container->mediaObjectsThingsLogo,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['organizationNumbersController'] = function( $container )
{
    return new OrganizationNumbersController
    (
        $container ,
        $container->organizationNumbers ,
        $container->organizations,
        $container->organizationOrganizationsNumbers,
        'numbers'
    ) ;
};

$container['organizationOrganizationsEmailsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->organizationOrganizationsEmails
    );
};

$container['organizationOrganizationsKeywordsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->organizationOrganizationsKeywords
    );
};

$container['organizationOrganizationsNumbersController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->organizationOrganizationsNumbers
    );
};

$container['organizationOrganizationsPhoneNumbersController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->organizationOrganizationsPhoneNumbers
    );
};

$container['organizationOrganizationsTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->organizationsTypes,
        $container->organizations,
        $container->organizationOrganizationsTypes
    );
};

$container['organizationPhoneNumbersController'] = function( $container )
{
    return new PhoneNumbersController
    (
        $container,
        $container->organizationPhoneNumbers,
        $container->organizations,
        $container->organizationOrganizationsPhoneNumbers,
        'telephone'
    ) ;
};

$container['organizationPlacesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->places ,
        $container->organizations ,
        $container->organizationPlaces
    )  ;
};

$container['organizationMembersController'] = function( $container )
{
    return new EdgesAuthoritiesController
    (
        $container ,
        $container->people,
        $container->organizations,
        $container->organizations,
        $container->organizationMembersPeople,
        $container->organizationMembersOrganizations,
        'members'
    ) ;
};

$container['organizationMembersOrganizationsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->organizationMembersOrganizations
    );
};

$container['organizationMembersPeopleController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->organizationMembersPeople
    );
};

$container['organizationPhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->organizations,
        $container->mediaObjectsOrganizationsPhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ) ;
};

$container['organizationPostalAddressController'] = function( $container )
{
    return new PostalAddressController( $container , $container->organizations );
};

$container['organizationProvidersController'] = function( $container )
{
    return new EdgesAuthoritiesController
    (
        $container ,
        $container->people,
        $container->organizations,
        $container->organizations,
        $container->organizationProvidersPeople,
        $container->organizationProvidersOrganizations,
        'providers'
    ) ;
};

$container['organizationSubOrganizationsController'] = function( $container )
{
    return new EdgesSubController
    (
        $container,
        $container->organizations,
        $container->organizations,
        $container->organizationSubOrganizations
    ) ;
};

$container['organizationTranslationController'] = function( $container )
{
    return new OrganizationTranslationController
    (
        $container,
        $container->organizations,
        'organizations',
        'about,additional,alternateName,description'
    ) ;
};

$container['organizationAdditionalTypeController'] = function( $container )
{
    return new EdgesFieldController
    (
        $container,
        $container->organizations,
        'additionalType',
        TRUE
    ) ;
};

$container['organizationVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->organizations,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['organizationVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->organizations,
        $container->mediaObjectsOrganizationsVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ) ;
};

$container['organizationWebsitesController'] = function( $container )
{
    return new WebsitesController
    (
        $container ,
        $container->organizationWebsites,
        $container->organizations,
        $container->organizationOrganizationsWebsites,
        'websites'
    ) ;
};

$container['organizationOrganizationsWebsitesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->organizationOrganizationsWebsites
    );
};
