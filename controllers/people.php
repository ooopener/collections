<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesSingleController ;
use com\ooopener\controllers\EmailsController ;
use com\ooopener\controllers\KeywordsController ;
use com\ooopener\controllers\MediaObjectsOrderController ;
use com\ooopener\controllers\PhoneNumbersController ;
use com\ooopener\controllers\PostalAddressController ;
use com\ooopener\controllers\ThingStatusController ;
use com\ooopener\controllers\WebsitesController ;

use com\ooopener\controllers\people\PeopleController ;
use com\ooopener\controllers\people\PeopleTranslationController ;

$container['peopleController'] = function( $container )
{
    return new PeopleController( $container , $container->people , 'people' )  ;
};

$container['peopleActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->people , 'people' )  ;
};

$container['peopleAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->people,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['peopleAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container ,
        $container->mediaObjects,
        $container->people,
        $container->mediaObjectsPeopleAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ) ;
};

$container['peopleWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->people , 'people' )  ;
};

$container['peopleEmailsController'] = function( $container )
{
    return new EmailsController
    (
        $container,
        $container->peopleEmails,
        $container->people,
        $container->peoplePeopleEmails,
        'email'
    ) ;
};

$container['peopleImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->people,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['peopleKeywordsController'] = function( $container )
{
    return new KeywordsController
    (
        $container,
        $container->peopleKeywords,
        $container->people,
        $container->peoplePeopleKeywords,
        'keywords'
    ) ;
};

$container['peoplePeopleEmailsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->peoplePeopleEmails
    );
};

$container['peoplePeopleKeywordsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->peoplePeopleKeywords
    );
};

$container['peoplePeoplePhoneNumbersController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->peoplePeoplePhoneNumbers
    );
};

$container['peoplePeopleWebsitesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->peoplePeopleWebsites
    );
};

$container['peopleJobsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->jobs ,
        $container->people,
        $container->peopleJobs
    ) ;
};

$container['peoplePhoneNumbersController'] = function( $container )
{
    return new PhoneNumbersController
    (
        $container,
        $container->peoplePhoneNumbers,
        $container->people,
        $container->peoplePeoplePhoneNumbers,
        'telephone'
    ) ;
};

$container['peoplePhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container ,
        $container->mediaObjects,
        $container->people,
        $container->mediaObjectsPeoplePhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ) ;
};

$container['peoplePostalAddressController'] = function( $container )
{
    return new PostalAddressController( $container , $container->people );
};

$container['peopleTranslationController'] = function( $container )
{
    return new PeopleTranslationController
    (
        $container,
        $container->people,
        'people',
        'about,description,remarks'
    ) ;
};

$container['peopleVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->people,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['peopleVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container ,
        $container->mediaObjects,
        $container->people,
        $container->mediaObjectsPeopleVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ) ;
};

$container['peopleWebsitesController'] = function( $container )
{
    return new WebsitesController
    (
        $container,
        $container->peopleWebsites,
        $container->people,
        $container->peoplePeopleWebsites,
        'websites'
    ) ;
};
