<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesSingleController ;
use com\ooopener\controllers\EdgesSubController ;
use com\ooopener\controllers\EmailsController ;
use com\ooopener\controllers\GeoCoordinatesController ;
use com\ooopener\controllers\KeywordsController ;
use com\ooopener\controllers\MediaObjectsOrderController ;
use com\ooopener\controllers\MultiEdgeFieldController ;
use com\ooopener\controllers\OffersController ;
use com\ooopener\controllers\OpeningHoursController ;
use com\ooopener\controllers\PhoneNumbersController ;
use com\ooopener\controllers\PostalAddressController ;
use com\ooopener\controllers\EdgesFieldController ;
use com\ooopener\controllers\ThingStatusController ;
use com\ooopener\controllers\WebsitesController ;

use com\ooopener\controllers\places\PlacesController ;
use com\ooopener\controllers\places\PlaceTranslationController ;

$container['placesController'] = function( $container )
{
    return new PlacesController( $container , $container->places , 'places' )  ;
};

$container['placeActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->places , 'places' )  ;
};

$container['placeAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->places,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['placeAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->places,
        $container->mediaObjectsPlacesAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ) ;
};

$container['placeWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->places , 'places' )  ;
};

$container['placeActivitiesController'] = function( $container )
{
    return new MultiEdgeFieldController
    (
        $container,
        'activities',
        $container->activities,
        $container->places,
        $container->placeActivities,
        'places/activities'
    ) ;
};

$container['placeEmailsController'] = function( $container )
{
    return new EmailsController
    (
        $container,
        $container->placeEmails,
        $container->places,
        $container->placePlacesEmails,
        'email'
    ) ;
};

$container['placeGeoCoordinatesController'] = function( $container )
{
    return new GeoCoordinatesController( $container , $container->places );
};

$container['placeImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->places,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['placeKeywordsController'] = function( $container )
{
    return new KeywordsController
    (
        $container,
        $container->placeKeywords,
        $container->places,
        $container->placePlacesKeywords,
        'keywords'
    ) ;
};

$container['placeLogoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->places,
        $container->mediaObjectsThingsLogo,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['placeMuseumsTypesController'] = function( $container )
{
    return new MultiEdgeFieldController
    (
        $container,
        'museumType',
        $container->museumsTypes,
        $container->places,
        $container->placeMuseumsTypes,
        'places/museumType'
    ) ;
};

$container['placePermitsController'] = function( $container )
{
    return new MultiEdgeFieldController
    (
        $container,
        'permits',
        $container->placesRegulations,
        $container->places,
        $container->placePermits,
        'places/permits'
    ) ;
};

$container['placePhoneNumbersController'] = function( $container )
{
    return new PhoneNumbersController
    (
        $container,
        $container->placePhoneNumbers,
        $container->places,
        $container->placePlacesPhoneNumbers,
        'telephone'
    ) ;
};

$container['placePlacesEmailsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->placePlacesEmails
    );
};

$container['placePlacesKeywordsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->placePlacesKeywords
    );
};

$container['placePlacesOffersController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->placePlacesOffers
    );
};

$container['placePlacesPhoneNumbersController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->placePlacesPhoneNumbers
    );
};

$container['placePlacesOpeningHoursController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->placePlacesOpeningHours
    );
};

$container['placePlacesTypesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->placesTypes,
        $container->places,
        $container->placePlacesTypes
    );
};

$container['placeProhibitionsController'] = function( $container )
{
    return new MultiEdgeFieldController
    (
        $container,
        'prohibitions',
        $container->placesRegulations,
        $container->places,
        $container->placeProhibitions,
        'places/prohibitions'
    ) ;
};

$container['placeOffersController'] = function( $container )
{
    return new OffersController
    (
        $container ,
        $container->placeOffers,
        $container->places,
        $container->placePlacesOffers,
        'offers'
    ) ;
};

$container['placeOpeningHoursController'] = function( $container )
{
    return new OpeningHoursController
    (
        $container ,
        $container->placeOpeningHours,
        $container->places,
        $container->placePlacesOpeningHours,
        'openingHoursSpecification'
    ) ;
};

$container['placePhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->places,
        $container->mediaObjectsPlacesPhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ) ;
};

$container['placePostalAddressController'] = function( $container )
{
    return new PostalAddressController( $container , $container->places );
};

$container['placeServicesController'] = function( $container )
{
    return new MultiEdgeFieldController
    (
        $container,
        'services',
        $container->services,
        $container->places,
        $container->placeServices,
        'places/services'
    ) ;
};

$container['placeContainsPlacesController'] = function( $container )
{
    return new EdgesSubController
    (
        $container,
        $container->places,
        $container->places,
        $container->placeContainsPlaces
    ) ;
};

$container['placeStatusController'] = function( $container )
{
    return new EdgesFieldController
    (
        $container,
        $container->places,
        'status',
        TRUE
    ) ;
};

$container['placeTranslationController'] = function( $container )
{
    return new PlaceTranslationController
    (
        $container,
        $container->places,
        'places',
        'about,additional,alternateName,description'
    ) ;
};

$container['placeAdditionalTypeController'] = function( $container )
{
    return new EdgesFieldController
    (
        $container,
        $container->places,
        'additionalType',
        TRUE
    ) ;
};

$container['placeVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->places,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['placeVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->places,
        $container->mediaObjectsPlacesVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ) ;
};

$container['placeWebsitesController'] = function( $container )
{
    return new WebsitesController
    (
        $container ,
        $container->placeWebsites,
        $container->places,
        $container->placePlacesWebsites,
        'websites'
    ) ;
};

$container['placePlacesWebsitesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->placePlacesWebsites
    );
};
