<?php

use com\ooopener\controllers\SettingsListController ;

$container['settingsListController'] = function( $container )
{
    return new SettingsListController( $container , $container->settingsList , 'settings' ) ;
};
