<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesSingleController ;
use com\ooopener\controllers\KeywordsController ;
use com\ooopener\controllers\MediaObjectsOrderController ;
use com\ooopener\controllers\MultiEdgeFieldController ;
use com\ooopener\controllers\ThingStatusController ;
use com\ooopener\controllers\WebsitesController ;

use com\ooopener\controllers\stages\StagesController ;
use com\ooopener\controllers\stages\StageDiscoverController;
use com\ooopener\controllers\stages\StageTranslationController;

$container['stagesController'] = function( $container )
{
    return new StagesController( $container , $container->stages , 'stages' ) ;
};

$container['stageActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->stages , 'stages' )  ;
};

$container['stageWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->stages , 'stages' )  ;
};

$container['stageAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->stages,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['stageAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->stages,
        $container->mediaObjectsStagesAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ) ;
};

$container['stageDiscoverController'] = function( $container )
{
    return new StageDiscoverController( $container , $container->stages , 'discover' ) ;
};

$container['stageImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->stages,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['stageKeywordsController'] = function( $container )
{
    return new KeywordsController
    (
        $container,
        $container->stageKeywords,
        $container->stages,
        $container->stagePlacesKeywords,
        'keywords'
    ) ;
};

$container['stageStagesKeywordsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->stageStagesKeywords
    );
};

$container['stagePhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->stages,
        $container->mediaObjectsStagesPhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ) ;
};

$container['stageActivitiesController'] = function( $container )
{
    return new MultiEdgeFieldController
    (
        $container ,
        'activities',
        $container->activities,
        $container->stages,
        $container->stageActivities
    );
};

$container['stageVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->stages,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['stageVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->stages,
        $container->mediaObjectsStagesVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ) ;
};

$container['stageWebsitesController'] = function( $container )
{
    return new WebsitesController
    (
        $container ,
        $container->stageWebsites,
        $container->stages,
        $container->stageStagesWebsites,
        'websites'
    ) ;
};

$container['stageStagesWebsitesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,
        null,
        $container->stageStagesWebsites
    );
};

$container['stageCoursesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->courses ,
        $container->stages ,
        $container->stageCourses
    )  ;
};

$container['stageEventsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->events ,
        $container->stages ,
        $container->stageEvents
    )  ;
};

$container['stageLocationsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->places ,
        $container->stages ,
        $container->stageLocations
    )  ;
};

$container['stageArticlesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->articles ,
        $container->stages ,
        $container->stageArticles
    )  ;
};

$container['stageConceptualObjectsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->conceptualObjects ,
        $container->stages ,
        $container->stageConceptualObjects
    )  ;
};

$container['stageOrganizationsController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->organizations ,
        $container->stages ,
        $container->stageOrganizations
    )  ;
};

$container['stagePeopleController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->people ,
        $container->stages ,
        $container->stagePeople
    )  ;
};

$container['stagePlacesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->places ,
        $container->stages ,
        $container->stagePlaces
    )  ;
};

$container['stageStagesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->stages ,
        $container->stages ,
        $container->stageStages
    )  ;
};

$container['stageCoursesStatusController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->coursesStatus,
        $container->stages,
        $container->stageCoursesStatus
    );
};

$container['stageTranslationController'] = function( $container )
{
    return new StageTranslationController
    (
        $container,
        $container->stages,
        'stages',
        'alternativeHeadline,headline,text,description'
    ) ;
};
