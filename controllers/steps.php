<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController ;
use com\ooopener\controllers\EdgesSingleController ;
use com\ooopener\controllers\MediaObjectsOrderController ;
use com\ooopener\controllers\ThingStatusController ;

use com\ooopener\controllers\steps\StepsController;
use com\ooopener\controllers\steps\StepTranslationController;

$container['stepsController'] = function( $container )
{
    return new StepsController
    (
        $container ,
        $container->steps ,
        $container->courses ,
        'steps'
    ) ;
};

$container['stepActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->steps , 'steps' )  ;
};

$container['stepAudioController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->steps,
        $container->mediaObjectsThingsAudio,
        [ 'doc.encodingFormat =~ "audio"' ],
        'mediaObjects'
    );
};

$container['stepAudiosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->steps,
        $container->mediaObjectsStepsAudios,
        [ 'doc.encodingFormat =~ "audio"' ],
        'audios'
    ) ;
};

$container['stepWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->steps , 'steps' )  ;
};

$container['stepImageController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->steps,
        $container->mediaObjectsThingsImage,
        [ 'doc.encodingFormat =~ "image"' ],
        'mediaObjects'
    );
};

$container['stepPhotosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->steps,
        $container->mediaObjectsStepsPhotos,
        [ 'doc.encodingFormat =~ "image"' ],
        'photos'
    ) ;
};

$container['stepStagesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        $container->stages,
        $container->steps,
        $container->stepStages
    ) ;
};

$container['stepTranslationController'] = function( $container )
{
    return new StepTranslationController
    (
        $container,
        $container->steps,
        'steps',
        'alternativeHeadline,description,headline,notes,text'
    ) ;
};

$container['stepVideoController'] = function( $container )
{
    return new EdgesSingleController
    (
        $container,
        $container->mediaObjects,
        $container->steps,
        $container->mediaObjectsThingsVideo,
        [ 'doc.encodingFormat =~ "video"' ],
        'mediaObjects'
    );
};

$container['stepVideosController'] = function( $container )
{
    return new MediaObjectsOrderController
    (
        $container,
        $container->mediaObjects,
        $container->steps,
        $container->mediaObjectsStepsVideos,
        [ 'doc.encodingFormat =~ "video"' ],
        'videos'
    ) ;
};
