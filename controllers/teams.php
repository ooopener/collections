<?php

use com\ooopener\controllers\teams\TeamPermissionsController ;
use com\ooopener\controllers\teams\TeamsController ;

use com\ooopener\controllers\teams\TeamTranslationController ;

$container['teamsController'] = function( $container )
{
    return new TeamsController( $container , $container->teams ) ;
};

$container['teamPermissionsController'] = function( $container )
{
    return new TeamPermissionsController( $container , $container->teams ) ;
};

$container['teamTranslationController'] = function( $container )
{
    return new TeamTranslationController
    (
        $container,
        $container->teams,
        'teams',
        'alternateName,description'
    ) ;
};