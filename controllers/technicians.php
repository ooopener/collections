<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController;
use com\ooopener\controllers\ThingStatusController;

use com\ooopener\controllers\technicians\TechniciansController ;

$container['techniciansController'] = function( $container )
{
    return new TechniciansController( $container , $container->technicians , 'technicians' );
};

$container['technicianActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->technicians , 'technicians' )  ;
};

$container['technicianWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->technicians , 'technicians' )  ;
};

$container['technicianMedicalSpecialtiesController'] = function ( $container )
{
    return new EdgesController
    (
        $container ,
        $container->medicalSpecialties ,
        $container->technicians ,
        $container->technicianMedicalSpecialties
    );
};

$container['technicianPeopleController'] = function ( $container )
{
    return new EdgesController
    (
        $container ,
        $container->people ,
        $container->technicians ,
        $container->technicianPeople
    );
};
