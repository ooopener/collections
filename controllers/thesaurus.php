<?php

use com\ooopener\controllers\ThesaurusListController ;

$container['thesaurusListController'] = function( $container )
{
    return new ThesaurusListController( $container , $container->thesaurusList , 'thesaurus' ) ;
};

