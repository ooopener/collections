<?php

use com\ooopener\controllers\CollectionsController ;

$container['thingsController'] = function( $container )
{
    return new CollectionsController
    (
        $container ,
        $container->things ,
        'things'
    );
};
