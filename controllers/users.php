<?php

use com\ooopener\controllers\users\UserActivityLogsController ;
use com\ooopener\controllers\users\UserPermissionsController ;
use com\ooopener\controllers\users\UserPostalAddressController ;
use com\ooopener\controllers\users\UsersController ;
use com\ooopener\controllers\users\UserSessionsController ;

use com\ooopener\controllers\EdgesController ;

$container['usersController'] = function( $container )
{
    return new UsersController( $container , $container->users , 'users' ) ;
};

$container['usersActivityLogsController'] = function( $container )
{
    return new UserActivityLogsController( $container , $container->activityLogs , 'users/activityLogs' ) ;
};

$container['userFavoritesController'] = function( $container )
{
    return new EdgesController
    (
        $container ,
        null,//$container->things ,
        $container->users ,
        $container->userFavorites
    ) ;
};

$container['userPermissionsController'] = function( $container )
{
    return new UserPermissionsController( $container , $container->users , $container->userPermissions ) ;
};

$container['userPostalAddressController'] = function( $container )
{
    return new UserPostalAddressController( $container , $container->users ) ;
};

$container['userSessionsController'] = function( $container )
{
    return new UserSessionsController( $container , $container->sessions , 'users/sessions' ) ;
};
