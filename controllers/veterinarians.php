<?php

use com\ooopener\controllers\ActiveController ;
use com\ooopener\controllers\EdgesController;
use com\ooopener\controllers\EdgesAuthoritiesController ;
use com\ooopener\controllers\ThingStatusController ;

use com\ooopener\controllers\veterinarians\VeterinariansController ;

$container['veterinariansController'] = function( $container )
{
    return new VeterinariansController( $container , $container->veterinarians , 'veterinarians' );
};

$container['veterinarianActiveController'] = function( $container )
{
    return new ActiveController( $container , $container->veterinarians , 'veterinarians' )  ;
};

$container['veterinarianWithStatusController'] = function( $container )
{
    return new ThingStatusController( $container , $container->veterinarians , 'veterinarians' )  ;
};

$container['veterinarianAuthorityController'] = function( $container )
{
    return new EdgesAuthoritiesController
    (
        $container ,
        $container->people,
        $container->organizations,
        $container->veterinarians,
        $container->veterinarianPeople,
        $container->veterinarianOrganizations,
        'authority'
    ) ;
};

$container['veterinarianMedicalSpecialtiesController'] = function ( $container )
{
    return new EdgesController
    (
        $container ,
        $container->medicalSpecialties ,
        $container->veterinarians ,
        $container->veterinarianMedicalSpecialties
    );
};

$container['veterinarianOrganizationsController'] = function ( $container )
{
    return new EdgesController
    (
        $container ,
        $container->organizations ,
        $container->veterinarians ,
        $container->veterinarianOrganizations
    );
};

$container['veterinarianPeopleController'] = function ( $container )
{
    return new EdgesController
    (
        $container ,
        $container->people ,
        $container->veterinarians ,
        $container->veterinarianPeople
    );
};
