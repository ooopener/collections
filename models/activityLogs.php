<?php

use com\ooopener\models\ActivityLogs ;

$container['activityLogs'] =  function( $container )
{
    return new ActivityLogs
    (
        $container ,
        'users_activity_logs',
        [
            'facetable' =>
            [
                'resource' =>
                [
                    'resource' => 'field'
                ],
                'user' =>
                [
                    'user' => 'field'
                ]
            ],
            'searchable' =>
            [
                'user' , 'method' , 'resource'
            ],
            'sortable' =>
            [
                'id'      => '_key',
                'created' => 'created'
            ],
            'joins' =>
            [
                [
                    'name'       => 'user',
                    'controller' => 'usersController',
                    'key'        => 'uuid',
                    'skin'       => '',
                    'joins'      =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'list'
                        ],
                        [
                            'name'       => 'person',
                            'controller' => 'peopleController',
                            'skin'       => '',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'email',
                                    'controller'     => 'peopleEmailsController',
                                    'edgeController' => 'peoplePeopleEmailsController',
                                    'skin'           => 'normal',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'emailsTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'telephone',
                                    'controller'     => 'peoplePhoneNumbersController',
                                    'edgeController' => 'peoplePeoplePhoneNumbersController',
                                    'skin'           => 'normal',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'phoneNumbersTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins'      =>
                            [
                                [
                                    'name'       => 'gender',
                                    'controller' => 'gendersController',
                                    'skin'       => 'normal'
                                ],
                                [
                                    'name'       => 'honorificPrefix',
                                    'controller' => 'honorificPrefixController',
                                    'skin'       => 'normal'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ) ;
};
