<?php

use com\ooopener\models\Edges ;
use com\ooopener\models\Applications ;
use com\ooopener\models\Websites ;

$container['applications'] = function( $container )
{
    return new Applications
    (
        $container ,
        "applications" ,
        [
            'facetable' =>
            [

            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'additionalType',
                    'controller'     => 'applicationsTypesController',
                    'edgeController' => 'applicationApplicationsTypesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'owner',
                    'controller'     => 'usersController',
                    'edgeController' => 'applicationUsersController',
                    'skin'           => '',
                    'joins'          =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'list'
                        ],
                        [
                            'name'       => 'person',
                            'controller' => 'peopleController',
                            'skin'       => '',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'email',
                                    'controller'     => 'peopleEmailsController',
                                    'edgeController' => 'peoplePeopleEmailsController',
                                    'skin'           => 'normal',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'emailsTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'telephone',
                                    'controller'     => 'peoplePhoneNumbersController',
                                    'edgeController' => 'peoplePeoplePhoneNumbersController',
                                    'skin'           => 'normal',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'phoneNumbersTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins'      =>
                            [
                                [
                                    'name'       => 'gender',
                                    'controller' => 'gendersController',
                                    'skin'       => 'normal'
                                ],
                                [
                                    'name'       => 'honorificPrefix',
                                    'controller' => 'honorificPrefixController',
                                    'skin'       => 'normal'
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'websites',
                    'controller'     => 'applicationWebsitesController',
                    'edgeController' => 'applicationApplicationsWebsitesController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'websitesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'           => 'audios',
                    'controller'     => 'audioObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'photos',
                    'controller'     => 'imageObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'videos',
                    'controller'     => 'videoObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ]
            ]
        ]
    ) ;
};

$container['applicationApplicationsTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applications_applicationsTypes',
        [
            'from' =>
            [
                'name'       => 'applications_types',
                'controller' => 'applicationsTypesController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['applicationProducerOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applications_producer_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['applicationProducerPeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applications_producer_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['applicationPublisherOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applications_publisher_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['applicationPublisherPeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applications_publisher_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['applicationSponsorOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applications_sponsor_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['applicationSponsorPeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applications_sponsor_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['applicationUsers'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applications_users',
        [
            'from' =>
            [
                'name'       => 'users',
                'controller' => 'usersController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['applicationWebsites'] = function( $container )
{
    return new Websites
    (
        $container ,
        'applications_websites' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'websitesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['applicationApplicationsWebsites'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applications_applicationsWebsites',
        [
            'from' =>
            [
                'name'       => 'applications_websites',
                'controller' => 'applicationWebsitesController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};
