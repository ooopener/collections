<?php

use com\ooopener\models\Articles;
use com\ooopener\models\Edges;
use com\ooopener\models\Websites;

$container['articles'] = function( $container )
{
    return new Articles
    (
        $container ,
        'articles' ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    '_key'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]
            ]
            ,
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'                => '_key',
                'name'              => 'name',
                'created'           => 'created',
                'modified'          => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'additionalType',
                    'controller'     => 'articlesTypesController',
                    'edgeController' => 'articleArticlesTypesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'hasPart',
                    'controller'     => 'articlesController',
                    'edgeController' => 'articleHasPartController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'articlesTypesController',
                            'edgeController' => 'articleArticlesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'isPartOf',
                    'controller'     => 'articlesController',
                    'edgeController' => 'articleHasPartController',
                    'direction'      => 'reverse',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'articlesTypesController',
                            'edgeController' => 'articleArticlesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'isRelatedTo',
                    'controller'     => 'articlesController',
                    'edgeController' => 'articleIsRelatedToController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'articlesTypesController',
                            'edgeController' => 'articleArticlesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'isSimilarTo',
                    'controller'     => 'articlesController',
                    'edgeController' => 'articleIsSimilarToController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'articlesTypesController',
                            'edgeController' => 'articleArticlesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'websites',
                    'controller'     => 'articleWebsitesController',
                    'edgeController' => 'articleArticlesWebsitesController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'websitesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'           => 'audios',
                    'controller'     => 'audioObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'photos',
                    'controller'     => 'imageObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'videos',
                    'controller'     => 'videoObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ]
            ]
        ]
    );
};

$container['articleArticlesTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'articles_articlesTypes',
        [
            'from' =>
            [
                'name'       => 'articles_types',
                'controller' => 'articlesTypesController'
            ],
            'to' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ]
        ]
    ) ;
};

$container['articleHasPart'] = function( $container )
{
    return new Edges
    (
        $container ,
        'articles_hasPart' ,
        [
            'from' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ],
            'to' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ]
        ]
    ) ;
};

$container['articleIsRelatedTo'] = function( $container )
{
    return new Edges
    (
        $container ,
        'articles_isRelatedTo' ,
        [
            'from' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ],
            'to' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ]
        ]
    ) ;
};

$container['articleIsSimilarTo'] = function( $container )
{
    return new Edges
    (
        $container ,
        'articles_isSimilarTo' ,
        [
            'from' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ],
            'to' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ]
        ]
    ) ;
};

$container['articleWebsites'] = function( $container )
{
    return new Websites
    (
        $container ,
        'articles_websites' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'websitesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['articleArticlesWebsites'] = function( $container )
{
    return new Edges
    (
        $container ,
        'articles_articlesWebsites',
        [
            'from' =>
            [
                'name'       => 'articles_websites',
                'controller' => 'articleWebsitesController'
            ],
            'to' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ]
        ]
    ) ;
};
