<?php

use com\ooopener\models\conceptualObjects\ConceptualObjectMarks ;
use com\ooopener\models\conceptualObjects\ConceptualObjectMaterials ;
use com\ooopener\models\conceptualObjects\ConceptualObjectMeasurements ;
use com\ooopener\models\conceptualObjects\ConceptualObjectNumbers ;

use com\ooopener\models\Edges ;
use com\ooopener\models\Keywords ;
use com\ooopener\models\ConceptualObjects ;
use com\ooopener\models\Photos ;
use com\ooopener\models\Websites ;

$container['conceptualObjects'] = function( $container )
{
    return new ConceptualObjects
    (
        $container ,
        'conceptualObjects',
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ],
                'location' =>
                [
                    '_key' => 'edge' ,
                    'edge' => 'conceptualObjects_places'
                ]
                ,
                'category' =>
                [
                    'id'     => 'thesaurus' ,
                    'edge'   => 'conceptualObjects_conceptualObjectsCategories'
                ]
                ,
                'movement' =>
                [
                    'id'     => 'thesaurus' ,
                    'edge'   => 'conceptualObjects_artMovements'
                ]
                ,
                'temporal' =>
                [
                    'id'     => 'thesaurus' ,
                    'edge'   => 'conceptualObjects_artTemporalEntities'
                ]
            ]
            ,
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'       => '_key',
                'active'   => 'active',
                'name'     => 'name',
                'created'  => 'created',
                'modified' => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'category',
                    'controller'     => 'conceptualObjectsCategoriesController',
                    'edgeController' => 'conceptualObjectConceptualObjectsCategoriesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'keywords',
                    'controller'     => 'conceptualObjectKeywordsController',
                    'edgeController' => 'conceptualObjectConceptualObjectsKeywordsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'location',
                    'controller'     => 'placesController',
                    'edgeController' => 'conceptualObjectPlacesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'marks',
                    'controller'     => 'conceptualObjectMarksController',
                    'edgeController' => 'conceptualObjectConceptualObjectsMarksController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType' ,
                            'controller' => 'marksTypesController',
                            'skin'       => 'list'
                        ],
                        [
                            'name'       => 'language' ,
                            'controller' => 'languagesController',
                            'skin'       => 'list'
                        ],
                        [
                            'name'       => 'technique',
                            'controller' => 'conceptualObjectsTechniquesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'materials',
                    'controller'     => 'conceptualObjectMaterialsController',
                    'edgeController' => 'conceptualObjectConceptualObjectsMaterialsController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'material' ,
                            'controller' => 'materialsController',
                            'skin'       => 'list'
                        ],
                        [
                            'name'       => 'technique',
                            'controller' => 'conceptualObjectsTechniquesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'measurements',
                    'controller'     => 'conceptualObjectMeasurementsController',
                    'edgeController' => 'conceptualObjectConceptualObjectsMeasurementsController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'dimension' ,
                            'controller' => 'measurementsDimensionsController',
                            'skin'       => 'list'
                        ],
                        [
                            'name'       => 'unit',
                            'controller' => 'measurementsUnitsController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'movement',
                    'controller'     => 'artMovementsController',
                    'edgeController' => 'conceptualObjectArtMovementsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'numbers',
                    'controller'     => 'conceptualObjectNumbersController',
                    'edgeController' => 'conceptualObjectConceptualObjectsNumbersController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType' ,
                            'controller' => 'conceptualObjectsNumberTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'temporal',
                    'controller'     => 'artTemporalEntitiesController',
                    'edgeController' => 'conceptualObjectArtTemporalEntitiesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'websites',
                    'controller'     => 'conceptualObjectWebsitesController',
                    'edgeController' => 'conceptualObjectConceptualObjectsWebsitesController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'websitesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'           => 'audios',
                    'controller'     => 'audioObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'photos',
                    'controller'     => 'imageObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'videos',
                    'controller'     => 'videoObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ]
            ]
        ]

    ) ;
};

$container['conceptualObjectArtMovements'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_artMovements',
        [
            'from' =>
            [
                'name'       => 'art_movements',
                'controller' => 'artMovementsController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectArtTemporalEntities'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_artTemporalEntities',
        [
            'from' =>
            [
                'name'       => 'art_temporal_entities',
                'controller' => 'artTemporalEntitiesController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectKeywords'] = function( $container )
{
    return new Keywords( $container , 'conceptualObjects_keywords' ) ;
};

$container['conceptualObjectConceptualObjectsKeywords'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsKeywords',
        [
            'from' =>
            [
                'name'       => 'conceptualObjects_keywords',
                'controller' => 'conceptualObjectKeywordsController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectConceptualObjectsCategories'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsCategories',
        [
            'from' =>
            [
                'name'       => 'conceptualObjects_categories',
                'controller' => 'conceptualObjectsCategoriesController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectConceptualObjectsMarks'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsMarks',
        [
            'from' =>
            [
                'name'       => 'conceptualObjects_marks',
                'controller' => 'conceptualObjectMarksController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectConceptualObjectsMaterials'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsMaterials',
        [
            'from' =>
            [
                'name'       => 'conceptualObjects_materials',
                'controller' => 'conceptualObjectMaterialsController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectConceptualObjectsMeasurements'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsMeasurements',
        [
            'from' =>
            [
                'name'       => 'conceptualObjects_measurements',
                'controller' => 'conceptualObjectMeasurementsController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectConceptualObjectsNumbers'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsNumbers',
        [
            'from' =>
            [
                'name'       => 'conceptualObjects_numbers',
                'controller' => 'conceptualObjectNumbersController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectMarks'] = function( $container )
{
    return new ConceptualObjectMarks
    (
        $container ,
        'conceptualObjects_marks' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType' ,
                    'controller' => 'marksTypesController',
                    'skin'       => 'list'
                ],
                [
                    'name'       => 'language' ,
                    'controller' => 'languagesController',
                    'skin'       => 'list'
                ],
                [
                    'name'       => 'technique',
                    'controller' => 'conceptualObjectsTechniquesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['conceptualObjectMaterials'] = function( $container )
{
    return new ConceptualObjectMaterials
    (
        $container ,
        'conceptualObjects_materials' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'material' ,
                    'controller' => 'materialsController',
                    'skin'       => 'list'
                ],
                [
                    'name'       => 'technique',
                    'controller' => 'conceptualObjectsTechniquesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['conceptualObjectMeasurements'] = function( $container )
{
    return new ConceptualObjectMeasurements
    (
        $container ,
        'conceptualObjects_measurements' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'dimension' ,
                    'controller' => 'measurementsDimensionsController',
                    'skin'       => 'list'
                ],
                [
                    'name'       => 'unit',
                    'controller' => 'measurementsUnitsController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['conceptualObjectNumbers'] = function( $container )
{
    return new ConceptualObjectNumbers
    (
        $container ,
        'conceptualObjects_numbers' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType' ,
                    'controller' => 'conceptualObjectsNumberTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['conceptualObjectPlaces'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_places',
        [
            'from' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectProductionsOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_productions_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectProductionsPeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_productions_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['conceptualObjectPhotos'] = function( $container )
{
    return new Photos
    (
        $container ,
        'conceptualObjects_photos' ,
        [
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'author'    => 'author' ,
                'license'   => 'license' ,
                'publisher' => 'publisher' ,
                'width'     => 'width' ,
                'height'    => 'height' ,
                'primary'   => 'first'
            ]
        ]
    ) ;
};

$container['conceptualObjectWebsites'] = function( $container )
{
    return new Websites
    (
        $container ,
        'conceptualObjects_websites' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'websitesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['conceptualObjectConceptualObjectsWebsites'] = function( $container )
{
    return new Edges
    (
        $container ,
        'conceptualObjects_conceptualObjectsWebsites',
        [
            'from' =>
            [
                'name'       => 'conceptualObjects_websites',
                'controller' => 'conceptualObjectWebsitesController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};
