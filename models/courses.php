<?php

use com\ooopener\models\Courses ;
use com\ooopener\models\Edges ;
use com\ooopener\models\OpeningHoursSpecifications ;
use com\ooopener\models\Transportations ;

$container['courses'] = function( $container )
{
    return new Courses
    (
        $container ,
        "courses",
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ],
                'idsSorted' =>
                [
                    '_key'   => 'listFieldSorted'
                ],
                'additionalType' =>
                [
                    'id'    => 'thesaurus' ,
                    'edge'  => 'courses_coursesTypes'
                ]
            ]
            ,
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'audience',
                    'controller'     => 'coursesAudiencesController',
                    'edgeController' => 'courseCoursesAudiencesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'additionalType',
                    'controller'     => 'coursesTypesController',
                    'edgeController' => 'courseCoursesTypesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'level',
                    'controller'     => 'coursesLevelsController',
                    'edgeController' => 'courseCoursesLevelsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'openingHoursSpecification',
                    'controller'     => 'courseOpeningHoursController',
                    'edgeController' => 'courseCoursesOpeningHoursController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'path',
                    'controller'     => 'coursesPathsController',
                    'edgeController' => 'courseCoursesPathsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'status',
                    'controller'     => 'coursesStatusController',
                    'edgeController' => 'courseCoursesStatusController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'transportations',
                    'controller'     => 'courseTransportationsController',
                    'edgeController' => 'courseCoursesTransportationsController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'transportationsController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'articles',
                    'controller'     => 'articlesController',
                    'edgeController' => 'courseArticlesController',
                    'skin'           => 'list',
                    'edges'           =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'articlesTypesController',
                            'edgeController' => 'articleArticlesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'courses',
                    'controller'     => 'coursesController',
                    'edgeController' => 'courseCoursesController',
                    'skin'           => 'list',
                    'edges'          =>
                        [
                            [
                                'name'           => 'additionalType',
                                'controller'     => 'coursesTypesController',
                                'edgeController' => 'courseCoursesTypesController',
                                'skin'           => 'list'
                            ],
                            [
                                'name'           => 'audio',
                                'controller'     => 'audioObjectsController',
                                'edgeController' => 'mediaObjectsThingsAudioController',
                                'skin'           => 'list'
                            ],
                            [
                                'name'           => 'image',
                                'controller'     => 'imageObjectsController',
                                'edgeController' => 'mediaObjectsThingsImageController',
                                'skin'           => 'list'
                            ],
                            [
                                'name'           => 'level',
                                'controller'     => 'coursesLevelsController',
                                'edgeController' => 'courseCoursesLevelsController',
                                'skin'           => 'list'
                            ],
                            [
                                'name'           => 'openingHoursSpecification',
                                'controller'     => 'courseOpeningHoursController',
                                'edgeController' => 'courseCoursesOpeningHoursController',
                                'skin'           => 'list'
                            ],
                            [
                                'name'           => 'status',
                                'controller'     => 'coursesStatusController',
                                'edgeController' => 'courseCoursesStatusController',
                                'skin'           => 'list'
                            ],
                            [
                                'name'           => 'video',
                                'controller'     => 'videoObjectsController',
                                'edgeController' => 'mediaObjectsThingsVideoController',
                                'skin'           => 'list'
                            ]
                        ]
                ],
                [
                    'name'           => 'events',
                    'controller'     => 'eventsController',
                    'edgeController' => 'courseEventsController',
                    'skin'           => 'normal',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'eventsTypesController',
                            'edgeController' => 'eventEventsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'eventPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'offers',
                            'controller'     => 'eventOffersController',
                            'edgeController' => 'eventEventsOffersController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'category',
                                    'controller' => 'offersCategoriesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'eventStatus' ,
                            'controller' => 'eventsStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'conceptualObjects',
                    'controller'     => 'conceptualObjectsController',
                    'edgeController' => 'courseConceptualObjectsController',
                    'skin'           => '',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'category',
                            'controller'     => 'conceptualObjectsCategoriesController',
                            'edgeController' => 'conceptualObjectConceptualObjectsCategoriesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'movement',
                            'controller'     => 'artMovementsController',
                            'edgeController' => 'conceptualObjectArtMovementsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'organizations',
                    'controller'     => 'organizationsController',
                    'edgeController' => 'courseOrganizationsController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'organizationsTypesController',
                            'edgeController' => 'organizationOrganizationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'organizationPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'people',
                    'controller'     => 'peopleController',
                    'edgeController' => 'coursePeopleController',
                    'skin'           => '',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'email',
                            'controller'     => 'peopleEmailsController',
                            'edgeController' => 'peoplePeopleEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'peoplePhoneNumbersController',
                            'edgeController' => 'peoplePeoplePhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'normal'
                        ],
                        [
                            'name'       => 'honorificPrefix',
                            'controller' => 'honorificPrefixController',
                            'skin'       => 'normal'
                        ]
                    ]
                ],
                [
                    'name'           => 'places',
                    'controller'     => 'placesController',
                    'edgeController' => 'coursePlacesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'stages',
                    'controller'     => 'stagesController',
                    'edgeController' => 'courseStagesController',
                    'skin'           => '',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'stageLocationsController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'status',
                            'controller'     => 'coursesStatusController',
                            'edgeController' => 'stageCoursesStatusController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ]
            ],
            "joins" =>
            [
                [
                    'name'           => 'audios',
                    'controller'     => 'audioObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'photos',
                    'controller'     => 'imageObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'steps',
                    'controller'     => 'stepsController',
                    'array'          => true,
                    'skin'           => 'full',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'stage',
                            'controller'     => 'stagesController',
                            'edgeController' => 'stepStagesController',
                            'skin'           => 'normal',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'location',
                                    'controller'     => 'placesController',
                                    'edgeController' => 'stageLocationsController',
                                    'skin'           => 'list',
                                    'edges'          =>
                                    [
                                        [
                                            'name'           => 'additionalType',
                                            'controller'     => 'placesTypesController',
                                            'edgeController' => 'placePlacesTypesController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'audio',
                                            'controller'     => 'audioObjectsController',
                                            'edgeController' => 'mediaObjectsThingsAudioController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'conceptualObjects',
                                            'controller'     => 'conceptualObjectsController',
                                            'edgeController' => 'conceptualObjectPlacesController',
                                            'direction'      => 'reverse'
                                        ],
                                        [
                                            'name'           => 'events',
                                            'controller'     => 'eventsController',
                                            'edgeController' => 'eventPlacesController',
                                            'direction'      => 'reverse',
                                        ],
                                        [
                                            'name'           => 'image',
                                            'controller'     => 'imageObjectsController',
                                            'edgeController' => 'mediaObjectsThingsImageController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'logo',
                                            'controller'     => 'imageObjectsController',
                                            'edgeController' => 'mediaObjectsThingsLogoController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'video',
                                            'controller'     => 'videoObjectsController',
                                            'edgeController' => 'mediaObjectsThingsVideoController',
                                            'skin'           => 'list'
                                        ]
                                    ],
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'status' ,
                                            'controller' => 'placesStatusTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'status',
                                    'controller'     => 'coursesStatusController',
                                    'edgeController' => 'stageCoursesStatusController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'           => 'audios',
                            'controller'     => 'audioObjectsController',
                            'array'          => true,
                            'skin'           => 'extend'
                        ],
                        [
                            'name'           => 'photos',
                            'controller'     => 'imageObjectsController',
                            'array'          => true,
                            'skin'           => 'extend'
                        ],
                        [
                            'name'           => 'videos',
                            'controller'     => 'videoObjectsController',
                            'array'          => true,
                            'skin'           => 'extend'
                        ]
                    ]
                ],
                [
                    'name'           => 'videos',
                    'controller'     => 'videoObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ]
            ]
        ]
    ) ;
};

$container['courseCoursesAudiences'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_coursesAudiences',
        [
            'from' =>
            [
                'name'       => 'courses_audiences',
                'controller' => 'coursesAudiencesController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseCoursesLevels'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_coursesLevels',
        [
            'from' =>
            [
                'name'       => 'courses_levels',
                'controller' => 'coursesLevelsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseCoursesOpeningHours'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_coursesOpeningHours',
        [
            'from' =>
            [
                'name'       => 'courses_openingHours',
                'controller' => 'courseOpeningHoursController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseCoursesPaths'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_coursesPaths',
        [
            'from' =>
            [
                'name'       => 'courses_paths',
                'controller' => 'coursesPathsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseCoursesStatus'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_coursesStatus',
        [
            'from' =>
            [
                'name'       => 'courses_status',
                'controller' => 'coursesStatusController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseCoursesTransportations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_coursesTransportations',
        [
            'from' =>
            [
                'name'       => 'courses_transportations',
                'controller' => 'courseTransportationsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseCoursesTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_coursesTypes',
        [
            'from' =>
            [
                'name'       => 'courses_types',
                'controller' => 'coursesTypesController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['coursePlacesOpeningHours'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_coursesOpeningHours',
        [
            'from' =>
            [
                'name'       => 'courses_openingHours',
                'controller' => 'courseOpeningHoursController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseOpeningHours'] = function( $container )
{
    return new OpeningHoursSpecifications( $container , 'courses_opening_hours' ) ;
};

$container['courseSteps'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_steps',
        [
            'from' =>
            [
                'name'       => 'steps',
                'controller' => 'stepsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseTransportations'] = function( $container )
{
    return new Transportations
    (
        $container ,
        'courses_transportations',
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'transportationsController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

///// discover

$container['courseArticles'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_articles',
        [
            'from' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseConceptualObjects'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_conceptualObjects',
        [
            'from' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseCourses'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_courses',
        [
            'from' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseEvents'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_events',
        [
            'from' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['coursePeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['coursePlaces'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_places',
        [
            'from' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['courseStages'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courses_stages',
        [
            'from' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};
