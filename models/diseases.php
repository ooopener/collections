<?php

use com\ooopener\models\Diseases ;
use com\ooopener\models\Edges ;
use com\ooopener\models\MedicalAnalysis ;

$container['diseases'] = function( $container )
{
    return new Diseases
    (
        $container ,
        'diseases',
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ],
                'additionalType' =>
                [
                    'id'   => 'thesaurus',
                    'edge' => 'diseases_diseasesTypes'
                ],
                'level' =>
                [
                    'id'   => 'thesaurus',
                    'edge' => 'diseases_diseasesLevels'
                ],
                'observationType' =>
                [
                    'id'   => 'thesaurus',
                    'edge' => 'diseases_observationsTypes'
                ]
            ]
            ,
            'searchable' =>
            [
                'name' , 'alternateName'
            ]
            ,
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'additionalType' ,
                    'controller'     => 'diseasesTypesController',
                    'edgeController' => 'diseaseDiseasesTypesController',
                    'skin'           => 'list',
                ],
                [
                    'name'           => 'analysisMethod' ,
                    'controller'     => 'diseaseAnalysisMethodController',
                    'edgeController' => 'diseaseDiseasesAnalysisMethodController',
                    'skin'           => 'list',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'analysisMethodsController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'analysisSampling' ,
                    'controller'     => 'diseaseAnalysisSamplingController',
                    'edgeController' => 'diseaseDiseasesAnalysisSamplingController',
                    'skin'           => 'list',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'samplingsController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'level' ,
                    'controller'     => 'diseasesLevelsController',
                    'edgeController' => 'diseaseDiseasesLevelsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'transmissionMethod' ,
                    'controller'     => 'transmissionsMethodsController',
                    'edgeController' => 'diseaseTransmissionsMethodsController',
                    'skin'           => 'list'
                ]
            ],
            'joins' =>
            [
            ]
        ]
    );
};

$container['analysisMethod'] = function( $container )
{
    return new MedicalAnalysis
    (
        $container ,
        'diseases_analysisMethod' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'analysisMethodsController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['diseaseDiseasesAnalysisMethod'] = function( $container )
{
    return new Edges
    (
        $container ,
        'diseases_diseasesAnalysisMethod',
        [
            'from' =>
            [
                'name'       => 'diseases_analysisMethod',
                'controller' => 'diseaseAnalysisMethodController'
            ],
            'to' =>
            [
                'name'       => 'diseases',
                'controller' => 'diseasesController'
            ]
        ]
    ) ;
};

$container['analysisSampling'] = function( $container )
{
    return new MedicalAnalysis
    (
        $container ,
        'diseases_analysisSampling' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'samplingsController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['diseaseDiseasesAnalysisSampling'] = function( $container )
{
    return new Edges
    (
        $container ,
        'diseases_diseasesAnalysisSampling',
        [
            'from' =>
            [
                'name'       => 'diseases_analysisSampling',
                'controller' => 'diseaseAnalysisSamplingController'
            ],
            'to' =>
            [
                'name'       => 'diseases',
                'controller' => 'diseasesController'
            ]
        ]
    ) ;
};

$container['diseaseDiseasesLevels'] = function( $container )
{
    return new Edges
    (
        $container ,
        'diseases_diseasesLevels',
        [
            'from' =>
            [
                'name'       => 'diseases_levels',
                'controller' => 'diseasesLevelsController'
            ],
            'to' =>
            [
                'name'       => 'diseases',
                'controller' => 'diseasesController'
            ]
        ]
    ) ;
};

$container['diseaseDiseasesTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'diseases_diseasesTypes',
        [
            'from' =>
            [
                'name'       => 'diseases_types',
                'controller' => 'diseasesTypesController'
            ],
            'to' =>
            [
                'name'       => 'diseases',
                'controller' => 'diseasesController'
            ]
        ]
    ) ;
};

$container['diseaseTransmissionsMethods'] = function( $container )
{
    return new Edges
    (
        $container ,
        'diseases_transmissionsMethods',
        [
            'from' =>
            [
                'name'       => 'medical_transmissions_methods',
                'controller' => 'transmissionsMethodsController'
            ],
            'to' =>
            [
                'name'       => 'diseases',
                'controller' => 'diseasesController'
            ]
        ]
    ) ;
};
