<?php

use com\ooopener\models\Edges ;
use com\ooopener\models\Events ;
use com\ooopener\models\Keywords ;
use com\ooopener\models\Offers ;
use com\ooopener\models\Photos ;
use com\ooopener\models\Translations ;
use com\ooopener\models\Websites ;

$container['events'] = function( $container )
{
    return new Events
    (
        $container ,
        'events' ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    '_key'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ],
                'idsSorted' =>
                [
                    '_key'   => 'listFieldSorted'
                ],
                'location' =>
                [
                    '_key' => 'edge' ,
                    'edge' => 'events_places'
                ]
                /*,
                'eventStatus' =>
                [
                    'id'     => 'thesaurus' ,
                    'edge'  => 'events_eventsStatusTypes'
                ]*/
                ,
                'additionalType' =>
                [
                    'id'     => 'thesaurus' ,
                    'edge'   => 'events_eventsTypes'
                ]
            ]
            ,
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'startDate' => 'startDate',
                'endDate'   => 'endDate'
            ],
            'edges' =>
            [
                [
                    'name'           => 'additionalType',
                    'controller'     => 'eventsTypesController',
                    'edgeController' => 'eventEventsTypesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'keywords',
                    'controller'     => 'eventKeywordsController',
                    'edgeController' => 'eventEventsKeywordsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'location',
                    'controller'     => 'placesController',
                    'edgeController' => 'eventPlacesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'offers',
                    'controller'     => 'eventOffersController',
                    'edgeController' => 'eventEventsOffersController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'category',
                            'controller' => 'offersCategoriesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'subEvent',
                    'controller'     => 'eventsController',
                    'edgeController' => 'eventSubEventsController',
                    'skin'           => 'normal',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'eventsTypesController',
                            'edgeController' => 'eventEventsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'eventPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'offers',
                            'controller'     => 'eventOffersController',
                            'edgeController' => 'eventEventsOffersController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'category',
                                    'controller' => 'offersCategoriesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'eventStatus' ,
                            'controller' => 'eventsStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'superEvent',
                    'controller'     => 'eventsController',
                    'edgeController' => 'eventSubEventsController',
                    'direction'      => 'reverse',
                    'skin'           => 'normal',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'eventsTypesController',
                            'edgeController' => 'eventEventsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'eventPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'offers',
                            'controller'     => 'eventOffersController',
                            'edgeController' => 'eventEventsOffersController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'category',
                                    'controller' => 'offersCategoriesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'eventStatus' ,
                            'controller' => 'eventsStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'websites',
                    'controller'     => 'eventWebsitesController',
                    'edgeController' => 'eventEventsWebsitesController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'websitesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'workFeatured',
                    'controller'     => 'conceptualObjectsController',
                    'edgeController' => 'eventWorksFeaturedController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'category',
                            'controller'     => 'conceptualObjectsCategoriesController',
                            'edgeController' => 'conceptualObjectConceptualObjectsCategoriesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'movement',
                            'controller'     => 'artMovementsController',
                            'edgeController' => 'conceptualObjectArtMovementsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'temporal',
                            'controller'     => 'artTemporalEntitiesController',
                            'edgeController' => 'conceptualObjectArtTemporalEntitiesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'           => 'audios',
                    'controller'     => 'audioObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'       => 'eventStatus' ,
                    'controller' => 'eventsStatusTypesController',
                    'skin'       => 'list'
                ],
                [
                    'name'           => 'photos',
                    'controller'     => 'imageObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'videos',
                    'controller'     => 'videoObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ]
            ]
        ]
    );
};

$container['eventKeywords'] = function( $container )
{
    return new Keywords( $container , 'events_keywords' ) ;
};

$container['eventEventsKeywords'] = function( $container )
{
    return new Edges
    (
        $container ,
        'events_eventsKeywords',
        [
            'from' =>
            [
                'name'       => 'events_keywords',
                'controller' => 'eventKeywordsController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['eventEventsOffers'] = function( $container )
{
    return new Edges
    (
        $container ,
        'events_eventsOffers',
        [
            'from' =>
            [
                'name'       => 'events_offers',
                'controller' => 'eventOffersController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['eventEventsTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'events_eventsTypes',
        [
            'from' =>
            [
                'name'       => 'events_types',
                'controller' => 'eventsTypesController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['eventOffers'] = function( $container )
{
    return new Offers
    (
        $container ,
        'events_offers'  ,
        [
            'joins' =>
            [
                [
                    'name'       => 'category',
                    'controller' => 'offersCategoriesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['eventOrganizersOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'events_organizers_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['eventOrganizersPeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'events_organizers_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['eventPhotos'] = function( $container )
{
    return new Photos
    (
        $container ,
        'events_photos' ,
        [
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'author'    => 'author' ,
                'license'   => 'license' ,
                'publisher' => 'publisher' ,
                'width'     => 'width' ,
                'height'    => 'height' ,
                'primary'   => 'first'
            ]
        ]
    ) ;
};

$container['eventPlaces'] = function( $container )
{
    return new Edges
    (
        $container ,
        'events_places',
        [
            'from' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['eventSubEvents'] = function( $container )
{
    return new Edges
    (
        $container ,
        'events_subEvents' ,
        [
            'from' =>
            [
                'name'       => 'events' ,
                'controller' => 'eventsController'
            ],
            'to' =>
            [
                'name'       => 'events' ,
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['eventTranslations'] = function( $container )
{
    return new Translations( $container , "events_translation" ) ;
};

$container['eventWebsites'] = function( $container )
{
    return new Websites
    (
        $container ,
        'events_websites' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'websitesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['eventEventsWebsites'] = function( $container )
{
    return new Edges
    (
        $container ,
        'events_eventsWebsites',
        [
            'from' =>
            [
                'name'       => 'events_websites',
                'controller' => 'eventWebsitesController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['eventWorksFeatured'] = function( $container )
{
    return new Edges
    (
        $container ,
        'events_works_featured'  ,
        [
            'from' =>
            [
                'name'       => 'conceptualObjects' ,
                'controller' => 'conceptualObjectsController'
            ],
            'to' =>
            [
                'name'       => 'events' ,
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};
