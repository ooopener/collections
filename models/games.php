<?php

use com\ooopener\models\Edges;
use com\ooopener\models\Games;

$container['applicationsGames'] = function( $container )
{
    return new Games
    (
        $container ,
        'applications_games',
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]
            ]
            ,
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'quest',
                    'controller'     => 'coursesGamesController',
                    'edgeController' => 'applicationsGamesCoursesGamesController',
                    'skin'           => 'full',
                    'edges'          =>
                    [
                        [
                            'name'           => 'quest',
                            'controller'     => 'questionsGamesController',
                            'edgeController' => 'coursesGamesQuestionsGamesController',
                            'skin'           => 'full',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'about',
                                    'controller' => 'stepsController',
                                    'skin'       => 'game',
                                    'edges'      =>
                                    [
                                        [
                                            'name'           => 'audio',
                                            'controller'     => 'audioObjectsController',
                                            'edgeController' => 'mediaObjectsThingsAudioController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'image',
                                            'controller'     => 'imageObjectsController',
                                            'edgeController' => 'mediaObjectsThingsImageController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'video',
                                            'controller'     => 'videoObjectsController',
                                            'edgeController' => 'mediaObjectsThingsVideoController',
                                            'skin'           => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'       => 'additionalType' ,
                                    'controller' => 'gamesTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'about',
                            'controller' => 'coursesController',
                            'skin'       => 'game',
                            'edges'      =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'coursesTypesController',
                                    'edgeController' => 'courseCoursesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'level',
                                    'controller'     => 'coursesLevelsController',
                                    'edgeController' => 'courseCoursesLevelsController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'openingHoursSpecification',
                                    'controller'     => 'courseOpeningHoursController',
                                    'edgeController' => 'courseCoursesOpeningHoursController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'status',
                                    'controller'     => 'coursesStatusController',
                                    'edgeController' => 'courseCoursesStatusController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'       => 'additionalType' ,
                            'controller' => 'gamesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'       => 'additionalType' ,
                    'controller' => 'gamesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    );
};

$container['applicationsGamesCoursesGames'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applicationsGames_coursesGames',
        [
            'from' =>
            [
                'name'       => 'courses_games',
                'controller' => 'coursesGamesController'
            ],
            'to' =>
            [
                'name'       => 'applications_games',
                'controller' => 'applicationsGamesController'
            ]
        ]
    );
};

$container['applicationsGamesGamesTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'applicationGames_gamesTypes',
        [
            'from' =>
            [
                'name'       => 'games_types',
                'controller' => 'gamesTypesController'
            ],
            'to' =>
            [
                'name'       => 'applications_games',
                'controller' => 'applicationsGamesController'
            ]
        ]
    );
};

$container['coursesGames'] = function( $container )
{
    return new Games
    (
        $container ,
        'courses_games',
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ]
            ]
            ,
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'quest',
                    'controller'     => 'questionsGamesController',
                    'edgeController' => 'coursesGamesQuestionsGamesController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'about',
                            'controller' => 'stepsController',
                            'skin'       => 'game',
                            'edges'      =>
                            [
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'       => 'additionalType' ,
                            'controller' => 'gamesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'       => 'about',
                    'controller' => 'coursesController',
                    'skin'       => 'game',
                    'edges'      =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'coursesTypesController',
                            'edgeController' => 'courseCoursesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'level',
                            'controller'     => 'coursesLevelsController',
                            'edgeController' => 'courseCoursesLevelsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'openingHoursSpecification',
                            'controller'     => 'courseOpeningHoursController',
                            'edgeController' => 'courseCoursesOpeningHoursController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'status',
                            'controller'     => 'coursesStatusController',
                            'edgeController' => 'courseCoursesStatusController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'       => 'additionalType' ,
                    'controller' => 'gamesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    );
};

$container['coursesGamesGamesTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'courseGames_gamesTypes',
        [
            'from' =>
            [
                'name'       => 'games_types',
                'controller' => 'gamesTypesController'
            ],
            'to' =>
            [
                'name'       => 'courses_games',
                'controller' => 'coursesGamesController'
            ]
        ]
    );
};

$container['coursesGamesQuestionsGames'] = function( $container )
{
    return new Edges
    (
        $container ,
        'coursesGames_questionsGames',
        [
            'from' =>
            [
                'name'       => 'questions_games',
                'controller' => 'questionsGamesController'
            ],
            'to' =>
            [
                'name'       => 'courses_games',
                'controller' => 'coursesGamesController'
            ]
        ]
    );
};

$container['questionsGames'] = function( $container )
{
    return new Games
    (
        $container ,
        'questions_games',
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ]
            ]
            ,
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'edges' =>
            [
            ],
            'joins' =>
            [
                [
                    'name'       => 'about',
                    'controller' => 'stepsController',
                    'skin'       => 'game',
                    'edges'      =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'       => 'additionalType' ,
                    'controller' => 'gamesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    );
};

$container['questionGamesGamesTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'questionGames_gamesTypes',
        [
            'from' =>
            [
                'name'       => 'games_types',
                'controller' => 'gamesTypesController'
            ],
            'to' =>
            [
                'name'       => 'questions_games',
                'controller' => 'questionsGamesController'
            ]
        ]
    );
};
