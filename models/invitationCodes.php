<?php

use com\ooopener\models\InvitationCodes ;

$container['invitationCodes'] = function( $container )
{
    return new InvitationCodes( $container , "invitation_codes" ) ;
};

