<?php

use com\ooopener\models\Edges ;
use com\ooopener\models\Livestocks ;
use com\ooopener\models\LivestockNumbers ;
use com\ooopener\models\Observations ;
use com\ooopener\models\Sectors ;
use com\ooopener\models\Workplaces ;
use com\ooopener\models\Workshops ;

$container['livestocks'] = function( $container )
{
    return new Livestocks
    (
        $container ,
        'livestocks' ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]
                ,
                'numbers' =>
                [
                    '_key' => 'edgeComplex' ,
                    'edge' => 'livestocks_livestocksNumbers'
                ]
            ],
            'searchable' =>
            [
                'organization.name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'organization.name',
                'created'   => 'created',
                'modified'  => 'modified',
                //'after'     => 'organization.name'
            ],
            'edges' =>
            [
                [
                    'name'           => 'organization' ,
                    'controller'     => 'organizationsController' ,
                    'edgeController' => 'livestockOrganizationsController' ,
                    'skin'           => 'normal',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'organizationsTypesController',
                            'edgeController' => 'organizationOrganizationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'email',
                            'controller'     => 'organizationEmailsController',
                            'edgeController' => 'organizationOrganizationsEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'organizationPhoneNumbersController',
                            'edgeController' => 'organizationOrganizationsPhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'organizationPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'websites',
                            'controller'     => 'organizationWebsitesController',
                            'edgeController' => 'organizationOrganizationsWebsitesController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'websitesTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'name'           => 'numbers',
                    'controller'     => 'livestockNumbersController',
                    'edgeController' => 'livestocksLivestocksNumbersController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'livestocksNumbersTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'technicians',
                    'controller'     => 'techniciansController' ,
                    'edgeController' => 'livestockTechniciansController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'           => 'person',
                            'controller'     => 'peopleController',
                            'skin'           => 'normal',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'email',
                                    'controller'     => 'peopleEmailsController',
                                    'edgeController' => 'peoplePeopleEmailsController',
                                    'skin'           => 'normal',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'emailsTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'telephone',
                                    'controller'     => 'peoplePhoneNumbersController',
                                    'edgeController' => 'peoplePeoplePhoneNumbersController',
                                    'skin'           => 'normal',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'phoneNumbersTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'websites',
                                    'controller'     => 'peopleWebsitesController',
                                    'edgeController' => 'peoplePeopleWebsitesController',
                                    'skin'           => 'list',
                                    'joins'          =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'websitesTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ]
                            ],
                            'joins'          =>
                            [
                                [
                                    'name'       => 'gender',
                                    'controller' => 'gendersController',
                                    'skin'       => 'normal'
                                ],
                                [
                                    'name'       => 'honorificPrefix',
                                    'controller' => 'honorificPrefixController',
                                    'skin'       => 'normal'
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'name'           => 'veterinarians',
                    'controller'     => 'veterinariansController' ,
                    'edgeController' => 'livestockVeterinariansController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'workshops',
                    'controller'     => 'workshopsController',
                    'edgeController' => 'livestocksWorkshopsController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'breeding',
                            'controller'     => 'breedingsTypesController',
                            'edgeController' => 'workshopsBreedingsTypesController',
                            'skin'           => 'full'
                        ],
                        [
                            'name'           => 'production',
                            'controller'     => 'productionsTypesController',
                            'edgeController' => 'workshopsProductionsTypesController',
                            'skin'           => 'full'
                        ],
                        [
                            'name'           => 'water',
                            'controller'     => 'waterOriginsController',
                            'edgeController' => 'workshopsWaterOriginsController',
                            'skin'           => 'full'
                        ],
                        [
                            'name'           => 'workplaces',
                            'controller'     => 'workplacesController',
                            'edgeController' => 'workshopsWorkplacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'sectors',
                                    'controller'     => 'sectorsController',
                                    'edgeController' => 'workplacesSectorsController',
                                    'skin'           => 'full',
                                    'edges'          =>
                                    [
                                        [
                                            'name'           => 'additionalType',
                                            'controller'     => 'sectorsTypesController',
                                            'edgeController' => 'sectorSectorsTypesController',
                                            'skin'           => 'list'
                                        ]
                                    ]
                                ]
                            ],
                            'joins'          =>
                            [
                                [
                                    'name'       => 'identifier',
                                    'controller' => 'livestockNumbersController',
                                    'skin'       => 'list',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'livestocksNumbersTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'identifier',
                            'controller' => 'livestockNumbersController',
                            'skin'       => 'list',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'livestocksNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
            ]
        ]
    ) ;
};

$container['livestockNumbers'] = function( $container )
{
    return new LivestockNumbers
    (
        $container ,
        'livestocks_numbers' ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ]

            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'livestocksNumbersTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['livestockOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'livestocks_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations' ,
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'livestocks' ,
                'controller' => 'livestocksController'
            ]
        ]
    );
};

$container['livestockTechnicians'] = function( $container )
{
    return new Edges
    (
        $container ,
        'livestocks_technicians',
        [
            'from' =>
            [
                'name'       => 'technicians' ,
                'controller' => 'techniciansController'
            ],
            'to' =>
            [
                'name'       => 'livestocks' ,
                'controller' => 'livestocksController'
            ]
        ]
    );
};

$container['livestockVeterinarians'] = function( $container )
{
    return new Edges
    (
        $container ,
        'livestocks_veterinarians',
        [
            'from' =>
            [
                'name'       => 'veterinarians' ,
                'controller' => 'veterinariansController'
            ],
            'to' =>
            [
                'name'       => 'livestocks' ,
                'controller' => 'livestocksController'
            ]
        ]
    );
};

$container['livestocksLivestocksNumbers'] = function( $container )
{
    return new Edges
    (
        $container ,
        'livestocks_livestocksNumbers' ,
        [
            'from' =>
            [
                'name'       => 'livestock_numbers',
                'controller' => 'livestockNumbersController'
            ],
            'to' =>
            [
                'name'       => 'livestocks',
                'controller' => 'livestocksController'
            ]
        ]
    ) ;
};

$container['observations'] = function( $container )
{
    return new Observations
    (
        $container ,
        'observations',
        [
            'facetable' =>
            [
                'livestock' =>
                [
                    '_key' => 'edge',
                    'edge' => 'observations_livestocks'
                ],
                'workshop' =>
                [
                    '_key' => 'edge',
                    'edge' => 'observations_workshops'
                ]
            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'about',
                    'controller'     => 'livestocksController',
                    'edgeController' => 'observationLivestocksController',
                    'skin'           => 'normal',
                    'edges'          =>
                    [
                        [
                            'name'           => 'organization' ,
                            'controller'     => 'organizationsController' ,
                            'edgeController' => 'livestockOrganizationsController' ,
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'organizationsTypesController',
                                    'edgeController' => 'organizationOrganizationsTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'location',
                                    'controller'     => 'placesController',
                                    'edgeController' => 'organizationPlacesController',
                                    'skin'           => 'list',
                                    'edges'          =>
                                    [
                                        [
                                            'name'           => 'additionalType',
                                            'controller'     => 'placesTypesController',
                                            'edgeController' => 'placePlacesTypesController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'conceptualObjects',
                                            'controller'     => 'conceptualObjectsController',
                                            'edgeController' => 'conceptualObjectPlacesController',
                                            'direction'      => 'reverse'
                                        ],
                                        [
                                            'name'           => 'events',
                                            'controller'     => 'eventsController',
                                            'edgeController' => 'eventPlacesController',
                                            'direction'      => 'reverse',
                                        ],
                                        [
                                            'name'           => 'image',
                                            'controller'     => 'imageObjectsController',
                                            'edgeController' => 'mediaObjectsThingsImageController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'logo',
                                            'controller'     => 'imageObjectsController',
                                            'edgeController' => 'mediaObjectsThingsLogoController',
                                            'skin'           => 'list'
                                        ]
                                    ],
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'status' ,
                                            'controller' => 'placesStatusTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        [
                            'name'           => 'numbers',
                            'controller'     => 'livestockNumbersController',
                            'edgeController' => 'livestocksLivestocksNumbersController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'livestocksNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'name'           => 'eventStatus',
                    'controller'     => 'observationsStatusController',
                    'edgeController' => 'observationObservationsStatusController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'additionalType',
                    'controller'     => 'observationsTypesController',
                    'edgeController' => 'observationObservationsTypesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'actor',
                    'controller'     => 'peopleController',
                    'edgeController' => 'observationActorsController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'job',
                            'controller'     => 'jobsController',
                            'edgeController' => 'peopleJobsController',
                            'skin'           => 'normal'
                        ],
                        [
                            'name'           => 'email',
                            'controller'     => 'peopleEmailsController',
                            'edgeController' => 'peoplePeopleEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'peoplePhoneNumbersController',
                            'edgeController' => 'peoplePeoplePhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'normal'
                        ],
                        [
                            'name'       => 'honorificPrefix',
                            'controller' => 'honorificPrefixController',
                            'skin'       => 'normal'
                        ]
                    ]
                ],
                [
                    'name'           => 'attendee',
                    'controller'     => 'peopleController',
                    'edgeController' => 'observationAttendeesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'job',
                            'controller'     => 'jobsController',
                            'edgeController' => 'peopleJobsController',
                            'skin'           => 'normal'
                        ],
                        [
                            'name'           => 'email',
                            'controller'     => 'peopleEmailsController',
                            'edgeController' => 'peoplePeopleEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'peoplePhoneNumbersController',
                            'edgeController' => 'peoplePeoplePhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'normal'
                        ],
                        [
                            'name'       => 'honorificPrefix',
                            'controller' => 'honorificPrefixController',
                            'skin'       => 'normal'
                        ]
                    ]
                ],
                [
                    'name'           => 'location',
                    'controller'     => 'placesController',
                    'edgeController' => 'observationPlacesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'owner',
                    'controller'     => 'usersController' ,
                    'edgeController' => 'observationOwnersController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'list'
                        ],
                        [
                            'name'       => 'person',
                            'controller' => 'peopleController',
                            'skin'       => '',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'email',
                                    'controller'     => 'peopleEmailsController',
                                    'edgeController' => 'peoplePeopleEmailsController',
                                    'skin'           => 'normal',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'emailsTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'telephone',
                                    'controller'     => 'peoplePhoneNumbersController',
                                    'edgeController' => 'peoplePeoplePhoneNumbersController',
                                    'skin'           => 'normal',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'phoneNumbersTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ]
                            ],
                            'joins'      =>
                            [
                                [
                                    'name'       => 'gender',
                                    'controller' => 'gendersController',
                                    'skin'       => 'normal'
                                ],
                                [
                                    'name'       => 'honorificPrefix',
                                    'controller' => 'honorificPrefixController',
                                    'skin'       => 'normal'
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'name'           => 'subject',
                    'controller'     => 'workshopsController',
                    'edgeController' => 'observationWorkshopsController',
                    'skin'           => 'full',
                    'edges' =>
                    [
                        [
                            'name'           => 'breeding',
                            'controller'     => 'breedingsTypesController',
                            'edgeController' => 'workshopsBreedingsTypesController',
                            'skin'           => 'full'
                        ],
                        [
                            'name'           => 'production',
                            'controller'     => 'productionsTypesController',
                            'edgeController' => 'workshopsProductionsTypesController',
                            'skin'           => 'full'
                        ],
                        [
                            'name'           => 'water',
                            'controller'     => 'waterOriginsController',
                            'edgeController' => 'workshopsWaterOriginsController',
                            'skin'           => 'full'
                        ],
                        [
                            'name'           => 'workplaces',
                            'controller'     => 'workplacesController',
                            'edgeController' => 'workshopsWorkplacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'sectors',
                                    'controller'     => 'sectorsController',
                                    'edgeController' => 'workplacesSectorsController',
                                    'skin'           => 'full',
                                    'edges'          =>
                                    [
                                        [
                                            'name'           => 'additionalType',
                                            'controller'     => 'sectorsTypesController',
                                            'edgeController' => 'sectorSectorsTypesController',
                                            'skin'           => 'list'
                                        ]
                                    ]
                                ]
                            ],
                            'joins'          =>
                            [
                                [
                                    'name'       => 'identifier',
                                    'controller' => 'livestockNumbersController',
                                    'skin'       => 'list',
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'additionalType',
                                            'controller' => 'livestocksNumbersTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'identifier',
                            'controller' => 'livestockNumbersController',
                            'skin'       => 'list',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'livestocksNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    );
};

$container['observationActors'] = function( $container )
{
    return new Edges
    (
        $container ,
        'observations_actors' ,
        [
            'from' =>
            [
                'name'       => 'people' ,
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'observations' ,
                'controller' => 'observationsController'
            ]
        ]
    ) ;
};

$container['observationAttendees'] = function( $container )
{
    return new Edges
    (
        $container ,
        'observations_attendees' ,
        [
            'from' =>
            [
                'name'       => 'people' ,
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'observations' ,
                'controller' => 'observationsController'
            ]
        ]
    ) ;
};

$container['observationAuthorityTechnicians'] = function( $container )
{
    return new Edges
    (
        $container ,
        'observations_authority_technicians',
        [
            'from' =>
            [
                'name'       => 'technicians',
                'controller' => 'techniciansController'
            ],
            'to' =>
            [
                'name'       => 'observations',
                'controller' => 'observationsController'
            ]
        ]
    ) ;
};

$container['observationAuthorityVeterinarians'] = function( $container )
{
    return new Edges
    (
        $container ,
        'observations_authority_veterinarians',
        [
            'from' =>
            [
                'name'       => 'veterinarians',
                'controller' => 'veterinariansController'
            ],
            'to' =>
            [
                'name'       => 'observations',
                'controller' => 'observationsController'
            ]
        ]
    ) ;
};

$container['observationLivestocks'] = function( $container )
{
    return new Edges
    (
        $container,
        'observations_livestocks',
        [
            'from' =>
            [
                'name'       => 'livestocks',
                'controller' => 'livestocksController'
            ],
            'to' =>
            [
                'name'       => 'observations',
                'controller' => 'observationsController'
            ]
        ]
    );
};

$container['observationObservationsStatus'] = function( $container )
{
    return new Edges
    (
        $container,
        'observations_observationsStatus',
        [
            'from' =>
            [
                'name'       => 'observations_status',
                'controller' => 'observationsStatusController'
            ],
            'to' =>
            [
                'name'       => 'observations',
                'controller' => 'observationsController'
            ]
        ]
    );
};

$container['observationObservationsTypes'] = function( $container )
{
    return new Edges
    (
        $container,
        'observations_observationsTypes',
        [
            'from' =>
            [
                'name'       => 'observations_types',
                'controller' => 'observationsTypesController'
            ],
            'to' =>
            [
                'name'       => 'observations',
                'controller' => 'observationsController'
            ]
        ]
    );
};

$container['observationWorkshops'] = function( $container )
{
    return new Edges
    (
        $container,
        'observations_workshops',
        [
            'from' =>
            [
                'name'       => 'workshops',
                'controller' => 'workshopsController'
            ],
            'to' =>
            [
                'name'       => 'observations',
                'controller' => 'observationsController'
            ]
        ]
    );
};

$container['observationPeople'] = function( $container )
{
    return new Edges
    (
        $container,
        'observations_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'observations',
                'controller' => 'observationsController'
            ]
        ]
    );
};

$container['observationPlaces'] = function( $container )
{
    return new Edges
    (
        $container,
        'observations_places',
        [
            'from' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ],
            'to' =>
            [
                'name'       => 'observations',
                'controller' => 'observationsController'
            ]
        ]
    );
};

$container['observationOwners'] = function( $container )
{
    return new Edges
    (
        $container,
        'observations_owners',
        [
            'from' =>
            [
                'name'       => 'users',
                'controller' => 'usersController'
            ],
            'to' =>
            [
                'name'       => 'observations',
                'controller' => 'observationsController'
            ]
        ]
    );
};

$container['sectors'] = function( $container )
{
    return new Sectors
    (
        $container ,
        'sectors' ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ]

            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'joins' =>
            [
                [
                    'name'           => 'additionalType',
                    'controller'     => 'sectorsTypesController',
                    'edgeController' => 'sectorSectorsTypesController',
                    'skin'           => 'list'
                ]
            ]
        ]
    ) ;
};

$container['sectorsSectorsTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'sectors_sectorsTypes',
        [
            'from' =>
            [
                'name'       => 'sectors_types',
                'controller' => 'sectorsTypesController'
            ],
            'to' =>
            [
                'name'       => 'sectors' ,
                'controller' => 'sectorsController'
            ]
        ]
    );
};

$container['workplaces'] = function( $container )
{
    return new Workplaces
    (
        $container ,
        'workplaces' ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ]

            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'edges'      =>
            [
                [
                    'name'           => 'sectors',
                    'controller'     => 'sectorsController',
                    'edgeController' => 'workplacesSectorsController',
                    'skin'           => 'full',
                    'joins'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'sectorsTypesController',
                            'edgeController' => 'sectorSectorsTypesController',
                            'skin'           => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'       => 'identifier',
                    'controller' => 'livestockNumbersController',
                    'skin'       => 'list',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'livestocksNumbersTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ]
        ]
    ) ;
};

$container['workplacesSectors'] = function( $container )
{
    return new Edges
    (
        $container ,
        'workplaces_sectors' ,
        [
            'from' =>
            [
                'name'       => 'sectors',
                'controller' => 'sectorsController'
            ],
            'to' =>
            [
                'name'       => 'workplaces',
                'controller' => 'workplacesController'
            ]
        ]
    ) ;
};

$container['workshops'] = function( $container )
{
    return new Workshops
    (
        $container ,
        "workshops",
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ]

            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'breeding',
                    'controller'     => 'breedingsTypesController',
                    'edgeController' => 'workshopsBreedingsTypesController',
                    'skin'           => 'full'
                ],
                [
                    'name'           => 'production',
                    'controller'     => 'productionsTypesController',
                    'edgeController' => 'workshopsProductionsTypesController',
                    'skin'           => 'full'
                ],
                [
                    'name'           => 'water',
                    'controller'     => 'waterOriginsController',
                    'edgeController' => 'workshopsWaterOriginsController',
                    'skin'           => 'full'
                ],
                [
                    'name'           => 'workplaces',
                    'controller'     => 'workplacesController',
                    'edgeController' => 'workshopsWorkplacesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'sectors',
                            'controller'     => 'sectorsController',
                            'edgeController' => 'workplacesSectorsController',
                            'skin'           => 'full',
                            'joins'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'sectorsTypesController',
                                    'edgeController' => 'sectorSectorsTypesController',
                                    'skin'           => 'list'
                                ]
                            ]
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'identifier',
                            'controller' => 'livestockNumbersController',
                            'skin'       => 'list',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'livestocksNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'       => 'identifier',
                    'controller' => 'livestockNumbersController',
                    'skin'       => 'list',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'livestocksNumbersTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ]
        ]
    ) ;
};

$container['workshopsBreedingsTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'workshops_breedingsTypes' ,
        [
            'from' =>
            [
                'name'       => 'breedings_types',
                'controller' => 'breedingsTypesController'
            ],
            'to' =>
            [
                'name'       => 'workshops',
                'controller' => 'workshopsController'
            ]
        ]
    ) ;
};

$container['workshopsProductionsTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'workshops_productionsTypes' ,
        [
            'from' =>
            [
                'name'       => 'productions_types',
                'controller' => 'productionsTypesController'
            ],
            'to' =>
            [
                'name'       => 'workshops',
                'controller' => 'workshopsController'
            ]
        ]
    ) ;
};

$container['workshopsWaterOrigins'] = function( $container )
{
    return new Edges
    (
        $container ,
        'workshops_waterOrigins' ,
        [
            'from' =>
            [
                'name'       => 'water_origins',
                'controller' => 'waterOriginsController'
            ],
            'to' =>
            [
                'name'       => 'workshops',
                'controller' => 'workshopsController'
            ]
        ]
    ) ;
};

$container['workshopsWorkplaces'] = function( $container )
{
    return new Edges
    (
        $container ,
        'workshops_workplaces' ,
        [
            'from' =>
            [
                'name'       => 'workplaces',
                'controller' => 'workplacesController'
            ],
            'to' =>
            [
                'name'       => 'workshops',
                'controller' => 'workshopsController'
            ]
        ]
    ) ;
};

$container['livestocksWorkshops'] = function( $container )
{
    return new Edges
    (
        $container ,
        'livestocks_workshops' ,
        [
            'from' =>
            [
                'name'       => 'workshops',
                'controller' => 'workshopsController'
            ],
            'to' =>
            [
                'name'       => 'livestocks',
                'controller' => 'livestocksController'
            ]
        ]
    ) ;
};
