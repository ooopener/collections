<?php

use com\ooopener\models\Edges ;

use com\ooopener\models\creativeWork\MediaObjects ;

$container['mediaObjects'] = function( $container )
{
    return new MediaObjects
    (
        $container ,
        "mediaObjects",
        "mediaObjects_things_audio",
        "mediaObjects_things_audios",
        "mediaObjects_things_image",
        "mediaObjects_things_logo",
        "mediaObjects_things_photos",
        "mediaObjects_things_video",
        "mediaObjects_things_videos",
        [
            'applications',
            'articles',
            'conceptualObjects',
            'courses',
            'events',
            'organizations',
            'people',
            'places',
            'stages',
            'steps'
        ],
        [
            'facetable' =>
            [
                'id' =>
                [
                    '_key'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ],
                'encodingFormat' =>
                [
                    'encodingFormat' => 'field'
                ],
                'keywords' =>
                [
                    'keywords' => 'list'
                ]

            ],
            'searchable' =>
            [
                'headline' ,
                'alternativeHeadline',
                'name'
            ],
            'sortable' =>
            [
                'id'             => '_key',
                'name'           => 'name',
                'encodingFormat' => 'encodingFormat',
                'created'        => 'created',
                'modified'       => 'modified'
            ]
        ]
    ) ;
};

$container['mediaObjectsThingsAudio'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audio',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'things',
                'controller' => 'thingsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsThingsImage'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_image',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'things',
                'controller' => 'thingsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsThingsLogo'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_logo',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'things',
                'controller' => 'thingsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsThingsVideo'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_video',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'things',
                'controller' => 'thingsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsThingsAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'things',
                'controller' => 'thingsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsThingsPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'things',
                'controller' => 'thingsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsThingsVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'things',
                'controller' => 'thingsController'
            ]
        ]
    ) ;
};

//////////////////////////////
//////////////////////////////
//////////////////////////////

//////
// applications
//////

$container['mediaObjectsApplicationsAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsApplicationsPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsApplicationsVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'applications',
                'controller' => 'applicationsController'
            ]
        ]
    ) ;
};

//////
// articles
//////

$container['mediaObjectsArticlesAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ]
        ]
    ) ;
};

$container['mediaObjectsArticlesPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ]
        ]
    ) ;
};

$container['mediaObjectsArticlesVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ]
        ]
    ) ;
};

//////
// conceptualObjects
//////

$container['mediaObjectsConceptualObjectsAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsConceptualObjectsPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsConceptualObjectsVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ]
        ]
    ) ;
};

//////
// courses
//////

$container['mediaObjectsCoursesAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['mediaObjectsCoursesPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

$container['mediaObjectsCoursesVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ]
        ]
    ) ;
};

//////
// events
//////

$container['mediaObjectsEventsAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsEventsPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsEventsVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ]
        ]
    ) ;
};

//////
// organizations
//////

$container['mediaObjectsOrganizationsAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsOrganizationsPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsOrganizationsVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
                [
                    'name'       => 'mediaObjects',
                    'controller' => 'videoObjectsController'
                ],
            'to' =>
                [
                    'name'       => 'organizations',
                    'controller' => 'organizationsController'
                ]
        ]
    ) ;
};

//////
// people
//////

$container['mediaObjectsPeopleAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ]
        ]
    ) ;
};

$container['mediaObjectsPeoplePhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ]
        ]
    ) ;
};

$container['mediaObjectsPeopleVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ]
        ]
    ) ;
};

//////
// places
//////

$container['mediaObjectsPlacesAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['mediaObjectsPlacesPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['mediaObjectsPlacesVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

//////
// stages
//////

$container['mediaObjectsStagesAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['mediaObjectsStagesPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['mediaObjectsStagesVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

//////
// steps
//////

$container['mediaObjectsStepsAudios'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_audios',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'audioObjectsController'
            ],
            'to' =>
            [
                'name'       => 'steps',
                'controller' => 'stepsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsStepsPhotos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_photos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'imageObjectsController'
            ],
            'to' =>
            [
                'name'       => 'steps',
                'controller' => 'stepsController'
            ]
        ]
    ) ;
};

$container['mediaObjectsStepsVideos'] = function( $container )
{
    return new Edges
    (
        $container ,
        'mediaObjects_things_videos',
        [
            'from' =>
            [
                'name'       => 'mediaObjects',
                'controller' => 'videoObjectsController'
            ],
            'to' =>
            [
                'name'       => 'steps',
                'controller' => 'stepsController'
            ]
        ]
    ) ;
};



