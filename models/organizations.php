<?php

use com\ooopener\models\Edges ;
use com\ooopener\models\Emails ;
use com\ooopener\models\Keywords ;
use com\ooopener\models\Organizations ;
use com\ooopener\models\PhoneNumbers ;
use com\ooopener\models\Photos ;
use com\ooopener\models\Websites ;

use com\ooopener\models\organizations\OrganizationNumbers ;

$container['organizations'] = function( $container )
{
    return new Organizations
    (
        $container ,
        'organizations' ,
        [
            'facetable' =>
            [
                'addressCountry' =>
                [
                    'id'   => 'field'
                ]
                ,
                'addressDepartment' =>
                [
                    'id'   => 'field'
                ]
                ,
                'addressLocality' =>
                [
                    'id'   => 'field'
                ]
                ,
                'addressRegion' =>
                [
                    'id'   => 'field'
                ]
                ,
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]
                ,
                'additionalType' =>
                [
                    'id'     => 'thesaurus' ,
                    'edge'   => 'organizations_organizationsTypes'
                ]
                ,
                'numbers' =>
                [
                    '_key' => 'edgeComplex' ,
                    'edge' => 'organizations_organizationsNumbers'
                ]
            ]
            ,
            'searchable' =>
            [
                'name' , 'addressCountry', 'addressDepartment'
            ]
            ,
            'sortable' =>
            [
                'id'                => '_key',
                'addressCountry'    => 'address.addressCountry',
                'addressDepartment' => 'address.addressDepartment',
                'addressLocality'   => 'address.addressLocality',
                'addressRegion'     => 'address.addressRegion',
                'postalCode'        => 'address.postalCode',
                'streetAddress'     => 'address.streetAddress',
                'name'              => 'name',
                'created'           => 'created',
                'modified'          => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'additionalType',
                    'controller'     => 'organizationsTypesController',
                    'edgeController' => 'organizationOrganizationsTypesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'ape',
                    'controller'     => 'organizationsNafController',
                    'edgeController' => 'organizationApeController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'department',
                    'controller'     => 'organizationsController',
                    'edgeController' => 'organizationDepartmentController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'organizationsTypesController',
                            'edgeController' => 'organizationOrganizationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'organizationPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'email',
                    'controller'     => 'organizationEmailsController',
                    'edgeController' => 'organizationOrganizationsEmailsController',
                    'skin'           => 'normal',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'emailsTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'employees',
                    'controller'     => 'peopleController',
                    'edgeController' => 'organizationEmployeesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'email',
                            'controller'     => 'peopleEmailsController',
                            'edgeController' => 'peoplePeopleEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'peoplePhoneNumbersController',
                            'edgeController' => 'peoplePeoplePhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'job',
                            'controller'     => 'jobsController',
                            'edgeController' => 'peopleJobsController',
                            'skin'           => 'normal'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'normal'
                        ],
                        [
                            'name'       => 'honorificPrefix',
                            'controller' => 'honorificPrefixController',
                            'skin'       => 'normal'
                        ]
                    ]
                ],
                [
                    'name'           => 'founder',
                    'controller'     => 'peopleController',
                    'edgeController' => 'organizationFounderController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'email',
                            'controller'     => 'peopleEmailsController',
                            'edgeController' => 'peoplePeopleEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'peoplePhoneNumbersController',
                            'edgeController' => 'peoplePeoplePhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'job',
                            'controller'     => 'jobsController',
                            'edgeController' => 'peopleJobsController',
                            'skin'           => 'normal'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'normal'
                        ],
                        [
                            'name'       => 'honorificPrefix',
                            'controller' => 'honorificPrefixController',
                            'skin'       => 'normal'
                        ]
                    ]
                ],
                [
                    'name'           => 'foundingLocation',
                    'controller'     => 'placesController',
                    'edgeController' => 'organizationFoundingLocationController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'keywords',
                    'controller'     => 'organizationKeywordsController',
                    'edgeController' => 'organizationOrganizationsKeywordsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'legalForm',
                    'controller'     => 'businessEntityTypesController',
                    'edgeController' => 'organizationLegalFormController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'location',
                    'controller'     => 'placesController',
                    'edgeController' => 'organizationPlacesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'logo',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsLogoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'memberOf',
                    'controller'     => 'organizationsController',
                    'edgeController' => 'organizationMembersOrganizationsController',
                    'direction'      => 'reverse',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'organizationsTypesController',
                            'edgeController' => 'organizationOrganizationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'organizationPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'numbers',
                    'controller'     => 'organizationNumbersController',
                    'edgeController' => 'organizationOrganizationsNumbersController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'organizationsNumbersTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'owns',
                    'controller'     => 'conceptualObjectsController',
                    'edgeController' => 'conceptualObjectProductionsOrganizationsController',
                    'direction'      => 'reverse',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'category',
                            'controller'     => 'conceptualObjectsCategoriesController',
                            'edgeController' => 'conceptualObjectConceptualObjectsCategoriesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'movement',
                            'controller'     => 'artMovementsController',
                            'edgeController' => 'conceptualObjectArtMovementsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'temporal',
                            'controller'     => 'artTemporalEntitiesController',
                            'edgeController' => 'conceptualObjectArtTemporalEntitiesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'parentOrganization',
                    'controller'     => 'organizationsController',
                    'edgeController' => 'organizationSubOrganizationsController',
                    'direction'      => 'reverse',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'organizationsTypesController',
                            'edgeController' => 'organizationOrganizationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'organizationPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'subOrganization',
                    'controller'     => 'organizationsController',
                    'edgeController' => 'organizationSubOrganizationsController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'organizationsTypesController',
                            'edgeController' => 'organizationOrganizationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'organizationPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'telephone',
                    'controller'     => 'organizationPhoneNumbersController',
                    'edgeController' => 'organizationOrganizationsPhoneNumbersController',
                    'skin'           => 'normal',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'phoneNumbersTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'websites',
                    'controller'     => 'organizationWebsitesController',
                    'edgeController' => 'organizationOrganizationsWebsitesController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'websitesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'           => 'audios',
                    'controller'     => 'audioObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'photos',
                    'controller'     => 'imageObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'videos',
                    'controller'     => 'videoObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ]
            ]
        ]
    );
};

$container['organizationApe'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_ape',
        [
            'from' =>
            [
                'name'       => 'organizations_naf',
                'controller' => 'organizationsNafController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationDepartment'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_department' ,
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationEmails'] = function( $container )
{
    return new Emails
    (
        $container ,
        'organizations_emails',
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'emailsTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['organizationEmployees'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_employees' ,
        [
            'from' =>
            [
                'name'       => 'people' ,
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'organizations' ,
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationFounder'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_founder' ,
        [
            'from' =>
            [
                'name'       => 'people' ,
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'organizations' ,
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationFoundingLocation'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_foundingLocation',
        [
            'from' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationKeywords'] = function( $container )
{
    return new Keywords( $container , 'organizations_keywords' ) ;
};

$container['organizationLegalForm'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_legalForm',
        [
            'from' =>
            [
                'name'       => 'business_entity_types',
                'controller' => 'businessEntityTypesController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationMembersOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_members_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationMembersPeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_members_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationNumbers'] = function( $container )
{
    return new OrganizationNumbers
    (
        $container ,
        'organizations_numbers' ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ]

            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'created'   => 'created',
                'modified'  => 'modified'
            ],
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'organizationsNumbersTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['organizationPhoneNumbers'] = function( $container )
{
    return new PhoneNumbers
    (
        $container ,
        'organizations_phoneNumbers',
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'phoneNumbersTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['organizationProvidersOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_providers_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationProvidersPeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_providers_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationOrganizationsEmails'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_organizationsEmails',
        [
            'from' =>
            [
                'name'       => 'organizations_emails',
                'controller' => 'organizationEmailsController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationOrganizationsKeywords'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_organizationsKeywords',
        [
            'from' =>
            [
                'name'       => 'organizations_keywords',
                'controller' => 'organizationKeywordsController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationOrganizationsNumbers'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_organizationsNumbers' ,
        [
            'from' =>
            [
                'name'       => 'organization_numbers',
                'controller' => 'organizationNumbersController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationOrganizationsPhoneNumbers'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_organizationsPhoneNumbers',
        [
            'from' =>
            [
                'name'       => 'organizations_phoneNumbers',
                'controller' => 'organizationPhoneNumbersController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationOrganizationsTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_organizationsTypes',
        [
            'from' =>
            [
                'name'       => 'organizations_types',
                'controller' => 'organizationsTypesController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationPhotos'] = function( $container )
{
    return new Photos
    (
        $container ,
        'organizations_photos' ,
        [
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'author'    => 'author' ,
                'license'   => 'license' ,
                'publisher' => 'publisher' ,
                'width'     => 'width' ,
                'height'    => 'height' ,
                'primary'   => 'first'
            ]
        ]
    ) ;
};

$container['organizationPlaces'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_places',
        [
            'from' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationSubOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_subOrganizations' ,
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};

$container['organizationWebsites'] = function( $container )
{
    return new Websites
    (
        $container ,
        'organizations_websites' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'websitesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['organizationOrganizationsWebsites'] = function( $container )
{
    return new Edges
    (
        $container ,
        'organizations_organizationsWebsites',
        [
            'from' =>
            [
                'name'       => 'organizations_websites',
                'controller' => 'organizationWebsitesController'
            ],
            'to' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ]
        ]
    ) ;
};
