<?php

use com\ooopener\models\People ;

use com\ooopener\models\Edges ;
use com\ooopener\models\Emails ;
use com\ooopener\models\Keywords ;
use com\ooopener\models\PhoneNumbers ;
use com\ooopener\models\Websites ;

$container['people'] = function( $container )
{
    return new People
    (
        $container ,
        'people',
        [
            'facetable' =>
            [
                'gender' =>
                [
                    'id'     => 'thesaurus' ,
                    'model'  => 'people_genders'
                ],
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]
            ]
            ,
            'searchable' =>
            [
                'name','givenName','familyName','alternateName'
            ]
            ,
            'sortable' =>
            [
                'id'                => '_key',
                'addressCountry'    => 'address.addressCountry',
                'addressDepartment' => 'address.addressDepartment',
                'addressLocality'   => 'address.addressLocality',
                'addressRegion'     => 'address.addressRegion',
                'postalCode'        => 'address.postalCode',
                'streetAddress'     => 'address.streetAddress',
                'name'              => 'name',
                'nationality'       => 'nationality',
                'alternateName'     => 'alternateName',
                'familyName'        => 'familyName',
                'givenName'         => 'givenName',
                'created'           => 'created',
                'modified'          => 'modified',
                'birthDate'         => 'birthDate',
                'birthPlace'        => 'birthPlace',
                'deathDate'         => 'deathDate',
                'deathPlace'        => 'deathPlace'
            ],
            'edges' =>
            [
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'email',
                    'controller'     => 'peopleEmailsController',
                    'edgeController' => 'peoplePeopleEmailsController',
                    'skin'           => 'normal',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'emailsTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'job',
                    'controller'     => 'jobsController',
                    'edgeController' => 'peopleJobsController',
                    'skin'           => 'normal'
                ],
                [
                    'name'           => 'keywords',
                    'controller'     => 'peopleKeywordsController',
                    'edgeController' => 'peoplePeopleKeywordsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'memberOf',
                    'controller'     => 'organizationsController',
                    'edgeController' => 'organizationMembersPeopleController',
                    'direction'      => 'reverse',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'organizationsTypesController',
                            'edgeController' => 'organizationOrganizationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'organizationPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'owns',
                    'controller'     => 'conceptualObjectsController',
                    'edgeController' => 'conceptualObjectProductionsPeopleController',
                    'direction'      => 'reverse',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'category',
                            'controller'     => 'conceptualObjectsCategoriesController',
                            'edgeController' => 'conceptualObjectConceptualObjectsCategoriesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'movement',
                            'controller'     => 'artMovementsController',
                            'edgeController' => 'conceptualObjectArtMovementsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'temporal',
                            'controller'     => 'artTemporalEntitiesController',
                            'edgeController' => 'conceptualObjectArtTemporalEntitiesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'telephone',
                    'controller'     => 'peoplePhoneNumbersController',
                    'edgeController' => 'peoplePeoplePhoneNumbersController',
                    'skin'           => 'normal',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'phoneNumbersTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'websites',
                    'controller'     => 'peopleWebsitesController',
                    'edgeController' => 'peoplePeopleWebsitesController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'websitesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'worksFor',
                    'controller'     => 'organizationsController',
                    'edgeController' => 'organizationEmployeesController',
                    'direction'      => 'reverse',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'organizationsTypesController',
                            'edgeController' => 'organizationOrganizationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'organizationPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'           => 'audios',
                    'controller'     => 'audioObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'       => 'gender',
                    'controller' => 'gendersController',
                    'skin'       => 'normal'
                ],
                [
                    'name'       => 'honorificPrefix',
                    'controller' => 'honorificPrefixController',
                    'skin'       => 'normal'
                ],
                [
                    'name'           => 'photos',
                    'controller'     => 'imageObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'videos',
                    'controller'     => 'videoObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ]
            ]
        ]
    ) ;
};

$container['peopleEmails'] = function( $container )
{
    return new Emails
    (
        $container ,
        'people_emails',
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'emailsTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['peopleKeywords'] = function( $container )
{
    return new Keywords( $container , 'people_keywords' ) ;
};

$container['peoplePeopleEmails'] = function( $container )
{
    return new Edges
    (
        $container ,
        'people_peopleEmails',
        [
            'from' =>
            [
                'name'       => 'people_emails',
                'controller' => 'peopleEmailsController'
            ],
            'to' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ]
        ]
    ) ;
};

$container['peoplePeopleKeywords'] = function( $container )
{
    return new Edges
    (
        $container ,
        'people_peopleKeywords',
        [
            'from' =>
            [
                'name'       => 'people_keywords',
                'controller' => 'peopleKeywordsController'
            ],
            'to' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ]
        ]
    ) ;
};

$container['peoplePeoplePhoneNumbers'] = function( $container )
{
    return new Edges
    (
        $container ,
        'people_peoplePhoneNumbers',
        [
            'from' =>
            [
                'name'       => 'people_phoneNumbers',
                'controller' => 'peoplePhoneNumbersController'
            ],
            'to' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ]
        ]
    ) ;
};

$container['peoplePeopleWebsites'] = function( $container )
{
    return new Edges
    (
        $container ,
        'people_peopleWebsites',
        [
            'from' =>
            [
                'name'       => 'people_websites',
                'controller' => 'peopleWebsitesController'
            ],
            'to' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ]
        ]
    ) ;
};

$container['peoplePhoneNumbers'] = function( $container )
{
    return new PhoneNumbers
    (
        $container ,
        'people_phoneNumbers',
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'phoneNumbersTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['peopleJobs'] = function( $container )
{
    return new Edges
    (
        $container ,
        'people_jobs' ,
        [
            'from' =>
            [
                'name'       => 'jobs',
                'controller' => 'jobsController'
            ],
            'to' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ]
        ]
    ) ;
};

$container['peopleWebsites'] = function( $container )
{
    return new Websites
    (
        $container ,
        'people_websites' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'websitesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};
