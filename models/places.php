<?php

use com\ooopener\models\Edges ;
use com\ooopener\models\Emails ;
use com\ooopener\models\Keywords ;
use com\ooopener\models\Offers ;
use com\ooopener\models\OpeningHoursSpecifications ;
use com\ooopener\models\PhoneNumbers ;
use com\ooopener\models\Places ;
use com\ooopener\models\Photos ;
use com\ooopener\models\Websites ;

$container['places'] = function( $container )
{
    return new Places
    (
        $container ,
        'places' ,
        [
            'facetable' =>
            [
                'addressCountry' =>
                [
                    'address.addressCountry'   => 'field'
                ]
                ,
                'addressDepartment' =>
                [
                    'address.addressDepartment'   => 'field'
                ]
                ,
                'addressLocality' =>
                [
                    'address.addressLocality'   => 'field'
                ]
                ,
                'addressRegion' =>
                [
                    'address.addressRegion'   => 'field'
                ]
                ,
                'id' =>
                [
                    '_key'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]
                ,
                'additionalType' =>
                [
                    'id'    => 'thesaurus' ,
                    'edge'  => 'places_placesTypes'
                ]
                ,
                'services' =>
                [
                    'id'    => 'thesaurus' ,
                    'edge'  => 'places_services'
                ]
            ]
            ,
            'searchable' =>
            [
                'name' , 'addressCountry', 'addressDepartment'
            ]
            ,
            'sortable' =>
            [
                'id'                => '_key',
                'addressCountry'    => 'address.addressCountry',
                'addressDepartment' => 'address.addressDepartment',
                'addressLocality'   => 'address.addressLocality',
                'addressRegion'     => 'address.addressRegion',
                'postalCode'        => 'address.postalCode',
                'streetAddress'     => 'address.streetAddress',
                'name'              => 'name',
                'created'           => 'created',
                'modified'          => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'activities',
                    'controller'     => 'activitiesController',
                    'edgeController' => 'placeActivitiesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'additionalType',
                    'controller'     => 'placesTypesController',
                    'edgeController' => 'placePlacesTypesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'conceptualObjects',
                    'controller'     => 'conceptualObjectsController',
                    'edgeController' => 'conceptualObjectPlacesController',
                    'direction'      => 'reverse',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'category',
                            'controller'     => 'conceptualObjectsCategoriesController',
                            'edgeController' => 'conceptualObjectConceptualObjectsCategoriesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'movement',
                            'controller'     => 'artMovementsController',
                            'edgeController' => 'conceptualObjectArtMovementsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'temporal',
                            'controller'     => 'artTemporalEntitiesController',
                            'edgeController' => 'conceptualObjectArtTemporalEntitiesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'containsPlace',
                    'controller'     => 'placesController',
                    'edgeController' => 'placeContainsPlacesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'containedInPlace',
                    'controller'     => 'placesController',
                    'edgeController' => 'placeContainsPlacesController',
                    'direction'      => 'reverse',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'events',
                    'controller'     => 'eventsController',
                    'edgeController' => 'eventPlacesController',
                    'direction'      => 'reverse',
                    'skin'           => 'normal',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'eventsTypesController',
                            'edgeController' => 'eventEventsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'eventPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'offers',
                            'controller'     => 'eventOffersController',
                            'edgeController' => 'eventEventsOffersController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'category',
                                    'controller' => 'offersCategoriesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'eventStatus' ,
                            'controller' => 'eventsStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'email',
                    'controller'     => 'placeEmailsController',
                    'edgeController' => 'placePlacesEmailsController',
                    'skin'           => 'normal',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'emailsTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'keywords',
                    'controller'     => 'placeKeywordsController',
                    'edgeController' => 'placePlacesKeywordsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'logo',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsLogoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'museumType',
                    'controller'     => 'museumsTypesController',
                    'edgeController' => 'placeMuseumsTypesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'offers',
                    'controller'     => 'placeOffersController',
                    'edgeController' => 'placePlacesOffersController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'category',
                            'controller' => 'offersCategoriesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'openingHoursSpecification',
                    'controller'     => 'placeOpeningHoursController',
                    'edgeController' => 'placePlacesOpeningHoursController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'permits',
                    'controller'     => 'placesRegulationsController',
                    'edgeController' => 'placePermitsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'prohibitions',
                    'controller'     => 'placesRegulationsController',
                    'edgeController' => 'placeProhibitionsController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'services',
                    'controller'     => 'servicesController',
                    'edgeController' => 'placeServicesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'telephone',
                    'controller'     => 'placePhoneNumbersController',
                    'edgeController' => 'placePlacesPhoneNumbersController',
                    'skin'           => 'normal',
                    'joins' =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'phoneNumbersTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'websites',
                    'controller'     => 'placeWebsitesController',
                    'edgeController' => 'placePlacesWebsitesController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'websitesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'           => 'audios',
                    'controller'     => 'audioObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'photos',
                    'controller'     => 'imageObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'       => 'status' ,
                    'controller' => 'placesStatusTypesController',
                    'skin'       => 'list'
                ],
                [
                    'name'           => 'videos',
                    'controller'     => 'videoObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ]
            ]
        ]
    );
};

$container['placeActivities'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_activities',
        [
            'from' =>
            [
                'name'       => 'activities',
                'controller' => 'activitiesController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placeEmails'] = function( $container )
{
    return new Emails
    (
        $container ,
        'places_emails',
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'emailsTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['placeKeywords'] = function( $container )
{
    return new Keywords( $container , 'places_keywords' ) ;
};

$container['placeOffers'] = function( $container )
{
    return new Offers
    (
        $container ,
        'places_offers' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'category',
                    'controller' => 'offersCategoriesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['placeOpeningHours'] = function( $container )
{
    return new OpeningHoursSpecifications( $container , 'places_opening_hours' ) ;
};

$container['placePermits'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_permits',
        [
            'from' =>
            [
                'name'       => 'places_regulations',
                'controller' => 'placesRegulationsController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placePhoneNumbers'] = function( $container )
{
    return new PhoneNumbers
    (
        $container ,
        'places_phoneNumbers',
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'phoneNumbersTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['placePhotos'] = function( $container )
{
    return new Photos
    (
        $container ,
        'places_photos' ,
        [
            'sortable' =>
            [
                'id'        => '_key',
                'name'      => 'name',
                'created'   => 'created',
                'modified'  => 'modified',
                'author'    => 'author' ,
                'license'   => 'license' ,
                'publisher' => 'publisher' ,
                'width'     => 'width' ,
                'height'    => 'height' ,
                'primary'   => 'first'
            ]
        ]
    ) ;
};

$container['placeContainsPlaces'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_subPlaces' ,
        [
            'from' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placeMuseumsTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_museumsTypes',
        [
            'from' =>
                [
                    'name'       => 'museums_types',
                    'controller' => 'museumsTypesController'
                ],
            'to' =>
                [
                    'name'       => 'places',
                    'controller' => 'placesController'
                ]
        ]
    ) ;
};

$container['placePlacesEmails'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_placesEmails',
        [
            'from' =>
            [
                'name'       => 'places_emails',
                'controller' => 'placeEmailsController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placePlacesKeywords'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_placesKeywords',
        [
            'from' =>
            [
                'name'       => 'places_keywords',
                'controller' => 'placeKeywordsController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placePlacesOffers'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_placesOffers',
        [
            'from' =>
            [
                'name'       => 'places_offers',
                'controller' => 'placeOffersController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placePlacesOpeningHours'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_placesOpeningHours',
        [
            'from' =>
            [
                'name'       => 'places_openingHours',
                'controller' => 'placeOpeningHoursController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placePlacesPhoneNumbers'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_placesPhoneNumbers',
        [
            'from' =>
            [
                'name'       => 'places_phoneNumbers',
                'controller' => 'placePhoneNumbersController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placePlacesTypes'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_placesTypes',
        [
            'from' =>
            [
                'name'       => 'places_types',
                'controller' => 'placesTypesController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placePlacesWebsites'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_placesWebsites',
        [
            'from' =>
            [
                'name'       => 'places_websites',
                'controller' => 'placeWebsitesController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placeProhibitions'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_prohibitions',
        [
            'from' =>
            [
                'name'       => 'places_regulations',
                'controller' => 'placesRegulationsController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placeServices'] = function( $container )
{
    return new Edges
    (
        $container ,
        'places_services',
        [
            'from' =>
            [
                'name'       => 'services',
                'controller' => 'servicesController'
            ],
            'to' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ]
        ]
    ) ;
};

$container['placeWebsites'] = function( $container )
{
    return new Websites
    (
        $container ,
        'places_websites' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'websitesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};
