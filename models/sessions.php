<?php

use com\ooopener\models\Sessions ;

$container['sessions'] = function( $container )
{
    return new Sessions
    (
        $container ,
        "sessions" ,
        [
            'facetable' =>
            [
                'type' =>
                [
                    'type' => 'field'
                ],
                'user' =>
                [
                    'user' => 'field'
                ]
            ],
            'searchable' =>
            [
                'user' , 'type'
            ],
            'sortable' =>
            [
                'id'      => '_key',
                'created' => 'created',
                'type'    => 'type'
            ],
            'joins' =>
            [
                [
                    'name'       => 'app' ,
                    'controller' => 'applicationsController' ,
                    'key'        => 'oAuth.client_id' ,
                    'skin'       => 'list',
                    'edges'      =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'applicationsTypesController',
                            'edgeController' => 'applicationApplicationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ]
            ]
        ]
    ) ;
};

