<?php

use com\ooopener\models\Collections ;

$container['settingsList' ] = function( $container )
{
    return new Collections
    (
        $container ,
        "settings" ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ]
                ,
                'category' =>
                [
                    'id'   => 'field'
                ]
            ],
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'       => '_key',
                'name'     => 'name',
                'category' => 'category',
            ]
        ]
    ) ;
};
