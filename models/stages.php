<?php

use com\ooopener\models\Stages ;
use com\ooopener\models\Edges ;
use com\ooopener\models\Keywords ;
use com\ooopener\models\Websites ;

$container['stages'] = function( $container )
{
    return new Stages
    (
        $container ,
        'stages' ,
        [
            'facetable' =>
            [

                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]

            ]
            ,
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'                => '_key',
                'name'              => 'name',
                'created'           => 'created',
                'modified'          => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'location',
                    'controller'     => 'placesController',
                    'edgeController' => 'stageLocationsController',
                    'skin'           => 'normal',
                    'edges'          =>
                    [
                        [
                            'name'           => 'activities',
                            'controller'     => 'activitiesController',
                            'edgeController' => 'placeActivitiesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'email',
                            'controller'     => 'placeEmailsController',
                            'edgeController' => 'placePlacesEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'placePhoneNumbersController',
                            'edgeController' => 'placePlacesPhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'offers',
                            'controller'     => 'placeOffersController',
                            'edgeController' => 'placePlacesOffersController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'category',
                                    'controller' => 'offersCategoriesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'openingHoursSpecification',
                            'controller'     => 'placeOpeningHoursController',
                            'edgeController' => 'placePlacesOpeningHoursController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'permits',
                            'controller'     => 'placesRegulationsController',
                            'edgeController' => 'placePermitsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'prohibitions',
                            'controller'     => 'placesRegulationsController',
                            'edgeController' => 'placeProhibitionsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'services',
                            'controller'     => 'servicesController',
                            'edgeController' => 'placeServicesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'websites',
                            'controller'     => 'placeWebsitesController',
                            'edgeController' => 'placePlacesWebsitesController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'websitesTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'status',
                    'controller'     => 'coursesStatusController',
                    'edgeController' => 'stageCoursesStatusController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'activities',
                    'controller'     => 'activitiesController',
                    'edgeController' => 'stageActivitiesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'articles',
                    'controller'     => 'articlesController',
                    'edgeController' => 'stageArticlesController',
                    'skin'           => 'list',
                    'edges'           =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'articlesTypesController',
                            'edgeController' => 'articleArticlesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'courses',
                    'controller'     => 'coursesController',
                    'edgeController' => 'stageCoursesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'coursesTypesController',
                            'edgeController' => 'courseCoursesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'level',
                            'controller'     => 'coursesLevelsController',
                            'edgeController' => 'courseCoursesLevelsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'openingHoursSpecification',
                            'controller'     => 'courseOpeningHoursController',
                            'edgeController' => 'courseCoursesOpeningHoursController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'status',
                            'controller'     => 'coursesStatusController',
                            'edgeController' => 'courseCoursesStatusController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'events',
                    'controller'     => 'eventsController',
                    'edgeController' => 'stageEventsController',
                    'skin'           => 'normal',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'eventsTypesController',
                            'edgeController' => 'eventEventsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'eventPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'offers',
                            'controller'     => 'eventOffersController',
                            'edgeController' => 'eventEventsOffersController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'category',
                                    'controller' => 'offersCategoriesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'eventStatus' ,
                            'controller' => 'eventsStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'conceptualObjects',
                    'controller'     => 'conceptualObjectsController',
                    'edgeController' => 'stageConceptualObjectsController',
                    'skin'           => '',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'category',
                            'controller'     => 'conceptualObjectsCategoriesController',
                            'edgeController' => 'conceptualObjectConceptualObjectsCategoriesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'movement',
                            'controller'     => 'artMovementsController',
                            'edgeController' => 'conceptualObjectArtMovementsController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'organizations',
                    'controller'     => 'organizationsController',
                    'edgeController' => 'stageOrganizationsController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'organizationsTypesController',
                            'edgeController' => 'organizationOrganizationsTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'organizationPlacesController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'people',
                    'controller'     => 'peopleController',
                    'edgeController' => 'stagePeopleController',
                    'skin'           => '',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'email',
                            'controller'     => 'peopleEmailsController',
                            'edgeController' => 'peoplePeopleEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'peoplePhoneNumbersController',
                            'edgeController' => 'peoplePeoplePhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'normal'
                        ],
                        [
                            'name'       => 'honorificPrefix',
                            'controller' => 'honorificPrefixController',
                            'skin'       => 'normal'
                        ]
                    ]
                ],
                [
                    'name'           => 'places',
                    'controller'     => 'placesController',
                    'edgeController' => 'stagePlacesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'additionalType',
                            'controller'     => 'placesTypesController',
                            'edgeController' => 'placePlacesTypesController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'conceptualObjects',
                            'controller'     => 'conceptualObjectsController',
                            'edgeController' => 'conceptualObjectPlacesController',
                            'direction'      => 'reverse'
                        ],
                        [
                            'name'           => 'events',
                            'controller'     => 'eventsController',
                            'edgeController' => 'eventPlacesController',
                            'direction'      => 'reverse',
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'logo',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsLogoController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins' =>
                    [
                        [
                            'name'       => 'status' ,
                            'controller' => 'placesStatusTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'stages',
                    'controller'     => 'stagesController',
                    'edgeController' => 'stageStagesController',
                    'skin'           => '',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'location',
                            'controller'     => 'placesController',
                            'edgeController' => 'stageLocationsController',
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'placesTypesController',
                                    'edgeController' => 'placePlacesTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'audio',
                                    'controller'     => 'audioObjectsController',
                                    'edgeController' => 'mediaObjectsThingsAudioController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'conceptualObjects',
                                    'controller'     => 'conceptualObjectsController',
                                    'edgeController' => 'conceptualObjectPlacesController',
                                    'direction'      => 'reverse'
                                ],
                                [
                                    'name'           => 'events',
                                    'controller'     => 'eventsController',
                                    'edgeController' => 'eventPlacesController',
                                    'direction'      => 'reverse',
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'video',
                                    'controller'     => 'videoObjectsController',
                                    'edgeController' => 'mediaObjectsThingsVideoController',
                                    'skin'           => 'list'
                                ]
                            ],
                            'joins' =>
                            [
                                [
                                    'name'       => 'status' ,
                                    'controller' => 'placesStatusTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'status',
                            'controller'     => 'coursesStatusController',
                            'edgeController' => 'stageCoursesStatusController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'websites',
                    'controller'     => 'stageWebsitesController',
                    'edgeController' => 'stageStagesWebsitesController',
                    'skin'           => 'list',
                    'joins'          =>
                    [
                        [
                            'name'       => 'additionalType',
                            'controller' => 'websitesTypesController',
                            'skin'       => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'           => 'audios',
                    'controller'     => 'audioObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'photos',
                    'controller'     => 'imageObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ],
                [
                    'name'           => 'videos',
                    'controller'     => 'videoObjectsController',
                    'array'          => true,
                    'skin'           => 'extend'
                ]
            ]
        ]
    );
};

$container['stageArticles'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_articles',
        [
            'from' =>
            [
                'name'       => 'articles',
                'controller' => 'articlesController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageCourses'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_courses',
        [
            'from' =>
            [
                'name'       => 'courses',
                'controller' => 'coursesController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageEvents'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_events',
        [
            'from' =>
            [
                'name'       => 'events',
                'controller' => 'eventsController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageLocations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_locations',
        [
            'from' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageConceptualObjects'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_conceptualObjects',
        [
            'from' =>
            [
                'name'       => 'conceptualObjects',
                'controller' => 'conceptualObjectsController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stagePeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stagePlaces'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_places',
        [
            'from' =>
            [
                'name'       => 'places',
                'controller' => 'placesController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageStages'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_stages',
        [
            'from' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageCoursesStatus'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_coursesStatus',
        [
            'from' =>
            [
                'name'       => 'courses_status',
                'controller' => 'coursesStatusController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageActivities'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_activities',
        [
            'from' =>
            [
                'name'       => 'activities',
                'controller' => 'activitiesController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageKeywords'] = function( $container )
{
    return new Keywords( $container , 'stages_keywords' ) ;
};

$container['stageStagesKeywords'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_stagesKeywords',
        [
            'from' =>
            [
                'name'       => 'stages_keywords',
                'controller' => 'stageKeywordsController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};

$container['stageWebsites'] = function( $container )
{
    return new Websites
    (
        $container ,
        'stages_websites' ,
        [
            'joins' =>
            [
                [
                    'name'       => 'additionalType',
                    'controller' => 'websitesTypesController',
                    'skin'       => 'list'
                ]
            ]
        ]
    ) ;
};

$container['stageStagesWebsites'] = function( $container )
{
    return new Edges
    (
        $container ,
        'stages_stagesWebsites',
        [
            'from' =>
            [
                'name'       => 'stages_websites',
                'controller' => 'stageWebsitesController'
            ],
            'to' =>
            [
                'name'       => 'stages',
                'controller' => 'stagesController'
            ]
        ]
    ) ;
};
