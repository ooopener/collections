<?php

use com\ooopener\models\Teams ;

$container['teams'] = function( $container )
{
    return new Teams
    (
        $container ,
        "teams",
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]

            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'         => '_key',
                'active'     => 'active',
                'fr'         => 'alternateName.fr',
                'identifier' => 'identifier',
                'name'       => 'name',
                'created'    => 'created',
                'modified'   => 'modified'
            ]
        ]
    ) ;
};
