<?php

use com\ooopener\models\Edges;

use com\ooopener\models\Technicians;

$container['technicians'] = function( $container )
{
    return new Technicians
    (
        $container,
        'technicians',
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]
            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'authority.name',
                'created'   => 'created',
                'modified'  => 'modified',
                'after'     => 'authority.name'
            ],
            'edges' =>
            [
                [
                    'name'           => 'medicalSpecialties' ,
                    'controller'     => 'medicalSpecialtiesController',
                    'edgeController' => 'technicianMedicalSpecialtiesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'livestocks',
                    'controller'     => 'livestocksController' ,
                    'edgeController' => 'livestockTechniciansController' ,
                    'direction'      => 'reverse' ,
                    'skin'           => '',
                    'edges'          =>
                    [
                        [
                            'name'           => 'organization' ,
                            'controller'     => 'organizationsController' ,
                            'edgeController' => 'livestockOrganizationsController' ,
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'organizationsTypesController',
                                    'edgeController' => 'organizationOrganizationsTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'location',
                                    'controller'     => 'placesController',
                                    'edgeController' => 'organizationPlacesController',
                                    'skin'           => 'list',
                                    'edges'          =>
                                    [
                                        [
                                            'name'           => 'additionalType',
                                            'controller'     => 'placesTypesController',
                                            'edgeController' => 'placePlacesTypesController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'conceptualObjects',
                                            'controller'     => 'conceptualObjectsController',
                                            'edgeController' => 'conceptualObjectPlacesController',
                                            'direction'      => 'reverse'
                                        ],
                                        [
                                            'name'           => 'events',
                                            'controller'     => 'eventsController',
                                            'edgeController' => 'eventPlacesController',
                                            'direction'      => 'reverse',
                                        ],
                                        [
                                            'name'           => 'image',
                                            'controller'     => 'imageObjectsController',
                                            'edgeController' => 'mediaObjectsThingsImageController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'logo',
                                            'controller'     => 'imageObjectsController',
                                            'edgeController' => 'mediaObjectsThingsLogoController',
                                            'skin'           => 'list'
                                        ]
                                    ],
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'status' ,
                                            'controller' => 'placesStatusTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'numbers',
                            'controller'     => 'livestockNumbersController',
                            'edgeController' => 'livestocksLivestocksNumbersController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'livestocksNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'           => 'person',
                    'controller'     => 'peopleController',
                    'skin'           => 'normal',
                    'edges'          =>
                    [
                        [
                            'name'           => 'email',
                            'controller'     => 'peopleEmailsController',
                            'edgeController' => 'peoplePeopleEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'peoplePhoneNumbersController',
                            'edgeController' => 'peoplePeoplePhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'websites',
                            'controller'     => 'peopleWebsitesController',
                            'edgeController' => 'peoplePeopleWebsitesController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'websitesTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ],
                    'joins'          =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'normal'
                        ],
                        [
                            'name'       => 'honorificPrefix',
                            'controller' => 'honorificPrefixController',
                            'skin'       => 'normal'
                        ]
                    ]
                ]
            ]
        ]
    );
};

$container['technicianMedicalSpecialties'] = function( $container )
{
    return new Edges
    (
        $container ,
        'technicians_medicalSpecialties',
        [
            'from' =>
            [
                'name'       => 'medicalSpecialties',
                'controller' => 'medicalSpecialtiesController'
            ],
            'to' =>
            [
                'name'       => 'technicians',
                'controller' => 'techniciansController'
            ]
        ]
    );
};

$container['technicianPeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'technicians_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'technicians',
                'controller' => 'techniciansController'
            ]
        ]
    );
};
