<?php

use com\ooopener\models\Collections ;

$container['thesaurusList' ] = function( $container )
{
    return new Collections
    (
        $container ,
        "thesaurus" ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ]
                ,
                'category' =>
                [
                    'id'   => 'field'
                ]
            ],
            'searchable' =>
            [
                'name'
            ]
            ,
            'sortable' =>
            [
                'id'       => '_key',
                'name'     => 'name',
                'category' => 'category',
            ]
        ]
    ) ;
};

