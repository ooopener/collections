<?php

use com\ooopener\models\Collections ;

$container['things'] = function( $container )
{
    return new Collections
    (
        $container ,
        NULL,
        [
            'facetable' =>
            [
                'type' =>
                [
                    'path' => 'field'
                ]
            ],
            'searchable' =>
            [
                'name' , 'headline' , 'alternativeHeadline' , 'alternateName'
            ],
            'sortable' =>
            [
                'name', 'created' , 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'audio',
                    'controller'     => 'audioObjectsController',
                    'edgeController' => 'mediaObjectsThingsAudioController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'image',
                    'controller'     => 'imageObjectsController',
                    'edgeController' => 'mediaObjectsThingsImageController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'video',
                    'controller'     => 'videoObjectsController',
                    'edgeController' => 'mediaObjectsThingsVideoController',
                    'skin'           => 'list'
                ]
            ]
        ]
    ) ;
};
