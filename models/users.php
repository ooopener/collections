<?php

use com\ooopener\models\Edges ;
use com\ooopener\models\UserPermissions ;
use com\ooopener\models\Users ;

$container['users'] = function( $container )
{
    return new Users
    (
        $container ,
        "users" ,
        [
            'facetable' =>
            [
                'id' =>
                [
                    '_key' => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]
                ,
                'provider' =>
                [
                    'provider' => 'field'

                ]
            ],
            'searchable' =>
            [
                'name' , 'givenName' , 'familyName' , 'email'
            ],
            'sortable' =>
            [
                'id'         => '_key',
                'givenName'  => 'givenName',
                'familyName' => 'familyName',
                'name'       => 'name',
                'created'    => 'created',
                'modified'   => 'modified'
            ],
            'edges' =>
            [
                [
                    'name'           => 'favorites' ,
                    'controller'     => 'thingsController',
                    'edgeController' => 'userFavoritesController',
                    'skin'           => 'list',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ]
                ]
            ],
            'joins' =>
            [
                [
                    'name'       => 'gender',
                    'controller' => 'gendersController',
                    'skin'       => 'list'
                ],
                [
                    'name'       => 'person',
                    'controller' => 'peopleController',
                    'skin'       => '',
                    'edges'          =>
                    [
                        [
                            'name'           => 'audio',
                            'controller'     => 'audioObjectsController',
                            'edgeController' => 'mediaObjectsThingsAudioController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'email',
                            'controller'     => 'peopleEmailsController',
                            'edgeController' => 'peoplePeopleEmailsController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'emailsTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'image',
                            'controller'     => 'imageObjectsController',
                            'edgeController' => 'mediaObjectsThingsImageController',
                            'skin'           => 'list'
                        ],
                        [
                            'name'           => 'telephone',
                            'controller'     => 'peoplePhoneNumbersController',
                            'edgeController' => 'peoplePeoplePhoneNumbersController',
                            'skin'           => 'normal',
                            'joins' =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'phoneNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'video',
                            'controller'     => 'videoObjectsController',
                            'edgeController' => 'mediaObjectsThingsVideoController',
                            'skin'           => 'list'
                        ]
                    ],
                    'joins'      =>
                    [
                        [
                            'name'       => 'gender',
                            'controller' => 'gendersController',
                            'skin'       => 'normal'
                        ],
                        [
                            'name'       => 'honorificPrefix',
                            'controller' => 'honorificPrefixController',
                            'skin'       => 'normal'
                        ]
                    ]
                ]
            ]
        ]
    ) ;
};

$container['userFavorites'] = function( $container)
{
    return new Edges
    (
        $container ,
        'users_favorites' ,
        [
            'from' =>
            [
                'name'       => 'things',
                'controller' => 'thingsController'
            ],
            'to' =>
            [
                'name'       => 'users' ,
                'controller' => 'usersController'
            ]
        ]
    );
};

$container['userPermissions'] = function( $container )
{
    return new UserPermissions
    (
        $container ,
        "users_permissions"
    );
};
