<?php

use com\ooopener\models\UsersAuthApps ;

$container['usersAuthApps'] = function( $container )
{
    return new UsersAuthApps( $container , "users_auth_apps" ) ;
};

