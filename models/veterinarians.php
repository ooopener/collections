<?php

use com\ooopener\models\Edges;

use com\ooopener\models\Veterinarians;

$container['veterinarians'] = function( $container )
{
    return new Veterinarians
    (
        $container,
        'veterinarians',
        [
            'facetable' =>
            [
                'id' =>
                [
                    'id'   => 'field'
                ],
                'ids' =>
                [
                    '_key'   => 'listField'
                ]
            ],
            'searchable' =>
            [
                'name'
            ],
            'sortable' =>
            [
                'id'        => '_key',
                'active'    => 'active',
                'name'      => 'authority.name',
                'created'   => 'created',
                'modified'  => 'modified',
                'after'     => 'authority.name'
            ],
            'edges' =>
            [
                [
                    'name'           => 'medicalSpecialties' ,
                    'controller'     => 'medicalSpecialtiesController',
                    'edgeController' => 'veterinarianMedicalSpecialtiesController',
                    'skin'           => 'list'
                ],
                [
                    'name'           => 'livestocks',
                    'controller'     => 'livestocksController' ,
                    'edgeController' => 'livestockVeterinariansController' ,
                    'direction'      => 'reverse' ,
                    'skin'           => '',
                    'edges'          =>
                    [
                        [
                            'name'           => 'organization' ,
                            'controller'     => 'organizationsController' ,
                            'edgeController' => 'livestockOrganizationsController' ,
                            'skin'           => 'list',
                            'edges'          =>
                            [
                                [
                                    'name'           => 'additionalType',
                                    'controller'     => 'organizationsTypesController',
                                    'edgeController' => 'organizationOrganizationsTypesController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'image',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsImageController',
                                    'skin'           => 'list'
                                ],
                                [
                                    'name'           => 'location',
                                    'controller'     => 'placesController',
                                    'edgeController' => 'organizationPlacesController',
                                    'skin'           => 'list',
                                    'edges'          =>
                                    [
                                        [
                                            'name'           => 'additionalType',
                                            'controller'     => 'placesTypesController',
                                            'edgeController' => 'placePlacesTypesController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'conceptualObjects',
                                            'controller'     => 'conceptualObjectsController',
                                            'edgeController' => 'conceptualObjectPlacesController',
                                            'direction'      => 'reverse'
                                        ],
                                        [
                                            'name'           => 'events',
                                            'controller'     => 'eventsController',
                                            'edgeController' => 'eventPlacesController',
                                            'direction'      => 'reverse',
                                        ],
                                        [
                                            'name'           => 'image',
                                            'controller'     => 'imageObjectsController',
                                            'edgeController' => 'mediaObjectsThingsImageController',
                                            'skin'           => 'list'
                                        ],
                                        [
                                            'name'           => 'logo',
                                            'controller'     => 'imageObjectsController',
                                            'edgeController' => 'mediaObjectsThingsLogoController',
                                            'skin'           => 'list'
                                        ]
                                    ],
                                    'joins' =>
                                    [
                                        [
                                            'name'       => 'status' ,
                                            'controller' => 'placesStatusTypesController',
                                            'skin'       => 'list'
                                        ]
                                    ]
                                ],
                                [
                                    'name'           => 'logo',
                                    'controller'     => 'imageObjectsController',
                                    'edgeController' => 'mediaObjectsThingsLogoController',
                                    'skin'           => 'list'
                                ]
                            ]
                        ],
                        [
                            'name'           => 'numbers',
                            'controller'     => 'livestockNumbersController',
                            'edgeController' => 'livestocksLivestocksNumbersController',
                            'skin'           => 'list',
                            'joins'          =>
                            [
                                [
                                    'name'       => 'additionalType',
                                    'controller' => 'livestocksNumbersTypesController',
                                    'skin'       => 'list'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    );
};

$container['veterinarianMedicalSpecialties'] = function( $container )
{
    return new Edges
    (
        $container ,
        'veterinarians_medicalSpecialties',
        [
            'from' =>
            [
                'name'       => 'medicalSpecialties',
                'controller' => 'medicalSpecialtiesController'
            ],
            'to' =>
            [
                'name'       => 'veterinarians',
                'controller' => 'veterinariansController'
            ]
        ]
    );
};

$container['veterinarianOrganizations'] = function( $container )
{
    return new Edges
    (
        $container ,
        'veterinarians_organizations',
        [
            'from' =>
            [
                'name'       => 'organizations',
                'controller' => 'organizationsController'
            ],
            'to' =>
            [
                'name'       => 'veterinarians',
                'controller' => 'veterinariansController'
            ]
        ]
    );
};

$container['veterinarianPeople'] = function( $container )
{
    return new Edges
    (
        $container ,
        'veterinarians_people',
        [
            'from' =>
            [
                'name'       => 'people',
                'controller' => 'peopleController'
            ],
            'to' =>
            [
                'name'       => 'veterinarians',
                'controller' => 'veterinariansController'
            ]
        ]
    );
};
