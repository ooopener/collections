<?php

/**
 * @OA\Tag(
 *     name="account",
 *     description="Account paths"
 * )
 */

require "account/account.php" ;
require "account/activityLogs.php" ;
require "account/address.php" ;
require "account/favorites.php" ;
require "account/sessions.php" ;
