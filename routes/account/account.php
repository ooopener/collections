<?php

/**
 * @OA\Get(
 *     path="/account",
 *     tags={"account"},
 *     description="Get account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W",
 *             "account:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/userResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/account' ,
    [ $container->usersController , 'getForAccount'   ]
)
->setName('api.account') ;

/**
 * @OA\Put(
 *     path="/account",
 *     tags={"account"},
 *     description="Edit account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/putUser"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/userResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/account' , [ $container->usersController , 'putForAccount'   ]
)
->setName('api.account.put');
