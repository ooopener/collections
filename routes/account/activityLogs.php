<?php

/**
 * @OA\Get(
 *     path="/account/activityLogs",
 *     tags={"account"},
 *     description="List activityLogs account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W",
 *             "account:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/ActivityLog"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/account/activityLogs' , [ $container->usersActivityLogsController , 'allForAccount'   ]
)
->setName('api.account.activityLogs') ;
