<?php

/**
 * @OA\Get(
 *     path="/account/address",
 *     tags={"account"},
 *     description="Get address account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W",
 *             "account:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/account/address' ,
    [ $container->userPostalAddressController , 'getForAccount' ]
)
->setName('api.account.address');

/**
 * @OA\Patch(
 *     path="/account/address",
 *     tags={"account"},
 *     description="Patch address account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/patchAddress"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/account/address' ,
    [ $container->userPostalAddressController , 'patchForAccount' ]
)
->setName('api.account.address.patch');
