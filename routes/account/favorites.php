<?php

/**
 * @OA\Get(
 *     path="/account/favorites",
 *     tags={"account"},
 *     description="Get favorites account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W",
 *             "account:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/account/favorites' ,
    [ $container->usersController , 'getFavorites' ]
)
->setName('api.account.favorites');

/**
 * @OA\Delete(
 *     path="/account/favorites",
 *     tags={"account"},
 *     description="Delete favorites to account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/account/favorites' ,
    [ $container->usersController , 'deleteFavorites' ]
)
->setName('api.account.favorites.delete');

/**
 * @OA\Post(
 *     path="/account/favorites",
 *     tags={"account"},
 *     description="Add favorites to account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/account/favorites' ,
    [ $container->usersController , 'postFavorites' ]
)
->setName('api.account.favorites.post');
