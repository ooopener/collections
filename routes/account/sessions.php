<?php

/**
 * @OA\Get(
 *     path="/account/sessions/{active}",
 *     tags={"account"},
 *     description="List sessions account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W",
 *             "account:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/activeSession"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of sessions",type="array",items=@OA\Items(ref="#/components/schemas/UserSession"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/account/sessions/{active:true|false|TRUE|FALSE}' ,
    [ $container->userSessionsController , 'allForAccount' ]
)
->setName('api.account.sessions.active');

/**
 * @OA\Patch(
 *     path="/account/sessions/all",
 *     tags={"account"},
 *     description="Revoke all sessions account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/account/sessions/all' ,
    [ $container->userSessionsController , 'revokeAllForAccount' ]
)
->setName('api.account.sessions.revoke.all');

/**
 * @OA\Patch(
 *     path="/account/sessions",
 *     tags={"account"},
 *     description="Revoke sessions account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/account/sessions' ,
    [ $container->userSessionsController , 'revokeAllListForAccount' ]
)
    ->setName('api.account.sessions.revoke.allList');

/**
 * @OA\Patch(
 *     path="/account/sessions/{id}",
 *     tags={"account"},
 *     description="Revoke session account",
 *     security={
 *         {"OAuth2"={
 *             "account:A",
 *             "account:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/account/sessions/{session}' ,
    [ $container->userSessionsController , 'revokeForAccount' ]
)
->setName('api.account.sessions.revoke');
