<?php

/**
 * @OA\Tag(
 *     name="activityLogs",
 *     description="ActivityLogs paths"
 * )
 */

$application->get
(
    '/activityLogs' , [ $container->usersActivityLogsController , 'all'   ]
)
->setName('api.activityLogs.all');
