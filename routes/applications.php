<?php

/**
 * @OA\Tag(
 *     name="applications",
 *     description="Appications paths"
 * )
 */

require "applications/applications.php" ;
require "applications/audio.php" ;
require "applications/audios.php" ;
require "applications/image.php" ;
require "applications/photos.php" ;
require "applications/producer.php" ;
require "applications/publisher.php" ;
require "applications/sponsor.php" ;
require "applications/translation.php" ;
require "applications/video.php" ;
require "applications/videos.php" ;
require "applications/websites.php" ;
