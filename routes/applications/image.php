<?php

/**
 * @OA\Get(
 *     path="/applications/{id}/image",
 *     tags={"applications"},
 *     description="Get application image",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ImageObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/applications/{id:[0-9]+}/image' ,
    [ $container->applicationImageController , 'get' ]
)
->setName('api.applications.image.get');

/**
 * @OA\Delete(
 *     path="/applications/{id}/image",
 *     tags={"applications"},
 *     description="Delete resource image",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/applications/{id:[0-9]+}/image' ,
    [ $container->applicationImageController , 'delete' ]
)
->setName('api.applications.image.delete');

/**
 * @OA\Patch(
 *     path="/applications/{owner}/image/{id}",
 *     tags={"applications"},
 *     description="Patch resource image",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ImageObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'OPTIONS' , 'PATCH' ] ,
    '/applications/{owner:[0-9]+}/image/{id:[0-9]+}' ,
    [ $container->applicationImageController , 'patch' ]
)
->setName('api.applications.image.patch');
