<?php

/**
 * @OA\Get(
 *     path="/applications/{id}/sponsor",
 *     tags={"applications"},
 *     description="Get sponsor application",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/authorities")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/applications/{id:[0-9]+}/sponsor',
    [ $container->applicationSponsorController , 'all' ]
)
->setName('api.applications.sponsor.all') ;

/**
 * @OA\Get(
 *     path="/applications/{id}/sponsor/count",
 *     tags={"applications"},
 *     description="Count application sponsor",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of sponsor",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/applications/{id:[0-9]+}/sponsor/count' ,
    [ $container->applicationSponsorController , 'count' ]
)
->setName('api.applications.sponsor.count') ;

$application->options
(
    '/applications/{owner:[0-9]+}/sponsor/{id:[0-9]+}',
    null
) ;

/**
 * @OA\Delete(
 *     path="/applications/{owner}/sponsor/{id}",
 *     tags={"applications"},
 *     description="Delete sponsor application",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/deleteAuthority"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/authorities")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/applications/{owner:[0-9]+}/sponsor/{id:[0-9]+}',
    [ $container->applicationSponsorController , 'delete' ]
)
->setName('api.applications.sponsor.delete') ;

/**
 * @OA\Post(
 *     path="/applications/{owner}/sponsor/{id}",
 *     tags={"applications"},
 *     description="Create sponsor application",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postAuthority"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/authorities")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/applications/{owner:[0-9]+}/sponsor/{id:[0-9]+}',
    [ $container->applicationSponsorController , 'post' ]
)
->setName('api.applications.sponsor.post') ;

