<?php

/**
 * @OA\Get(
 *     path="/applications/{id}/videos",
 *     tags={"applications"},
 *     description="List application videos",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with videos",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of video objects",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/applications/{id:[0-9]+}/videos',
    [ $container->applicationVideosController , 'all' ]
)
->setName('api.applications.videos.all') ;

/**
 * @OA\Post(
 *     path="/applications/{id}/videos",
 *     tags={"applications"},
 *     description="Create application videos",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="Result with videos",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of video objects",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/applications/{id:[0-9]+}/videos',
    [ $container->applicationVideosController , 'post' ]
)
->setName('api.applications.videos.post') ;

/**
 * @OA\Get(
 *     path="/applications/{id}/videos/count",
 *     tags={"applications"},
 *     description="Count application videos",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with videos",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of video objects",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/applications/{id:[0-9]+}/videos/count' ,
    [ $container->applicationVideosController , 'count' ]
)
->setName('api.applications.videos.count') ;

/**
 * @OA\Delete(
 *     path="/applications/{id}/videos",
 *     tags={"applications"},
 *     description="Delete application videos",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/applications/{id:[0-9]+}/videos' ,
    [ $container->applicationVideosController , 'deleteAll' ]
)
->setName('api.applications.videos.delete.all') ;

/**
 * @OA\Get(
 *     path="/applications/{owner}/videos/{id}",
 *     tags={"applications"},
 *     description="Get application video",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W",
 *             "applications:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/VideoObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/applications/{owner:[0-9]+}/videos/{id:[0-9]+}' ,
    [ $container->applicationVideosController , 'get' ]
)
->setName('api.applications.videos.get') ;

/**
 * @OA\Delete(
 *     path="/applications/{owner}/videos/{id}",
 *     tags={"applications"},
 *     description="Delete application video",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/applications/{owner:[0-9]+}/videos/{id:[0-9]+}' ,
    [ $container->applicationVideosController , 'delete' ]
)
->setName('api.applications.videos.delete') ;

/**
 * @OA\Patch(
 *     path="/applications/{owner}/videos/{id}/position/{position}",
 *     tags={"applications"},
 *     description="Patch application video position",
 *     security={
 *         {"OAuth2"={
 *             "applications:A",
 *             "applications:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(name="position",in="path",description="",required=true,@OA\Schema(type="integer")),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/VideoObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'OPTIONS' , 'PATCH' ] ,
    '/applications/{owner:[0-9]+}/videos/{id:[0-9]+}/position/{position:[0-9]+}' ,
    [ $container->applicationVideosController , 'patchPosition' ]
)
->setName('api.applications.videos.position') ;
