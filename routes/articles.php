<?php

/**
 * @OA\Tag(
 *     name="articles",
 *     description="Articles paths"
 * )
 */

require "articles/active.php" ;
require "articles/additionalType.php" ;
require "articles/articles.php" ;
require "articles/audio.php" ;
require "articles/audios.php" ;
require "articles/hasPart.php" ;
require "articles/image.php" ;
require "articles/isPartOf.php" ;
require "articles/isRelatedTo.php" ;
require "articles/isSimilarTo.php" ;
require "articles/photos.php" ;
require "articles/translation.php" ;
require "articles/video.php" ;
require "articles/videos.php" ;
require "articles/websites.php" ;
