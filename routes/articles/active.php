<?php

/**
 * @OA\Get(
 *     path="/articles/{id}/active",
 *     tags={"articles"},
 *     description="Get active article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/successActive")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/articles/{id:[0-9]+}/active' ,
    [ $container->articleActiveController , 'get'  ]
)
->setName('api.articles.active.get') ;

/**
 * @OA\Patch(
 *     path="/articles/{id}/active/{bool}",
 *     tags={"articles"},
 *     description="Patch active article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(
 *         name="bool",
 *         in="path",
 *         description="",
 *         required=true,
 *         @OA\Schema(type="boolean")
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/successActive")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/articles/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' ,
    [ $container->articleActiveController , 'patch' ]
)
->setName('api.articles.active.patch') ;

