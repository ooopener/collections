<?php

/**
 * @OA\Get(
 *     path="/articles/{id}/audio",
 *     tags={"articles"},
 *     description="Get article audio",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/articles/{id:[0-9]+}/audio' ,
    [ $container->articleAudioController , 'get' ]
)
->setName('api.articles.audio.get');

/**
 * @OA\Delete(
 *     path="/articles/{id}/audio",
 *     tags={"articles"},
 *     description="Delete resource audio",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/articles/{id:[0-9]+}/audio' ,
    [ $container->articleAudioController , 'delete' ]
)
->setName('api.articles.audio.delete');

/**
 * @OA\Patch(
 *     path="/articles/{owner}/audio/{id}",
 *     tags={"articles"},
 *     description="Patch resource audio",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'OPTIONS' , 'PATCH' ] ,
    '/articles/{owner:[0-9]+}/audio/{id:[0-9]+}' ,
    [ $container->articleAudioController , 'patch' ]
)
->setName('api.articles.audio.patch');
