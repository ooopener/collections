<?php

/**
 * @OA\Get(
 *     path="/articles/{id}/hasPart",
 *     tags={"articles"},
 *     description="Get hasPart article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/articles/{id:[0-9]+}/hasPart',
    [ $container->articleHasPartController , 'all' ]
)
->setName('api.articles.hasPart.all') ;

/**
 * @OA\Get(
 *     path="/articles/{id}/hasPart/count",
 *     tags={"articles"},
 *     description="Count article hasPart",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of hasPart",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/articles/{id:[0-9]+}/hasPart/count' ,
    [ $container->articleHasPartController , 'count' ]
)
->setName('api.articles.hasPart.count') ;

$application->options
(
    '/articles/{owner:[0-9]+}/hasPart/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/articles/{owner}/hasPart/{id}",
 *     tags={"articles"},
 *     description="Delete hasPart article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/articles/{owner:[0-9]+}/hasPart/{id:[0-9]+}',
    [ $container->articleHasPartController , 'delete' ]
)
->setName('api.articles.hasPart.delete') ;

/**
 * @OA\Post(
 *     path="/articles/{owner}/hasPart/{id}",
 *     tags={"articles"},
 *     description="Create hasPart article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/articles/{owner:[0-9]+}/hasPart/{id:[0-9]+}',
    [ $container->articleHasPartController , 'post' ]
)
->setName('api.articles.hasPart.post') ;
