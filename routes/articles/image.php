<?php

/**
 * @OA\Get(
 *     path="/articles/{id}/image",
 *     tags={"articles"},
 *     description="Get article image",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ImageObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/articles/{id:[0-9]+}/image' ,
    [ $container->articleImageController , 'get' ]
)
->setName('api.articles.image.get');

/**
 * @OA\Delete(
 *     path="/articles/{id}/image",
 *     tags={"articles"},
 *     description="Delete resource image",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/articles/{id:[0-9]+}/image' ,
    [ $container->articleImageController , 'delete' ]
)
->setName('api.articles.image.delete');

/**
 * @OA\Patch(
 *     path="/articles/{owner}/image/{id}",
 *     tags={"articles"},
 *     description="Patch resource image",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ImageObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'OPTIONS' , 'PATCH' ] ,
    '/articles/{owner:[0-9]+}/image/{id:[0-9]+}' ,
    [ $container->articleImageController , 'patch' ]
)
->setName('api.articles.image.patch');
