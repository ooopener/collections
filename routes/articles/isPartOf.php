<?php

/**
 * @OA\Get(
 *     path="/articles/{id}/isPartOf",
 *     tags={"articles"},
 *     description="Get isPartOf article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/articles/{id:[0-9]+}/isPartOf',
    [ $container->articleHasPartController , 'all' ]
)
->setName('api.articles.isPartOf.all') ;

/**
 * @OA\Get(
 *     path="/articles/{id}/isPartOf/count",
 *     tags={"articles"},
 *     description="Count article isPartOf",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of isPartOf",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/articles/{id:[0-9]+}/isPartOf/count' ,
    [ $container->articleHasPartController , 'count' ]
)
->setName('api.articles.isPartOf.count') ;

$application->options
(
    '/articles/{owner:[0-9]+}/isPartOf/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/articles/{owner}/isPartOf/{id}",
 *     tags={"articles"},
 *     description="Delete isPartOf article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/articles/{owner:[0-9]+}/isPartOf/{id:[0-9]+}',
    [ $container->articleHasPartController , 'delete' ]
)
->setName('api.articles.isPartOf.delete') ;

/**
 * @OA\Post(
 *     path="/articles/{owner}/isPartOf/{id}",
 *     tags={"articles"},
 *     description="Create isPartOf article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/articles/{owner:[0-9]+}/isPartOf/{id:[0-9]+}',
    [ $container->articleHasPartController , 'post' ]
)
->setName('api.articles.isPartOf.post') ;
