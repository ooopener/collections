<?php

/**
 * @OA\Get(
 *     path="/articles/{id}/isRelatedTo",
 *     tags={"articles"},
 *     description="Get isRelatedTo article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/articles/{id:[0-9]+}/isRelatedTo',
    [ $container->articleIsRelatedToController , 'all' ]
)
->setName('api.articles.isRelatedTo.all') ;

/**
 * @OA\Get(
 *     path="/articles/{id}/isRelatedTo/count",
 *     tags={"articles"},
 *     description="Count article isRelatedTo",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of isRelatedTo",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/articles/{id:[0-9]+}/isRelatedTo/count' ,
    [ $container->articleIsRelatedToController , 'count' ]
)
->setName('api.articles.isRelatedTo.count') ;

$application->options
(
    '/articles/{owner:[0-9]+}/isRelatedTo/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/articles/{owner}/isRelatedTo/{id}",
 *     tags={"articles"},
 *     description="Delete isRelatedTo article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/articles/{owner:[0-9]+}/isRelatedTo/{id:[0-9]+}',
    [ $container->articleIsRelatedToController , 'delete' ]
)
->setName('api.articles.isRelatedTo.delete') ;

/**
 * @OA\Post(
 *     path="/articles/{owner}/isRelatedTo/{id}",
 *     tags={"articles"},
 *     description="Create isRelatedTo article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/articles/{owner:[0-9]+}/isRelatedTo/{id:[0-9]+}',
    [ $container->articleIsRelatedToController , 'post' ]
)
->setName('api.articles.isRelatedTo.post') ;
