<?php

/**
 * @OA\Get(
 *     path="/articles/{id}/isSimilarTo",
 *     tags={"articles"},
 *     description="Get isSimilarTo article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/articles/{id:[0-9]+}/isSimilarTo',
    [ $container->articleIsSimilarToController , 'all' ]
)
->setName('api.articles.isSimilarTo.all') ;

/**
 * @OA\Get(
 *     path="/articles/{id}/isSimilarTo/count",
 *     tags={"articles"},
 *     description="Count article isSimilarTo",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W",
 *             "articles:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of isSimilarTo",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/articles/{id:[0-9]+}/isSimilarTo/count' ,
    [ $container->articleIsSimilarToController , 'count' ]
)
->setName('api.articles.isSimilarTo.count') ;

$application->options
(
    '/articles/{owner:[0-9]+}/isSimilarTo/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/articles/{owner}/isSimilarTo/{id}",
 *     tags={"articles"},
 *     description="Delete isSimilarTo article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/articles/{owner:[0-9]+}/isSimilarTo/{id:[0-9]+}',
    [ $container->articleIsSimilarToController , 'delete' ]
)
->setName('api.articles.isSimilarTo.delete') ;

/**
 * @OA\Post(
 *     path="/articles/{owner}/isSimilarTo/{id}",
 *     tags={"articles"},
 *     description="Create isSimilarTo article",
 *     security={
 *         {"OAuth2"={
 *             "articles:A",
 *             "articles:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/articleResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/articles/{owner:[0-9]+}/isSimilarTo/{id:[0-9]+}',
    [ $container->articleIsSimilarToController , 'post' ]
)
->setName('api.articles.isSimilarTo.post') ;
