<?php

/**
 * @OA\Tag(
 *     name="conceptualObjects",
 *     description="ConceptualObjects paths"
 * )
 */

require "conceptualObjects/active.php" ;
require "conceptualObjects/audio.php" ;
require "conceptualObjects/audios.php" ;
require "conceptualObjects/conceptualObjects.php" ;
require "conceptualObjects/image.php" ;
require "conceptualObjects/keywords.php" ;
require "conceptualObjects/marks.php" ;
require "conceptualObjects/materials.php" ;
require "conceptualObjects/measurements.php" ;
require "conceptualObjects/numbers.php" ;
require "conceptualObjects/photos.php" ;
require "conceptualObjects/productions.php" ;
require "conceptualObjects/translation.php" ;
require "conceptualObjects/video.php" ;
require "conceptualObjects/videos.php" ;
require "conceptualObjects/websites.php" ;
require "conceptualObjects/withStatus.php" ;
