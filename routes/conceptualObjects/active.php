<?php


/**
 * @OA\Get(
 *     path="/conceptualObjects/{id}/active",
 *     tags={"conceptualObjects"},
 *     description="Get active conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/successActive")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/conceptualObjects/{id:[0-9]+}/active' ,
    [ $container->conceptualObjectActiveController , 'get'  ]
)
->setName('api.conceptualObjects.active.get') ;

/**
 * @OA\Patch(
 *     path="/conceptualObjects/{id}/active/{bool}",
 *     tags={"conceptualObjects"},
 *     description="Patch active conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(
 *         name="bool",
 *         in="path",
 *         description="",
 *         required=true,
 *         @OA\Schema(type="boolean")
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/successActive")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/conceptualObjects/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' ,
    [ $container->conceptualObjectActiveController , 'patch' ]
)
->setName('api.conceptualObjects.active.patch') ;

