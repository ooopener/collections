<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/conceptualObjects/{id:[0-9]+}/keywords',
    [ $container->conceptualObjectKeywordsController , 'all' ]
)
->setName('api.conceptualObjects.keywords.all') ;

$application->post
(
    '/conceptualObjects/{id:[0-9]+}/keywords',
    [ $container->conceptualObjectKeywordsController , 'post' ]
)
->setName('api.conceptualObjects.keywords.post') ;

$application->get
(
    '/conceptualObjects/{id:[0-9]+}/keywords/count' ,
    [ $container->conceptualObjectKeywordsController , 'count' ]
)
->setName('api.conceptualObjects.keywords.count') ;

$application->options
(
    '/conceptualObjects/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    null
);

$application->delete
(
    '/conceptualObjects/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->conceptualObjectKeywordsController , 'delete' ]
)
->setName('api.conceptualObjects.keywords.delete') ;

$application->put
(
    '/conceptualObjects/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->conceptualObjectKeywordsController , 'put' ]
)
->setName('api.conceptualObjects.keywords.put') ;
