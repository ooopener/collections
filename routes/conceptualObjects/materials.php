<?php

/**
 * @OA\Get(
 *     path="/conceptualObjects/{id}/materials",
 *     tags={"conceptualObjects"},
 *     description="Get materials conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ConceptualObjectMaterial")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/conceptualObjects/{id:[0-9]+}/materials',
    [ $container->conceptualObjectMaterialsController , 'all' ]
)
->setName('api.conceptualObjects.materials.all') ;

/**
 * @OA\Post(
 *     path="/conceptualObjects/{id}/materials",
 *     tags={"conceptualObjects"},
 *     description="Create materials conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postConceptualObjectMaterial"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ConceptualObjectMaterial")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/conceptualObjects/{id:[0-9]+}/materials',
    [ $container->conceptualObjectMaterialsController , 'post' ]
)
->setName('api.conceptualObjects.materials.post') ;

/**
 * @OA\Get(
 *     path="/conceptualObjects/{id}/materials/count",
 *     tags={"conceptualObjects"},
 *     description="Count conceptualObject materials",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of materials",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/conceptualObjects/{id:[0-9]+}/materials/count' ,
    [ $container->conceptualObjectMaterialsController , 'count' ]
)
->setName('api.conceptualObjects.materials.count') ;

$application->options
(
    '/conceptualObjects/{owner:[0-9]+}/materials/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/conceptualObjects/{owner}/materials/{id}",
 *     tags={"conceptualObjects"},
 *     description="Delete materials conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ConceptualObjectMaterial")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/conceptualObjects/{owner:[0-9]+}/materials/{id:[0-9]+}',
    [ $container->conceptualObjectMaterialsController , 'delete' ]
)
->setName('api.conceptualObjects.materials.delete') ;

/**
 * @OA\Put(
 *     path="/conceptualObjects/{owner}/materials/{id}",
 *     tags={"conceptualObjects"},
 *     description="Edit materials conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putConceptualObjectMaterial"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ConceptualObjectMaterial")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/conceptualObjects/{owner:[0-9]+}/materials/{id:[0-9]+}',
    [ $container->conceptualObjectMaterialsController , 'put' ]
)
->setName('api.conceptualObjects.materials.put') ;
