<?php

/**
 * @OA\Get(
 *     path="/conceptualObjects/{id}/numbers",
 *     tags={"conceptualObjects"},
 *     description="Get numbers conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ConceptualObjectNumber")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/conceptualObjects/{id:[0-9]+}/numbers',
    [ $container->conceptualObjectNumbersController , 'all' ]
)
->setName('api.conceptualObjects.numbers.all') ;

/**
 * @OA\Post(
 *     path="/conceptualObjects/{id}/numbers",
 *     tags={"conceptualObjects"},
 *     description="Create numbers conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postConceptualObjectNumber"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ConceptualObjectNumber")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/conceptualObjects/{id:[0-9]+}/numbers',
    [ $container->conceptualObjectNumbersController , 'post' ]
)
->setName('api.conceptualObjects.numbers.post') ;

/**
 * @OA\Get(
 *     path="/conceptualObjects/{id}/numbers/count",
 *     tags={"conceptualObjects"},
 *     description="Count conceptualObject numbers",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of numbers",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/conceptualObjects/{id:[0-9]+}/numbers/count' ,
    [ $container->conceptualObjectNumbersController , 'count' ]
)
->setName('api.conceptualObjects.numbers.count') ;

$application->options
(
    '/conceptualObjects/{owner:[0-9]+}/numbers/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/conceptualObjects/{owner}/numbers/{id}",
 *     tags={"conceptualObjects"},
 *     description="Delete numbers conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ConceptualObjectNumber")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/conceptualObjects/{owner:[0-9]+}/numbers/{id:[0-9]+}',
    [ $container->conceptualObjectNumbersController , 'delete' ]
)
->setName('api.conceptualObjects.numbers.delete') ;

/**
 * @OA\Put(
 *     path="/conceptualObjects/{owner}/numbers/{id}",
 *     tags={"conceptualObjects"},
 *     description="Edit numbers conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putConceptualObjectNumber"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/ConceptualObjectNumber")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/conceptualObjects/{owner:[0-9]+}/numbers/{id:[0-9]+}',
    [ $container->conceptualObjectNumbersController , 'put' ]
)
->setName('api.conceptualObjects.numbers.put') ;
