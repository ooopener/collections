<?php

/**
 * @OA\Get(
 *     path="/conceptualObjects/{id}/websites",
 *     tags={"conceptualObjects"},
 *     description="Get websites conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/Website")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/conceptualObjects/{id:[0-9]+}/websites',
    [ $container->conceptualObjectWebsitesController , 'all' ]
)
->setName('api.conceptualObjects.websites.all') ;

/**
 * @OA\Post(
 *     path="/conceptualObjects/{id}/websites",
 *     tags={"conceptualObjects"},
 *     description="Create websites conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postWebsite"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/Website")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/conceptualObjects/{id:[0-9]+}/websites',
    [ $container->conceptualObjectWebsitesController , 'post' ]
)
->setName('api.conceptualObjects.websites.post') ;

/**
 * @OA\Get(
 *     path="/conceptualObjects/{id}/websites/count",
 *     tags={"conceptualObjects"},
 *     description="Count conceptualObject websites",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W",
 *             "conceptualObjects:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of websites",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/conceptualObjects/{id:[0-9]+}/websites/count' ,
    [ $container->conceptualObjectWebsitesController , 'count' ]
)
->setName('api.conceptualObjects.websites.count') ;

$application->options
(
    '/conceptualObjects/{owner:[0-9]+}/websites/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/conceptualObjects/{owner}/websites/{id}",
 *     tags={"conceptualObjects"},
 *     description="Delete websites conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/Website")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/conceptualObjects/{owner:[0-9]+}/websites/{id:[0-9]+}',
    [ $container->conceptualObjectWebsitesController , 'delete' ]
)
->setName('api.conceptualObjects.websites.delete') ;

/**
 * @OA\Put(
 *     path="/conceptualObjects/{owner}/websites/{id}",
 *     tags={"conceptualObjects"},
 *     description="Edit websites conceptualObject",
 *     security={
 *         {"OAuth2"={
 *             "conceptualObjects:A",
 *             "conceptualObjects:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putWebsite"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/Website")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/conceptualObjects/{owner:[0-9]+}/websites/{id:[0-9]+}',
    [ $container->conceptualObjectWebsitesController , 'put' ]
)
->setName('api.conceptualObjects.websites.put') ;

