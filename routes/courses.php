<?php

/**
 * @OA\Tag(
 *     name="courses",
 *     description="Courses paths"
 * )
 */

require "courses/active.php" ;
require "courses/additionalType.php" ;
require "courses/audio.php" ;
require "courses/audios.php" ;
require "courses/courses.php" ;
require "courses/discover.php" ;
require "courses/image.php" ;
require "courses/openingHours.php" ;
require "courses/photos.php" ;
require "courses/translation.php" ;
require "courses/transportations.php" ;
require "courses/video.php" ;
require "courses/videos.php" ;
require "courses/withStatus.php" ;

require "courses/steps/active.php" ;
require "courses/steps/audio.php" ;
require "courses/steps/audios.php" ;
require "courses/steps/image.php" ;
require "courses/steps/photos.php" ;
require "courses/steps/steps.php" ;
require "courses/steps/translation.php" ;
require "courses/steps/video.php" ;
require "courses/steps/videos.php" ;
require "courses/steps/withStatus.php" ;
