<?php

/**
 * @OA\Get(
 *     path="/courses/{id}/discover",
 *     tags={"courses"},
 *     description="List course discover",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/discoverResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/courses/{id:[0-9]+}/discover',
    [ $container->courseDiscoverController , 'get' ]
)
->setName('api.courses.discover.all') ;

/**
 * @OA\Delete(
 *     path="/courses/{id}/discover",
 *     tags={"courses"},
 *     description="Delete course discover",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/deleteCourseDiscover"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/courses/{id:[0-9]+}/discover',
    [ $container->courseDiscoverController , 'delete' ]
)
->setName('api.courses.discover.delete') ;

/**
 * @OA\Patch(
 *     path="/courses/{id}/discover",
 *     tags={"courses"},
 *     description="Patch course discover",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchCourseDiscover"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/discoverResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/courses/{id:[0-9]+}/discover',
    [ $container->courseDiscoverController , 'patch' ]
)
->setName('api.courses.discover.patch') ;

/**
 * @OA\Post(
 *     path="/courses/{id}/discover",
 *     tags={"courses"},
 *     description="Create course discover",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postCourseDiscover"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/discoverResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/courses/{id:[0-9]+}/discover',
    [ $container->courseDiscoverController , 'post' ]
)
->setName('api.courses.discover.post') ;




