<?php

/**
 * @OA\Get(
 *     path="/courses/{id}/openingHoursSpecification",
 *     tags={"courses"},
 *     description="List course openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with openingHoursSpecification",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/OpeningHoursSpecification"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/courses/{id:[0-9]+}/openingHoursSpecification',
    [ $container->courseOpeningHoursController , 'all' ]
)
->setName('api.courses.openingHoursSpecification.all') ;

/**
 * @OA\Post(
 *     path="/courses/{id}/openingHoursSpecification",
 *     tags={"courses"},
 *     description="Create course openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postOpeningHoursSpecification"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with openingHoursSpecification",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/OpeningHoursSpecification"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/courses/{id:[0-9]+}/openingHoursSpecification',
    [ $container->courseOpeningHoursController , 'post' ]
)
->setName('api.courses.openingHoursSpecification.post') ;

/**
 * @OA\Get(
 *     path="/courses/{id}/openingHoursSpecification/count",
 *     tags={"courses"},
 *     description="Count course openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with openingHoursSpecification",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of openingHoursSpecification",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/courses/{id:[0-9]+}/openingHoursSpecification/count' ,
    [ $container->courseOpeningHoursController , 'count' ]
)
->setName('api.courses.openingHoursSpecification.count') ;

$application->options
(
    '/courses/{owner:[0-9]+}/openingHoursSpecification/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/courses/{owner}/openingHoursSpecification/{id}",
 *     tags={"courses"},
 *     description="Delete course openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/courses/{owner:[0-9]+}/openingHoursSpecification/{id:[0-9]+}',
    [ $container->courseOpeningHoursController , 'delete' ]
)
->setName('api.courses.openingHoursSpecification.delete') ;

/**
 * @OA\Put(
 *     path="/courses/{owner}/openingHoursSpecification/{id}",
 *     tags={"courses"},
 *     description="Edit course openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putOpeningHoursSpecification"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/courses/{owner:[0-9]+}/openingHoursSpecification/{id:[0-9]+}',
    [ $container->courseOpeningHoursController , 'put' ]
)
->setName('api.courses.openingHoursSpecification.put') ;
