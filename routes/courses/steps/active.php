<?php

/**
 * @OA\Get(
 *     path="/courses/steps/{id}/active",
 *     tags={"courses"},
 *     description="Get active course step",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/successActive")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/courses/steps/{id:[0-9]+}/active' ,
    [ $container->stepActiveController , 'get'  ]
)
->setName('api.courses.steps.active.get') ;

/**
 * @OA\Patch(
 *     path="/courses/steps/{id}/active/{bool}",
 *     tags={"courses"},
 *     description="Patch active course step",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(
 *         name="bool",
 *         in="path",
 *         description="",
 *         required=true,
 *         @OA\Schema(type="boolean")
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/successActive")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/courses/steps/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' ,
    [ $container->stepActiveController , 'patch' ]
)
->setName('api.courses.steps.active.patch') ;

