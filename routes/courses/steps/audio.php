<?php

/**
 * @OA\Get(
 *     path="/courses/steps/{id}/audio",
 *     tags={"courses"},
 *     description="Get course step audio",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/courses/steps/{id:[0-9]+}/audio' ,
    [ $container->stepAudioController , 'get' ]
)
->setName('api.courses.steps.audio.get');

/**
 * @OA\Delete(
 *     path="/courses/steps/{id}/audio",
 *     tags={"courses"},
 *     description="Delete resource audio",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/courses/steps/{id:[0-9]+}/audio' ,
    [ $container->stepAudioController , 'delete' ]
)
->setName('api.courses.steps.audio.delete');

/**
 * @OA\Patch(
 *     path="/courses/steps/{owner}/audio/{id}",
 *     tags={"courses"},
 *     description="Patch resource audio",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'OPTIONS' , 'PATCH' ] ,
    '/courses/steps/{owner:[0-9]+}/audio/{id:[0-9]+}' ,
    [ $container->stepAudioController , 'patch' ]
)
->setName('api.course.steps.audio.patch');
