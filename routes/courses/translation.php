<?php

/**
 * @OA\Get(
 *     path="/courses/{id}/alternativeHeadline",
 *     tags={"courses"},
 *     description="Get course alternativeHeadline",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/courses/{id:[0-9]+}/alternativeHeadline',
    [ $container->courseTranslationController , 'alternativeHeadline' ]
)
->setName('api.courses.alternativeHeadline') ;

/**
 * @OA\Patch(
 *     path="/courses/{id}/alternativeHeadline",
 *     tags={"courses"},
 *     description="Patch course alternativeHeadline",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/courses/{id:[0-9]+}/alternativeHeadline',
    [ $container->courseTranslationController , 'patchAlternativeHeadline' ]
)
->setName('api.courses.alternativeHeadline.patch') ;

/**
 * @OA\Get(
 *     path="/courses/{id}/notes",
 *     tags={"courses"},
 *     description="Get course notes",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/courses/{id:[0-9]+}/notes',
    [ $container->courseTranslationController , 'notes' ]
)
->setName('api.courses.notes') ;

/**
 * @OA\Patch(
 *     path="/courses/{id}/notes",
 *     tags={"courses"},
 *     description="Patch course notes",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/courses/{id:[0-9]+}/notes',
    [ $container->courseTranslationController , 'patchNotes' ]
)
->setName('api.courses.notes.patch') ;

/**
 * @OA\Get(
 *     path="/courses/{id}/headline",
 *     tags={"courses"},
 *     description="Get course headline",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/courses/{id:[0-9]+}/headline',
    [ $container->courseTranslationController , 'headline' ]
)
->setName('api.courses.headline') ;

/**
 * @OA\Patch(
 *     path="/courses/{id}/headline",
 *     tags={"courses"},
 *     description="Patch course headline",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/courses/{id:[0-9]+}/headline',
    [ $container->courseTranslationController , 'patchHeadline' ]
)
->setName('api.courses.headline.patch') ;

/**
 * @OA\Get(
 *     path="/courses/{id}/description",
 *     tags={"courses"},
 *     description="Get course description",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/courses/{id:[0-9]+}/description',
    [ $container->courseTranslationController , 'description' ]
)
->setName('api.courses.description') ;

/**
 * @OA\Patch(
 *     path="/courses/{id}/description",
 *     tags={"courses"},
 *     description="Patch course description",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/courses/{id:[0-9]+}/description',
    [ $container->courseTranslationController , 'patchDescription' ]
)
->setName('api.courses.description.patch') ;

/**
 * @OA\Get(
 *     path="/courses/{id}/text",
 *     tags={"courses"},
 *     description="Get course text",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/courses/{id:[0-9]+}/text',
    [ $container->courseTranslationController , 'text' ]
)
->setName('api.courses.text') ;

/**
 * @OA\Patch(
 *     path="/courses/{id}/text",
 *     tags={"courses"},
 *     description="Patch course text",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/courses/{id:[0-9]+}/text',
    [ $container->courseTranslationController , 'patchText' ]
)
->setName('api.courses.text.patch') ;
