<?php

/**
 * @OA\Get(
 *     path="/courses/{id}/transportations",
 *     tags={"courses"},
 *     description="Get transportations course",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/CourseTransportation")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/courses/{id:[0-9]+}/transportations',
    [ $container->courseTransportationsController , 'all' ]
)
->setName('api.courses.transportations.all') ;

/**
 * @OA\Post(
 *     path="/courses/{id}/transportations",
 *     tags={"courses"},
 *     description="Create transportations course",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postCourseTransportation"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/CourseTransportation")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/courses/{id:[0-9]+}/transportations',
    [ $container->courseTransportationsController , 'post' ]
)
->setName('api.courses.transportations.post') ;

/**
 * @OA\Get(
 *     path="/courses/{id}/transportations/count",
 *     tags={"courses"},
 *     description="Count course transportations",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W",
 *             "courses:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of transportations",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/courses/{id:[0-9]+}/transportations/count' ,
    [ $container->courseTransportationsController , 'count' ]
)
->setName('api.courses.transportations.count') ;

$application->options
(
    '/courses/{owner:[0-9]+}/transportations/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/courses/{owner}/transportations/{id}",
 *     tags={"courses"},
 *     description="Delete transportations course",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/CourseTransportation")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/courses/{owner:[0-9]+}/transportations/{id:[0-9]+}',
    [ $container->courseTransportationsController , 'delete' ]
)
->setName('api.courses.transportations.delete') ;

/**
 * @OA\Put(
 *     path="/courses/{owner}/transportations/{id}",
 *     tags={"courses"},
 *     description="Edit transportations course",
 *     security={
 *         {"OAuth2"={
 *             "courses:A",
 *             "courses:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putCourseTransportation"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/CourseTransportation")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/courses/{owner:[0-9]+}/transportations/{id:[0-9]+}',
    [ $container->courseTransportationsController , 'put' ]
)
->setName('api.courses.transportations.put') ;
