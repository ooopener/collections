<?php

require "diseases/active.php" ;
require "diseases/analysisMethod.php" ;
require "diseases/analysisSampling.php" ;
require "diseases/diseases.php" ;
require "diseases/translation.php" ;
require "diseases/transmissionMethod.php" ;
require "diseases/withStatus.php" ;
