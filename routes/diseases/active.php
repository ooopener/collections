<?php

$application->get
(
    '/diseases/{id:[0-9]+}/active' ,
    [ $container->diseaseActiveController , 'get'  ]
)
->setName('api.diseases.active.get') ;

$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' ,
    [ $container->diseaseActiveController , 'patch' ]
)
->setName('api.diseases.active.patch') ;

