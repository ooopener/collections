<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}/analysisMethod',
    [ $container->diseaseAnalysisMethodController , 'all' ]
)
->setName('api.diseases.analysisMethod.all') ;

$application->post
(
    '/diseases/{id:[0-9]+}/analysisMethod',
    [ $container->diseaseAnalysisMethodController , 'post' ]
)
->setName('api.diseases.analysisMethod.post') ;

$application->get
(
    '/diseases/{id:[0-9]+}/analysisMethod/count' ,
    [ $container->diseaseAnalysisMethodController , 'count' ]
)
->setName('api.diseases.analysisMethod.count') ;

$application->options
(
    '/diseases/{owner:[0-9]+}/analysisMethod/{id:[0-9]+}',
    null
);

$application->delete
(
    '/diseases/{owner:[0-9]+}/analysisMethod/{id:[0-9]+}',
    [ $container->diseaseAnalysisMethodController , 'delete' ]
)
->setName('api.diseases.analysisMethod.delete') ;

$application->put
(
    '/diseases/{owner:[0-9]+}/analysisMethod/{id:[0-9]+}',
    [ $container->diseaseAnalysisMethodController , 'put' ]
)
->setName('api.diseases.analysisMethod.put') ;