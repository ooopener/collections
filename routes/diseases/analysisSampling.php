<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}/analysisSampling',
    [ $container->diseaseAnalysisSamplingController , 'all' ]
)
->setName('api.diseases.analysisSampling.all') ;

$application->post
(
    '/diseases/{id:[0-9]+}/analysisSampling',
    [ $container->diseaseAnalysisSamplingController , 'post' ]
)
->setName('api.diseases.analysisSampling.post') ;

$application->get
(
    '/diseases/{id:[0-9]+}/analysisSampling/count' ,
    [ $container->diseaseAnalysisSamplingController , 'count' ]
)
->setName('api.diseases.analysisSampling.count') ;

$application->options
(
    '/diseases/{owner:[0-9]+}/analysisSampling/{id:[0-9]+}',
    null
);

$application->delete
(
    '/diseases/{owner:[0-9]+}/analysisSampling/{id:[0-9]+}',
    [ $container->diseaseAnalysisSamplingController , 'delete' ]
)
->setName('api.diseases.analysisSampling.delete') ;

$application->put
(
    '/diseases/{owner:[0-9]+}/analysisSampling/{id:[0-9]+}',
    [ $container->diseaseAnalysisSamplingController , 'put' ]
)
->setName('api.diseases.analysisSampling.put') ;