<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/diseases' , [ $container->diseasesController , 'all'   ]
)
->setName('api.diseases.all');

$application->delete
(
    '/diseases' , [ $container->diseasesController , 'deleteAll'   ]
)
->setName('api.diseases.delete.all') ;

$application->post
(
    '/diseases' , [ $container->diseasesController , 'post'   ]
)
->setName('api.diseases.post');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}' , [ $container->diseasesController , 'get'   ]
)
->setName('api.diseases.get');

$application->delete
(
    '/diseases/{id:[0-9]+}' , [ $container->diseasesController , 'delete'   ]
)
->setName('api.diseases.delete');

$application->patch
(
    '/diseases/{id:[0-9]+}' , [ $container->diseasesController , 'patch'   ]
)
->setName('api.game.patch');