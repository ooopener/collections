<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}/alternateName',
    [ $container->diseaseTranslationController , 'alternateName' ]
)
->setName('api.diseases.alternateName') ;

$application->patch
(
    '/diseases/{id:[0-9]+}/alternateName',
    [ $container->diseaseTranslationController , 'patchAlternateName' ]
)
->setName('api.diseases.alternateName.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}/description',
    [ $container->diseaseTranslationController , 'description' ]
)
->setName('api.diseases.description') ;

$application->patch
(
    '/diseases/{id:[0-9]+}/description',
    [ $container->diseaseTranslationController , 'patchDescription' ]
)
->setName('api.diseases.description.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}/notes',
    [ $container->diseaseTranslationController , 'notes' ]
)
->setName('api.diseases.notes') ;

$application->patch
(
    '/diseases/{id:[0-9]+}/notes',
    [ $container->diseaseTranslationController , 'patchNotes' ]
)
->setName('api.diseases.notes.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}/text',
    [ $container->diseaseTranslationController , 'text' ]
)
->setName('api.diseases.text') ;

$application->patch
(
    '/diseases/{id:[0-9]+}/text',
    [ $container->diseaseTranslationController , 'patchText' ]
)
->setName('api.diseases.text.patch') ;