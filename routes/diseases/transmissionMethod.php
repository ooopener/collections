<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}/transmissionMethod',
    [ $container->diseaseTransmissionsMethodsController , 'all' ]
)
->setName('api.diseases.transmissionMethod.all') ;

$application->get
(
    '/diseases/{id:[0-9]+}/transmissionMethod/count' ,
    [ $container->diseaseTransmissionsMethodsController , 'count' ]
)
->setName('api.diseases.transmissionMethod.count') ;

$application->put
(
    '/diseases/{id:[0-9]+}/transmissionMethod',
    [ $container->diseaseTransmissionsMethodsController , 'put' ]
)
->setName('api.diseases.transmissionMethod.put') ;