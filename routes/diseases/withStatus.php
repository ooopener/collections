<?php

$application->get
(
    '/diseases/{id:[0-9]+}/withStatus' ,
    [ $container->diseaseWithStatusController , 'get'  ]
)
->setName('api.diseases.withStatus.get') ;

$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/diseases/{id:[0-9]+}/withStatus/{status}' ,
    [ $container->diseaseWithStatusController , 'patch' ]
)
->setName('api.diseases.withStatus.patch') ;

