<?php

/**
 * @OA\Tag(
 *     name="events",
 *     description="Events paths"
 * )
 */

require "events/active.php" ;
require "events/additionalType.php" ;
require "events/audio.php" ;
require "events/audios.php" ;
require "events/events.php" ;
require "events/image.php" ;
require "events/keywords.php" ;
require "events/offers.php" ;
require "events/organizer.php" ;
require "events/photos.php" ;
require "events/status.php" ;
require "events/subEvent.php" ;
require "events/superEvent.php" ;
require "events/translation.php" ;
require "events/video.php" ;
require "events/videos.php" ;
require "events/websites.php" ;
require "events/withStatus.php" ;
require "events/workFeatured.php" ;
