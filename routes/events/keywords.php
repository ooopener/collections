<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/events/{id:[0-9]+}/keywords',
    [ $container->eventKeywordsController , 'all' ]
)
->setName('api.events.keywords.all') ;

$application->post
(
    '/events/{id:[0-9]+}/keywords',
    [ $container->eventKeywordsController , 'post' ]
)
->setName('api.events.keywords.post') ;

$application->get
(
    '/events/{id:[0-9]+}/keywords/count' ,
    [ $container->eventKeywordsController , 'count' ]
)
->setName('api.events.keywords.count') ;

$application->options
(
    '/events/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    null
);

$application->delete
(
    '/events/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->eventKeywordsController , 'delete' ]
)
->setName('api.events.keywords.delete') ;

$application->put
(
    '/events/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->eventKeywordsController , 'put' ]
)
->setName('api.events.keywords.put') ;