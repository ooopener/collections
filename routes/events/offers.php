<?php

/**
 * @OA\Get(
 *     path="/events/{id}/offers",
 *     tags={"events"},
 *     description="List event offers",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with offers",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of offers",type="array",items=@OA\Items(ref="#/components/schemas/Offer"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/events/{id:[0-9]+}/offers',
    [ $container->eventOffersController , 'all' ]
)
->setName('api.events.offers.all') ;

/**
 * @OA\Post(
 *     path="/events/{id}/offers",
 *     tags={"events"},
 *     description="Create event offer",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postOffer"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with offers",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of offers",type="array",items=@OA\Items(ref="#/components/schemas/Offer"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/events/{id:[0-9]+}/offers',
    [ $container->eventOffersController , 'post' ]
)
->setName('api.events.offers.post') ;

/**
 * @OA\Get(
 *     path="/events/{id}/offers/count",
 *     tags={"events"},
 *     description="Count event offers",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with offers",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of offers",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/events/{id:[0-9]+}/offers/count' ,
    [ $container->eventOffersController , 'count' ]
)
->setName('api.events.offers.count') ;

$application->options
(
    '/events/{owner:[0-9]+}/offers/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/events/{owner}/offers/{id}",
 *     tags={"events"},
 *     description="Delete event offer",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/events/{owner:[0-9]+}/offers/{id:[0-9]+}',
    [ $container->eventOffersController , 'delete' ]
)
->setName('api.events.offers.delete') ;

/**
 * @OA\Put(
 *     path="/events/{owner}/offers/{id}",
 *     tags={"events"},
 *     description="Edit event offer",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putOffer"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/events/{owner:[0-9]+}/offers/{id:[0-9]+}',
    [ $container->eventOffersController , 'put' ]
)
->setName('api.events.offers.put') ;
