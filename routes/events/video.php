<?php

/**
 * @OA\Get(
 *     path="/events/{id}/video",
 *     tags={"events"},
 *     description="Get event video",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/VideoObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/events/{id:[0-9]+}/video' ,
    [ $container->eventVideoController , 'get' ]
)
->setName('api.events.video.get');

/**
 * @OA\Delete(
 *     path="/events/{id}/video",
 *     tags={"events"},
 *     description="Delete resource video",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/events/{id:[0-9]+}/video' ,
    [ $container->eventVideoController , 'delete' ]
)
->setName('api.events.video.delete');

/**
 * @OA\Patch(
 *     path="/events/{owner}/video/{id}",
 *     tags={"events"},
 *     description="Patch resource video",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/VideoObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'OPTIONS' , 'PATCH' ] ,
    '/events/{owner:[0-9]+}/video/{id:[0-9]+}' ,
    [ $container->eventVideoController , 'patch' ]
)
->setName('api.events.video.patch');
