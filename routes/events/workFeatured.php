<?php

/**
 * @OA\Get(
 *     path="/events/{id}/workFeatured",
 *     tags={"events"},
 *     description="Get workFeatured event",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/conceptualObjectListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/events/{id:[0-9]+}/workFeatured',
    [ $container->eventWorksFeaturedController , 'all' ]
)
->setName('api.events.workFeatured.all') ;

/**
 * @OA\Get(
 *     path="/events/{id}/workFeatured/count",
 *     tags={"events"},
 *     description="Count event workFeatured",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W",
 *             "events:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of workFeatured",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/events/{id:[0-9]+}/workFeatured/count' ,
    [ $container->eventWorksFeaturedController , 'count' ]
)
->setName('api.events.workFeatured.count') ;

$application->options
(
    '/events/{owner:[0-9]+}/workFeatured/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/events/{owner}/workFeatured/{id}",
 *     tags={"events"},
 *     description="Delete workFeatured event",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/events/{owner:[0-9]+}/workFeatured/{id:[0-9]+}',
    [ $container->eventWorksFeaturedController , 'delete' ]
)
->setName('api.events.workFeatured.delete') ;

/**
 * @OA\Post(
 *     path="/events/{owner}/workFeatured/{id}",
 *     tags={"events"},
 *     description="Create workFeatured event",
 *     security={
 *         {"OAuth2"={
 *             "events:A",
 *             "events:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/conceptualObjectResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/events/{owner:[0-9]+}/workFeatured/{id:[0-9]+}',
    [ $container->eventWorksFeaturedController , 'post' ]
)
->setName('api.events.workFeatured.post') ;
