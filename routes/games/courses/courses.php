<?php

$application->get
(
    '/games/courses/{id:[0-9]+}' , [ $container->coursesGamesController , 'get'   ]
)
->setName('api.games.courses.get');

$application->options
(
    '/games/courses/{id:[0-9]+}' , null
);

$application->get
(
    '/games/courses/{id:[0-9]+}/quest' , [ $container->questionsGamesController , 'all'   ]
)
->setName('api.games.courses.quest.all');

$application->post
(
    '/games/courses/{id:[0-9]+}/quest' , [ $container->questionsGamesController , 'post'   ]
)
->setName('api.games.courses.quest.post');

$application->delete
(
    '/games/courses/{id:[0-9]+}' , [ $container->coursesGamesController , 'delete'   ]
)
->setName('api.games.courses.delete');

$application->patch
(
    '/games/courses/{id:[0-9]+}' , [ $container->coursesGamesController , 'patch'   ]
)
->setName('api.game.courses.patch');