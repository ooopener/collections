<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/courses/{id:[0-9]+}/item' , [ $container->courseBadgeItemController , 'get'   ]
)
->setName('api.games.courses.item.all');

$application->patch
(
    '/games/courses/{id:[0-9]+}/item' , [ $container->courseBadgeItemController , 'patch'   ]
)
->setName('api.games.courses.item.patch');
