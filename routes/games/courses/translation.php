<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/courses/{id:[0-9]+}/alternateName',
    [ $container->courseGamesTranslationController , 'alternateName' ]
)
->setName('api.games.courses.alternateName') ;

$application->patch
(
    '/games/courses/{id:[0-9]+}/alternateName',
    [ $container->courseGamesTranslationController , 'patchAlternateName' ]
)
->setName('api.games.courses.alternateName.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/courses/{id:[0-9]+}/description',
    [ $container->courseGamesTranslationController , 'description' ]
)
->setName('api.games.courses.description') ;

$application->patch
(
    '/games/courses/{id:[0-9]+}/description',
    [ $container->courseGamesTranslationController , 'patchDescription' ]
)
->setName('api.games.courses.description.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/courses/{id:[0-9]+}/notes',
    [ $container->courseGamesTranslationController , 'notes' ]
)
->setName('api.games.courses.notes') ;

$application->patch
(
    '/games/courses/{id:[0-9]+}/notes',
    [ $container->courseGamesTranslationController , 'patchNotes' ]
)
->setName('api.games.courses.notes.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/courses/{id:[0-9]+}/text',
    [ $container->courseGamesTranslationController , 'text' ]
)
->setName('api.games.courses.text') ;

$application->patch
(
    '/games/courses/{id:[0-9]+}/text',
    [ $container->courseGamesTranslationController , 'patchText' ]
)
->setName('api.games.courses.text.patch') ;