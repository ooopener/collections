<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games' , [ $container->applicationsGamesController , 'all'   ]
)
->setName('api.games.all');

$application->post
(
    '/games' , [ $container->applicationsGamesController , 'post'   ]
)
->setName('api.games.post');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/{id:[0-9]+}' , [ $container->applicationsGamesController , 'get'   ]
)
->setName('api.games.get');

$application->get
(
    '/games/{id:[0-9]+}/quest' , [ $container->coursesGamesController , 'all'   ]
)
->setName('api.games.quest.all');

$application->post
(
    '/games/{id:[0-9]+}/quest' , [ $container->coursesGamesController , 'post'   ]
)
->setName('api.games.quest.post');

$application->delete
(
    '/games/{id:[0-9]+}' , [ $container->applicationsGamesController , 'delete'   ]
)
->setName('api.games.delete');

$application->patch
(
    '/games/{id:[0-9]+}' , [ $container->applicationsGamesController , 'patch'   ]
)
->setName('api.game.patch');