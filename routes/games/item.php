<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/{id:[0-9]+}/item' , [ $container->applicationBadgeItemController , 'get'   ]
)
->setName('api.games.item.all');

$application->patch
(
    '/games/{id:[0-9]+}/item' , [ $container->applicationBadgeItemController , 'patch'   ]
)
->setName('api.games.item.patch');
