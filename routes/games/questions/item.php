<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/questions/{id:[0-9]+}/item' , [ $container->questionBadgeItemController , 'get'   ]
)
->setName('api.games.questions.item.all');

$application->patch
(
    '/games/questions/{id:[0-9]+}/item' , [ $container->questionBadgeItemController , 'patch'   ]
)
->setName('api.games.questions.item.patch');
