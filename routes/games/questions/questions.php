<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/questions' , [ $container->questionsGamesController , 'all'   ]
)
->setName('api.games.questions.all');

$application->post
(
    '/games/questions' , [ $container->questionsGamesController , 'post'   ]
)
->setName('api.games.questions.post');

$application->get
(
    '/games/questions/{id:[0-9]+}' , [ $container->questionsGamesController , 'get'   ]
)
->setName('api.games.questions.get');

$application->options
(
    '/games/questions/{id:[0-9]+}' , null
);

$application->delete
(
    '/games/questions/{id:[0-9]+}' , [ $container->questionsGamesController , 'delete'   ]
)
->setName('api.games.questions.delete');

$application->patch
(
    '/games/questions/{id:[0-9]+}' , [ $container->questionsGamesController , 'patch'   ]
)
->setName('api.game.questions.patch');