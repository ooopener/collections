<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/questions/{id:[0-9]+}/alternateName',
    [ $container->questionGamesTranslationController , 'alternateName' ]
)
->setName('api.games.questions.alternateName') ;

$application->patch
(
    '/games/questions/{id:[0-9]+}/alternateName',
    [ $container->questionGamesTranslationController , 'patchAlternateName' ]
)
->setName('api.games.questions.alternateName.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/questions/{id:[0-9]+}/description',
    [ $container->questionGamesTranslationController , 'description' ]
)
->setName('api.games.questions.description') ;

$application->patch
(
    '/games/questions/{id:[0-9]+}/description',
    [ $container->questionGamesTranslationController , 'patchDescription' ]
)
->setName('api.games.questions.description.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/questions/{id:[0-9]+}/notes',
    [ $container->questionGamesTranslationController , 'notes' ]
)
->setName('api.games.questions.notes') ;

$application->patch
(
    '/games/questions/{id:[0-9]+}/notes',
    [ $container->questionGamesTranslationController , 'patchNotes' ]
)
->setName('api.games.questions.notes.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/questions/{id:[0-9]+}/text',
    [ $container->questionGamesTranslationController , 'text' ]
)
->setName('api.games.questions.text') ;

$application->patch
(
    '/games/questions/{id:[0-9]+}/text',
    [ $container->questionGamesTranslationController , 'patchText' ]
)
->setName('api.games.questions.text.patch') ;