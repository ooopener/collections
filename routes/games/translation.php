<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/{id:[0-9]+}/alternateName',
    [ $container->applicationGamesTranslationController , 'alternateName' ]
)
->setName('api.games.alternateName') ;

$application->patch
(
    '/games/{id:[0-9]+}/alternateName',
    [ $container->applicationGamesTranslationController , 'patchAlternateName' ]
)
->setName('api.games.alternateName.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/{id:[0-9]+}/description',
    [ $container->applicationGamesTranslationController , 'description' ]
)
->setName('api.games.description') ;

$application->patch
(
    '/games/{id:[0-9]+}/description',
    [ $container->applicationGamesTranslationController , 'patchDescription' ]
)
->setName('api.games.description.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/{id:[0-9]+}/notes',
    [ $container->applicationGamesTranslationController , 'notes' ]
)
->setName('api.games.notes') ;

$application->patch
(
    '/games/{id:[0-9]+}/notes',
    [ $container->applicationGamesTranslationController , 'patchNotes' ]
)
->setName('api.games.notes.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/games/{id:[0-9]+}/text',
    [ $container->applicationGamesTranslationController , 'text' ]
)
->setName('api.games.text') ;

$application->patch
(
    '/games/{id:[0-9]+}/text',
    [ $container->applicationGamesTranslationController , 'patchText' ]
)
->setName('api.games.text.patch') ;