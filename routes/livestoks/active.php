<?php

$application->get
(
    '/livestocks/{id:[0-9]+}/active' ,
    [ $container->livestockActiveController , 'get'  ]
)
->setName('api.livestocks.active.get') ;

$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/livestocks/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' ,
    [ $container->livestockActiveController , 'patch' ]
)
->setName('api.livestocks.active.patch') ;

