<?php

$application->delete
(
    '/livestocks' , [ $container->livestocksController , 'deleteAll'   ]
)
->setName('api.livestocks.delete.all') ;

$application->delete
(
    '/livestocks/{id:[0-9]+}' ,
    [ $container->livestocksController , 'delete' ]
)
->setName('api.livestocks.delete');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks' ,
    [ $container->livestocksController , 'all'   ]
)
->setName('api.livestocks.all');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/{id:[0-9]+}' ,
    [ $container->livestocksController , 'get' ]
)
->setName('api.livestocks.get');

$application->post
(
    '/livestocks' , [ $container->livestocksController , 'post'   ]
)
->setName('api.livestocks.post') ;

$application->put
(
    '/livestocks/{id:[0-9]+}' ,
    [ $container->livestocksController , 'put' ]
)
->setName('api.livestocks.put');