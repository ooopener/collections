<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/{id:[0-9]+}/numbers',
    [ $container->livestockNumbersController , 'all' ]
)
->setName('api.livestocks.numbers.all') ;

$application->post
(
    '/livestocks/{id:[0-9]+}/numbers',
    [ $container->livestockNumbersController , 'post' ]
)
->setName('api.livestocks.numbers.post') ;

$application->get
(
    '/livestocks/{id:[0-9]+}/numbers/count' ,
    [ $container->livestockNumbersController , 'count' ]
)
->setName('api.livestocks.numbers.count') ;

$application->options
(
    '/livestocks/{owner:[0-9]+}/numbers/{id:[0-9]+}',
    null
);

$application->delete
(
    '/livestocks/{owner:[0-9]+}/numbers/{id:[0-9]+}',
    [ $container->livestockNumbersController , 'delete' ]
)
->setName('api.livestocks.numbers.delete') ;

$application->put
(
    '/livestocks/{owner:[0-9]+}/numbers/{id:[0-9]+}',
    [ $container->livestockNumbersController , 'put' ]
)
->setName('api.livestocks.numbers.put') ;