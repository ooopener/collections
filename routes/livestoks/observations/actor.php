<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/observations/{id:[0-9]+}/actor',
    [ $container->observationActorsController , 'all' ]
)
->setName('api.livestocks.observations.actor.all') ;

$application->options
(
    '/livestocks/observations/{owner:[0-9]+}/actor/{id:[0-9]+}',
    null
);

$application->post
(
    '/livestocks/observations/{owner:[0-9]+}/actor/{id:[0-9]+}',
    [ $container->observationActorsController , 'post' ]
)
->setName('api.livestocks.observations.actor.post') ;

$application->get
(
    '/livestocks/observations/{id:[0-9]+}/actor/count' ,
    [ $container->observationActorsController , 'count' ]
)
->setName('api.livestocks.observations.actor.count') ;

$application->delete
(
    '/livestocks/observations/{owner:[0-9]+}/actor/{id:[0-9]+}',
    [ $container->observationActorsController , 'delete' ]
)
->setName('api.livestocks.observations.actor.delete') ;

