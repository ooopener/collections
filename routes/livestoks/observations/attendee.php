<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/observations/{id:[0-9]+}/attendee',
    [ $container->observationAttendeesController , 'all' ]
)
->setName('api.livestocks.observations.attendee.all') ;

$application->options
(
    '/livestocks/observations/{owner:[0-9]+}/attendee/{id:[0-9]+}',
    null
);

$application->post
(
    '/livestocks/observations/{owner:[0-9]+}/attendee/{id:[0-9]+}',
    [ $container->observationAttendeesController , 'post' ]
)
->setName('api.livestocks.observations.attendee.post') ;

$application->get
(
    '/livestocks/observations/{id:[0-9]+}/attendee/count' ,
    [ $container->observationAttendeesController , 'count' ]
)
->setName('api.livestocks.observations.attendee.count') ;

$application->delete
(
    '/livestocks/observations/{owner:[0-9]+}/attendee/{id:[0-9]+}',
    [ $container->observationAttendeesController , 'delete' ]
)
->setName('api.livestocks.observations.attendee.delete') ;

