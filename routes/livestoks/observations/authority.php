<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/observations/{id:[0-9]+}/authority',
    [ $container->observationAuthorityController , 'all' ]
)
->setName('api.livestocks.observations.authority.all') ;

$application->options
(
    '/livestocks/observations/{owner:[0-9]+}/authority/{id:[0-9]+}',
    null
) ;

$application->post
(
    '/livestocks/observations/{owner:[0-9]+}/authority/{id:[0-9]+}',
    [ $container->observationAuthorityController , 'post' ]
)
->setName('api.livestocks.observations.authority.post') ;

$application->get
(
    '/livestocks/observations/{id:[0-9]+}/authority/count' ,
    [ $container->observationAuthorityController , 'count' ]
)
->setName('api.livestocks.observations.authority.count') ;

$application->delete
(
    '/livestocks/observations/{owner:[0-9]+}/authority/{id:[0-9]+}',
    [ $container->observationAuthorityController , 'delete' ]
)
->setName('api.livestocks.observations.authority.delete') ;

