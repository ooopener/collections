<?php

$application->get
(
    '/livestocks/{id:[0-9]+}/observations' , [ $container->observationsController , 'allLivestock'   ]
)
->setName('api.livestocks.observations.allLivestock');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/workshops/{id:[0-9]+}/observations' , [ $container->observationsController , 'allWorkshop'   ]
)
->setName('api.livestocks.workshops.observations.allWorkshop');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/observations/{id:[0-9]+}' ,
    [ $container->observationsController , 'get'   ]
)
->setName('api.livestocks.observations.get');

$application->map
(
    [ 'DELETE' , 'OPTIONS' ] ,
    '/livestocks/observations' , [ $container->observationsController , 'deleteAll'   ]
)
->setName('api.livestocks.observations.deleteAll');

$application->delete
(
    '/livestocks/observations/{id:[0-9]+}' , [ $container->observationsController , 'delete'   ]
)
->setName('api.livestocks.observations.delete');

$application->post
(
    '/livestocks/workshops/{id:[0-9]+}/observations' , [ $container->observationsController , 'post'   ]
)
->setName('api.livestocks..workshops.observations.post');

$application->patch
(
    '/livestocks/observations/{id:[0-9]+}' , [ $container->observationsController , 'patch'   ]
)
->setName('api.livestocks.observations.patch');
