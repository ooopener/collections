<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/observations/{id:[0-9]+}/alternateName',
    [ $container->observationTranslationController , 'alternateName' ]
)
->setName('api.livestocks.observations.alternateName') ;

$application->patch
(
    '/livestocks/observations/{id:[0-9]+}/alternateName',
    [ $container->observationTranslationController , 'patchAlternateName' ]
)
->setName('api.livestocks.observations.alternateName.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/observations/{id:[0-9]+}/description',
    [ $container->observationTranslationController , 'description' ]
)
->setName('api.livestocks.observations.description') ;

$application->patch
(
    '/livestocks/observations/{id:[0-9]+}/description',
    [ $container->observationTranslationController , 'patchDescription' ]
)
->setName('api.livestocks.observations.description.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/observations/{id:[0-9]+}/notes',
    [ $container->observationTranslationController , 'notes' ]
)
->setName('api.livestocks.observations.notes') ;

$application->patch
(
    '/livestocks/observations/{id:[0-9]+}/notes',
    [ $container->observationTranslationController , 'patchNotes' ]
)
->setName('api.livestocks.observations.notes.patch') ;

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/observations/{id:[0-9]+}/text',
    [ $container->observationTranslationController , 'text' ]
)
->setName('api.livestocks.observations.text') ;

$application->patch
(
    '/livestocks/observations/{id:[0-9]+}/text',
    [ $container->observationTranslationController , 'patchText' ]
)
->setName('api.livestocks.observations.text.patch') ;