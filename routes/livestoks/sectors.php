<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/workshops/{owner:[0-9]+}/workplaces/{id:[0-9]+}/sectors' , [ $container->sectorsController , 'all'   ]
)
->setName('api.workshops.workplaces.sectors.all');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/workshops/{workshop:[0-9]+}/workplaces/{owner:[0-9]+}/sectors/{id:[0-9]+}' , [ $container->sectorsController , 'get'   ]
)
->setName('api.workshops.workplaces.sectors.get');

$application->delete
(
    '/livestocks/workshops/{workshop:[0-9]+}/workplaces/{owner:[0-9]+}/sectors/{id:[0-9]+}' , [ $container->sectorsController , 'delete'   ]
)
->setName('api.workshops.workplaces.sectors.delete');

$application->post
(
    '/livestocks/workshops/{owner:[0-9]+}/workplaces/{id:[0-9]+}/sectors' , [ $container->sectorsController , 'post'   ]
)
->setName('api.workshops.workplaces.sectors.post') ;

$application->put
(
    '/livestocks/workshops/{workshop:[0-9]+}/workplaces/{owner:[0-9]+}/sectors/{id:[0-9]+}' , [ $container->sectorsController , 'put'   ]
)
->setName('api.workshops.workplaces.sectors.put');