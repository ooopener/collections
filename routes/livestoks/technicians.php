<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/{id:[0-9]+}/technicians',
    [ $container->livestockTechniciansController , 'all' ]
)
->setName('api.livestocks.technicians.all') ;

$application->options
(
    '/livestocks/{owner:[0-9]+}/technicians/{id:[0-9]+}',
    null
);

$application->post
(
    '/livestocks/{owner:[0-9]+}/technicians/{id:[0-9]+}',
    [ $container->livestockTechniciansController , 'post' ]
)
->setName('api.livestocks.technicians.post') ;

$application->get
(
    '/livestocks/{id:[0-9]+}/technicians/count' ,
    [ $container->livestockTechniciansController , 'count' ]
)
->setName('api.livestocks.technicians.count') ;

$application->delete
(
    '/livestocks/{owner:[0-9]+}/technicians/{id:[0-9]+}',
    [ $container->livestockTechniciansController , 'delete' ]
)
->setName('api.livestocks.technicians.delete') ;

