<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/{id:[0-9]+}/veterinarians',
    [ $container->livestockVeterinariansController , 'all' ]
)
->setName('api.livestocks.veterinarians.all') ;

$application->options
(
    '/livestocks/{owner:[0-9]+}/veterinarians/{id:[0-9]+}',
    null
);

$application->post
(
    '/livestocks/{owner:[0-9]+}/veterinarians/{id:[0-9]+}',
    [ $container->livestockVeterinariansController , 'post' ]
)
->setName('api.livestocks.veterinarians.post') ;

$application->get
(
    '/livestocks/{id:[0-9]+}/veterinarians/count' ,
    [ $container->livestockVeterinariansController , 'count' ]
)
->setName('api.livestocks.veterinarians.count') ;

$application->delete
(
    '/livestocks/{owner:[0-9]+}/veterinarians/{id:[0-9]+}',
    [ $container->livestockVeterinariansController , 'delete' ]
)
->setName('api.livestocks.veterinarians.delete') ;

