<?php

$application->get
(
    '/livestocks/{id:[0-9]+}/withStatus' ,
    [ $container->livestockWithStatusController , 'get'  ]
)
->setName('api.livestocks.withStatus.get') ;

$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/livestocks/{id:[0-9]+}/withStatus/{status}' ,
    [ $container->livestockWithStatusController , 'patch' ]
)
->setName('api.livestocks.withStatus.patch') ;

