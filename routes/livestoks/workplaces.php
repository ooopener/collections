<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/workshops/{id:[0-9]+}/workplaces' , [ $container->workplacesController , 'all'   ]
)
->setName('api.workshops.workplaces.all');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/workshops/{owner:[0-9]+}/workplaces/{id:[0-9]+}' , [ $container->workplacesController , 'get'   ]
)
->setName('api.workshops.workplaces.get');

$application->delete
(
    '/livestocks/workshops/{owner:[0-9]+}/workplaces/{id:[0-9]+}' , [ $container->workplacesController , 'delete'   ]
)
->setName('api.workshops.workplaces.delete');

$application->post
(
    '/livestocks/workshops/{id:[0-9]+}/workplaces' , [ $container->workplacesController , 'post'   ]
)
->setName('api.workshops.workplaces.post') ;

$application->put
(
    '/livestocks/workshops/{owner:[0-9]+}/workplaces/{id:[0-9]+}' , [ $container->workplacesController , 'put'   ]
)
->setName('api.workshops.workplaces.put');