<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/{id:[0-9]+}/workshops' , [ $container->workshopsController , 'all'   ]
)
->setName('api.livestocks.workshops.all');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/livestocks/workshops/{id:[0-9]+}' , [ $container->workshopsController , 'get'   ]
)
->setName('api.livestocks.workshops.get');

$application->options
(
    '/livestocks/{owner:[0-9]+}/workshops/{id:[0-9]+}' , null
);

$application->delete
(
    '/livestocks/{owner:[0-9]+}/workshops/{id:[0-9]+}' , [ $container->workshopsController , 'delete'   ]
)
->setName('api.livestocks.workshops.delete');

$application->post
(
    '/livestocks/{id:[0-9]+}/workshops' , [ $container->workshopsController , 'post'   ]
)
->setName('api.livestocks.workshops.post') ;

$application->put
(
    '/livestocks/{owner:[0-9]+}/workshops/{id:[0-9]+}' , [ $container->workshopsController , 'put'   ]
)
->setName('api.livestocks.workshops.put');
