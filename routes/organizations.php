<?php

/**
 * @OA\Tag(
 *     name="organizations",
 *     description="Organizations paths"
 * )
 */

require "organizations/active.php" ;
require "organizations/additionalType.php" ;
require "organizations/address.php" ;
require "organizations/audio.php" ;
require "organizations/audios.php" ;
require "organizations/department.php" ;
require "organizations/email.php" ;
require "organizations/employees.php" ;
require "organizations/founder.php" ;
require "organizations/image.php" ;
require "organizations/keywords.php" ;
require "organizations/logo.php" ;
require "organizations/memberOf.php" ;
require "organizations/members.php" ;
require "organizations/numbers.php" ;
require "organizations/organizations.php" ;
require "organizations/parentOrganization.php" ;
require "organizations/photos.php" ;
require "organizations/providers.php" ;
require "organizations/subOrganization.php" ;
require "organizations/telephone.php" ;
require "organizations/translation.php" ;
require "organizations/video.php" ;
require "organizations/videos.php" ;
require "organizations/websites.php" ;
require "organizations/withStatus.php" ;
