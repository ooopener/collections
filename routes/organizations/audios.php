<?php

/**
 * @OA\Get(
 *     path="/organizations/{id}/audios",
 *     tags={"organizations"},
 *     description="List organization audios",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W",
 *             "organizations:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with audios",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of audio objects",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/organizations/{id:[0-9]+}/audios',
    [ $container->organizationAudiosController , 'all' ]
)
->setName('api.organizations.audios.all') ;

/**
 * @OA\Post(
 *     path="/organizations/{id}/audios",
 *     tags={"organizations"},
 *     description="Create organization audios",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="Result with audios",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of audio objects",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/organizations/{id:[0-9]+}/audios',
    [ $container->organizationAudiosController , 'post' ]
)
->setName('api.organizations.audios.post') ;

/**
 * @OA\Get(
 *     path="/organizations/{id}/audios/count",
 *     tags={"organizations"},
 *     description="Count organization audios",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W",
 *             "organizations:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with audios",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of audio objects",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/organizations/{id:[0-9]+}/audios/count' ,
    [ $container->organizationAudiosController , 'count' ]
)
->setName('api.organizations.audios.count') ;

/**
 * @OA\Delete(
 *     path="/organizations/{id}/audios",
 *     tags={"organizations"},
 *     description="Delete organization audios",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/organizations/{id:[0-9]+}/audios' ,
    [ $container->organizationAudiosController , 'deleteAll' ]
)
->setName('api.organizations.audios.delete.all') ;

/**
 * @OA\Get(
 *     path="/organizations/{owner}/audios/{id}",
 *     tags={"organizations"},
 *     description="Get organization audio",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W",
 *             "organizations:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/organizations/{owner:[0-9]+}/audios/{id:[0-9]+}' ,
    [ $container->organizationAudiosController , 'get' ]
)
->setName('api.organizations.audios.get') ;

/**
 * @OA\Delete(
 *     path="/organizations/{owner}/audios/{id}",
 *     tags={"organizations"},
 *     description="Delete organization audio",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/organizations/{owner:[0-9]+}/audios/{id:[0-9]+}' ,
    [ $container->organizationAudiosController , 'delete' ]
)
->setName('api.organizations.audios.delete') ;

/**
 * @OA\Patch(
 *     path="/organizations/{owner}/audios/{id}/position/{position}",
 *     tags={"organizations"},
 *     description="Patch organization audio position",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(name="position",in="path",description="",required=true,@OA\Schema(type="integer")),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'OPTIONS' , 'PATCH' ] ,
    '/organizations/{owner:[0-9]+}/audios/{id:[0-9]+}/position/{position:[0-9]+}' ,
    [ $container->organizationAudiosController , 'patchPosition' ]
)
->setName('api.organizations.audios.position') ;
