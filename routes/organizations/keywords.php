<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/organizations/{id:[0-9]+}/keywords',
    [ $container->organizationKeywordsController , 'all' ]
)
->setName('api.organizations.keywords.all') ;

$application->post
(
    '/organizations/{id:[0-9]+}/keywords',
    [ $container->organizationKeywordsController , 'post' ]
)
->setName('api.organizations.keywords.post') ;

$application->get
(
    '/organizations/{id:[0-9]+}/keywords/count' ,
    [ $container->organizationKeywordsController , 'count' ]
)
->setName('api.organizations.keywords.count') ;

$application->options
(
    '/organizations/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    null
);

$application->delete
(
    '/organizations/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->organizationKeywordsController , 'delete' ]
)
->setName('api.organizations.keywords.delete') ;

$application->put
(
    '/organizations/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->organizationKeywordsController , 'put' ]
)
->setName('api.organizations.keywords.put') ;