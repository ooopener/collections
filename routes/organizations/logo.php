<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/organizations/{id:[0-9]+}/logo' ,
    [ $container->organizationLogoController , 'get' ]
)
->setName('api.organizations.logo.get');

/**
 * @OA\Delete(
 *     path="/organizations/{id}/logo",
 *     tags={"organizations"},
 *     description="Delete resource logo",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/organizations/{id:[0-9]+}/logo' ,
    [ $container->organizationLogoController , 'delete' ]
)
->setName('api.organizations.logo.delete');

/**
 * @OA\Patch(
 *     path="/organizations/{owner}/logo/{id}",
 *     tags={"organizations"},
 *     description="Patch resource logo",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'OPTIONS' , 'PATCH' ] ,
    '/organizations/{owner:[0-9]+}/logo/{id:[0-9]+}' ,
    [ $container->organizationLogoController , 'patch' ]
)
->setName('api.organizations.logo.patch');
