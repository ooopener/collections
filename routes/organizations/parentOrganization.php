<?php

/**
 * @OA\Get(
 *     path="/organizations/{id}/parentOrganization",
 *     tags={"organizations"},
 *     description="Get parentOrganization organization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W",
 *             "organizations:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/organizationListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/organizations/{id:[0-9]+}/parentOrganization',
    [ $container->organizationSubOrganizationsController , 'allReverse' ]
)
->setName('api.organizations.parentOrganization.allReverse') ;

/**
 * @OA\Get(
 *     path="/organizations/{id}/parentOrganization/count",
 *     tags={"organizations"},
 *     description="Count organization parentOrganization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W",
 *             "organizations:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of parentOrganization",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/organizations/{id:[0-9]+}/parentOrganization/count' ,
    [ $container->organizationSubOrganizationsController , 'countReverse' ]
)
->setName('api.organizations.parentOrganization.countReverse') ;

$application->options
(
    '/organizations/{owner:[0-9]+}/parentOrganization/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/organizations/{owner}/parentOrganization/{id}",
 *     tags={"organizations"},
 *     description="Delete parentOrganization organization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/organizations/{owner:[0-9]+}/parentOrganization/{id:[0-9]+}',
    [ $container->organizationSubOrganizationsController , 'deleteReverse' ]
)
->setName('api.organizations.parentOrganization.deleteReverse') ;

/**
 * @OA\Post(
 *     path="/organizations/{owner}/parentOrganization/{id}",
 *     tags={"organizations"},
 *     description="Create parentOrganization organization",
 *     security={
 *         {"OAuth2"={
 *             "organizations:A",
 *             "organizations:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/organizationResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/organizations/{owner:[0-9]+}/parentOrganization/{id:[0-9]+}',
    [ $container->organizationSubOrganizationsController , 'postReverse' ]
)
->setName('api.organizations.parentOrganization.postReverse') ;
