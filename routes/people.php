<?php

/**
 * @OA\Tag(
 *     name="people",
 *     description="People paths"
 * )
 */

require "people/active.php" ;
require "people/address.php" ;
require "people/audio.php" ;
require "people/audios.php" ;
require "people/email.php" ;
require "people/image.php" ;
require "people/keywords.php" ;
require "people/memberOf.php" ;
require "people/people.php" ;
require "people/photos.php" ;
require "people/telephone.php" ;
require "people/job.php";
require "people/translation.php" ;
require "people/video.php" ;
require "people/videos.php" ;
require "people/websites.php" ;
require "people/withStatus.php" ;
require "people/worksFor.php" ;
