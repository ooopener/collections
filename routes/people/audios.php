<?php

/**
 * @OA\Get(
 *     path="/people/{id}/audios",
 *     tags={"people"},
 *     description="List person audios",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W",
 *             "people:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with audios",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of audio objects",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/people/{id:[0-9]+}/audios',
    [ $container->peopleAudiosController , 'all' ]
)
->setName('api.people.audios.all') ;

/**
 * @OA\Post(
 *     path="/people/{id}/audios",
 *     tags={"people"},
 *     description="Create people audios",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="Result with audios",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of audio objects",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/people/{id:[0-9]+}/audios',
    [ $container->peopleAudiosController , 'post' ]
)
->setName('api.people.audios.post') ;

/**
 * @OA\Get(
 *     path="/people/{id}/audios/count",
 *     tags={"people"},
 *     description="Count person audios",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W",
 *             "people:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with audios",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of audio objects",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/people/{id:[0-9]+}/audios/count' ,
    [ $container->peopleAudiosController , 'count' ]
)
->setName('api.people.audios.count') ;

/**
 * @OA\Delete(
 *     path="/people/{id}/audios",
 *     tags={"people"},
 *     description="Delete person audios",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/people/{id:[0-9]+}/audios' ,
    [ $container->peopleAudiosController , 'deleteAll' ]
)
->setName('api.people.audios.delete.all') ;

/**
 * @OA\Get(
 *     path="/people/{owner}/audios/{id}",
 *     tags={"people"},
 *     description="Get person audio",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W",
 *             "people:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/people/{owner:[0-9]+}/audios/{id:[0-9]+}' ,
    [ $container->peopleAudiosController , 'get' ]
)
->setName('api.people.audios.get') ;

/**
 * @OA\Delete(
 *     path="/people/{owner}/audios/{id}",
 *     tags={"people"},
 *     description="Delete person audio",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/people/{owner:[0-9]+}/audios/{id:[0-9]+}' ,
    [ $container->peopleAudiosController , 'delete' ]
)
->setName('api.people.audios.delete') ;

/**
 * @OA\Patch(
 *     path="/people/{owner}/audios/{id}/position/{position}",
 *     tags={"people"},
 *     description="Patch person audio position",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(name="position",in="path",description="",required=true,@OA\Schema(type="integer")),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/AudioObject")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'OPTIONS' , 'PATCH' ] ,
    '/people/{owner:[0-9]+}/audios/{id:[0-9]+}/position/{position:[0-9]+}' ,
    [ $container->peopleAudiosController , 'patchPosition' ]
)
->setName('api.people.audios.position') ;
