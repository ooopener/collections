<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/people/{id:[0-9]+}/keywords',
    [ $container->peopleKeywordsController , 'all' ]
)
->setName('api.people.keywords.all') ;

$application->post
(
    '/people/{id:[0-9]+}/keywords',
    [ $container->peopleKeywordsController , 'post' ]
)
->setName('api.people.keywords.post') ;

$application->get
(
    '/people/{id:[0-9]+}/keywords/count' ,
    [ $container->peopleKeywordsController , 'count' ]
)
->setName('api.people.keywords.count') ;

$application->options
(
    '/people/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    null
);

$application->delete
(
    '/people/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->peopleKeywordsController , 'delete' ]
)
->setName('api.people.keywords.delete') ;

$application->put
(
    '/people/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->peopleKeywordsController , 'put' ]
)
->setName('api.people.keywords.put') ;