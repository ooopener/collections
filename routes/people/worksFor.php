<?php

/**
 * @OA\Get(
 *     path="/people/{id}/worksFor",
 *     tags={"people"},
 *     description="Get worksFor person",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W",
 *             "people:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/organizationListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/people/{id:[0-9]+}/worksFor',
    [ $container->organizationEmployeesController , 'allReverse' ]
)
->setName('api.people.worksFor.allReverse') ;

/**
 * @OA\Get(
 *     path="/people/{id}/worksFor/count",
 *     tags={"people"},
 *     description="Count organization worksFor",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W",
 *             "people:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of worksFor",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/people/{id:[0-9]+}/worksFor/count' ,
    [ $container->organizationEmployeesController , 'countReverse' ]
)
->setName('api.people.worksFor.countReverse') ;

$application->options
(
    '/people/{owner:[0-9]+}/worksFor/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/people/{owner}/worksFor/{id}",
 *     tags={"people"},
 *     description="Delete worksFor organization",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/people/{owner:[0-9]+}/worksFor/{id:[0-9]+}',
    [ $container->organizationEmployeesController , 'deleteReverse' ]
)
->setName('api.people.worksFor.deleteReverse') ;

/**
 * @OA\Post(
 *     path="/people/{owner}/worksFor/{id}",
 *     tags={"people"},
 *     description="Create worksFor person",
 *     security={
 *         {"OAuth2"={
 *             "people:A",
 *             "people:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/organizationResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/people/{owner:[0-9]+}/worksFor/{id:[0-9]+}',
    [ $container->organizationEmployeesController , 'postReverse' ]
)
->setName('api.people.worksFor.postReverse') ;
