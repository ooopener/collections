<?php

/**
 * @OA\Tag(
 *     name="places",
 *     description="Places paths"
 * )
 */

require "places/active.php" ;
require "places/activities.php" ;
require "places/additionalType.php" ;
require "places/address.php" ;
require "places/audio.php" ;
require "places/audios.php" ;
require "places/email.php" ;
require "places/geo.php" ;
require "places/image.php" ;
require "places/keywords.php" ;
require "places/logo.php" ;
require "places/places.php" ;
require "places/offers.php" ;
require "places/openingHours.php" ;
require "places/permits.php" ;
require "places/photos.php" ;
require "places/prohibitions.php" ;
require "places/services.php" ;
require "places/status.php" ;
require "places/containedInPlace.php" ;
require "places/containsPlace.php" ;
require "places/translation.php" ;
require "places/telephone.php" ;
require "places/video.php" ;
require "places/videos.php" ;
require "places/websites.php" ;
require "places/withStatus.php" ;
