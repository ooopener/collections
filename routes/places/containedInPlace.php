<?php

/**
 * @OA\Get(
 *     path="/places/{id}/containedInPlace",
 *     tags={"places"},
 *     description="Get containedInPlace place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/places/{id:[0-9]+}/containedInPlace',
    [ $container->placeContainsPlacesController , 'allReverse' ]
)
->setName('api.places.containedInPlace.allReverse') ;

/**
 * @OA\Get(
 *     path="/places/{id}/containedInPlace/count",
 *     tags={"places"},
 *     description="Count place containedInPlace",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of containedInPlace",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/places/{id:[0-9]+}/containedInPlace/count' ,
    [ $container->placeContainsPlacesController , 'countReverse' ]
)
->setName('api.places.containedInPlace.countReverse') ;

$application->options
(
    '/places/{owner:[0-9]+}/containedInPlace/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/places/{owner}/containedInPlace/{id}",
 *     tags={"places"},
 *     description="Delete containedInPlace place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/places/{owner:[0-9]+}/containedInPlace/{id:[0-9]+}',
    [ $container->placeContainsPlacesController , 'deleteReverse' ]
)
->setName('api.places.containedInPlace.delete') ;

/**
 * @OA\Post(
 *     path="/places/{owner}/containedInPlace/{id}",
 *     tags={"places"},
 *     description="Create containedInPlace place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/places/{owner:[0-9]+}/containedInPlace/{id:[0-9]+}',
    [ $container->placeContainsPlacesController , 'postReverse' ]
)
->setName('api.places.containedInPlace.postReverse') ;
