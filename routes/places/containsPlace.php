<?php

/**
 * @OA\Get(
 *     path="/places/{id}/containsPlace",
 *     tags={"places"},
 *     description="Get containsPlace place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/places/{id:[0-9]+}/containsPlace',
    [ $container->placeContainsPlacesController , 'all' ]
)
->setName('api.places.containsPlace.all') ;

/**
 * @OA\Get(
 *     path="/places/{id}/containsPlace/count",
 *     tags={"places"},
 *     description="Count place containsPlace",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of containsPlace",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/places/{id:[0-9]+}/containsPlace/count' ,
    [ $container->placeContainsPlacesController , 'count' ]
)
->setName('api.places.containsPlace.count') ;

$application->options
(
    '/places/{owner:[0-9]+}/containsPlace/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/places/{owner}/containsPlace/{id}",
 *     tags={"places"},
 *     description="Delete containsPlace place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/places/{owner:[0-9]+}/containsPlace/{id:[0-9]+}',
    [ $container->placeContainsPlacesController , 'delete' ]
)
->setName('api.places.containsPlace.delete') ;

/**
 * @OA\Post(
 *     path="/places/{owner}/containsPlace/{id}",
 *     tags={"places"},
 *     description="Create containsPlace place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/places/{owner:[0-9]+}/containsPlace/{id:[0-9]+}',
    [ $container->placeContainsPlacesController , 'post' ]
)
->setName('api.places.containsPlace.post') ;
