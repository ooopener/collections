<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/places/{id:[0-9]+}/keywords',
    [ $container->placeKeywordsController , 'all' ]
)
->setName('api.places.keywords.all') ;

$application->post
(
    '/places/{id:[0-9]+}/keywords',
    [ $container->placeKeywordsController , 'post' ]
)
->setName('api.places.keywords.post') ;

$application->get
(
    '/places/{id:[0-9]+}/keywords/count' ,
    [ $container->placeKeywordsController , 'count' ]
)
->setName('api.places.keywords.count') ;

$application->options
(
    '/places/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    null
);

$application->delete
(
    '/places/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->placeKeywordsController , 'delete' ]
)
->setName('api.places.keywords.delete') ;

$application->put
(
    '/places/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->placeKeywordsController , 'put' ]
)
->setName('api.places.keywords.put') ;