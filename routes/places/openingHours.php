<?php

/**
 * @OA\Get(
 *     path="/places/{id}/openingHoursSpecification",
 *     tags={"places"},
 *     description="List place openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with openingHoursSpecification",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/OpeningHoursSpecification"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/places/{id:[0-9]+}/openingHoursSpecification',
    [ $container->placeOpeningHoursController , 'all' ]
)
->setName('api.places.openingHoursSpecification.all') ;

/**
 * @OA\Post(
 *     path="/places/{id}/openingHoursSpecification",
 *     tags={"places"},
 *     description="Create place openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postOpeningHoursSpecification"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with openingHoursSpecification",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/OpeningHoursSpecification"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/places/{id:[0-9]+}/openingHoursSpecification',
    [ $container->placeOpeningHoursController , 'post' ]
)
->setName('api.places.openingHoursSpecification.post') ;

/**
 * @OA\Get(
 *     path="/places/{id}/openingHoursSpecification/count",
 *     tags={"places"},
 *     description="Count place openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with openingHoursSpecification",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of openingHoursSpecification",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/places/{id:[0-9]+}/openingHoursSpecification/count' ,
    [ $container->placeOpeningHoursController , 'count' ]
)
->setName('api.places.openingHoursSpecification.count') ;

$application->options
(
    '/places/{owner:[0-9]+}/openingHoursSpecification/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/places/{owner}/openingHoursSpecification/{id}",
 *     tags={"places"},
 *     description="Delete place openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/places/{owner:[0-9]+}/openingHoursSpecification/{id:[0-9]+}',
    [ $container->placeOpeningHoursController , 'delete' ]
)
->setName('api.places.openingHoursSpecification.delete') ;

/**
 * @OA\Put(
 *     path="/places/{owner}/openingHoursSpecification/{id}",
 *     tags={"places"},
 *     description="Edit place openingHoursSpecification",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putOpeningHoursSpecification"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/places/{owner:[0-9]+}/openingHoursSpecification/{id:[0-9]+}',
    [ $container->placeOpeningHoursController , 'put' ]
)
->setName('api.places.openingHoursSpecification.put') ;
