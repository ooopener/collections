<?php

/**
 * @OA\Get(
 *     path="/places/{id}/permits",
 *     tags={"places"},
 *     description="List place permits",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with permits",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of permits",type="array",items=@OA\Items(ref="#/components/schemas/Thesaurus"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/places/{id:[0-9]+}/permits',
    [ $container->placePermitsController , 'all' ]
)
->setName('api.places.permits.all') ;

/**
 * @OA\Get(
 *     path="/places/{id}/permits/count",
 *     tags={"places"},
 *     description="Count place permits",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with permits",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of permits",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/places/{id:[0-9]+}/permits/count' ,
    [ $container->placePermitsController , 'count' ]
)
->setName('api.places.permits.count') ;

/**
 * @OA\Put(
 *     path="/places/{id}/permits",
 *     tags={"places"},
 *     description="Edit place permits",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putMultiEdgeField"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/places/{id:[0-9]+}/permits',
    [ $container->placePermitsController , 'put' ]
)
->setName('api.places.permits.put') ;
