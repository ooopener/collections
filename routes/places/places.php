<?php



/**
 * @OA\Get(
 *     path="/places",
 *     tags={"places"},
 *     description="List places",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Parameter(ref="#/components/parameters/offset"),
 *     @OA\Parameter(ref="#/components/parameters/limit"),
 *     @OA\Parameter(name="sort",in="query",description="Sort result",@OA\Schema(type="string",default="name")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/places' ,
    [ $container->placesController , 'all'   ]
)
->setName('api.places.all') ;

/**
 * @OA\Delete(
 *     path="/places",
 *     tags={"places"},
 *     description="Delete places from list",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/NotValidList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/places' , [ $container->placesController , 'deleteAll'   ]
)
->setName('api.places.delete.all') ;

/**
 * @OA\Post(
 *     path="/places",
 *     tags={"places"},
 *     description="Create a new place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/postPlace"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/places' , [ $container->placesController , 'post'   ]
)
->setName('api.places.post') ;

/**
 * @OA\Get(
 *     path="/places/count",
 *     tags={"places"},
 *     description="Count places",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         description="Count of places",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of places",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/places/count' ,
    [ $container->placesController , 'count' ]
)
->setName('api.places.count') ;

/**
 * @OA\Get(
 *     path="/places/near",
 *     tags={"places"},
 *     description="Returns all the elements near the default position",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(name="latitude",in="query",description="The latitude of a location",@OA\Schema(type="number",minimum=-90,maximum=90)),
 *     @OA\Parameter(name="longitude",in="query",description="The longitude of a location",@OA\Schema(type="number",minimum=-180,maximum=180)),
 *     @OA\Parameter(name="distance",in="query",description="The distance in meter",@OA\Schema(type="number",minimum=0)),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 * @OA\Get(
 *     path="/places/near/{id}",
 *     tags={"places"},
 *     description="Returns all the elements near the place ID",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(name="distance",in="query",description="The distance in meter",@OA\Schema(type="number",minimum=0)),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/places/near[/{id:[0-9]+}]' ,
    [ $container->placesController , 'near' ]
)
->setName('api.places.near');

/**
 * @OA\Get(
 *     path="/places/{id}",
 *     tags={"places"},
 *     description="Get place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/places/{id:[0-9]+}' ,
    [ $container->placesController , 'get' ]
)
->setName('api.places.get');

/**
 * @OA\Delete(
 *     path="/places/{id}",
 *     tags={"places"},
 *     description="Delete place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/places/{id:[0-9]+}' ,
    [ $container->placesController , 'delete' ]
)
->setName('api.places.delete');

/**
 * @OA\Patch(
 *     path="/places/{id}",
 *     tags={"places"},
 *     description="Patch a place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchPlace"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/places/{id:[0-9]+}' ,
    [ $container->placesController , 'patch' ]
)
->setName('api.places.patch');

/**
 * @OA\Put(
 *     path="/places/{id}",
 *     tags={"places"},
 *     description="Edit a place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putPlace"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/placeResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/places/{id:[0-9]+}' ,
    [ $container->placesController , 'put' ]
)
->setName('api.places.put');
