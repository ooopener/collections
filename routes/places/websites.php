<?php

/**
 * @OA\Get(
 *     path="/places/{id}/websites",
 *     tags={"places"},
 *     description="Get websites place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/Website")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/places/{id:[0-9]+}/websites',
    [ $container->placeWebsitesController , 'all' ]
)
->setName('api.places.websites.all') ;

/**
 * @OA\Post(
 *     path="/places/{id}/websites",
 *     tags={"places"},
 *     description="Create websites place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/postWebsite"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/Website")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/places/{id:[0-9]+}/websites',
    [ $container->placeWebsitesController , 'post' ]
)
->setName('api.places.websites.post') ;

/**
 * @OA\Get(
 *     path="/places/{id}/websites/count",
 *     tags={"places"},
 *     description="Count place websites",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W",
 *             "places:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of websites",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/places/{id:[0-9]+}/websites/count' ,
    [ $container->placeWebsitesController , 'count' ]
)
->setName('api.places.websites.count') ;

$application->options
(
    '/places/{owner:[0-9]+}/websites/{id:[0-9]+}',
    null
);

/**
 * @OA\Delete(
 *     path="/places/{owner}/websites/{id}",
 *     tags={"places"},
 *     description="Delete websites place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/Website")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/places/{owner:[0-9]+}/websites/{id:[0-9]+}',
    [ $container->placeWebsitesController , 'delete' ]
)
->setName('api.places.websites.delete') ;

/**
 * @OA\Put(
 *     path="/places/{owner}/websites/{id}",
 *     tags={"places"},
 *     description="Edit websites place",
 *     security={
 *         {"OAuth2"={
 *             "places:A",
 *             "places:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/owner"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putWebsite"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/Website")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/places/{owner:[0-9]+}/websites/{id:[0-9]+}',
    [ $container->placeWebsitesController , 'put' ]
)
->setName('api.places.websites.put') ;

