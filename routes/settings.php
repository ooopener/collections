<?php

/**
 *
 * @OA\Tag(
 *     name="settings",
 *     description="Settings paths"
 * )
 *
 * @OA\Get(
 *     path="/settings",
 *     tags={"settings"},
 *     description="List of available settings",
 *     security={
 *         {"OAuth2"={
 *             "settings:A",
 *             "settings:W",
 *             "settings:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/settingsResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 *
 */
$application->get
(
    '/settings' , [ $container->settingsListController , 'all' ]
)
->setName('api.settings.all');
