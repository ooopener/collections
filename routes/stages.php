<?php

/**
 * @OA\Tag(
 *     name="stages",
 *     description="Stages paths"
 * )
 */

require "stages/active.php" ;
require "stages/activities.php" ;
require "stages/audio.php" ;
require "stages/audios.php" ;
require "stages/discover.php" ;
require "stages/image.php" ;
require "stages/photos.php" ;
require "stages/stages.php" ;
require "stages/translation.php" ;
require "stages/video.php" ;
require "stages/videos.php" ;
require "stages/websites.php" ;
require "stages/withStatus.php" ;
