<?php

/**
 * @OA\Get(
 *     path="/stages/{id}/active",
 *     tags={"stages"},
 *     description="Get active stage",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/successActive")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/stages/{id:[0-9]+}/active' ,
    [ $container->stageActiveController , 'get'  ]
)
->setName('api.stages.active.get') ;

/**
 * @OA\Patch(
 *     path="/stages/{id}/active/{bool}",
 *     tags={"stages"},
 *     description="Patch active stage",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(
 *         name="bool",
 *         in="path",
 *         description="",
 *         required=true,
 *         @OA\Schema(type="boolean")
 *     ),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/successActive")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/stages/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' ,
    [ $container->stageActiveController , 'patch' ]
)
->setName('api.stages.active.patch') ;

