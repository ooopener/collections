<?php

/**
 * @OA\Get(
 *     path="/stages/{id}/activities",
 *     tags={"stages"},
 *     description="List stage activities",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with activities",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of activities",type="array",items=@OA\Items(ref="#/components/schemas/Thesaurus"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/stages/{id:[0-9]+}/activities',
    [ $container->stageActivitiesController , 'all' ]
)
->setName('api.stages.activities.all') ;

/**
 * @OA\Get(
 *     path="/stages/{id}/activities/count",
 *     tags={"stages"},
 *     description="Count stage activities",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result with activities",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of activities",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/stages/{id:[0-9]+}/activities/count' ,
    [ $container->stageActivitiesController , 'count' ]
)
->setName('api.stages.activities.count') ;

/**
 * @OA\Put(
 *     path="/stages/{id}/activities",
 *     tags={"stages"},
 *     description="Edit stage activities",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putMultiEdgeField"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/stages/{id:[0-9]+}/activities',
    [ $container->stageActivitiesController , 'put' ]
)
->setName('api.stages.activities.put') ;

