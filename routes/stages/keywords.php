<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/stages/{id:[0-9]+}/keywords',
    [ $container->stageKeywordsController , 'all' ]
)
->setName('api.stages.keywords.all') ;

$application->post
(
    '/stages/{id:[0-9]+}/keywords',
    [ $container->stageKeywordsController , 'post' ]
)
->setName('api.stages.keywords.post') ;

$application->get
(
    '/stages/{id:[0-9]+}/keywords/count' ,
    [ $container->stageKeywordsController , 'count' ]
)
->setName('api.stages.keywords.count') ;

$application->options
(
    '/stages/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    null
);

$application->delete
(
    '/stages/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->stageKeywordsController , 'delete' ]
)
->setName('api.stages.keywords.delete') ;

$application->put
(
    '/stages/{owner:[0-9]+}/keywords/{id:[0-9]+}',
    [ $container->stageKeywordsController , 'put' ]
)
->setName('api.stages.keywords.put') ;