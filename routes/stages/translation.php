<?php

/**
 * @OA\Get(
 *     path="/stages/{id}/alternativeHeadline",
 *     tags={"stages"},
 *     description="Get stage alternativeHeadline",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/stages/{id:[0-9]+}/alternativeHeadline',
    [ $container->stageTranslationController , 'alternativeHeadline' ]
)
->setName('api.stages.alternativeHeadline') ;

/**
 * @OA\Patch(
 *     path="/stages/{id}/alternativeHeadline",
 *     tags={"stages"},
 *     description="Patch stage alternativeHeadline",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/stages/{id:[0-9]+}/alternativeHeadline',
    [ $container->stageTranslationController , 'patchAlternativeHeadline' ]
)
->setName('api.stages.alternativeHeadline.patch') ;

/**
 * @OA\Get(
 *     path="/stages/{id}/headline",
 *     tags={"stages"},
 *     description="Get stage headline",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/stages/{id:[0-9]+}/headline',
    [ $container->stageTranslationController , 'headline' ]
)
->setName('api.stages.headline') ;

/**
 * @OA\Patch(
 *     path="/stages/{id}/headline",
 *     tags={"stages"},
 *     description="Patch stage headline",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/stages/{id:[0-9]+}/headline',
    [ $container->stageTranslationController , 'patchHeadline' ]
)
->setName('api.stages.headline.patch') ;

/**
 * @OA\Get(
 *     path="/stages/{id}/notes",
 *     tags={"stages"},
 *     description="Get stage notes",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/stages/{id:[0-9]+}/notes',
    [ $container->stageTranslationController , 'notes' ]
)
->setName('api.stages.notes') ;

/**
 * @OA\Patch(
 *     path="/stages/{id}/notes",
 *     tags={"stages"},
 *     description="Patch stage notes",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/stages/{id:[0-9]+}/notes',
    [ $container->stageTranslationController , 'patchNotes' ]
)
->setName('api.stages.notes.patch') ;

/**
 * @OA\Get(
 *     path="/stages/{id}/text",
 *     tags={"stages"},
 *     description="Get stage text",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/stages/{id:[0-9]+}/text',
    [ $container->stageTranslationController , 'text' ]
)
->setName('api.stages.text') ;

/**
 * @OA\Patch(
 *     path="/stages/{id}/text",
 *     tags={"stages"},
 *     description="Patch stage text",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/stages/{id:[0-9]+}/text',
    [ $container->stageTranslationController , 'patchText' ]
)
->setName('api.stages.text.patch') ;

/**
 * @OA\Get(
 *     path="/stages/{id}/description",
 *     tags={"stages"},
 *     description="Get stage description",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W",
 *             "stages:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/stages/{id:[0-9]+}/description',
    [ $container->stageTranslationController , 'description' ]
)
->setName('api.stages.description') ;

/**
 * @OA\Patch(
 *     path="/stages/{id}/description",
 *     tags={"stages"},
 *     description="Patch stage description",
 *     security={
 *         {"OAuth2"={
 *             "stages:A",
 *             "stages:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/stages/{id:[0-9]+}/description',
    [ $container->stageTranslationController , 'patchDescription' ]
)
->setName('api.stages.description.patch') ;

