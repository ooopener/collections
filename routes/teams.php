<?php

/**
 * @OA\Tag(
 *     name="teams",
 *     description="Teams paths"
 * )
 */

require "teams/permissions.php" ;
require "teams/teams.php" ;
require "teams/translation.php" ;
