<?php

$application->get
(
    '/teams/{name}/permissions' ,
    [ $container->teamPermissionsController , 'all' ]
)
->setName('api.teams.permissions.all');