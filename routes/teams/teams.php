<?php

/**
 * @OA\Get(
 *     path="/teams",
 *     tags={"teams"},
 *     description="List teams",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W",
 *             "teams:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Parameter(ref="#/components/parameters/offset"),
 *     @OA\Parameter(ref="#/components/parameters/limit"),
 *     @OA\Parameter(name="sort",in="query",description="Sort result",@OA\Schema(type="string",default="name")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/teamListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/teams' , [ $container->teamsController , 'all'   ]
)
->setName('api.teams.all');

/**
 * @OA\Post(
 *     path="/teams",
 *     tags={"teams"},
 *     description="Create a new team",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W"
 *         }}
 *     },
 *     @OA\RequestBody(ref="#/components/requestBodies/postTeam"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/teamResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->post
(
    '/teams' , [ $container->teamsController , 'post'   ]
)
->setName('api.teams.post');

/**
 * @OA\Get(
 *     path="/teams/{id}",
 *     tags={"teams"},
 *     description="Get team",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W",
 *             "teams:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Parameter(ref="#/components/parameters/active"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/teamResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/teams/{id}' , [ $container->teamsController , 'getTeam'   ]
)
->setName('api.teams.get') ;

/**
 * @OA\Delete(
 *     path="/teams/{id}",
 *     tags={"teams"},
 *     description="Delete team",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/teams/{id}' , [ $container->teamsController , 'delete'   ]
)
->setName('api.teams.delete') ;

/**
 * @OA\Patch(
 *     path="/teams/{id}",
 *     tags={"teams"},
 *     description="Patch a team",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchTeam"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/teamResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/teams/{id}' , [ $container->teamsController , 'patch'   ]
)
->setName('api.teams.patch') ;
