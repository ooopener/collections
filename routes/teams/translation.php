<?php

/**
 * @OA\Get(
 *     path="/teams/{id}/alternateName",
 *     tags={"teams"},
 *     description="Get team alternateName",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W",
 *             "teams:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/teams/{id}/alternateName',
    [ $container->teamTranslationController , 'alternateName' ]
)
->setName('api.teams.alternateName') ;

/**
 * @OA\Patch(
 *     path="/teams/{id}/alternateName",
 *     tags={"teams"},
 *     description="Patch team alternateName",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/teams/{id}/alternateName',
    [ $container->teamTranslationController , 'patchAlternateName' ]
)
->setName('api.teams.alternateName.patch') ;

/**
 * @OA\Get(
 *     path="/teams/{id}/description",
 *     tags={"teams"},
 *     description="Get team description",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W",
 *             "teams:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/teams/{id}/description',
    [ $container->teamTranslationController , 'description' ]
)
->setName('api.teams.description') ;

/**
 * @OA\Patch(
 *     path="/teams/{id}/description",
 *     tags={"teams"},
 *     description="Patch team description",
 *     security={
 *         {"OAuth2"={
 *             "teams:A",
 *             "teams:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchText"),
 *     @OA\Response(
 *         response="200",
 *         description="Result",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",ref="#/components/schemas/text")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/teams/{id}/description',
    [ $container->teamTranslationController , 'patchDescription' ]
)
->setName('api.teams.description.patch') ;

