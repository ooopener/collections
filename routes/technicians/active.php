<?php

$application->get
(
    '/technicians/{id:[0-9]+}/active' ,
    [ $container->technicianActiveController , 'get'  ]
)
->setName('api.technicians.active.get') ;

$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/technicians/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' ,
    [ $container->technicianActiveController , 'patch' ]
)
->setName('api.technicians.active.patch') ;

