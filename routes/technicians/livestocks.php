<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/technicians/{id:[0-9]+}/livestocks',
    [ $container->livestockTechniciansController , 'allReverse' ]
)
->setName('api.technicians.livestocks.allReverse') ;

$application->options
(
    '/technicians/{owner:[0-9]+}/livestocks/{id:[0-9]+}',
    null
);

$application->post
(
    '/technicians/{owner:[0-9]+}/livestocks/{id:[0-9]+}',
    [ $container->livestockTechniciansController , 'postReverse' ]
)
->setName('api.technicians.livestocks.postReverse') ;

$application->get
(
    '/technicians/{id:[0-9]+}/livestocks/count' ,
    [ $container->livestockTechniciansController , 'countReverse' ]
)
->setName('api.technicians.livestocks.countReverse') ;

$application->delete
(
    '/technicians/{owner:[0-9]+}/livestocks/{id:[0-9]+}',
    [ $container->livestockTechniciansController , 'deleteReverse' ]
)
->setName('api.technicians.livestocks.deleteReverse') ;

