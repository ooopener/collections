<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/technicians/{id:[0-9]+}/medicalSpecialties',
    [ $container->technicianMedicalSpecialtiesController , 'all' ]
)
->setName('api.technicians.medicalSpecialties.all') ;

$application->options
(
    '/technicians/{owner:[0-9]+}/medicalSpecialties/{id:[0-9]+}',
    null
) ;

$application->post
(
    '/technicians/{owner:[0-9]+}/medicalSpecialties/{id:[0-9]+}',
    [ $container->technicianMedicalSpecialtiesController , 'post' ]
)
->setName('api.technicians.medicalSpecialties.post') ;

$application->get
(
    '/technicians/{id:[0-9]+}/medicalSpecialties/count' ,
    [ $container->technicianMedicalSpecialtiesController , 'count' ]
)
->setName('api.technicians.medicalSpecialties.count') ;

$application->delete
(
    '/technicians/{owner:[0-9]+}/medicalSpecialties/{id:[0-9]+}',
    [ $container->technicianMedicalSpecialtiesController , 'delete' ]
)
->setName('api.technicians.medicalSpecialties.delete') ;

