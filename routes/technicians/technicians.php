<?php

$application->delete
(
    '/technicians' , [ $container->techniciansController , 'deleteAll'   ]
)
->setName('api.technicians.delete.all') ;

$application->delete
(
    '/technicians/{id:[0-9]+}' ,
    [ $container->techniciansController , 'delete' ]
)
->setName('api.technicians.delete');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/technicians' , [ $container->techniciansController , 'all'   ]
)
->setName('api.technicians.all');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/technicians/{id:[0-9]+}' ,
    [ $container->techniciansController , 'get' ]
)
->setName('api.technicians.get');

$application->post
(
    '/technicians' , [ $container->techniciansController , 'post'   ]
)
->setName('api.technicians.post') ;

$application->put
(
    '/technicians/{id:[0-9]+}' ,
    [ $container->techniciansController , 'put' ]
)
->setName('api.technicians.put');