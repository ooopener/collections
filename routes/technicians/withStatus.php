<?php

$application->get
(
    '/technicians/{id:[0-9]+}/withStatus' ,
    [ $container->technicianWithStatusController , 'get'  ]
)
->setName('api.technicians.withStatus.get') ;

$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/technicians/{id:[0-9]+}/withStatus/{status}' ,
    [ $container->technicianWithStatusController , 'patch' ]
)
->setName('api.technicians.withStatus.patch') ;

