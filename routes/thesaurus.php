<?php

/**
 *
 * @OA\Tag(
 *     name="thesaurus",
 *     description="Thesaurus paths"
 * )
 *
 * @OA\Get(
 *     path="/thesaurus",
 *     tags={"thesaurus"},
 *     description="List of available thesauri",
 *     security={
 *         {"OAuth2"={
 *             "thesaurus:A",
 *             "thesaurus:W",
 *             "thesaurus:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/thesauriResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/thesaurus' , [ $container->thesaurusListController , 'all' ]
)
->setName('api.thesaurus.all');


