<?php

/**
 * @OA\Tag(
 *     name="users",
 *     description="Users paths"
 * )
 */

require "users/activityLogs.php" ;
require "users/address.php" ;
require "users/permissions.php" ;
require "users/sessions.php" ;
require "users/users.php" ;
