<?php

/**
 * @OA\Get(
 *     path="/users/{uuid}/address",
 *     tags={"users"},
 *     description="Get address user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W",
 *             "users:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/users/{id}/address' ,
    [ $container->userPostalAddressController , 'get' ]
)
->setName('api.users.address');

/**
 * @OA\Patch(
 *     path="/users/{uuid}/address",
 *     tags={"users"},
 *     description="Patch address user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\RequestBody(ref="#/components/requestBodies/patchAddress"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",ref="#/components/schemas/PostalAddress")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/users/{id}/address' ,
    [ $container->userPostalAddressController , 'patch' ]
)
->setName('api.users.address.patch');
