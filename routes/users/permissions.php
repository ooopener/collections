<?php

$application->get
(
    '/users/{id}/permissions' ,
    [ $container->userPermissionsController , 'all' ]
)
->setName('api.users.permissions.all');