<?php

/**
 * @OA\Get(
 *     path="/users/{uuid}/sessions/{active}",
 *     tags={"users"},
 *     description="List sessions user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W",
 *             "users:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Parameter(ref="#/components/parameters/activeSession"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="count",type="integer",description="Count of items"),
 *             @OA\Property(property="total",type="integer",description="Total of items"),
 *             @OA\Property(property="result",description="List of sessions",type="array",items=@OA\Items(ref="#/components/schemas/UserSession"))
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/users/{id}/sessions/{active:true|false|TRUE|FALSE}' ,
    [ $container->userSessionsController , 'allByUser' ]
)
->setName('api.users.sessions.active');

/**
 * @OA\Patch(
 *     path="/users/{uuid}/sessions/all",
 *     tags={"users"},
 *     description="Revoke all sessions user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/users/{id}/sessions/all' ,
    [ $container->userSessionsController , 'revokeAll' ]
)
->setName('api.users.sessions.revoke.all');

/**
 * @OA\Patch(
 *     path="/users/{uuid}/sessions",
 *     tags={"users"},
 *     description="Revoke sessions user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->patch
(
    '/users/{id}/sessions' ,
    [ $container->userSessionsController , 'revokeAllList' ]
)
->setName('api.users.sessions.revoke.allList');

/**
 * @OA\Patch(
 *     path="/users/{uuid}/sessions/{id}",
 *     tags={"users"},
 *     description="Revoke session user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Parameter(ref="#/components/parameters/id"),
 *     @OA\Response(
 *         response="200",
 *         description="",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="status", type="string",description="The request status",example="success"),
 *             @OA\Property(property="result",type="boolean",default="true")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/users/{id}/sessions/{session}' ,
    [ $container->userSessionsController , 'revoke' ]
)
->setName('api.users.sessions.revoke');
