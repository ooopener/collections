<?php

/**
 * @OA\Get(
 *     path="/users",
 *     tags={"users"},
 *     description="List users",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W",
 *             "users:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/offset"),
 *     @OA\Parameter(ref="#/components/parameters/limit"),
 *     @OA\Parameter(name="sort",in="query",description="Sort result",@OA\Schema(type="string",default="name")),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/userListResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/users' , [ $container->usersController , 'all'   ]
)
->setName('api.users.all');

/**
 * @OA\Delete(
 *     path="/users",
 *     tags={"users"},
 *     description="Delete users from list",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                     property="list",
 *                     type="array",
 *                     description="Array of IDs separated with comma",
 *                     @OA\Items(type="integer")
 *                 ),
 *                 required={"list"},
 *                 example={"list","101,503"}
 *             )
 *         ),
 *         required=true
 *     ),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/DeleteList"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/NotValidList"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/users' , [ $container->usersController , 'deleteAll' ]
)
->setName('api.users.delete.all');

/**
 * @OA\Get(
 *     path="/users/count",
 *     tags={"users"},
 *     description="Count users",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W",
 *             "users:R"
 *         }}
 *     },
 *     @OA\Response(
 *         response="200",
 *         description="Count of users",
 *         @OA\JsonContent(
 *             type="object",
 *             allOf={@OA\Schema(ref="#/components/schemas/success")},
 *             @OA\Property(property="result",description="Number of users",type="integer")
 *         )
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->get
(
    '/users/count' , [ $container->usersController , 'count'   ]
)
->setName('api.users.count');

/**
 * @OA\Get(
 *     path="/users/{uuid}",
 *     tags={"users"},
 *     description="Get an user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W",
 *             "users:R"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/userResponse"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/users/{id}' , [ $container->usersController , 'getUuid'   ]
)
->setName('api.users.profile') ;

/**
 * @OA\Delete(
 *     path="/users/{uuid}",
 *     tags={"users"},
 *     description="Delete an user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/Delete"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         ref="#/components/responses/NotFound"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->delete
(
    '/users/{id}' , [ $container->usersController , 'delete' ]
)
->setName('api.users.delete');

/**
 * @OA\Put(
 *     path="/users/{uuid}",
 *     tags={"users"},
 *     description="Edit an user",
 *     security={
 *         {"OAuth2"={
 *             "users:A",
 *             "users:W"
 *         }}
 *     },
 *     @OA\Parameter(ref="#/components/parameters/uuid"),
 *     @OA\RequestBody(ref="#/components/requestBodies/putUser"),
 *     @OA\Response(
 *         response="200",
 *         ref="#/components/responses/userResponse"
 *     ),
 *     @OA\Response(
 *         response="400",
 *         ref="#/components/responses/ErrorParameters"
 *     ),
 *     @OA\Response(
 *         response="401",
 *         ref="#/components/responses/Unauthorized"
 *     ),
 *     @OA\Response(
 *         response="500",
 *         ref="#/components/responses/Unexpected"
 *     )
 * )
 */
$application->put
(
    '/users/{id}' , [ $container->usersController , 'put'   ]
)
->setName('api.users.put');
