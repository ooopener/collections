<?php

$application->get
(
    '/veterinarians/{id:[0-9]+}/active' ,
    [ $container->veterinarianActiveController , 'get'  ]
)
->setName('api.veterinarians.active.get') ;

$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/veterinarians/{id:[0-9]+}/active/{bool:true|false|TRUE|FALSE}' ,
    [ $container->veterinarianActiveController , 'patch' ]
)
->setName('api.veterinarians.active.patch') ;

