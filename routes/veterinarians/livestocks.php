<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/veterinarians/{id:[0-9]+}/livestocks',
    [ $container->livestockVeterinariansController , 'allReverse' ]
)
->setName('api.veterinarians.livestocks.allReverse') ;

$application->options
(
    '/veterinarians/{owner:[0-9]+}/livestocks/{id:[0-9]+}',
    null
);

$application->post
(
    '/veterinarians/{owner:[0-9]+}/livestocks/{id:[0-9]+}',
    [ $container->livestockVeterinariansController , 'postReverse' ]
)
->setName('api.veterinarians.livestocks.postReverse') ;

$application->get
(
    '/veterinarians/{id:[0-9]+}/livestocks/count' ,
    [ $container->livestockVeterinariansController , 'countReverse' ]
)
->setName('api.veterinarians.livestocks.countReverse') ;

$application->delete
(
    '/veterinarians/{owner:[0-9]+}/livestocks/{id:[0-9]+}',
    [ $container->livestockVeterinariansController , 'deleteReverse' ]
)
->setName('api.veterinarians.livestocks.deleteReverse') ;

