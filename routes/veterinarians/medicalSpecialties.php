<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/veterinarians/{id:[0-9]+}/medicalSpecialties',
    [ $container->veterinarianMedicalSpecialtiesController , 'all' ]
)
->setName('api.veterinarians.medicalSpecialties.all') ;

$application->options
(
    '/veterinarians/{owner:[0-9]+}/medicalSpecialties/{id:[0-9]+}',
    null
) ;

$application->post
(
    '/veterinarians/{owner:[0-9]+}/medicalSpecialties/{id:[0-9]+}',
    [ $container->veterinarianMedicalSpecialtiesController , 'post' ]
)
->setName('api.veterinarians.medicalSpecialties.post') ;

$application->get
(
    '/veterinarians/{id:[0-9]+}/medicalSpecialties/count' ,
    [ $container->veterinarianMedicalSpecialtiesController , 'count' ]
)
->setName('api.veterinarians.medicalSpecialties.count') ;

$application->delete
(
    '/veterinarians/{owner:[0-9]+}/medicalSpecialties/{id:[0-9]+}',
    [ $container->veterinarianMedicalSpecialtiesController , 'delete' ]
)
->setName('api.veterinarians.medicalSpecialties.delete') ;

