<?php

$application->delete
(
    '/veterinarians' , [ $container->veterinariansController , 'deleteAll'   ]
)
->setName('api.veterinarians.delete.all') ;

$application->delete
(
    '/veterinarians/{id:[0-9]+}' ,
    [ $container->veterinariansController , 'delete' ]
)
->setName('api.veterinarians.delete');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/veterinarians' , [ $container->veterinariansController , 'all'   ]
)
->setName('api.veterinarians.all');

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/veterinarians/{id:[0-9]+}' ,
    [ $container->veterinariansController , 'get' ]
)
->setName('api.veterinarians.get');

$application->post
(
    '/veterinarians' , [ $container->veterinariansController , 'post'   ]
)
->setName('api.veterinarians.post') ;

$application->put
(
    '/veterinarians/{id:[0-9]+}' ,
    [ $container->veterinariansController , 'put' ]
)
->setName('api.veterinarians.put');