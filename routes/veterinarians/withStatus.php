<?php

$application->get
(
    '/veterinarians/{id:[0-9]+}/withStatus' ,
    [ $container->veterinarianWithStatusController , 'get'  ]
)
->setName('api.veterinarians.withStatus.get') ;

$application->map
(
    [ 'PATCH' , 'OPTIONS' ] ,
    '/veterinarians/{id:[0-9]+}/withStatus/{status}' ,
    [ $container->veterinarianWithStatusController , 'patch' ]
)
->setName('api.veterinarians.withStatus.patch') ;

