<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Model;



use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Container;

use Exception ;


/**
 * The active controller class.
 *
 * @OA\Schema(
 *     schema="successActive",
 *     allOf={},
 *     @OA\Property(property="active",type="boolean"),
 *     @OA\Property(property="modified",type="string",format="date-time"),
 *     @OA\Property(property="created",type="string",format="date-time"),
 *     @OA\Property(property="id",type="integer"),
 *     @OA\Property(property="url",type="string",format="uri")
 * )
 */
class ActiveController extends Controller
{
    /**
     * Creates a new ActiveController instance.
     *
     * @param Container $container
     * @param Model $model
     * @param string $path
     */
    public function __construct( Container $container , Model $model = NULL , $path = NULL )
    {
        parent::__construct( $container );
        $this->model = $model ;
        if( !empty($path) )
        {
            $this->path     = $path ;
            $this->fullPath = '/' . $path ;
        }
    }

    /**
     * The full path expression.
     */
    public $fullPath ;

    /**
     * The model reference.
     */
    public $model ;

    /**
     * The main route path expression.
     */
    public $path ;

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'   => NULL
    ] ;

    /**
     * Returns if the specific item is active.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id , [ 'fields' => '_key,active,modified,created' ] ) ;

            if( !$item )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $item->id = $item->_key ; // set id
            $item->active = (bool)$item->active ; // set bool
            unset( $item->_key ) ; // clean
            $item->url = $this->getFullPath() ; // set url

            // set ISO 8601 dates
            $item->created = $this->container->iso8601->formatTimeToISO8601( $item->created , TRUE ) ;
            $item->modified = $this->container->iso8601->formatTimeToISO8601( $item->modified , TRUE ) ;

            return $this->success( $response , $item ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'   => NULL ,
        'bool' => NULL
    ] ;

    /**
     * Switch the activity of the specific item.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id ) ;

            if( is_null($item) )
            {
                return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
            }

            if( is_string( $bool ) )
            {
                $bool = strtolower( $bool ) ;
            }

            if( $bool != 'true' && $bool != 'false' )
            {
                return $this->formatError( $response , '400', [ 'patch(' . $id . ',' . $bool . ')' ] );
            }

            $active = $bool == 'true' ;


            $result = $this->model->update( [ 'active' => (int)$active ] , $id ) ;

            if( $result )
            {
                $obj = $this->model->get( $id , [ 'fields' => '_key,active,modified,created' ] ) ;
                $obj->id = $obj->_key ; // set id
                $obj->active = (bool)$obj->active ; // set bool
                unset( $obj->_key ) ; // clean
                $obj->url = $this->getFullPath() ; // set url

                // set ISO 8601 dates
                $obj->created = $this->container->iso8601->formatTimeToISO8601( $obj->created , TRUE ) ;
                $obj->modified = $this->container->iso8601->formatTimeToISO8601( $obj->modified , TRUE ) ;

                return $this->success( $response , $obj ) ;
            }

            return $this->error( $response , 'error' ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}


