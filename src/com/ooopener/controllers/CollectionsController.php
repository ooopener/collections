<?php

namespace com\ooopener\controllers;

use com\ooopener\things\Thing;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use Exception ;

use com\ooopener\models\Collections;

/**
 * The collections controller.
 */
class CollectionsController extends Controller
{
    /**
     * Creates a new CollectionsController instance.
     *
     * @param Container $container
     * @param Collections $model
     * @param string $path
     *
     */
    public function __construct( Container $container , Collections $model = NULL , $path = NULL )
    {
        parent::__construct( $container );
        $this->model    = $model ;
        $this->path     = empty($path) ? '' : $path ;
        $this->fullPath = '/' . $path ;
    }

    /**
     * @OA\Parameter(
     *     name="active",
     *     in="query",
     *     description="Resource active",
     *     @OA\Schema(type="boolean",default=true)
     * )
     *
     * @OA\Parameter(
     *     name="offset",
     *     in="query",
     *     description="The number of items to skip before starting to collect the result set",
     *     @OA\Schema(type="integer",default="0",minimum=0)
     * )
     *
     * @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="The numbers of items to return",
     *     @OA\Schema(type="integer",default=100,minimum=1,maximum=1000)
     * )
     *
     * @OA\RequestBody(
     *     request="patchText",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/text")
     *     ),
     *     required=true
     * )
     *
     * @OA\Response(
     *     response="Delete",
     *     description="Delete",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result", type="integer",example="103")
     *     )
     * )
     *
     * @OA\Response(
     *     response="DeleteList",
     *     description="Delete from IDs",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result", type="string",example="ok")
     *     )
     * )
     *
     * @OA\Response(
     *     response="ErrorParameters",
     *     description="Error in parameters"
     * )
     *
     * @OA\Response(
     *     response="NotFound",
     *     description="Resource not found",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",example="error"),
     *         @OA\Property(property="code", type="string",example="404"),
     *         @OA\Property(property="message", type="string",example="Not Found")
     *     )
     * )
     *
     * @OA\Response(
     *     response="NotValidList",
     *     description="Some identifiers in the list are not valid",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",example="error"),
     *         @OA\Property(property="message", type="string",example="some identifiers in the list are not valid")
     *     )
     * )
     *
     * @OA\Schema(
     *     schema="Thing",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(type="string",property="url",format="uri",description="URL of the resource"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     *
     * @OA\Schema(
     *     schema="agent",
     *     anyOf={@OA\Schema(ref="#/components/schemas/PersonList"),@OA\Schema(ref="#/components/schemas/OrganizationList")}
     * )
     *
     * @OA\Schema(
     *     schema="status",
     *     @OA\Property(type="integer",property="active",minimum=0,maximum=1,description="Resource activated"),
     *     @OA\Property(property="withStatus",ref="#/components/schemas/withStatus")
     * )
     *
     * @OA\Schema(
     *     schema="altText",
     *     @OA\Property(property="alternateName",description="The alternate name of the resource",ref="#/components/schemas/text"),
     *     @OA\Property(property="description",description="The description of the resource",ref="#/components/schemas/text")
     * )
     *
     * @OA\Schema(
     *     schema="longText",
     *     @OA\Property(property="text",description="The text of the resource in HTML",ref="#/components/schemas/text"),
     *     @OA\Property(property="notes",description="The notes of the resource in HTML",ref="#/components/schemas/text")
     * )
     *
     * @OA\Schema(
     *     schema="headlineText",
     *     @OA\Property(property="headline",description="The headline of the resource",ref="#/components/schemas/text"),
     *     @OA\Property(property="alternativeHeadline",description="The alternative headline of the resource",ref="#/components/schemas/text"),
     *     @OA\Property(property="description",description="The description of the resource",ref="#/components/schemas/text")
     * )
     *
     * @OA\Schema(
     *     schema="text",
     *     type="object",
     *     @OA\AdditionalProperties(type="string"),
     *     example={"en":"English","fr":"Français"}
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'image'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'alternativeHeadline' => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'alternateName'       => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'description'         => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'headline'            => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'slogan'              => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
    ] ;

    /**
     * The full path expression.
     */
    public $fullPath ;

    /**
     * The model reference.
     */
    public $model ;

    /**
     * The path expression.
     */
    public $path ;

    ///////////////////////////

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return mixed
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        return $init ;
    }

    public function sortAfter( $items )
    {
        $sortable = $this->model->sortable ;

        if( $sortable && array_key_exists( 'after' , $sortable ) )
        {
            $after = explode( '.' , $sortable['after'] ) ;

            if( $after && is_array( $after ) && count( $after ) == 2 )
            {
                usort( $items , function ( $aItem , $bItem ) use ( $after )
                {
                    return strcmp( $aItem->{$after[0]}->{$after[1]} , $bItem->{$after[0]}->{$after[1]} ) ;
                }) ;
            }
        }
        return $items ;
    }

    public function getFields( $skin = NULL )
    {
        $fields = [] ;

        foreach( static::CREATE_PROPERTIES as $key => $options )
        {
            $filter = array_key_exists( 'filter' , $options ) ? $options['filter'] : NULL ;
            $skins = array_key_exists('skins', $options) ? $options['skins'] : NULL ;

            if( isset($skins) && is_array($skins) )
            {
                if( is_null($skin) || !in_array( $skin, $skins ) )
                {
                    continue;
                }
            }

            $fields[$key] = [ 'filter' => $filter ] ;

            //// set unique value for edge, edgeSingle and join filters
            switch( $filter )
            {
                case Thing::FILTER_EDGE :
                case Thing::FILTER_EDGE_SINGLE :
                case Thing::FILTER_EDGE_COUNT :
                    $fields[$key]['unique'] = $key . '_e' . mt_rand() ;
                    break;
                case Thing::FILTER_JOIN :
                case Thing::FILTER_JOIN_ARRAY :
                case Thing::FILTER_JOIN_MULTIPLE :
                    $fields[$key]['unique'] = $key . '_j' . mt_rand() ;
                    break;
                case Thing::FILTER_UNIQUE_NAME :
                    $fields[$key]['unique'] = $key . '_u' . mt_rand() ;
                    break;
            }
        }

        return $fields ;
    }

    ///////////////////////////

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'  => TRUE ,
        'conditions' => [] ,
        'facets'  => NULL ,
        'groupBy' => NULL ,
        'items'   => NULL ,
        'lang'    => NULL ,
        'limit'   => 0 ,
        'offset'  => NULL ,
        'options' => NULL ,
        'search'  => NULL ,
        'sort'    => NULL ,
        'params'  => NULL
    ] ;

    /**
     * Returns all the elements.
     * Ex: ../element?search=film
     * Ex: ../element?facets={"location":12}
     * Ex: ../element?facets={"type":"-event,visual/exhibition","eventStatus":"-scheduled"}
     * @param $request
     * @param $response
     * @param $args array init ex: [ 'limit' => 0 ]
     *
     * @return mixed
     */
    public function all( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all' ) ;
        }

        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        if( isset( $request ) )
        {
            $params = isset( $params ) ? $params : $request->getQueryParams();


            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }

            }

            // ----- limit

            $limit = intval( isset( $limit ) ? $limit : $api[ 'limit_default' ] );
            if( isset( $params[ 'limit' ] ) )
            {
                $limit             = filter_var
                (
                    $params[ 'limit' ] ,
                    FILTER_VALIDATE_INT ,
                    [
                        'options' =>
                            [
                                "min_range" => intval( $api[ 'minlimit' ] ) ,
                                "max_range" => intval( $api[ 'maxlimit' ] )
                            ]
                    ]
                );
                $params[ 'limit' ] = intval( ( $limit !== FALSE ) ? $limit : $api[ 'limit_default' ] );
            }

            // ----- offset

            $offset = intval( isset( $offset ) ? $offset : $api[ 'offset_default' ] );
            if( isset( $params[ 'offset' ] ) )
            {
                $offset             = filter_var
                (
                    $params[ 'offset' ] ,
                    FILTER_VALIDATE_INT ,
                    [
                        'options' =>
                            [
                                "min_range" => intval( $api[ 'minlimit' ] ) ,
                                "max_range" => intval( $api[ 'maxlimit' ] )
                            ]
                    ]
                );
                $params[ 'offset' ] = intval( ( $offset !== FALSE ) ? $offset : $api[ 'offset_default' ] );
            }

            // ----- facets

            if( isset( $params[ 'facets' ] ) )
            {

                try
                {
                    if( is_string( $params['facets'] ) )
                    {
                        $facets = array_merge
                        (
                            json_decode( $params[ 'facets' ] , TRUE ) ,
                            isset( $facets ) ? $facets : []
                        );
                    }
                    else
                    {
                        $facets = $params['facets'] ;
                    }

                }
                catch( Exception $e )
                {
                    $this->container->logger->warn( $this . ' all failed, the facets params failed to decode the json expression: ' . $params[ 'facets' ] );
                }
            }

            // ----- sort

            if( isset( $params[ 'sort' ] ) )
            {
                $sort = $params[ 'sort' ];
            }
            else if( is_null( $sort ) )
            {
                $sort = $set[ 'sort_default' ];
            }

            // ----- groupBy

            if( isset( $params[ 'groupBy' ] ) )
            {
                $groupBy = $params[ 'groupBy' ];
            }

            // ----- search

            if( isset( $params[ 'search' ] ) )
            {
                $search = $params[ 'search' ];
            }

            // ----- active

            if( isset( $params[ 'active' ] ) )
            {
                if
                (
                    $params[ 'active' ] == "0" ||
                    $params[ 'active' ] == 'false' ||
                    $params[ 'active' ] == 'FALSE'
                )
                {
                    $active = NULL;
                }
            }


            // ----- permissions

            try
            {
                $jwt = $this->container->jwt ;
                $permissions = json_decode( $jwt->scope ) ;

                if( $permissions && property_exists( $permissions , $this->model->table ) && $permissions->{$this->model->table} == 'read' )
                {
                    $list = [] ;
                    foreach ( $permissions as $key => $value )
                    {
                        if( preg_match( '/^'.$this->model->table.'\/[0-9]+$/' , $key , $matches ) )
                        {
                            array_push( $list , substr(strrchr( $key , '/' ) , 1 ) ) ;
                        }
                    }

                    if( !empty( $list ) )
                    {
                        $conditions = [ 'ID in (' . implode(',' , $list ) . ')' ] ;
                    }
                }
            }
            catch ( Exception $e )
            {
                $this->logger->error( $this . ' all -- permissions' ) ;
            }

        }

        // ------------

        try
        {
            $set = $this->container->settings[$this->path] ;

            $skin = NULL ;

            // ----- skin

            if( !isset($skin) && array_key_exists( 'skin_default' ,$set ) )
            {
                if( array_key_exists( 'skin_all', $set)  )
                {
                    $skin = $set['skin_all'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }

            $init = [
                'active'      => $active ,
                'conditions'  => $conditions ,
                'facets'      => $facets ,
                'search'      => $search
            ];

            $items = $this->model->all
            ([
                'active'      => $active ,
                'conditions'  => $conditions ,
                'facets'      => $facets ,
                'limit'       => $limit ,
                'offset'      => $offset ,
                'sort'        => $sort,
                'groupBy'     => $groupBy,
                'search'      => $search,
                'lang'        => $lang,
                'queryFields' => $this->getFields( $skin )
            ]) ;

            $options = [ 'total' => $limit != 0 ? $this->model->count( $init ) : count( $items ) ] ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'all' , $e->getMessage() ] , NULL , 500 );
        }

        return $this->outputAll( $request , $response , $items , $params , $options ) ;
    }

    /**
     * The default 'count' method options.
     */
    const ARGUMENTS_COUNT_DEFAULT =
    [
        'active' => TRUE,
        'conditions' => [] ,
        'facets' => NULL,
        'search' => NULL
    ] ;

    /**
     * Returns the number of items.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed the number of items.
     */
    public function count( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_COUNT_DEFAULT , $args ) ) ;

        $count = 0 ;

        if( $response )
        {
            $this->logger->debug( $this . ' count' ) ;
        }

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- active

            if( isset( $params[ 'active' ] ) )
            {
                if
                (
                    $params[ 'active' ] == "0" ||
                    $params[ 'active' ] == 'false' ||
                    $params[ 'active' ] == 'FALSE'
                )
                {
                    $active = NULL;
                }
            }

            // ----- facets

            if( isset($params['facets']) )
            {
                $facets = $params['facets'] ;

                try
                {
                    if( is_string( $params['facets'] ) )
                    {
                        $facets = json_decode($params['facets'] , TRUE ) ;
                    }
                }
                catch( Exception $e )
                {
                    $this->container->logger->warn( $this . ' all failed, the facets params failed to decode the json expression: ' . $params['facets'] ) ;
                }
            }

            // ----- search

            if( isset($params['search']) )
            {
                $search = $params['search'] ;
            }
        }

        // ------------

        try
        {
            $count = $this->model->count
            ([
                'active' => $active ,
                'conditions' => $conditions ,
                'facets' => $facets ,
                'search' => $search
            ]) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'count()', $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            return $this->success( $response , $count , $this->getFullPath() ) ;
        }

        return $count ;
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id' => NULL,
        'conditions' => []
    ] ;

    /**
     * Delete item
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        $this->logger->debug( $this . ' delete(' . $id . ')' ) ;

        try
        {
            if( !$this->model->exist( $id , [ 'conditions' => $conditions ] ) )
            {
                return $this->error( $response , null , '404' , null , 404 ) ;
            }

            $result = $this->model->delete( $id , [ 'conditions' => $conditions ] );

            if( $result )
            {
                return $this->success( $response , (int) $result->_key );
            }

            return $this->error( $response , 'not deleted' ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'delete all' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'conditions' => []
    ] ;

    /**
     * Delete all items
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ) ;

        $this->logger->debug( $this . ' deleteAll()' ) ;

        $params = $request->getParsedBody() ;

        if( $params )
        {
            if( isset( $params['list' ]) )
            {
                $list = $params['list'] ;

                try
                {
                    $items = explode( ',' , $list ) ;

                    $count = $this->model->existAll( $items , [ 'conditions' => $conditions ] ) ;

                    if( $count == count( $items ) )
                    {
                        // delete them all
                        foreach( $items as $item )
                        {
                            // delete
                            $result = $this->delete( NULL , NULL , [ 'id' => (string) $item ] ) ;
                            if( $result instanceof Response && $result->getStatusCode() != 200 )
                            {
                                return $result ;
                            }
                        }
                        if( $response )
                        {
                            return $this->success( $response , "ok" );
                        }

                        return true ;

                    }

                    return $this->error( $response , 'some identifiers in the list are not valid' ) ;

                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'deleteAll()' , $e->getMessage() ] , NULL , 500 );
                }
            }
        }

        return $this->error( $response , 'no list' ) ;
    }

    /**
     * The default 'deleteImage' methods options.
     */
    const ARGUMENTS_DELETE_IMAGE_DEFAULT =
    [
        'id'   => NULL
    ] ;

    public function deleteImage( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_IMAGE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteImage(' . $id . ')' ) ;
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'deleteImage(' . $id . ')' ], NULL , 404 );
            }

            // remove logo id in database
            $this->model->update( [ 'image' => NULL ] , $id ) ;

            return $this->success( $response , (int) $id ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteImage(' . $id . ')' , $e->getMessage() ], NULL , 500 );
        }
    }


    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'active'  => TRUE ,
        'conditions' => [] ,
        'id'      => NULL ,
        'item'    => NULL ,
        'lang'    => NULL ,
        'params'  => NULL ,
        'skin'    => NULL
    ] ;

    /**
     * Get a specific item reference.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        // ------------ init

        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }

            }

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( strtolower($params['skin']) , $set['skins'] ) )
                {
                    $params['skin'] = $skin = strtolower($params['skin']) ;
                }

            }

            // ----- active

            if( isset( $params[ 'active' ] ) )
            {
                if
                (
                    $params[ 'active' ] == "0" ||
                    $params[ 'active' ] == 'false' ||
                    $params[ 'active' ] == 'FALSE'
                )
                {
                    $active = NULL;
                }
            }
        }

        if( $skin == 'main' || !in_array( strtolower($skin) , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        // ----------------

        try
        {
            $item = $this->model->get( $id , [ 'active' => $active , 'conditions' => $conditions , 'lang' => $lang , 'queryFields' => $this->getFields( $skin ) ] ) ;

            if( $item )
            {
                $item = $this->create( $item , $lang , $skin , $params ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                // add header
                $response = $this->container->cache->withETag( $response , $item->modified );
                $response = $this->container->cache->withLastModified( $response , $item->modified );

                return $this->success( $response, $item, $this->getFullPath( $params ) );
            }

            return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;

        }

        return $item ;
    }

    /**
     * The default 'get image' method options.
     */
    const ARGUMENTS_GET_IMAGE_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * Display the image of the specific item.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return Response the image of the specific item
     */
    public function getImage( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_IMAGE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' getImage(' . $id . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id , [ 'fields' => 'image' ] ) ;

            if( is_null($item) && property_exists( $item , 'image') )
            {
                return $this->formatError( $response , '404', [ 'getImage(' . $id . ')' ], NULL , 404 );
            }

            // test media exists
            $exists = $this->container->mediaObjectsController->get( NULL , NULL , [ 'id' => $item->image ] ) ;

            if( !$exists )
            {
                return $this->formatError( $response , '404', [ 'getImage(' . $id . ')' ], NULL , 404 );
            }

            return $this->success( $response , $exists ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'getImage(' . $id . ')' , $e->getMessage() ], NULL , 500 );
        }
    }

    /**
     * The default 'patchImage' methods options.
     */
    const ARGUMENTS_PATCH_IMAGE_DEFAULT =
    [
        'id'    => NULL ,
        'media' => NULL
    ] ;

    public function patchImage( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_IMAGE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patchImage(' . $id . ')' ) ;
        }

        if( isset( $request ) )
        {
            $params = $request->getParsedBody() ;

            if( !empty($params['media']) )
            {
                $media = $params['media'] ;
            }
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'patchImage(' . $id . ')' ], NULL , 404 );
            }

            // test media exists
            $exists = $this->container->mediaObjectsController->get( NULL , NULL , [ 'id' => (string)$media , 'conditions' => [ 'doc.encodingFormat =~ "image"' ] ] ) ;

            if( !$exists )
            {
                return $this->formatError( $response , '404', [ 'patchImage(' . $id . ')' ], NULL , 404 );
            }

            // update database
            $this->model->update( [ 'image' => (int)$media ] , $id ) ;

            return $this->success( $response , $exists ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patchImage(' . $id . ')' , $e->getMessage() ], NULL , 500 );
        }
    }

    /**
     * Outputs the elements.
     *
     * @param Request $request
     * @param Response $response
     * @param array $items
     * @param array $params
     * @param array $options
     *
     * @return mixed
     */
    protected function outputAll( Request $request = NULL, Response $response = NULL , $items = NULL , $params = NULL , array $options = NULL )
    {
        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        $skin = NULL ;
        $lang = NULL ;

        if( isset( $request ) )
        {
            $params = isset($params) ? $params : $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }

            }

            // ----- skin

            if( !isset($skin) && array_key_exists( 'skin_default' ,$set ) )
            {
                if( array_key_exists( 'skin_all', $set)  )
                {
                    $skin = $set['skin_all'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( strtolower($params['skin']) , $set['skins'] ) )
                {
                    $params['skin'] = $skin = strtolower($params['skin']) ;
                }

            }

            if( $skin == 'main' || !in_array( strtolower($skin) , $set['skins'] ) )
            {
                $skin = NULL ;
            }
        }

        if( $items )
        {
            foreach( $items as $key => $value )
            {
                $items[$key] = $this->create( $value , $lang , $skin , $params ) ;
            }

            $items = $this->sortAfter( $items ) ;
        }

        if( $response )
        {
            return $this->success
            (
                $response,
                $items,
                $this->getFullPath( $params ) ,
                is_array($items) ? count($items) : NULL,
                $options
            );
        }

        return $items ;
    }
}


