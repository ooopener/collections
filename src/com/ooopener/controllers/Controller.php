<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Edges;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use core\Strings;

/**
 * The abstract Controller class.
 *
 * @OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="Resource ID",
 *     required=true,
 *     @OA\Schema(type="integer")
 * )
 *
 * @OA\Parameter(
 *     name="owner",
 *     in="path",
 *     description="Resource ID",
 *     required=true,
 *     @OA\Schema(type="integer")
 * )
 *
 * @OA\Response(
 *     response="BadRequest",
 *     description="Bad request",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="status", type="string",example="error"),
 *         @OA\Property(property="code", type="string",example="400"),
 *         @OA\Property(property="result", type="string",example="Bad request")
 *     )
 * )
 *
 * @OA\Response(
 *     response="Unauthorized",
 *     description="Not authorized",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="status", type="string",example="error JWT"),
 *         @OA\Property(property="message", type="string",example="Token not found")
 *     )
 * )
 *
 * @OA\Response(
 *     response="Unexpected",
 *     description="Unexpected error",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="status", type="string",example="error"),
 *         @OA\Property(property="code", type="string",example="500"),
 *         @OA\Property(property="result", type="string",example="Internal Server Error")
 *     )
 * )
 *
 * @OA\Schema(
 *     schema="success",
 *     description="Request success",
 *     @OA\Property(property="status", type="string",description="The request status",example="success"),
 *     @OA\Property(property="url", type="string",format="uri",description="URL of the request")
 * )
 *
 */
abstract class Controller
{
    /**
     * Creates a new Controller instance.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        $this->container = $container ;

        if( $this->container )
        {
            if( isset( $this->container->settings ) )
            {
                $this->config = $this->container->settings ;
            }

            if( isset( $this->container->logger ) )
            {
                $this->logger = $this->container->logger ;
            }

            if( isset($this->container->tracker) )
            {
                $this->tracker = $this->container->tracker ;
            }
        }
    }

    /**
     * The config reference.
     */
    public $config ;

    /**
     * The container reference.
     * @private
     */
    protected $container ;

    /**
     * The logger reference.
     */
    public $logger;

    /**
     * The Google Analytics tracker reference.
     */
    public $tracker;

    /**
     * Outputs an error message.
     * Ex:
     * return $this->error( $response , 'bad request' , '405' );
     *
     * @param Response $response
     * @param string $message
     * @param string $code
     * @param array $options
     * @param integer $status
     *
     * @return Response
     */
    public function error( Response $response , $message , $code = '' , $options = NULL , $status = 400 )
    {
        if( isset($response) )
        {
            $output = [ 'status' => 'error' ];

            if( !empty( $code ) )
            {
                $output[ 'code' ] = $code;
            }

            $output[ 'message' ] = $message ;

            if( $options && is_array($options) )
            {
                $output = array_merge( $output , $options ) ;
            }

            return $response->withJson( $output , $status , $this->container->jsonOptions );
        }
    }

    public function errorAPI( Response $response , $resultAPI )
    {
        return $this->error( $response , $resultAPI->message , $resultAPI->code ) ;
    }

    public function filterLanguages( $field , $html = NULL )
    {
        $api       = $this->config['api'] ;
        $languages = $api['languages'] ;

        $item = [];
        foreach( $languages as $lang )
        {
            if( isset( $field[$lang] ) )
            {
                // if html remove all styles
                $item[$lang] = $html == TRUE ? preg_replace('/(<[^>]+) style=".*?"/i', '$1', $field[$lang] ) : $field[$lang]  ;
            }
        }

        return $item ;
    }

    /**
     * Formats a specific error message with a code.
     * Ex:
     * return $this->formatError( $response , '405' , [ 'get(' . $args['name'] . ')' ] , [ 'errors' => ['test' => 2] ] ) ;
     *
     * @param Response $response
     * @param string $code
     * @param array $args
     * @param array $errors
     * @param integer $status
     *
     * @return Response
     */
    public function formatError( Response $response , $code , $args = NULL , $errors = NULL , $status = 400 )
    {
        $conf = $this->config ;

        if( !array_key_exists( $code , $conf['errors'] ) )
        {
            $code = $conf['errors']['code_default'] ;
        }

        $message = $conf['errors'][ $code ];

        if( isset($args) )
        {
            $str = Strings::fastformat( $message , $args ) ;

            if( $str != $message )
            {
                $message = $str ;
            }
        }

        if( $conf['useLogging'] )
        {
            $this->logger->error( $this . ' ' . $code . ' : ' . $message . ' -- ' . json_encode( $args ) );
        }

        if( isset( $this->tracker ) )
        {
            $pre = (bool) $conf['analytics']['useVersion']
                 ? '/' . $conf['version']
                 : '' ;

            $this->tracker->pageview( $pre . '/errors/' . $code , $message );
        }

        return $this->error( $response , $message , $code , $errors , $status );
    }

    /**
     * The default 'image' method options.
     */
    const ARGUMENTS_IMAGE_DEFAULT =
    [
        'id'      => NULL  ,
        'check'   => TRUE  ,
        'gray'    => FALSE ,
        'format'  => 'jpg' ,
        'image'   => NULL ,
        'quality' => 70    ,
        'params'  => NULL  ,
        'strip'   => FALSE
    ] ;

    /**
     * Display a specific image.
     * Ex: $controller->image( $request , $response , [ 'image' => new Imagick($url) ] ) ;
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function image( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_IMAGE_DEFAULT , is_array($args) ? $args : [] ) ) ;

        if( $response )
        {
            return $this->container->image->display( $response , $image , $quality , $gray , $format , $strip ) ;
        }
    }

    /**
     * Indicates the base path of the application.
     *
     * @param Request $request
     *
     * @return string
     */
    public function getBasePath( Request $request = NULL )
    {
        if( !isset($request) )
        {
            $request = $this->container->request ;
        }
        return $request->getUri()->getBasePath() ;
    }

    /**
     * Indicates the base url of the application.
     *
     * @param Request $request
     *
     * @return string
     */
    public function getBaseURL( Request $request = NULL )
    {
        if( !isset($request) )
        {
            $request = $this->container->request ;
        }
        return $request->getUri()->getBaseUrl() ;
    }

    /**
     * Generates the current application url.
     *
     * @param string $path
     * @param array $params
     * @param Request $request
     * @param boolean $useNow
     *
     * @return string
     */
    public function getPath( $path = '' , $params = NULL , Request $request = NULL , $useNow = FALSE )
    {
        if( !isset($request) )
        {
            $request = $this->container->request ;
        }
        return $request->getUri()->getBasePath()
             . $path
             . Strings::formatRequestArgs( is_array($params) ? $params : [] , $useNow ) ;
    }

    /**
     * Generates the current application url.
     *
     * @param array $params
     * @param Request $request
     * @param boolean $useNow
     *
     * @return string
     */
    public function getFullPath( $params = NULL , Request $request = NULL , $useNow = FALSE )
    {
        if( !isset($request) )
        {
            $request = $this->container->request ;
        }

        return $request->getUri()->getBaseUrl()
             . '/' . $request->getUri()->getPath()
             . Strings::formatRequestArgs( is_array($params) ? $params : [] , $useNow ) ;
    }

    /**
     * Generates the current application url.
     *
     * @param string $path
     * @param array $params
     * @param Request $request
     * @param boolean $useNow
     *
     * @return string
     */
    public function getUrl( $path = '' , $params = NULL , Request $request = NULL , $useNow = FALSE )
    {
        if( !isset($request) )
        {
            $request = $this->container->request ;
        }
        return $request->getUri()->getBaseUrl()
             . $path
             . Strings::formatRequestArgs( is_array($params) ? $params : [] , $useNow ) ;
    }

    /**
     * Redirect to the pathFor
     *
     * @param Response $response
     * @param string $name
     * @param array $params
     *
     * @return Response
     */
    public function pathFor( Response $response , $name , array $params = [] )
    {
        return $response->withRedirect( $this->container->router->pathFor( $name , $params ) );
    }

    /**
     * Render the specific view with the current template engine.
     *
     * @param Response $response
     * @param $view
     * @param $args
     *
     * @return Response
     */
    public function render( Response $response , $view , $args = [] )
    {
        if( isset($response) )
        {
            return $this->container->view->render( $response , $view , is_array($args) ? $args : [] );
        }
    }

    /**
     * Outputs the sucess message.
     * Ex:
     * return $this->success
     * (
     *     $response ,
     *     $data ,
     *     $this->getFullPath( $request->getParams()  )
     * );
     * @param Response $response The HTTP Response reference.
     * @param string $data The data object to returns (output a JSON object).
     * @param string $url The current uri of the service.
     * @param integer $count An optional count value.
     * @param array $options An Array with the optional custom key/value elements to merge into the output object.
     * @param integer $status
     *
     * @return Response
     */
    public function success( Response $response  = NULL , $data = NULL ,  $url = '' , $count = NULL , $options = NULL , $status = 200 )
    {
        if( isset($response) )
        {
            $output = [ 'status' => 'success' ];

            if( !empty($url) )
            {
                $output[ 'url' ] = $url;
            }

            if( !empty($count) || $count === 0 )
            {
                $output[ 'count' ] = $count;
            }

            if( $options && is_array($options) )
            {
                $output = array_merge( $output , $options ) ;
            }

            $output[ 'result' ] = $data ;

            return $response->withJson( $output , $status , $this->container->jsonOptions );
        }
    }

    /**
     * Build url from uri and parameters
     *
     * @param $uri
     * @param $params
     *
     * @return string
     */
    public function buildUri( $uri , $params )
    {
        $parse_url = parse_url( $uri ) ;

        foreach( $params as $key => $value )
        {
            if( isset( $parse_url[ $key ] ) )
            {
                $parse_url[ $key ] .= "&" . http_build_query( $value , '' , '&' ) ;
            }
            else
            {
                $parse_url[ $key ] = http_build_query( $value , '' , '&' ) ;
            }
        }

        return
              ( ( isset( $parse_url[ "scheme" ] ) ) ? $parse_url[ "scheme" ] . "://" : "" )
            . ( ( isset( $parse_url[ "user" ] ) ) ? $parse_url[ "user" ]
            . ( ( isset( $parse_url[ "pass" ] ) ) ? ":" . $parse_url[ "pass" ] : "" ) . "@" : "" )
            . ( ( isset( $parse_url[ "host" ] ) ) ? $parse_url[ "host" ] : "" )
            . ( ( isset( $parse_url[ "port" ] ) ) ? ":" . $parse_url[ "port" ] : "" )
            . ( ( isset( $parse_url[ "path" ] ) ) ? $parse_url[ "path" ] : "" )
            . ( ( isset( $parse_url[ "query" ] ) && !empty( $parse_url[ 'query' ] ) ) ? "?" . $parse_url[ "query" ] : "" )
            . ( ( isset( $parse_url[ "fragment" ] ) ) ? "#" . $parse_url[ "fragment" ] : "" )
            ;
    }

    public function translate( $texts , $lang = NULL )
    {
        $languages = $this->config['api']['languages'] ;

        if( $lang == NULL )
        {
            return $texts ;
        }
        else
        {
            if( array_key_exists( $lang , $texts ) )
            {
                return $texts[ $lang ] ;
            }
            else if( array_key_exists( $languages[0] , $texts ) )
            {
                return $texts[ $languages[0] ] ;
            }
        }
    }

    /**
     * @param $fieldId
     * @param string $to
     * @param Edges $edge
     */
    public function insertEdge( $fieldId , $to , Edges $edge )
    {
        if( $fieldId !== NULL )
        {
            // add edge
            $edge->insertEdge( $edge->from['name'] . '/' . $fieldId , $to ) ;
        }
    }

    /**
     * @param $fieldId
     * @param string $to
     * @param Edges $edge
     */
    public function updateEdge( $fieldId , $to , Edges $edge )
    {
        if( $fieldId !== NULL )
        {
            if( $fieldId === FALSE )
            {
                // delete all edges to be sure
                $edge->delete( $to , [ 'key' => '_to' ] ) ;
            }
            else
            {
                $idFrom = $edge->from['name'] . '/' . $fieldId ;

                if( !$edge->existEdge( $idFrom , $to ) )
                {
                    // delete all edges to be sure
                    $edge->delete( $to , [ 'key' => '_to' ] ) ;
                    // add
                    $edge->insertEdge( $idFrom , $to ) ;
                }
            }
        }
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class($this) . ']' ;
    }
}


