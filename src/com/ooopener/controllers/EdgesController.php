<?php

namespace com\ooopener\controllers ;

use com\ooopener\models\Collections;
use com\ooopener\things\Thing;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use Slim\Container;

use Exception ;

class EdgesController extends Controller
{
    /**
     * Creates a new EdgesController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct ($container );
        $this->edge  = $edge ;
        $this->model = $model ;
        $this->owner = $owner ;
        $this->path  = $path ;
    }

    const CREATE_PROPERTIES = [] ;

    /**
     * The edge reference.
     */
    public $edge ;

    /**
     * The model reference.
     */
    public $model ;

    /**
     * The owner model reference.
     */
    public $owner ;

    /**
     * The path reference.
     */
    public $path ;

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return mixed
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        return $init ;
    }

    public function sortAfter( $items )
    {
        $sortable = $this->model->sortable ;

        if( $sortable && array_key_exists( 'after' , $sortable ) )
        {
            $after = explode( '.' , $sortable['after'] ) ;

            if( $after && is_array( $after ) && count( $after ) == 2 )
            {
                usort( $items , function ( $a , $b ) use ( $after )
                {
                    return strcmp( $a->{$after[0]}->{$after[1]} , $b->{$after[0]}->{$after[1]} ) ;
                }) ;
            }
        }
        return $items ;
    }

    public function getFields( $skin = NULL )
    {
        $fields = [] ;

        foreach( static::CREATE_PROPERTIES as $key => $options )
        {
            $filter = array_key_exists( 'filter' , $options ) ? $options['filter'] : NULL ;
            $skins = array_key_exists('skins', $options) ? $options['skins'] : NULL ;

            if( isset($skins) && is_array($skins) )
            {
                if( is_null($skin) || !in_array( $skin, $skins ) )
                {
                    continue;
                }
            }

            $fields[$key] = [ 'filter' => $filter ] ;

            //// set unique value for edge, edgeSingle and join filters
            switch( $filter )
            {
                case Thing::FILTER_EDGE :
                case Thing::FILTER_EDGE_SINGLE :
                    $fields[$key]['unique'] = $key . '_e' . mt_rand() ;
                    break;
                case Thing::FILTER_JOIN :
                    $fields[$key]['unique'] = $key . '_j' . mt_rand() ;
                    break;
            }
        }

        return $fields ;
    }

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'  => TRUE ,
        'conditions' => [] ,
        'facets'  => NULL ,
        'groupBy' => NULL ,
        'items'   => NULL ,
        'lang'    => NULL ,
        'limit'   => NULL ,
        'offset'  => NULL ,
        'options' => NULL ,
        'search'  => NULL ,
        'sort'    => NULL ,
        'params'  => NULL
    ] ;

    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all(' . $id . ')' ) ;
        }

        if( isset( $request ) )
        {
            $params = $request->getQueryParams() ;
        }

        try
        {
            $owner = $this->owner->get( $id , [ 'fields' => '_key' ] ) ;

            if( !$owner )
            {
                return $this->formatError( $response , '404' , NULL , NULL, 404 );
            }

            $api = $this->container->settings['api'] ;
            $set = $this->container->settings[$this->path] ;

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            $skin = NULL ;

            // ----- sort

            if( isset( $params[ 'sort' ] ) )
            {
                $sort = $params[ 'sort' ];
            }
            else if( is_null( $sort ) )
            {
                $sort = $set[ 'sort_default' ];
            }

            // ----- skin

            if( !isset($skin) && array_key_exists( 'skin_default' ,$set ) )
            {
                if( array_key_exists( 'skin_all', $set)  )
                {
                    $skin = $set['skin_all'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }

            ////
            $result = $this->edge->getEdge( NULL , $id , [ 'skin' => $skin , 'lang' => $lang , 'sort' => $sort ] ) ;

            $items = $result->edge ;

            if( $items )
            {
                foreach( $items as $key => $value )
                {
                    $items[$key] = $this->container->{$this->edge->from['controller']}->create( (object) $value , $lang , $skin ) ;
                }

                $this->container->{$this->edge->from['controller']}->sortAfter( $items ) ;
            }

            return $this->success
            (
                $response ,
                $items ,
                $this->getFullPath( $params ) ,
                is_array( $items ) ? count( $items ) : NULL ,
                [ 'total' => is_array( $items ) ? count( $items ) : 0 ]
            ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ [ 'all(' . $id . ')' ] , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function allReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' allReverse(' . $id . ')' ) ;
        }

        if( isset( $request ) )
        {
            $params = $request->getQueryParams() ;
        }

        try
        {
            $model = $this->model->get( $id , [ 'fields' => '_key' ] ) ;

            if( !$model )
            {
                return $this->formatError( $response , '404' , NULL , NULL, 404 );
            }

            $api = $this->container->settings['api'] ;
            $set = $this->container->settings[$this->path] ;

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            $skin = NULL ;

            // ----- sort

            if( isset( $params[ 'sort' ] ) )
            {
                $sort = $params[ 'sort' ];
            }
            else if( is_null( $sort ) )
            {
                $sort = $set[ 'sort_default' ];
            }

            // ----- skin

            if( !isset($skin) && array_key_exists( 'skin_default' ,$set ) )
            {
                if( array_key_exists( 'skin_all', $set)  )
                {
                    $skin = $set['skin_all'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }

            ////
            $result = $this->edge->getEdge( $id , null , [ 'skin' => $skin , 'lang' => $lang , 'sort' => $sort ] ) ;

            $items = $result->edge ;

            if( $items )
            {
                foreach( $items as $key => $value )
                {
                    $items[$key] = $this->container->{$this->edge->to['controller']}->create( (object) $value , $lang , $skin ) ;
                }

                $this->container->{$this->edge->to['controller']}->sortAfter( $items ) ;
            }

            return $this->success
            (
                $response ,
                $items ,
                $this->getFullPath( $params ) ,
                is_array( $items ) ? count( $items ) : NULL ,
                [ 'total' => is_array( $items ) ? count( $items ) : 0 ]
            ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ [ 'allReverse(' . $id . ')' ] , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'count' methods options.
     */
    const ARGUMENTS_COUNT_DEFAULT =
    [
        'id'      => NULL ,
        'reverse' => FALSE
    ] ;

    public function count( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_COUNT_DEFAULT , $args ) ) ;

        $count = 0 ;

        if( $response )
        {
            $this->logger->debug( $this . ' count' ) ;
        }


        if( $response )
        {
            return $this->success( $response , $count , $this->getFullPath() ) ;
        }

        return $count ;
    }

    public function countReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_COUNT_DEFAULT , $args ) ) ;

        $count = 0 ;

        if( $response )
        {
            $this->logger->debug( $this . ' count' ) ;
        }


        if( $response )
        {
            return $this->success( $response , $count , $this->getFullPath() ) ;
        }

        return $count ;
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'      => NULL ,
        'owner'   => NULL ,
        'reverse' => FALSE
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|bool
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            /*if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }*/

            $idFrom = $this->model->table . '/' . $id ;
            $idTo = $this->owner->table . '/' . $owner;

            // delete edge
            if( $this->edge->existEdge( $idFrom , $idTo ) )
            {
                $del = $this->edge->deleteEdge( $idFrom , $idTo ) ;
            }

            // update owner
            $this->owner->updateDate( $owner ) ;

            // update child
            $this->model->updateDate( $id ) ;

            if( $response )
            {
                return $this->success( $response , $reverse ? (int)$owner : (int)$id );
            }
            else
            {
                return $del ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function deleteReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        // reverse

        $args['id']      = $owner ;
        $args['owner']   = $id ;
        $args['reverse'] = TRUE ;

        return $this->delete( $request , $response , $args ) ;
    }

    /**
     * The default 'deleteAll' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'owner'   => NULL ,
        'reverse' => FALSE
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|bool
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $owner . ')' ) ;
        }

        try
        {

            // get
            if( $reverse === FALSE )
            {
                $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
                if( !$o )
                {
                    return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
                }

                $all = $this->edge->getEdge( NULL , $owner ) ;
            }
            else
            {
                $o = $this->model->get( $owner , [ 'fields' => '_key' ] ) ;
                if( !$o )
                {
                    return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
                }

                $all = $this->edge->getEdge( $owner ) ;
            }

            if( $all && $all->edge )
            {
                $unref = FALSE ;

                // delete them all
                foreach( $all->edge as $edge )
                {
                    if( $edge['id'] != 0 )
                    {
                        // delete
                        if( $reverse === FALSE )
                        {
                            $setOwner = $owner ;
                            $setID    = (string) $edge['id'] ;
                        }
                        else
                        {
                            $setOwner = (string) $edge['id'] ;
                            $setID    = $owner ;
                        }
                        $result = $this->delete( NULL , $response , [ 'owner' => $setOwner , 'id' => $setID ] ) ;
                        if( $result instanceof Response && $result->getStatusCode() != 200 )
                        {
                            return $result ;
                        }
                    }
                    else
                    {
                        $unref = TRUE ;
                    }
                }

                // remove all edges
                if( $unref === TRUE )
                {

                    if( $reverse === FALSE )
                    {
                        $key = $this->owner->table . '/' . $owner ;
                        $result = $this->edge->deleteEdgeTo( $key ) ;
                    }
                    else
                    {
                        $key = $this->model->table . '/' . $owner ;
                        $result = $this->edge->deleteEdgeFrom( $key ) ;
                    }
                }
            }

            if( $response )
            {
                return $this->success( $response , 'ok' );
            }
            else
            {
                return true ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $owner . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'    => NULL ,
        'skin'  => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $set = $this->container->settings[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( strtolower($params['skin']) , $set['skins'] ) )
                {
                    $params['skin'] = $skin = strtolower($params['skin']) ;
                }
            }
        }

        try
        {
            $o = $this->owner->get( $id , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $result = $this->edge->getEdge( NULL , $id , [ 'skin' => $skin ] ) ;
            $result = $result->edge ;

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'      => NULL ,
        'owner'   => NULL ,
        'reverse' => FALSE
    ] ;

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_id' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $this->model->table . '/' . $id ;
            $idTo = $o->_id ;

            // check if edge already exists
            if( $this->edge->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '409', [ 'post(' . $owner . ',' . $id . ')' ] , 'exists' , 409 );
            }

            // insert edge
            $insert = $this->edge->insertEdge( $idFrom , $idTo ) ;

            // update owner
            $this->owner->updateDate( $owner ) ;

            // update child
            $this->model->updateDate( $id ) ;

            if( $reverse )
            {
                $edgeToController = $this->container[$this->edge->to['controller']] ;
                $item = $this->owner->get( $owner, [ 'queryFields' => $edgeToController->getFields() ] ) ;
                $result = $edgeToController->create( $item ) ;
            }
            else
            {
                $edgeFromController = $this->container[$this->edge->from['controller']] ;
                $item = $this->model->get( $id, [ 'queryFields' => $edgeFromController->getFields() ] ) ;
                $result = $edgeFromController->create( $item ) ;
            }

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function postReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        // reverse

        $args['id']      = $owner ;
        $args['owner']   = $id ;
        $args['reverse'] = TRUE ;

        return $this->post( $request , $response , $args ) ;
    }
}
