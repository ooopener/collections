<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Model;
use com\ooopener\things\Thing;
use Exception;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

/**
 * The edges field controller.
 */
class EdgesFieldController extends Controller
{
    /**
     * Creates a new EdgesFieldController instance.
     *
     * @param Container $container
     * @param Model $model
     * @param string $field
     * @param bool $single
     */
    public function __construct( Container $container , Model $model = NULL , $field = NULL , $single = FALSE )
    {
        parent::__construct( $container );
        $this->model  = $model ;
        $this->field  = $field ;
        $this->single = $single ;
    }

    /**
     * The eventStatus database field name.
     */
    public $field ;

    /**
     * The model reference.
     */
    public $model ;

    /**
     * The edge if is a single record.
     */
    public $single ;

    const CREATE_PROPERTIES = [] ;

    public function getFields( $skin = NULL )
    {
        $fields = [] ;

        foreach( static::CREATE_PROPERTIES as $key => $options )
        {
            $filter = array_key_exists( 'filter' , $options ) ? $options['filter'] : NULL ;
            $skins = array_key_exists('skins', $options) ? $options['skins'] : NULL ;

            if( isset($skins) && is_array($skins) )
            {
                if( is_null($skin) || !in_array( $skin, $skins ) )
                {
                    continue;
                }
            }

            $fields[$key] = [ 'filter' => $filter ] ;

            //// set unique value for edge, edgeSingle and join filters
            switch( $filter )
            {
                case Thing::FILTER_EDGE :
                case Thing::FILTER_EDGE_SINGLE :
                    $fields[$key]['unique'] = $key . '_e' . mt_rand() ;
                    break;
                case Thing::FILTER_JOIN :
                case Thing::FILTER_JOIN_ARRAY :
                case Thing::FILTER_JOIN_MULTIPLE :
                    $fields[$key]['unique'] = $key . '_j' . mt_rand() ;
                    break;
                case Thing::FILTER_UNIQUE_NAME :
                    $fields[$key]['unique'] = $key . '_u' . mt_rand() ;
                    break;
            }
        }

        return $fields ;
    }

    /**
     * The default 'all' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'     => NULL ,
        'lang'   => NULL ,
        'params' => NULL
    ] ;

    /**
     * Returns specific thesaurus field value.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return object the collection of all types of the specific indexed event.
     */
    public function get( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        // ------------ Query Params

        if( isset( $request ) )
        {
            $api    = $this->container->settings['api'] ;
            $params = $request->getQueryParams();

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $edgeType = Thing::FILTER_EDGE ;

            if( $this->single === TRUE )
            {
                $edgeType = Thing::FILTER_EDGE_SINGLE ;
            }
            $field = [ $this->field => [ 'filter' => $edgeType ] ] ;
            $item = $this->model->get( $id , [ 'queryFields' => $field ] ) ;

            if( $item )
            {
                $item = $item->{ $this->field } ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                return $this->success
                (
                    $response,
                    $item,
                    $this->getFullPath( $params )
                ) ;
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item ;
    }
}
