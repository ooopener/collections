<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Technicians;
use com\ooopener\models\Veterinarians;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Slim\Container ;

/**
 * The edges medical authorities controller.
 */
class EdgesMedicalAuthoritiesController extends EdgesController
{
    /**
     * Creates a new EdgesAuthoritiesController instance.
     *
     * @param Container $container
     * @param Veterinarians|NULL $veterinarians
     * @param Technicians|NULL $technicians
     * @param Collections|NULL $owner
     * @param Edges|NULL $edgeVeterinarians
     * @param Edges|NULL $edgeTechnicians
     * @param string|NULL $path
     */
    public function __construct( Container $container , Veterinarians $veterinarians = NULL , Technicians $technicians = NULL , Collections $owner = NULL , Edges $edgeVeterinarians = NULL , Edges $edgeTechnicians = NULL , $path = NULL )
    {
        parent::__construct( $container , $veterinarians , $owner , $edgeVeterinarians , $path );

        $this->edgeTechnicians   = $edgeTechnicians ;
        $this->edgeVeterinarians = $edgeVeterinarians ;

        $this->technicians   = $technicians ;
        $this->veterinarians = $veterinarians ;

        // check
        if( $owner->table == $technicians->table )
        {
            $this->isOwnerTechnicians = TRUE ;
        }
        elseif( $owner->table == $veterinarians->table )
        {
            $this->isOwnerVeterinarians = TRUE ;
        }
    }

    /**
     * The technicians edge reference.
     */
    public $edgeTechnicians ;

    /**
     * The veterinarians edge reference.
     */
    public $edgeVeterinarians ;

    public $isOwnerTechnicians = FALSE ;

    public $isOwnerVeterinarians = FALSE ;

    /**
     * The technicians reference.
     */
    public $technicians ;

    /**
     * The veterinarian reference.
     */
    public $veterinarians ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'  => TRUE ,
        'conditions' => [] ,
        'facets'  => NULL ,
        'groupBy' => NULL ,
        'id'      => NULL ,
        'items'   => NULL ,
        'lang'    => NULL ,
        'limit'   => NULL ,
        'offset'  => NULL ,
        'options' => NULL ,
        'search'  => NULL ,
        'skin'    => NULL ,
        'sort'    => NULL ,
        'params'  => NULL
    ] ;

    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all(' . $id . ')' ) ;
        }

        try
        {
            if( !$this->owner->exist( $id ) )
            {
                return $this->formatError( $response , '404' , NULL , NULL, 404 );
            }

            $items = [] ;

            // get veterinarians
            $veterinarians = $this->edgeVeterinarians->getEdge( NULL , $id , [ 'skin' => $skin ] ) ;

            if( $veterinarians && $veterinarians->edge && is_array( $veterinarians->edge ) && count( $veterinarians->edge ) > 0 )
            {
                $itemsVeterinarians = NULL ;
                foreach( $veterinarians->edge as $key => $value)
                {
                    $itemsVeterinarians[] = $this->container->veterinariansController->create( (object) $value ) ;
                }
                $items = array_merge( $items , $itemsVeterinarians ) ;
            }

            // get technicians
            $technicians = $this->edgeTechnicians->getEdge( NULL , $id , [ 'skin' => $skin ] ) ;

            if( $technicians && $technicians->edge && is_array( $technicians->edge ) && count( $technicians->edge ) > 0 )
            {
                $itemsTechnicians = NULL ;
                foreach( $technicians->edge as $key => $value)
                {
                    $itemsTechnicians[] = $this->container->techniciansController->create( (object) $value ) ;
                }
                $items = array_merge( $items , $itemsTechnicians ) ;
            }

            if( $response )
            {
                return $this->success( $response , $items ) ;
            }
            else
            {
                return $items ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ [ 'all(' . $id . ')' ] , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function count( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // TODO code it
        return NULL ;
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'      => NULL ,
        'owner'   => NULL ,
        'reverse' => FALSE ,
        'type'    => 'veterinarian'
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;

        if( $request )
        {
            $params = $request->getParsedBody() ;

            if( ( $params && isset( $params['type'] ) && $params['type'] == 'technician' ) || ( isset( $args['type'] )  && $args['type'] == 'technician' ) )
            {
                $type = 'technician' ;
            }
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( $type == 'technician' )
            {
                $idFrom = $this->technicians->table . '/' . $id ;
                $idTo = $o->_id ;

                if( !$this->edgeTechnicians->existEdge( $idFrom , $idTo ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $success = $this->edgeTechnicians->deleteEdge( $idFrom , $idTo ) ;
            }
            else
            {
                $idFrom = $this->veterinarians->table . '/' . $id ;
                $idTo = $o->_id ;

                if( !$this->edgeVeterinarians->existEdge( $idFrom , $idTo ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $success = $this->edgeVeterinarians->deleteEdge( $idFrom , $idTo ) ;
            }

            if( $success )
            {
                // update owner
                $this->owner->updateDate( $owner ) ;

                return $this->success( $response , (int) $id );
            }
            else
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function deleteReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        // reverse
        if( $response )
        {
            $this->logger->debug( $this . ' deleteReverse(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->technicians->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'deleteReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->technicians->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'deleteReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $o->_id ;
            $idTo = $this->technicians->table . '/' . $id ;

            if( !$this->edgeTechnicians->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '404' , [ 'deleteReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $success = $this->edgeTechnicians->deleteEdge( $idFrom , $idTo ) ;

            if( $success )
            {
                // update owner
                $this->owner->updateDate( $owner ) ;

                return $this->success( $response , (int) $id );
            }
            else
            {
                return $this->formatError( $response , '404', [ 'deleteReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteReverse(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function deleteReverseVeterinarians( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        // reverse
        if( $response )
        {
            $this->logger->debug( $this . ' deleteReverseVeterinarians(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->veterinarians->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'deleteReverseVeterinarians(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->technicians->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'deleteReverseVeterinarians(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $o->_id ;
            $idTo = $this->technicians->table . '/' . $id ;

            if( !$this->edgeVeterinarians->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '404' , [ 'deleteReverseVeterinarians(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $success = $this->edgeVeterinarians->deleteEdge( $idFrom , $idTo ) ;

            if( $success )
            {
                // update owner
                $this->owner->updateDate( $owner ) ;

                return $this->success( $response , (int) $id );
            }
            else
            {
                return $this->formatError( $response , '404', [ 'deleteReverseVeterinarians(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteReverseVeterinarians(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $owner . ')' ) ;
        }

        try
        {
            if( $reverse === FALSE )
            {
                $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
                if( !$o )
                {
                    return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
                }

                // get all veterinarians
                $allVeterinarians = $this->edgeVeterinarians->getEdge( NULL , $owner ) ;

                // get all technicians
                $allTechnicians = $this->edgeTechnicians->getEdge( NULL , $owner ) ;
            }
            else
            {
                // get all veterinarians
                $allVeterinarians = $this->edgeVeterinarians->getEdge( $owner ) ;

                // get all technicians
                $allTechnicians = $this->edgeTechnicians->getEdge( $owner ) ;
            }

            if( $allVeterinarians && $allVeterinarians->edge )
            {
                // delete them all
                foreach( $allVeterinarians->edge as $edge )
                {
                    // delete
                    if( $reverse === FALSE )
                    {
                        $setOwner = $owner ;
                        $setID    = (string) $edge['id'] ;
                    }
                    else
                    {
                        $setOwner = (string) $edge['id'] ;
                        $setID    = $owner ;
                    }
                    $result = $this->delete( NULL , $response , [ 'owner' => $setOwner , 'id' => $setID ] ) ;
                    if( $result instanceof Response && $result->getStatusCode() != 200 )
                    {
                        return $result ;
                    }
                }
            }

            if( $allTechnicians && $allTechnicians->edge )
            {
                // delete them all
                foreach( $allTechnicians->edge as $edge )
                {
                    // delete
                    if( $reverse === FALSE )
                    {
                        $setOwner = $owner ;
                        $setID    = (string) $edge['id'] ;
                    }
                    else
                    {
                        $setOwner = (string) $edge['id'] ;
                        $setID    = $owner ;
                    }
                    $result = $this->delete( NULL , $response , [ 'owner' => $setOwner , 'id' => $setID , 'type' => 'technician' ] ) ;
                    if( $result instanceof Response && $result->getStatusCode() != 200 )
                    {
                        return $result ;
                    }
                }
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $owner . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|object
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ',' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();
        $technician = FALSE ;

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $result = NULL ;

            if( isset( $params['type'] ) && $params['type'] == 'technician' )
            {
                if( $this->isOwnerTechnicians && ( $owner == $id ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                if( !$this->technicians->exist( $id ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $idFrom = $this->technicians->table . '/' . $id ;
                $idTo = $o->_id ;

                // test if already exist
                if( $this->edgeTechnicians->existEdge( $idFrom , $idTo ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                // insert edge
                $insert = $this->edgeTechnicians->insertEdge( $idFrom , $idTo ) ;

                $fields = $this->container->{ $this->edgeTechnicians->from['controller'] }->getFields() ;
                $result = $this->technicians->get( $id , [ 'queryFields' => $fields ] ) ;
            }
            else
            {
                if( $this->isOwnerVeterinarians && ( $owner == $id ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                if( !$this->veterinarians->exist( $id ) )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
                }

                $idFrom = $this->veterinarians->table . '/' . $id ;
                $idTo = $o->_id ;

                // test if already exist
                if( $this->edgeVeterinarians->existEdge( $idFrom , $idTo ) )
                {
                    return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
                }

                // insert edge
                $insert = $this->edgeVeterinarians->insertEdge( $idFrom , $idTo ) ;

                $fields = $this->container->{ $this->edgeVeterinarians->from['controller'] }->getFields() ;
                $result = $this->veterinarians->get( $id , [ 'queryFields' => $fields ] ) ;
                $result = $this->container->{ $this->edgeVeterinarians->from['controller'] }->create( $result );
            }

            if( $insert )
            {
                // update owner
                $this->owner->updateDate( $owner ) ;
            }

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function postReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        // reverse
        if( $response )
        {
            $this->logger->debug( $this . ' postReverse(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->technicians->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'postReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->technicians->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'postReverse(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( $this->isOwnerTechnicians && ( $owner == $id ) )
            {
                return $this->formatError( $response , '409' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 409 );
            }

            $idFrom = $o->_id ;
            $idTo = $this->technicians->table . '/' . $id ;

            // test if already exist
            if( $this->edgeTechnicians->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '409' , [ 'postReverse(' . $owner . ',' . $id . ')' ] , NULL , 409 );
            }

            // insert edge
            $insert = $this->edgeTechnicians->insertEdge( $idFrom , $idTo ) ;

            $fields = $this->container->{ $this->edgeTechnicians->from['controller'] }->getFields() ;
            $result = $this->technicians->get( $id , [ 'queryFields' => $fields ] ) ;

            if( $insert )
            {
                // update owner
                $this->owner->updateDate( $owner ) ;
            }

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'postReverse(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function postReverseVeterinarians( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        // reverse
        if( $response )
        {
            $this->logger->debug( $this . ' postReverseVeterinarians(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $p = $this->veterinarians->get( $owner , [ 'fields' => '_id' ]  ) ;
            if( !$p )
            {
                return $this->formatError( $response , '404' , [ 'postReverseVeterinarians(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->technicians->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'postReverseVeterinarians(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $p->_id ;
            $idTo = $this->technicians->table . '/' . $id ;

            // test if already exist
            if( $this->edgeVeterinarians->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '409' , [ 'postReverseVeterinarians(' . $owner . ',' . $id . ')' ] , NULL , 409 );
            }

            // insert edge
            $insert = $this->edgeVeterinarians->insertEdge( $idFrom , $idTo ) ;

            $fields = $this->container->{ $this->edgeVeterinarians->from['controller'] }->getFields() ;
            $result = $this->technicians->get( $id , [ 'queryFields' => $fields ] ) ;
            $result = $this->container->{ $this->edgeVeterinarians->from['controller'] }->create( $result ) ;

            if( $insert )
            {
                // update owner
                $this->owner->updateDate( $owner ) ;
            }

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'postReverseVeterinarians(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
