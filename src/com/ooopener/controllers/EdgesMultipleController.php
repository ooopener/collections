<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Exception;

use Slim\Container ;


class EdgesMultipleController extends EdgesController
{
    /**
     * Creates a new EdgesMultipleController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param array|NULL $conditions
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $conditions = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->conditions = $conditions ;
    }

    public $conditions ;

    // --------------------------------

    /**
     * The default 'deleteAll' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();

        try
        {
            $item = $this->owner->get( $id ) ;
            if( $item == NULL )
            {
                return $this->formatError( $response , '404' , [ 'deleteAll(' . $id . ')' ] , NULL , 404 );
            }

            if( isset( $params['list'] ) )
            {
                $list = $params['list'] ;

                if( $list != "" )
                {
                    $items = explode( ',' , $list ) ;

                    // remove duplicate value
                    $items = array_unique( $items ) ;

                    // check list
                    $count = $this->container[ $this->edge->from['controller'] ]->model->existAll( $items , [ 'conditions' => $this->conditions ] ) ;
                }
                else
                {
                    $items = [] ;
                    $count = 0 ;
                }

                if( $count == count( $items ) )
                {
                    // get current edges of 'to'
                    $currentEdges = $this->edge->getEdge( NULL , $id ) ;

                    /// hack
                    $currentEdges = $currentEdges->edge ;

                    $currents = [] ;

                    foreach( $currentEdges as $edge )
                    {
                        array_push( $currents , (string) $edge['id'] ) ;
                    }

                    // compare lists (currents and items)
                    $removeItems = array_intersect( $items , $currents ) ;

                    ///////  REMOVE edges
                    $idTo = $this->edge->to['name'] . '/' . $id ;

                    foreach( $removeItems as $item )
                    {
                        $idFrom = $this->edge->from['name'] . '/' . $item ;

                        // check exist
                        $exist = $this->edge->existEdge( $idFrom , $idTo ) ;

                        if( $exist )
                        {
                            // delete to edge
                            $this->edge->deleteEdge( $idFrom , $idTo ) ;
                        }
                    }

                    // get result
                    $result = $this->edge->getEdge( NULL , $id ) ;

                    /// hack
                    $result = $result->edge ;


                    if( $result )
                    {
                        // update owner
                        $this->owner->updateDate( $id ) ;
                    }

                    return $this->success( $response , $list ) ;
                }
                else
                {
                    return $this->error( $response , 'some identifiers in the list are not valid' ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '400' , [ 'deleteAll(' . $id . ')' , 'the parameter list is needed' ] );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();

        try
        {
            $item = $this->owner->get( $id ) ;
            if( $item == NULL )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $id . ')' ] , NULL , 404 );
            }

            if( isset( $params['list'] ) )
            {
                $list = $params['list'] ;

                if( $list != "" )
                {
                    $items = explode( ',' , $list ) ;

                    // remove duplicate value
                    $items = array_unique( $items ) ;

                    // check list
                    $count = $this->container[ $this->edge->from['controller'] ]->model->existAll( $items , [ 'conditions' => $this->conditions ] ) ;
                }
                else
                {
                    $items = [] ;
                    $count = 0 ;
                }

                if( $count == count( $items ) )
                {
                    // get current edges of 'to'
                    $currentEdges = $this->edge->getEdge( NULL , $id ) ;

                    /// hack
                    $currentEdges = $currentEdges->edge ;

                    $currents = [] ;

                    foreach( $currentEdges as $edge )
                    {
                        array_push( $currents , (string) $edge['id'] ) ;
                    }

                    // compare lists (currents and items)
                    $addItems = array_diff( $items , $currents ) ;

                    ///////  ADD edges
                    $idTo = $this->edge->to['name'] . '/' . $id ;
                    foreach( $addItems as $item )
                    {
                        $idFrom = $this->edge->from['name'] . '/' . $item ;

                        // check not exist
                        $exist = $this->edge->existEdge( $idFrom , $idTo ) ;

                        if( !$exist )
                        {
                            // save to edge
                            $this->edge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }

                    // get result
                    $result = $this->edge->getEdge( NULL , $id ) ;

                    /// hack
                    $result = $result->edge ;


                    if( $result )
                    {
                        // update owner
                        $this->owner->updateDate( $id ) ;
                    }

                    return $this->success( $response , $result ) ;
                }
                else
                {
                    return $this->error( $response , 'some identifiers in the list are not valid' ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '400' , [ 'post(' . $id . ')' , 'the parameter list is needed' ] );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
