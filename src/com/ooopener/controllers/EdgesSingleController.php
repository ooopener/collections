<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;

use Exception;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Container;

class EdgesSingleController extends EdgesController
{
    /**
     * Creates a new EdgesSingleController instance.
     *
     * @param Container $container
     * @param Collections|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param array|NULL $conditions
     * @param string|NULL $path
     */
    public function __construct( Container $container , Collections $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $conditions = NULL , $path = NULL )
    {
        parent::__construct ( $container , $model , $owner , $edge , $path ) ;
        $this->conditions = $conditions ;

        // replace edge to
        $edge->to['name'] = $owner->table ;
        $edge->to['controller'] = $owner->table . 'Controller' ;
    }

    public $conditions ;

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|bool
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_DELETE_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' delete(' . $id . ')');
        }

        try
        {
            $o = $this->owner->get($id , [ 'fields' => '_key' ]);
            if( !$o )
            {
                return $this->formatError($response , '404' , [ 'delete(' . $id . ')' ] , NULL , 404);
            }

            $idTo = $this->owner->table . '/' . $id ;

            // delete all edges to be sure
            $this->edge->delete( $idTo , [ 'key' => '_to' ] ) ;

            // update owner date
            $this->owner->updateDate( $id ) ;

            return $this->success( $response , (int) $id ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError($response , '500' , [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500);
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_PATCH_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' patch(' . $owner . ',' . $id . ')');
        }

        try
        {
            $o = $this->owner->get( $owner ) ;
            if( $o == NULL )
            {
                return $this->formatError( $response , '404' , [ 'patch(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $edgeFromController = $this->container[$this->edge->from['controller']] ;
            $item = $this->model->get( $id , [ 'conditions' => $this->conditions , 'queryFields' => $edgeFromController->getFields() ] ) ;
            if( $item == NULL )
            {
                return $this->formatError( $response , '404' , [ 'patch(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            // update edge
            $idFrom = $this->model->table . '/' . $id ;
            $idTo = $this->owner->table . '/' . $owner ;

            // delete all edges to be sure
            $this->edge->delete( $idTo , [ 'key' => '_to' ] ) ;
            // add edge
            $this->edge->insertEdge( $idFrom , $idTo ) ;

            // update owner date
            $this->owner->updateDate( $owner ) ;

            return $this->success( $response , $edgeFromController->create( $item ) ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
