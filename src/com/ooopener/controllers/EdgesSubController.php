<?php

namespace com\ooopener\controllers;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;

use Slim\Container;

use Exception ;

class EdgesSubController extends EdgesController
{
    /**
     * EdgesSubController constructor.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , string $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            // check if owner and sub are not equal
            if( $owner == $id )
            {
                return $this->formatError( $response , '409', [ 'post(' . $owner . ',' . $id . ')' ] , 'equal' , 409 );
            }

            // check exist owner
            $o = $this->owner->get( $owner , [ 'fields' => '_id' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            // check exist sub
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'post(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $idFrom = $this->model->table . '/' . $id ;
            $idTo = $o->_id ;

            // check if edge already exists
            if( $this->edge->existEdge( $idFrom , $idTo ) )
            {
                return $this->formatError( $response , '409', [ 'post(' . $owner . ',' . $id . ')' ] , 'exists' , 409 );
            }

            // check if invert edge exists
            if( $this->edge->existEdge( $idTo , $idFrom ) )
            {
                return $this->formatError( $response , '409', [ 'post(' . $owner . ',' . $id . ')' ] , 'invert exists' , 409 );
            }

            // insert edge
            $insert = $this->edge->insertEdge( $idFrom , $idTo ) ;

            // update owner
            $this->owner->updateDate( $owner ) ;

            if( $reverse )
            {
                $edgeToController = $this->container[$this->edge->to['controller']] ;
                $item = $this->owner->get( $owner, [ 'queryFields' => $edgeToController->getFields() ] ) ;
                $result = $edgeToController->create( $item ) ;
            }
            else
            {
                $edgeFromController = $this->container[$this->edge->from['controller']] ;
                $item = $this->model->get( $id, [ 'queryFields' => $edgeFromController->getFields() ] ) ;
                $result = $edgeFromController->create( $item ) ;
            }

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
