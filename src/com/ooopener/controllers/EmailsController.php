<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use com\ooopener\validations\EmailValidator ;

/**
 * The generic emails controller.
 */
class EmailsController extends ThingsEdgesController
{
    /**
     * Creates a new EmailsController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );

        $this->validator = new EmailValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="Email",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(property="alternateName",description="The alternate name of the resource",@OA\Items(ref="#/components/schemas/text"),example={"en":"English","fr":"French"}),
     *     @OA\Property(property="additionalType",description="The additional type ",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="value",description="The email value",format="email"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'value'         => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'alternateName'  => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'additionalType' => [ 'filter' =>  Thing::FILTER_JOIN     ]
    ];


    /**
     * Post new email
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postEmail",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="value",type="string",format="email",description="The email"),
     *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
     *             required={"value","additionalType"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['value'] ) && $params['value'] != '' )
        {
            $item['value'] = $params['value'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['additionalType'] )  )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']           , 'max(70)'            ] ,
            'value'          => [ $params['value']          , 'required|email'     ] ,
            'additionalType' => [ $params['additionalType'] , 'required|emailType' ]
        ] ;

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::post( $request , $response , $args ) ;
    }

    /**
     * Put specific email
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putEmail",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="value",type="string",format="email",description="The email"),
     *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
     *             required={"value","additionalType"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['value'] ) && $params['value'] != '' )
        {
            $item['value'] = $params['value'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['additionalType'] )  )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'name'  => [ $params['name']           , 'max(70)'            ] ,
            'value' => [ $params['value']          , 'required|email'     ] ,
            'type'  => [ $params['additionalType'] , 'required|emailType' ]
        ] ;

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::put( $request , $response , $args ) ;
    }
}
