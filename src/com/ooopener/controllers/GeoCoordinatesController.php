<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Model;
use com\ooopener\validations\GeoValidator;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Slim\Container ;

/**
 * The geo coordinates controller.
 *
 * @OA\Schema(
 *     schema="GeoCoordinates",
 *     description="The geographic coordinates of a place or event",
 *     type="object",
 *     allOf={},
 *     required={"latitude","longitude"},
 *     @OA\Property(property="latitude",type="number",description="The latitude of a location",minimum=-90,maximum=90),
 *     @OA\Property(property="longitude",type="number",description="The longitude of a location",minimum=-180,maximum=180),
 *     @OA\Property(property="elevation",type="number",description="The elevation of a location",minimum=-11500,maximum=8900)
 * )
 */
class GeoCoordinatesController extends Controller
{
    /**
     * Creates a new GeoCoordinatesController instance.
     *
     * @param Container $container
     * @param Model $model
     */
    public function __construct( Container $container , Model $model = NULL )
    {
        parent::__construct( $container );
        $this->model  = $model ;
    }

    /**
     * The model reference.
     */
    public $model ;

    /**
     * The default 'get methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'     => NULL
    ] ;

    /**
     * Returns the GeoCoordinates reference of the specific item reference.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|object the GeoCoordinates reference of the specific item reference.
     */
    public function get( Request $request = NULL , Response $response = NULL, array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , is_array($args) ? $args : [] ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ') ' ) ;
        }

        $item = NULL ;

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $item = $this->model->get( $id , [ 'fields' => 'geo' ] ) ;
            if( $item )
            {
                $item = $item->geo ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                return $this->success
                (
                    $response,
                    $item,
                    $this->getFullPath()
                );
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item  ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'     => NULL ,
    ] ;

    /**
     * Update the GeoCoordinates reference of the specific item reference.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|object the GeoCoordinates reference of the specific item reference.
     *
     * @OA\RequestBody(
     *     request="patchGeo",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(ref="#/components/schemas/GeoCoordinates")
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL, array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , is_array($args) ? $args : [] ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404' , [ 'patch(' . $id . ')' ] , NULL , 404 );
            }

            // check
            $params = $request->getParsedBody() ;

            $elevation = NULL ;

            $latitude = NULL ;
            $longitude = NULL ;

            $item = [];
            $conditions = [] ;

            if( isset( $params['latitude'] ) && isset( $params['longitude'] ) )
            {
                $item['latitude']  = $latitude  = (float)$params['latitude'] ;
                $item['longitude'] = $longitude = (float)$params['longitude'] ;

                $conditions['latitude']  = [ $latitude  , 'required|min(-90, number)|max(90,number)'  ] ;
                $conditions['longitude'] = [ $longitude , 'required|min(-180,number)|max(180,number)' ] ;
            }
            else
            {
                $item['latitude'] = NULL ;
                $item['longitude'] = NULL ;
            }

            if( isset( $params['elevation'] ) )
            {
                $item['elevation'] = $elevation = (int) $params['elevation'] ;
                $conditions['elevation'] = [ $elevation , 'min(-11500,number)|max(8900,number)' ] ;
            }
            else
            {
                $item['elevation'] = NULL ;
            }

            $validator = new GeoValidator( $this->container ) ;

            $validator->validate
            (
                $conditions
            );

            if( $validator->passes() )
            {

                $insert = [ 'geo' => $item ] ;

                $result = $this->model->update( $insert , $id );

                if( $result )
                {
                    return $this->success( $response , $item );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

}
