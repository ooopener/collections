<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use Exception;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;


/**
 * The edge field controller.
 */
class MultiEdgeFieldController extends EdgesController
{


    /**
     * Creates a new MultiEdgeFieldController instance.
     *
     * @param Container $container
     * @param string $field
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct(
        Container   $container = NULL ,
                    $field     = NULL ,
        Model       $model     = NULL ,
        Collections $owner     = NULL ,
        Edges       $edge      = NULL ,
                    $path      = NULL
    )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
        $this->field     = $field ;
    }

    /**
     * The field to target.
     */
    public $field ;

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putMultiEdgeField",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="list",type="string",description="List of resources"),
     *             required={"list"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();

        try
        {
            $item = $this->owner->get( $id ) ;
            if( $item == NULL )
            {
                return $this->formatError( $response , '404' , [ 'put(' . $id . ')' ] , NULL , 404 );
            }

            if( isset( $params[$this->field] ) )
            {
                $list = $params[$this->field] ;

                if( $list != "" )
                {
                    $items = explode( ',' , $list ) ;

                    // remove duplicate value
                    $items = array_unique( $items ) ;

                    // check list
                    $count = $this->container[ $this->edge->from['controller'] ]->model->existAll( $items ) ;
                }
                else
                {
                    $items = [] ;
                    $count = 0 ;
                }

                if( $count == count( $items ) )
                {
                    // get current edges of 'to'
                    $currentEdges = $this->edge->getEdge( NULL , $id ) ;

                    /// hack
                    $currentEdges = $currentEdges->edge ;

                    $currents = [] ;

                    foreach( $currentEdges as $edge )
                    {
                        array_push( $currents , (string) $edge['id'] ) ;
                    }

                    // compare lists (currents and items)
                    $addItems = array_diff( $items , $currents ) ;

                    ///////  ADD edges
                    $idTo = $this->edge->to['name'] . '/' . $id ;
                    foreach( $addItems as $item )
                    {
                        $idFrom = $this->edge->from['name'] . '/' . $item ;

                        // check not exist
                        $exist = $this->edge->existEdge( $idFrom , $idTo ) ;

                        if( !$exist )
                        {
                            // save to edge
                            $this->edge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }

                    ///////  REMOVE edges
                    $removeItems = array_diff( array_merge( $currents , $addItems ) , $items ) ;

                    foreach( $removeItems as $item )
                    {
                        $idFrom = $this->edge->from['name'] . '/' . $item ;

                        // check exist
                        $exist = $this->edge->existEdge( $idFrom , $idTo ) ;

                        if( $exist )
                        {
                            // delete to edge
                            $this->edge->deleteEdge( $idFrom , $idTo ) ;
                        }
                    }

                    // get result
                    $result = $this->edge->getEdge( NULL , $id ) ;

                    /// hack
                    $result = $result->edge ;


                    if( $result )
                    {
                        // update owner
                        $this->owner->updateDate( $id ) ;
                    }

                    return $this->success( $response , $result ) ;
                }
                else
                {
                    return $this->error( $response , 'some identifiers in the list are not valid' ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '400' , [ 'put(' . $id . ')' , 'the parameter ' . $this->field . ' is needed' ] );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}


