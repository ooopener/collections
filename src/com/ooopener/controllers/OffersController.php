<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;
use com\ooopener\validations\OfferValidator;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;


/**
 * The offers controller.
 * @example ../places/165/offers?prettyprint=true&category=full
 */
class OffersController extends ThingsEdgesController
{
    /**
     * Creates a new OffersController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );

        $this->validator = new OfferValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="Offer",
     *     description="An offer to transfer some rights to an item or to provide a service",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(property="description",ref="#/components/schemas/text"),
     *     @OA\Property(property="category",description="The category of the offer",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="number",property="price",description="The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes"),
     *     @OA\Property(type="string",property="priceCurrency",description="The currency of the price, or a price component when attached to PriceSpecification and its subtypes.Use standard formats: ISO 4217 currency format"),
     *     @OA\Property(type="string",property="validFrom",format="date-time",description="The date when the item becomes valid"),
     *     @OA\Property(type="string",property="validThrough",format="date-time",description="The date after when the item is not valid"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID        ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT   ] ,
        'description'   => [ 'filter' =>  Thing::FILTER_TRANSLATE ] ,
        'price'         => [ 'filter' =>  Thing::FILTER_DEFAULT   ] ,
        'priceCurrency' => [ 'filter' =>  Thing::FILTER_DEFAULT   ] ,
        'validFrom'     => [ 'filter' =>  Thing::FILTER_DEFAULT   ] ,
        'validThrough'  => [ 'filter' =>  Thing::FILTER_DEFAULT   ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME  ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME  ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE  ] ,
        'category'      => [ 'filter' =>  Thing::FILTER_JOIN      ]
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postOffer",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="category",type="integer",description="The id of the category"),
     *             required={"name","additionalType"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *             @OA\Property(property="description",ref="#/components/schemas/text"),
     *             @OA\Property(property="price",type="string",description="The price of the offer"),
     *             @OA\Property(property="validFrom",type="string",format="date",description="The date when the item becomes valid"),
     *             @OA\Property(property="validThrough",type="string",format="date",description="The date after when the item is not valid"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $set = $this->config['offer'] ;
        $params = $request->getParsedBody();

        $item = [];

        $item['priceCurrency'] = "EUR" ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['category'] ) )
        {
            $item['category'] = (int) $params['category'] ;
        }

        if( isset( $params['price'] ) )
        {
            if( $params['price'] != '' )
            {
                $item['price'] = (float) $params['price'] ;
            }
            else
            {
                $item['price'] = 0;
            }
        }

        if( isset( $params['description'] ) )
        {
            $item['description'] = $this->filterLanguages( $params['description'] ) ;
        }

        $conditions =
        [
            'name'         => [ $params['name']     , 'min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ] ,
            'category'     => [ $params['category'] , 'required|category'                                         ] ,
            'price'        => [ $params['price']    , 'price'                                            ]
        ];

        if( isset( $params['validFrom'] ) && $params['validFrom'] != '' )
        {
            $item['validFrom'] = $params['validFrom'] ;
            $conditions['validFrom'] = [ $params['validFrom'] , 'date' ] ;
        }

        if( isset( $params['validThrough'] ) && $params['validThrough'] != '' )
        {
            $item['validThrough'] = $params['validThrough'] ;
            $conditions['validThrough'] = [ $params['validThrough'] , 'date' ] ;
        }

        // ----------

        if( isset( $params['validFrom'] ) && isset( $params['validThrough'] ) )
        {
            $validPeriod = (boolean) (strtotime($params['validFrom']) <= strtotime($params['validThrough'])) ;
            $conditions['validPeriod'] = [ $validPeriod , 'true' ] ;
        }

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::post( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putOffer",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="category",type="integer",description="The id of the category"),
     *             required={"name","additionalType"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *             @OA\Property(property="description",ref="#/components/schemas/text"),
     *             @OA\Property(property="price",type="string",description="The price of the offer"),
     *             @OA\Property(property="validFrom",type="string",format="date",description="The date when the item becomes valid"),
     *             @OA\Property(property="validThrough",type="string",format="date",description="The date after when the item is not valid"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $set = $this->config['offer'] ;
        $params = $request->getParsedBody();

        $item = [];

        $item['priceCurrency'] = "EUR" ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['category'] ) )
        {
            $item['category'] = (int) $params['category'] ;
        }

        if( isset( $params['price'] ) )
        {
            if( $params['price'] != '' )
            {
                $item['price'] = (float) $params['price'] ;
            }
            else
            {
                $item['price'] = 0;
            }
        }

        if( isset( $params['description'] ) )
        {
            $item['description'] = $this->filterLanguages( $params['description'] ) ;
        }

        $conditions =
        [
            'name'         => [ $params['name']         , 'min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ] ,
            'category'     => [ $params['category']     , 'required|category'                                         ] ,
            'price'        => [ $params['price']        , 'price'                                                     ]
        ];

        if( isset( $params['validFrom'] ) )
        {
            if( $params['validFrom'] != '' )
            {
                $item['validFrom'] = $params['validFrom'] ;
                $conditions['validFrom'] = [ $params['validFrom'] , 'date' ] ;
            }
            else
            {
                $item['validFrom'] = NULL ;
            }
        }

        if( isset( $params['validThrough'] ) )
        {
            if( $params['validThrough'] != '' )
            {
                $item['validThrough'] = $params['validThrough'] ;
                $conditions['validThrough'] = [ $params['validThrough'] , 'date' ] ;
            }
            else
            {
                $item['validThrough'] = NULL ;
            }
        }

        // ----------

        if( isset( $params['validFrom'] ) && isset( $params['validThrough'] ) )
        {
            $validPeriod = (boolean) (strtotime($params['validFrom']) <= strtotime($params['validThrough'])) ;
            $conditions['validPeriod'] = [ $validPeriod , 'true' ] ;
        }

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::put( $request , $response , $args ) ;
    }
}
