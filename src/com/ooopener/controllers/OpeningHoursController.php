<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use DateTime ;
use Exception ;
use Slim\Container ;

use com\ooopener\validations\OpeningHoursSpecificationValidator;

/**
 * The opening hours controller.
 */
class OpeningHoursController extends ThingsEdgesController
{
    /**
     * Creates a new OpeningHoursController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );

        $this->validator = new OpeningHoursSpecificationValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="OpeningHoursSpecification",
     *     description="A structured value providing information about the opening hours of a place or a certain service inside a place",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(property="description",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="dayOfWeek",description="The day of the week for which these opening hours are valid"),
     *     @OA\Property(type="string",property="opens",description="The opening hour of the place or service on the given day(s) of the week"),
     *     @OA\Property(type="string",property="closes",description="The closing hour of the place or service on the given day(s) of the week"),
     *     @OA\Property(type="string",property="validFrom",format="date-time",description="The date when the item becomes valid"),
     *     @OA\Property(type="string",property="validThrough",format="date-time",description="The date after when the item is not valid"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'dayOfWeek'     => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'opens'         => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'closes'        => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'validFrom'     => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'validThrough'  => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE ]
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postOpeningHoursSpecification",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="opens",type="integer",description="The opening hour of the place or service on the given day(s) of the week"),
     *             @OA\Property(property="closes",type="integer",description="The closing hour of the place or service on the given day(s) of the week"),
     *             required={"opens","closes"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *             @OA\Property(property="description",ref="#/components/schemas/text"),
     *             @OA\Property(property="dayOfWeek",type="string",description="The day of the week for which these opening hours are valid"),
     *             @OA\Property(property="validFrom",type="string",format="date",description="The date when the item becomes valid"),
     *             @OA\Property(property="validThrough",type="string",format="date",description="The date after when the item is not valid"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['description'] ) )
        {
            $item['description'] = $this->filterLanguages( $params['description'] ) ;
        }

        if( isset( $params['dayOfWeek'] ) && !empty( $params['dayOfWeek'] ) )
        {
            $item['dayOfWeek'] = $params['dayOfWeek'] ;
            $params['dayOfWeek'] = explode( ',' , $params['dayOfWeek'] ) ;
        }

        if( isset( $params['opens'] ) )
        {
            $item['opens'] = $params['opens'] ;
        }

        if( isset( $params['closes'] ) )
        {
            $item['closes'] = $params['closes'] ;
        }

        if( isset( $params['validFrom'] ) && $params['validFrom'] != '' )
        {
            $item['validFrom'] = $params['validFrom'] ;
        }

        if( isset( $params['validThrough'] ) && $params['validThrough'] != '' )
        {
            $item['validThrough'] = $params['validThrough'] ;
        }

        // ----------

        $equals = true ;
        if( $params['opens'] && $params['closes'] )
        {
            $aCloses = explode( ',' , $params['closes'] ) ;
            $aOpens  = explode( ',' , $params['opens']  ) ;
            $equals = ( count($aCloses) == count($aOpens) ) ;
        }

        // ----------

        $timeStart = NULL ;
        $timeEnd   = NULL ;

        try
        {
            $date = DateTime::createFromFormat('Y-m-d',$params['validFrom']) ;
            if( $date )
            {
                $timeStart = $date->getTimestamp() ;
            }
        }
        catch (Exception $e)
        {
            $this->logger->warn( $this . " post failed, " . $e->getMessage() ) ;
        }

        try
        {
            $date = DateTime::createFromFormat('Y-m-d',$params['validThrough']);
            if( $date )
            {
                $timeEnd = $date->getTimestamp() ;
            }
        }
        catch (Exception $e)
        {
            $this->logger->warn( $this . " post failed, " . $e->getMessage() ) ;
        }

        $validPeriod = FALSE ;

        if( ($timeStart == NULL) && ($timeEnd == NULL) )
        {
            $validPeriod = TRUE ;
        }
        else if( ($timeStart != NULL) && ($timeEnd != NULL) && ($timeStart <= $timeEnd) )
        {
            $validPeriod = TRUE ;
        }

        $conditions =
        [
            'name'             => [ $params['name']         , 'min(2)|max(50)'     ] ,
            'dayOfWeek'        => [ $params['dayOfWeek']    , 'dayOfWeek'          ] ,
            'closes'           => [ $params['closes']       , 'required|times'     ] ,
            'opens'            => [ $params['opens']        , 'required|times'     ] ,
            'openCloseEquals'  => [ $equals                 , 'true'               ] ,
            'validFrom'        => [ $params['validFrom']    , 'date'               ] ,
            'validThrough'     => [ $params['validThrough'] , 'date'               ] ,
            'validPeriod'      => [ $validPeriod            , 'true'               ]
        ];

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::post( $request , $response , $args ) ;
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL ,
        'place' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putOpeningHoursSpecification",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="opens",type="integer",description="The opening hour of the place or service on the given day(s) of the week"),
     *             @OA\Property(property="closes",type="integer",description="The closing hour of the place or service on the given day(s) of the week"),
     *             required={"opens","closes"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *             @OA\Property(property="description",ref="#/components/schemas/text"),
     *             @OA\Property(property="dayOfWeek",type="string",description="The day of the week for which these opening hours are valid"),
     *             @OA\Property(property="validFrom",type="string",format="date",description="The date when the item becomes valid"),
     *             @OA\Property(property="validThrough",type="string",format="date",description="The date after when the item is not valid"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['description'] ) )
        {
            $item['description'] = $this->filterLanguages( $params['description'] ) ;
        }

        if( isset( $params['dayOfWeek'] ) )
        {
            $item['dayOfWeek'] = $params['dayOfWeek'] ;
            $params['dayOfWeek'] = explode( ',' , $params['dayOfWeek'] ) ;
        }

        if( isset( $params['opens'] ) )
        {
            $item['opens'] = $params['opens'] ;
        }

        if( isset( $params['closes'] ) )
        {
            $item['closes'] = $params['closes'] ;
        }

        if( isset( $params['validFrom'] ) )
        {
            if( $params['validFrom'] != '' )
            {
                $item['validFrom'] = $params['validFrom'] ;
            }
            else
            {
                $item['validFrom'] = NULL ;
            }
        }

        if( isset( $params['validThrough'] ) )
        {
            if( $params['validThrough'] != '' )
            {
                $item['validThrough'] = $params['validThrough'] ;
            }
            else
            {
                $item['validThrough'] = NULL ;
            }
        }

        // ----------

        $equals = true ;
        if( $params['opens'] && $params['closes'] )
        {
            $aCloses = explode( ',' , $params['closes'] ) ;
            $aOpens  = explode( ',' , $params['opens']  ) ;
            $equals = ( count($aCloses) == count($aOpens) ) ;
        }

        // ----------

        $timeStart = NULL ;
        $timeEnd   = NULL ;

        try
        {
            $date = DateTime::createFromFormat('Y-m-d',$params['validFrom']) ;
            if( $date )
            {
                $timeStart = $date->getTimestamp() ;
            }
        }
        catch (Exception $e)
        {
            $this->logger->warn( $this . " put failed, " . $e->getMessage() ) ;
        }

        try
        {
            $date = DateTime::createFromFormat('Y-m-d',$params['validThrough']);
            if( $date )
            {
                $timeEnd = $date->getTimestamp() ;
            }
        }
        catch (Exception $e)
        {
            $this->logger->warn( $this . " put failed, " . $e->getMessage() ) ;
        }

        $validPeriod = FALSE ;

        if( ($timeStart == NULL) && ($timeEnd == NULL) )
        {
            $validPeriod = TRUE ;
        }
        else if( ($timeStart != NULL) && ($timeEnd != NULL) && ($timeStart <= $timeEnd) )
        {
            $validPeriod = TRUE ;
        }

        $conditions =
        [
            'name'             => [ $params['name']         , 'min(2)|max(50)'     ] ,
            'dayOfWeek'        => [ $params['dayOfWeek']    , 'dayOfWeek'          ] ,
            'closes'           => [ $params['closes']       , 'required|times'     ] ,
            'opens'            => [ $params['opens']        , 'required|times'     ] ,
            'openCloseEquals'  => [ $equals                 , 'true'               ] ,
            'validFrom'        => [ $params['validFrom']    , 'date'               ] ,
            'validThrough'     => [ $params['validThrough'] , 'date'               ] ,
            'validPeriod'      => [ $validPeriod            , 'true'               ]
        ];

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::put( $request , $response , $args ) ;
    }
}
