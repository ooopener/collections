<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use com\ooopener\validations\PhoneNumberValidator ;

/**
 * The generic phone numbers controller.
 */
class PhoneNumbersController extends ThingsEdgesController
{
    /**
     * Creates a new PhoneNumbersController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );

        $this->validator = new PhoneNumberValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="PhoneNumber",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(property="additionalType",description="The additional type ",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="value",description="A valid phone number in international format"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'value'         => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'alternateName'  => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'additionalType' => [ 'filter' => Thing::FILTER_JOIN      ]
    ];


    /**
     * Post new keyword
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postPhoneNumber",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="value",type="string",description="The phone number in international format"),
     *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
     *             required={"value","additionalType"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['value'] ) && $params['value'] != '' )
        {
            $item['value'] = $this->formatPhoneNumber( $params['value'] , $params['regionCode'] ) ;
        }

        if( isset( $params['additionalType'] )  )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'value'          => [ $item['value']            , 'required'                 ] ,
            'additionalType' => [ $params['additionalType'] , 'required|phoneNumberType' ]
        ] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'max(70)' ] ;
        }

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::post( $request , $response , $args ) ;
    }

    /**
     * Put specific keyword
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putPhoneNumber",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="value",type="string",description="The phone number in international format"),
     *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
     *             required={"value","additionalType"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['value'] ) && $params['value'] != '' )
        {
            $item['value'] = $this->formatPhoneNumber( $params['value'] , $params['regionCode'] ) ;
        }

        if( isset( $params['additionalType'] )  )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'value'          => [ $item['value']            , 'required'                 ] ,
            'additionalType' => [ $params['additionalType'] , 'required|phoneNumberType' ]
        ] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'max(70)' ] ;
        }

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::put( $request , $response , $args ) ;
    }

    /**
     * @private
     */
    private function formatPhoneNumber( $value , $regionCode = NULL )
    {
        $phoneUtil = $this->container->phoneUtil ;

        try
        {
            $phone = $phoneUtil->parse( $value , $regionCode ) ;

            if( $phoneUtil->isValidNumber( $phone ) )
            {
                $value = $phoneUtil->format( $phone, PhoneNumberFormat::E164 ) ;
            }
            else
            {
                $value = NULL ;
            }
        }
        catch ( NumberParseException $e )
        {
            $value = NULL ;
        }

        return $value ;
    }
}
