<?php

namespace com\ooopener\controllers;


use com\ooopener\models\Collections;
use com\ooopener\validations\PostalAddressValidator;



use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Slim\Container ;

/**
 * The postal address controller.
 *
 * @OA\Schema(
 *     schema="PostalAddress",
 *     description="The mailing address",
 *     type="object",
 *     allOf={},
 *     @OA\Property(property="streetAddress",type="string",description="The street address"),
 *     @OA\Property(property="additionalAddress",type="string",description="The aaditional address"),
 *     @OA\Property(property="postOfficeBoxNumber",type="string",description="The post office box number for PO box addresses"),
 *     @OA\Property(property="addressLocality",type="string",description="The locality in which the street address is, and whish is in the region"),
 *     @OA\Property(property="postalCode",type="integer",description="The postal code"),
 *     @OA\Property(property="addressDepartment",type="string",description="The department in whishthe locality is"),
 *     @OA\Property(property="addressRegion",type="string",description="The region in which the locality is, and which is in the country"),
 *     @OA\Property(property="addressCountry",type="string",description="The country")
 * )
 */
class PostalAddressController extends Controller
{
    /**
     * Creates a new PostalAddressController instance.
     *
     * @param Container $container
     * @param Collections $model
     */
    public function __construct( Container $container , Collections $model = NULL )
    {
        parent::__construct( $container );
        $this->model = $model ;
    }

    /**
     * The model reference.
     */
    public $model ;

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'  => NULL ,
        'key' => '_key'
    ] ;

    /**
     * Returns the PostalAddress reference of the specific item reference.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return object the PostalAddress reference of the specific item reference.
     */
    public function get( Request $request = NULL , Response $response = NULL, array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $item = NULL ;

        try
        {
            $item = $this->model->get
            (
                $id ,
                [ 'key' => $key , 'fields' => 'address' ]
            ) ;

            if( $item )
            {
                $item = $item->address ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                return $this->success
                (
                    $response,
                    $item,
                    $this->getFullPath()
                );
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item  ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'  => NULL ,
        'key' => '_key'
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchAddress",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="streetAddress",type="string",description="The street address"),
     *             @OA\Property(property="additionalAddress",type="string",description="The aaditional address"),
     *             @OA\Property(property="postOfficeBoxNumber",type="string",description="The post office box number for PO box addresses"),
     *             @OA\Property(property="addressLocality",type="string",description="The locality in which the street address is, and whish is in the region"),
     *             @OA\Property(property="postalCode",type="integer",description="The postal code"),
     *             @OA\Property(property="addressDepartment",type="string",description="The department in whishthe locality is"),
     *             @OA\Property(property="addressRegion",type="string",description="The region in which the locality is, and which is in the country"),
     *             @OA\Property(property="addressCountry",type="string",description="The country")
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();

        try
        {
            if( !$this->model->exist( $id , [ 'key' => $key ] ) )
            {
                return $this->formatError( $response , '404' , [ 'patch(' . $id . ')' ] , NULL , 404 );
            }

            $item = [];

            if( isset( $params['postalCode'] ) )
            {
                $item['postalCode'] = $params['postalCode'] ;
            }

            if( isset( $params['postOfficeBoxNumber'] ) )
            {
                $item['postOfficeBoxNumber'] = $params['postOfficeBoxNumber'] ;
            }

            if( isset( $params['additionalAddress'] ) )
            {
                $item['additionalAddress'] = $params['additionalAddress'] ;
            }

            if( isset( $params['addressLocality'] ) )
            {
                $item['addressLocality'] = $params['addressLocality'] ;
            }

            if( isset( $params['addressCountry'] ) )
            {
                $item['addressCountry'] = $params['addressCountry'] ;
            }

            if( isset( $params['addressDepartment'] ) )
            {
                $item['addressDepartment'] = $params['addressDepartment'] ;
            }

            if( isset( $params['addressRegion'] ) )
            {
                $item['addressRegion'] = $params['addressRegion'] ;
            }

            if( isset( $params['streetAddress'] ) )
            {
                $item['streetAddress'] = $params['streetAddress'] ;
            }

            if( isset( $params['email'] ) )
            {
                $item['email'] = $params['email'] ;
            }

            if( isset($params['telephone']) && $params['telephone'] != '' )
            {
                $item['telephone'] = $this->formatPhoneNumber( $params['telephone'] , $params['regionCode'] ) ;
            }

            if( isset($params['faxNumber']) && $params['faxNumber'] != '' )
            {
                $item['faxNumber'] = $this->formatPhoneNumber( $params['faxNumber'] , $params['regionCode'] ) ;
            }

            $conditions =
            [
                'postalCode'        => [ $params['postalCode']        , 'number'   ],
                'streetAddress'     => [ $params['streetAddress']     , 'max(255)' ],
                'additionalAddress' => [ $params['additionalAddress'] , 'max(255)' ],
                'email'             => [ $params['email']             , 'email'    ]
            ];

            $validator = new PostalAddressValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {

                $address = [ 'address' => $item ] ;
                $update = $this->model->update
                (
                    $address,
                    $id,
                    [ 'key' => $key ]
                ) ;

                if( $update )
                {
                    $add = $this->model->get( $id , [ 'key' => $key , 'fields' => 'address,created,modified' ] ) ;

                    if( $add )
                    {
                        return $this->success
                        (
                            $response,
                            [
                                "address"  => $add->address ,
                                "created"  => $add->created ,
                                "modified" => $add->modified
                            ]
                        ) ;
                    }
                }
            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * @private
     */
    private function formatPhoneNumber( $value , $regionCode = NULL )
    {
        $phoneUtil = $this->container->phoneUtil ;

        try
        {
            $phone = $phoneUtil->parse( $value , $regionCode ) ;

            if( $phoneUtil->isValidNumber( $phone ) )
            {
                $value = $phoneUtil->format( $phone, PhoneNumberFormat::E164 ) ;
            }
            else
            {
                $value = NULL ;
            }
        }
        catch ( NumberParseException $e )
        {
            $value = NULL ;
        }

        return $value ;
    }
}
