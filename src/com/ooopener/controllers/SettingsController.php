<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Edges;
use Slim\Container;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

/**
 * The settings controller class.
 */
class SettingsController extends Controller
{
    /**
     * SettingsController constructor.
     * @param Container $container
     * @param Edges $edge
     * @param string $pathTo
     * @param string $pathFrom
     */
    public function __construct( Container $container , Edges $edge = NULL , $pathTo = null , $pathFrom = null )
    {
        parent::__construct($container);
        $this->edge = $edge ;
        $this->setPath( $pathTo , $pathFrom ) ;
    }

    /**
     * The edge reference.
     */
    public $edge ;

    /**
     * The fullPath reference.
     */
    public $fullPath ;

    public $path ;

    /**
     * The pattern reference.
     */
    public $pattern ;


    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'lang'     => NULL ,
        'limit'    => NULL ,
        'offset'   => NULL ,
        'search'   => NULL ,
        'sort'     => NULL ,
        'skin'     => NULL ,
        'skinFrom' => NULL ,
        'params'   => NULL
    ] ;

    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all' ) ;
        }

        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[ $this->fullPath ] ;

        $format = $this->container['format'] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- limit

            $limit = intval( isset($limit) ? $limit : $api['limit_default'] ) ;
            if( isset($params['limit']) )
            {
                $limit = filter_var
                (
                    $params['limit'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                            [
                                "min_range" => intval( $api['minlimit'] ),
                                "max_range" => intval( $api['maxlimit'] )
                            ]
                    ]
                ) ;
                $params['limit'] = intval( ($limit !== FALSE) ? $limit : $api['limit_default'] ) ;
            }

            // ----- offset

            $offset = intval( isset($offset) ? $offset : $api['offset_default'] ) ;
            if( isset($params['offset']) )
            {
                $offset = filter_var
                (
                    $params['offset'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                            [
                                "min_range" => intval( $api['minlimit'] ),
                                "max_range" => intval( $api['maxlimit'] )
                            ]
                    ]
                ) ;
                $params['offset'] = intval( ($offset !== FALSE) ? $offset : $api['offset_default'] ) ;
            }

            // ----- skin
            if( isset($params['skin']) )
            {
                $skin = $params['skin'] ;
            }

            // ----- skinFrom
            if( isset($params['skinFrom']) )
            {
                $skinFrom = $params['skinFrom'] ;
            }

            // ----- sort

            if( isset($params['sort']) )
            {
                $sort = $params['sort'] ;
            }
            else if( is_null($sort) )
            {
                $sort = $set[ 'sort_default' ] ;
            }

            // ----- search

            if( isset($params['search']) )
            {
                $search = $params['search'] ;
            }

        }

        // ------------

        $result  = NULL ;
        $options = NULL ;

        try
        {
            $init =
            [
                'search'      => $search
            ];

            $result = $this->edge->allEdges
            ([
                'limit'       => $limit ,
                'offset'      => $offset ,
                'skin'        => $skin,
                'skinFrom'    => $skinFrom,
                'sort'        => $sort,
                'search'      => $search,
                'lang'        => $lang
            ]) ;

            //$options = [ 'total' => (int) $this->edge->foundRows() ] ;
            $options = [ 'total' => $this->container[$this->edge->to['controller']]->model->count( $init ) ] ;

            // get setting
            $path = $request->getUri()->getPath();
            $setting = $this->container->settingsList->get( $path , [ 'key' => 'path' , 'queryFields' => $this->container->settingsListController->getFields() ] );

            $options[ 'object' ] = $setting ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'all' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            switch( $format )
            {
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response,
                        $result,
                        $this->getFullPath( $params ),
                        is_array($result) ? count($result) : NULL ,
                        $options
                    ) ;
                }
            }
        }

        return $result ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'from'    => NULL ,
        'to'      => NULL
    ] ;

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $to . ',' . $from . ')' ) ;
        }

        try
        {
            $idFrom = $this->edge->from['name'] . '/' . $from ;
            $idTo = $this->edge->to['name'] . '/' . $to ;

            // check to
            $check = $this->edge->existEdge( $idFrom , $idTo ) ;

            if( !$check )
            {
                return $this->formatError( $response , '404', [ 'delete(' . $to . ',' . $from . ')' ], NULL , 404 );
            }


            // delete
            $result = $this->edge->deleteEdge( $idFrom , $idTo ) ;

            if( $result )
            {
                return $this->success( $response , 'ok' );
            }
            else
            {
                return $this->error( $response ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $to . ',' . $from . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'deleteAll' method options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'to'      => NULL
    ] ;

    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $to . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        if( $params )
        {
            if( isset( $params['list' ]) )
            {
                $list = $params['list'] ;

                try
                {
                    $items = explode( ',' , $list ) ;

                    // remove duplicate value
                    $items = array_unique( $items ) ;

                    // check to exists
                    $checkTo = $this->container[ $this->edge->to['controller'] ]->get( NULL , NULL , [ 'id' => $to ] ) ;

                    if( !$checkTo )
                    {
                        return $this->formatError( $response , '404', [ 'deleteAll(' . $to . ')' ], NULL , 404 );
                    }

                    $idTo = $this->edge->to['name'] . '/' . $to ;
                    foreach( $items as $item )
                    {
                        $idFrom = $this->edge->from['name'] . '/' . $item ;

                        // check exist
                        $exist = $this->edge->existEdge( $idFrom , $idTo ) ;

                        if( $exist )
                        {
                            // delete to edge
                            $this->edge->deleteEdge( $idFrom , $idTo ) ;
                        }
                    }

                    return $this->success( $response , 'ok' ) ;

                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'deleteAll(' . $to . ')' , $e->getMessage() ] , NULL , 500 );
                }
            }
        }

        return $this->error( $response , 'no list' ) ;
    }

    /**
     * The default 'exists' method options.
     */
    const ARGUMENTS_EXISTS_DEFAULT =
    [
        'from' => NULL ,
        'to'   => NULL
    ] ;

    public function exists( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_EXISTS_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' exists(' . $to . ',' . $from . ')' ) ;
        }

        try
        {
            $idFrom = $this->edge->from['name'] . '/' . $from ;
            $idTo = $this->edge->to['name'] . '/' . $to ;

            // check edge exist
            $check = $this->edge->existEdge( $idFrom , $idTo ) ;

            if( $response )
            {
                $this->success( $response , $check ) ;
            }
            else
            {
                return $check ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'exists(' . $to. ',' . $from . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'from'    => NULL ,
        'to'      => NULL ,
        'lang'    => NULL ,
        'params'  => NULL ,
        'skin'    => NULL
    ] ;

    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $to . ',' . $from . ')' ) ;
        }

        $api = $this->container->settings['api'] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- skin
            if( isset($params['skin']) )
            {
                $skin = $params['skin'] ;
            }
        }

        // ----------------

        $item = NULL ;

        try
        {
            $item = $this->edge->getEdge( $from , $to , [ 'lang' => $lang , 'skin' => $skin ] ) ;

            // hack to only show edge result
            $item = $item->edge ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $to . ',' . $from . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            return $this->success
            (
                $response,
                $item,
                $this->getFullPath( $params )
            ) ;
        }

        return $item ;
    }

    /**
     * The default 'put' method options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'to'    => NULL
    ] ;

    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $to . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        if( $params )
        {
            if( isset( $params['list' ]) )
            {
                $list = $params['list'] ;

                try
                {
                    $items = explode( ',' , $list ) ;

                    // remove duplicate value
                    $items = array_unique( array_filter( $items ) ) ;

                    // check to exists
                    $checkTo = $this->container[ $this->edge->to['controller'] ]->get( NULL , NULL , [ 'id' => $to ] ) ;

                    if( !$checkTo )
                    {
                        return $this->formatError( $response , '404', [ 'put(' . $to . ')' ], NULL , 404 );
                    }

                    // check list
                    if( count( $items ) > 0 )
                    {
                        $count = $this->container[ $this->edge->from['controller'] ]->model->existAll( $items ) ;
                    }
                    else
                    {
                        $count = 0 ;
                    }

                    if( $count == count( $items ) )
                    {
                        // get current edges of 'to'
                        $currentEdges = $this->edge->getEdge( NULL , $to ) ;

                        /// hack
                        $currentEdges = $currentEdges->edge ;

                        $currents = [] ;

                        foreach( $currentEdges as $edge )
                        {
                            array_push( $currents , (string) $edge['id'] ) ;
                        }

                        // compare lists (currents and items)
                        $addItems = array_diff( $items , $currents ) ;

                        ///////  ADD edges
                        $idTo = $this->edge->to['name'] . '/' . $to ;
                        foreach( $addItems as $item )
                        {
                            $idFrom = $this->edge->from['name'] . '/' . $item ;

                            // check not exist
                            $exist = $this->edge->existEdge( $idFrom , $idTo ) ;

                            if( !$exist )
                            {
                                // save to edge
                                $this->edge->insertEdge( $idFrom , $idTo ) ;
                            }
                        }

                        ///////  REMOVE edges
                        $removeItems = array_diff( array_merge( $currents , $addItems ) , $items ) ;

                        foreach( $removeItems as $item )
                        {
                            $idFrom = $this->edge->from['name'] . '/' . $item ;

                            // check exist
                            $exist = $this->edge->existEdge( $idFrom , $idTo ) ;

                            if( $exist )
                            {
                                // delete to edge
                                $this->edge->deleteEdge( $idFrom , $idTo ) ;
                            }
                        }


                        // get result
                        $result = $this->edge->getEdge( NULL , $to ) ;

                        /// hack
                        $result = $result->edge ;

                        return $this->success( $response , $result ) ;
                    }
                    else
                    {
                        return $this->error( $response , 'some identifiers in the list are not valid' ) ;
                    }
                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'put(' . $to . ')' , $e->getMessage() ] , NULL , 500 );
                }
            }
        }

        return $this->error( $response , 'no list' ) ;
    }

    /**
     * The default 'patch' method options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'from'  => NULL,
        'to'    => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $to . ',' . $from . ')' ) ;
        }

        try
        {
            // check to exists
            $checkTo = $this->container[ $this->edge->to['controller'] ]->get( NULL , NULL , [ 'id' => $to ] ) ;

            if( !$checkTo )
            {
                return $this->formatError( $response , '404', [ 'patch(' . $to . ',' . $from . ')' ], NULL , 404 );
            }

            // check from exists
            $checkFrom = $this->container[ $this->edge->from['controller'] ]->get( NULL , NULL , [ 'id' => $from ] ) ;

            if( !$checkFrom )
            {
                return $this->formatError( $response , '404', [ 'patch(' . $to . ',' . $from . ')' ], NULL , 404 );
            }

            $idFrom = $this->edge->from['name'] . '/' . $from ;
            $idTo = $this->edge->to['name'] . '/' . $to ;

            // check edge exist
            $check = $this->edge->existEdge( $idFrom , $idTo ) ;

            if( !$check )
            {
                // add
                $this->edge->insertEdge( $idFrom , $idTo ) ;
            }

            // get result
            $result = $this->edge->getEdge( NULL , $to ) ;

            /// hack
            $result = $result->edge ;

            return $this->success( $response , $result );
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $to . ',' . $from . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'putAll' method options.
     */
    const ARGUMENTS_PUT_ALL_DEFAULT =
    [
        'to'    => NULL
    ] ;

    public function putAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PUT_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' putAll(' . $to . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        if( $params )
        {
            if( isset( $params['list' ]) )
            {
                $list = $params['list'] ;

                try
                {
                    $items = explode( ',' , $list ) ;

                    // remove duplicate value
                    $items = array_unique( array_filter( $items ) ) ;

                    // check to exists
                    $checkTo = $this->container[ $this->edge->to['controller'] ]->get( NULL , NULL , [ 'id' => $to ] ) ;

                    if( !$checkTo )
                    {
                        return $this->formatError( $response , '404', [ 'putAll(' . $to . ')' ], NULL , 404 );
                    }

                    // check list
                    if( count( $items ) > 0 )
                    {
                        $count = $this->container[ $this->edge->from['controller'] ]->model->existAll( $items ) ;
                    }
                    else
                    {
                        $count = 0 ;
                    }

                    if( $count == count( $items ) )
                    {
                        $idTo = $this->edge->to['name'] . '/' . $to ;
                        foreach( $items as $item )
                        {
                            $idFrom = $this->edge->from['name'] . '/' . $item ;

                            // check not exist
                            $exist = $this->edge->existEdge( $idFrom , $idTo ) ;

                            if( !$exist )
                            {
                                // save to edge
                                $this->edge->insertEdge( $idFrom , $idTo ) ;
                            }
                        }

                        // get result
                        $result = $this->edge->getEdge( NULL , $to ) ;

                        /// hack
                        $result = $result->edge ;

                        return $this->success( $response , $result ) ;
                    }
                    else
                    {
                        return $this->error( $response , 'some identifiers in the list are not valid' ) ;
                    }
                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'putAll(' . $to . ')' , $e->getMessage() ] , NULL , 500 );
                }
            }
        }

        return $this->error( $response , 'no list' ) ;
    }

    public function setPath( $to = NULL , $from = NULL )
    {
        $this->path = '/settings/' ;
        $this->fullPath = $to . '/' . $from ;
        $this->pattern = $to . '{0}/' . $from . '{1}';
    }
}
