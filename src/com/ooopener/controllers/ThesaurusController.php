<?php

namespace com\ooopener\controllers;

use com\ooopener\helpers\Strings;
use com\ooopener\things\Thing;
use com\ooopener\validations\ThesaurusValidator;



use Exception;
use Imagick ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use com\ooopener\data\Builder ;

use com\ooopener\models\Thesaurus ;
use com\ooopener\things\Word ;

/**
 * The thesaurus controller class.
 */
class ThesaurusController extends Controller implements Builder
{
    /**
     * Creates a new ThesaurusController instance.
     *
     * @param Container $container
     * @param Thesaurus $model
     * @param string $path
     */
    public function __construct( Container $container , Thesaurus $model = NULL , $path = NULL )
    {
        parent::__construct( $container );
        $this->model = $model ;
        $this->setPath( $path ) ;
    }

    /**
     * The full path expression.
     */
    public $fullPath = '' ;

    /**
     * The icon path expression.
     */
    public $iconPath = '' ;

    /**
     * The model reference.
     */
    public $model ;

    /**
     * The word entry url pattern expression.
     */
    public $pattern = '' ;

    /**
     * The main route path expression.
     */
    public $path = '' ;

    /**
     * The settings object.
     */
    public $settings ;

    // --------------------------------

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="Thesaurus",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/status")},
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(property="name",description="The name of the resource",@OA\Items(ref="#/components/schemas/text"),example={"en":"English","fr":"French"}),
     *     @OA\Property(type="string",property="alternateName",description="The name of the resource"),
     *     @OA\Property(property="description",description="The description of the resource",@OA\Items(ref="#/components/schemas/text"),example={"en":"English","fr":"French"}),
     *     @OA\Property(type="string",property="format",description="Format of the resource"),
     *     @OA\Property(type="string",property="pattern",description="Pattern of the resource"),
     *     @OA\Property(type="string",property="validator",description="Validator of the resource"),
     *     @OA\Property(type="string",property="image",description="Image of the resource",format="uri"),
     *     @OA\Property(type="string",property="color",description="Color of the resource"),
     *     @OA\Property(type="string",property="bgcolor",description="Background color of the resource"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     *
     * @OA\Response(
     *     response="thesaurusResponse",
     *     description="Thesaurus result",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Thesaurus")
     *     )
     * )
     *
     * @OA\Response(
     *     response="thesaurusListResponse",
     *     description="Result list of thesaurus",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/Thesaurus"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL            ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID              ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL             ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_TRANSLATE       ] ,
        'alternateName' => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'description'   => [ 'filter' =>  Thing::FILTER_TRANSLATE       ] ,
        'format'        => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'pattern'       => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'validator'     => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'image'         => [ 'filter' =>  Thing::FILTER_THESAURUS_IMAGE ] ,
        'color'         => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'bgcolor'       => [ 'filter' =>  Thing::FILTER_DEFAULT         ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME        ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME        ]
    ];

    public function getFields( $skin = NULL )
    {
        $fields = [] ;

        foreach( static::CREATE_PROPERTIES as $key => $options )
        {
            $filter = array_key_exists( 'filter' , $options ) ? $options['filter'] : NULL ;
            $skins = array_key_exists('skins', $options) ? $options['skins'] : NULL ;

            if( isset($skins) && is_array($skins) )
            {
                if( is_null($skin) || !in_array( $skin, $skins ) )
                {
                    continue;
                }
            }

            $fields[$key] = [ 'filter' => $filter ] ;

            //// set unique value for edge, edgeSingle and join filters
            switch( $filter )
            {
                case Thing::FILTER_EDGE :
                case Thing::FILTER_EDGE_SINGLE :
                    $fields[$key]['unique'] = $key . '_e' . mt_rand() ;
                    break;
                case Thing::FILTER_JOIN :
                    $fields[$key]['unique'] = $key . '_j' . mt_rand() ;
                    break;
            }
        }

        return $fields ;
    }

    public function sortAfter( $items )
    {
        $sortable = $this->model->sortable ;

        if( $sortable && array_key_exists( 'after' , $sortable ) )
        {
            $after = explode( '.' , $sortable['after'] ) ;

            if( $after && is_array( $after ) && count( $after ) == 2 )
            {
                usort( $items , function ( $a , $b ) use ( $after )
                {
                    return strcmp( $a->{$after[0]}->{$after[1]} , $b->{$after[0]}->{$after[1]} ) ;
                }) ;
            }
        }
        return $items ;
    }

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'  => TRUE ,
        'facets'  => NULL ,
        'groupBy' => NULL ,
        'lang'    => NULL ,
        'limit'   => NULL ,
        'offset'  => NULL ,
        'search'  => NULL ,
        'sort'    => NULL ,
        'params'  => NULL
    ] ;

    /**
     * Returns of all the words.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed of all the words.
     */
    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all' ) ;
        }

        $api = $this->container->settings['api'] ;
        $set = $this->getSettings() ;

        $format = $this->container['format'] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- limit

            $limit = intval( isset($limit) ? $limit : $api['limit_default'] ) ;
            if( isset($params['limit']) )
            {
                $limit = filter_var
                (
                    $params['limit'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api['minlimit'] ),
                            "max_range" => intval( $api['maxlimit'] )
                        ]
                    ]
                ) ;
                $params['limit'] = intval( ($limit !== FALSE) ? $limit : $api['limit_default'] ) ;
            }

            // ----- offset

            $offset = intval( isset($offset) ? $offset : $api['offset_default'] ) ;
            if( isset($params['offset']) )
            {
                $offset = filter_var
                (
                    $params['offset'],
                    FILTER_VALIDATE_INT,
                    [
                        'options' =>
                        [
                            "min_range" => intval( $api['minlimit'] ),
                            "max_range" => intval( $api['maxlimit'] )
                        ]
                    ]
                ) ;
                $params['offset'] = intval( ($offset !== FALSE) ? $offset : $api['offset_default'] ) ;
            }

            // ----- facets

            if( isset( $params[ 'facets' ] ) )
            {

                try
                {
                    if( is_string( $params['facets'] ) )
                    {
                        $facets = array_merge
                        (
                            json_decode( $params[ 'facets' ] , TRUE ) ,
                            isset( $facets ) ? $facets : []
                        );
                    }
                    else
                    {
                        $facets = $params['facets'] ;
                    }

                }
                catch( Exception $e )
                {
                    $this->container->logger->warn( $this . ' all failed, the facets params failed to decode the json expression: ' . $params[ 'facets' ] );
                }
            }

            // ----- sort

            if( isset($params['sort']) )
            {
                $sort = $params['sort'] ;
            }
            else if( is_null($sort) )
            {
                $sort = $set[ 'sort_default' ] ;
            }

            // ----- groupBy

            if( isset($params['groupBy']) )
            {
                $groupBy = $params['groupBy'] ;
            }

            // ----- search

            if( isset($params['search']) )
            {
                $search = $params['search'] ;
            }

            // ----- active

            if( isset($params['active']) )
            {
                if
                (
                    $params['active'] == "0"  ||
                    $params['active'] == 'false'  ||
                    $params['active'] == 'FALSE'
                )
                {
                    $active = NULL  ;
                }
            }
        }

        // ------------

        $result  = NULL ;
        $options = NULL ;

        try
        {
            $init =
            [
                'active'      => $active ,
                'facets'      => $facets ,
                'search'      => $search
            ];

            $result = $this->model->all
            ([
                'active'      => $active ,
                'facets'      => $facets ,
                'limit'       => $limit ,
                'offset'      => $offset ,
                'sort'        => $sort,
                'groupBy'     => $groupBy,
                'search'      => $search,
                'lang'        => $lang,
                'queryFields' => $this->getFields()
            ]) ;

            //$options = [ 'total' => (int) $this->model->foundRows() ] ;
            $options = [ 'total' => $this->model->count( $init ) ] ;

            // get thesaurus
            $path = $request->getUri()->getPath();
            $thesaurus = $this->container->thesaurusList->get( $path , [ 'key' => 'path' , 'queryFields' => $this->container->thesaurusListController->getFields() ] );

            $options[ 'object' ] = $thesaurus ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'all' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            switch( $format )
            {
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response,
                        $result,
                        $this->getFullPath( $params ),
                        is_array($result) ? count($result) : NULL ,
                        $options
                    ) ;
                }
            }
        }

        return $result ;
    }

    // --------------------------------


    /**
     * The default 'build' method options.
     */
    const ARGUMENTS_BUILD_DEFAULT =
    [
        'lang' => NULL
    ] ;

    /**
     * Build a new object.
     *
     * @param object $init
     * @param array $options
     *
     * @return Word|NULL
     */
    public function build( $init = NULL , array $options = [] )
    {
        extract( array_merge( self::ARGUMENTS_BUILD_DEFAULT , is_array($options) ? $options : [] ) ) ;
        return $this->create( $init , $lang ) ;
    }

    /**
     * Creates and populates a new item.
     *
     * @param object $init
     * @param string $lang
     *
     * @return object|NULL
     */
    public function create( $init , $lang = NULL )
    {
        return $init ;
    }


    // --------------------------------

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_COUNT_DEFAULT =
    [
        'active' => TRUE
    ] ;

    /**
     * Returns the number of items.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return integer|Response the number of items.
     */
    public function count( Request $request = NULL , Response $response = NULL, array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_COUNT_DEFAULT , $args ) ) ;

        $count = 0 ;

        if( $response )
        {
            $this->logger->debug( $this . ' count' ) ;
        }

        try
        {
            $count = $this->model->count( [ 'active' => $active ] ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'count' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            return $this->success( $response , $count , $this->getFullPath() ) ;
        }

        return $count ;
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'   => NULL
    ] ;

    /**
     * Delete the item
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $id . ')' ) ;
        }

        try
        {
            $result = $this->model->delete( $id ) ;

            if( $result )
            {
                return $this->success( $response , $id );
            }
            else
            {
                return $this->error( $response , 'error' ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    /**
     * Delete all thesaurus
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $this->logger->debug( $this . ' deleteAll()' ) ;

        $params = $request->getParsedBody() ;

        if( $params )
        {
            if( isset( $params['list' ]) )
            {
                $list = $params['list'] ;

                try
                {
                    $items = explode( ',' , $list ) ;

                    $result = $this->model->deleteAll( $items );

                    if( $result )
                    {
                        return $this->success( $response , "ok" );
                    }
                    else
                    {
                        return $this->error( $response , "not deleted" ) ;
                    }
                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'deleteAll()' , $e->getMessage() ] , NULL , 500 );
                }
            }
        }

        return $this->error( $response , 'no list' ) ;
    }

    // --------------------------------

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'active'  => TRUE ,
        'id'      => NULL ,
        'item'    => NULL ,
        'lang'    => NULL ,
        'params'  => NULL
    ] ;

    /**
     * Returns the specific item by id.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $api = $this->container->settings['api'] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }
        }

        // ----------------

        $item = NULL ;

        try
        {
            $item = $this->model->get( $id , [ 'lang' => $lang , 'queryFields' => $this->getFields() ] ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                // add header
                $response = $this->container->cache->withLastModified( $response , $item->modified );

                return $this->success
                (
                    $response,
                    $item,
                    $this->getFullPath( $params )
                ) ;
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item ;
    }

    // --------------------------------

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_BY_ALTERNATE_NAME_DEFAULT =
    [
        'active'  => TRUE ,
        'name'    => NULL ,
        'item'    => NULL ,
        'lang'    => NULL ,
        'params'  => NULL
    ] ;

    /**
     * Returns the specific item by the alternateName.
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return object the specific item by the alternateName.
     */
    public function getByAlternateName( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_BY_ALTERNATE_NAME_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' getByAlternateName(' . $name . ')' ) ;
        }

        $api = $this->container->settings['api'] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }
        }

        // ----------------

        $item = NULL ;

        try
        {
            $item = $this->model->getByAlternateName( $name ) ;

            if( $item )
            {
                $item = $this->create( $item , $lang ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'getByAlternateName(' . $name . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                return $this->success
                (
                    $response,
                    $item,
                    $this->getFullPath( $params )
                ) ;
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item ;
    }

    // --------------------------------

    /**
     * The default 'get method options.
     */
    const ARGUMENTS_GET_IMAGE_DEFAULT =
    [
        'id'      => NULL ,
        'bgcolor' => NULL ,
        'color'   => NULL ,
        'margin'  => 2    ,
        'params'  => NULL ,
        'render'  => NULL ,
        'shadow'  => NULL ,
        'w'       => NULL ,
        'h'       => NULL
    ] ;

    /**
     * Display the image of the specific item.
     * Ex: ../image?shadow=true
     * Ex: ../image?render=map&bgcolor=ff0000&color=ffffff&margin=4&shadow=40,2,20,20
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return Response the image of the specific item
     */
    public function getImage( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_IMAGE_DEFAULT , $args ) ) ;

        $icons = $this->config['icons'] ;
        $w     = $icons['width'] ;
        $h     = $icons['height'] ;

        if( $response )
        {
            $this->logger->debug( $this . ' getImage(' . $id . ')' ) ;
        }

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            if( isset($params['w']) )
            {
                $w = filter_var
                (
                    $params['w'],
                    FILTER_VALIDATE_INT,
                    [ 'options' => [ "min_range" => $icons['minrange'], "max_range" => $icons['maxrange'] ] ]
                ) ;
                $w = ($w !== FALSE) ? $w : $icons['width'] ;
            }

            if( isset($params['h']) )
            {
                $h = filter_var
                (
                    $params['h'],
                    FILTER_VALIDATE_INT,
                    [ 'options' => [ "min_range" => $icons['minrange'], "max_range" => $icons['maxrange'] ] ]
                ) ;
                $h = ($h !== FALSE) ? $h : $icons['height'] ;
            }

            if( isset($params['color']) )
            {
                $color = filter_var
                (
                    strtolower($params['color']),
                    FILTER_VALIDATE_REGEXP,
                    [ 'options' => [ 'regexp' => '/^[a-f0-9]{6}$/i' ] ]
                ) ;
                $color = ($color !== FALSE) ? $color : NULL ;
            }

            if( isset($params['bgcolor']) )
            {
                $bgcolor = filter_var
                (
                    strtolower($params['bgcolor']),
                    FILTER_VALIDATE_REGEXP,
                    [ 'options' => [ 'regexp' => '/^[a-f0-9]{6}$/i' ] ]
                ) ;
                $bgcolor = ($bgcolor !== FALSE) ? $bgcolor : NULL ;
            }

            if( !empty($params['render']) )
            {
                if( in_array( strtolower($params['render']) , $icons['renders'] ) )
                {
                    $params['render'] = $render
                                      = strtolower($params['render']) ;
                }
            }

            if( isset($params['margin']) )
            {
                $margin = filter_var
                (
                    $params['margin'],
                    FILTER_VALIDATE_INT,
                    [ 'options' => [ "min_range" => $icons['minmargin'], "max_range" => $icons['maxmargin'] ] ]
                ) ;
                $margin = ($margin !== FALSE) ? $margin : $icons['margin'] ;
            }

            if( !empty($params['shadow']) )
            {
                $shadow = $params['shadow'] ;
            }
        }

        // -------

        $url = $this->getImagePath( $id , TRUE ) ;

        if( file_exists( $url ) )
        {
            // add header
            $response = $this->container->cache->withLastModified( $response , filemtime( $url ) );

            $response = $this->container->image->icon
            (
                $response , $url ,
                [
                    'w'       => $w ,
                    'h'       => $h ,
                    'bgcolor' => $bgcolor ,
                    'color'   => $color ,
                    'render'  => $render,
                    'margin'  => $margin,
                    'shadow'  => $shadow
                ]
            ) ;

            return $response ;
        }

        return $this->formatError( $response , '204' , [ 'getImage(' . $id . ')' ] , NULL , 204 ) ;
    }

    /**
     * Returns the specific image path.
     *
     * @param integer $id
     * @param bool $useDefault
     * @param bool $lazy
     *
     * @return string
     */
    public function getImagePath( $id , $useDefault = FALSE , $lazy = FALSE )
    {
        try
        {
            if( !is_dir( $this->iconPath ) )
            {
                if( !mkdir( $this->iconPath , 0755 , TRUE ) )
                {
                    $this->container->logger->warn( $this . ' getImagePath(' . $id . ') failed, can\'t create the image path:[' . $this->iconPath . ']' ) ;
                }
            }

            $set  = $this->getSettings() ;
            $icon = $this->iconPath
            . '/'
            . (isset($set['iconPrefix']) ? $set['iconPrefix'] : '')
            . str_pad( $id , 4, "0", STR_PAD_LEFT )
            .  $set['iconSuffix'] ;

            if( $lazy )
            {
                return $icon ;
            }

            if( file_exists($icon) )
            {
                return $icon ;
            }

            if( $useDefault )
            {
                $icon = $this->iconPath
                . '/'
                . $set['iconDefault']
                . (isset($set['iconSuffix']) ? $set['iconSuffix'] : '') ;

                if( file_exists($icon) )
                {
                    return $icon ;
                }
                else
                {
                    $this->container->logger->warn( $this . ' getImagePath(' . $id . ') failed, the default image don\'t exist with the path:[' . $icon . ']' ) ;
                }
            }

        }
        catch( Exception $e )
        {
            $this->container->logger->error( $this . ' getImagePath(' . $id . ') failed, the following Exception occurred: ' . $e->getMessage() ) ;
        }
    }

    /**
     * Returns the current thesaurus settings
     *
     * @return array
     */
    public function getSettings()
    {
        if( isset( $this->settings ) )
        {
            return $this->settings ;
        }

        try
        {
            $this->settings = $this->container->settings[ $this->path ] ;
        }
        catch( Exception $e )
        {
            $this->logger->warn( $this . ' getSettings failed, ' . $e->getMessage() ) ;
            $this->settings = NULL ;
        }

        return $this->settings ;
    }

    /**
     * Add new thesaurus
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        if( isset( $request ) )
        {
            $params = $request->getParsedBody() ;

            $init = [] ;
            $conditions = [] ;

            $init['active'] = 1 ;
            $init['path'] = 'thesaurus/' . $this->path ;

            if( isset( $params['name'] ) )
            {
                $init['name'] = $this->filterLanguages( $params['name'] ) ;
            }

            if( isset( $params['description'] ) )
            {
                $init['description'] = $this->filterLanguages( $params['description'] ) ;
            }

            // -------- alternateName

            if( isset($params['alternateName']) )
            {
                $init['alternateName'] = strtolower( Strings::latinize( $params['alternateName'] ) ) ;
                $conditions['alternateName'] = [ $init['alternateName'] , 'required|alnumSlash|uniqueAlternateName|max(150)' ] ;
            }

            // -------- colors

            if( isset( $params['color'] ) )
            {
                $init['color'] = $params['color'] ;
                $conditions['color'] = [ $params['color'] , 'min(3)|max(6)|isColor' ] ;
            }

            if( isset( $params['bgcolor'] ) )
            {
                $init['bgcolor'] = $params['bgcolor'] ;
                $conditions['bgcolor'] = [ $params['bgcolor'] , 'min(3)|max(6)|isColor' ] ;
            }

            if( isset( $params['pattern'] ) )
            {
                $init['pattern'] = $params['pattern'] ;
            }

            if( isset( $params['format'] ) )
            {
                $init['format'] = $params['format'] ;
            }

            if( isset( $params['validator'] ) )
            {
                $init['validator'] = $params['validator'] ;
            }

            $validator = new ThesaurusValidator( $this->container , $this->model ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {
                try
                {
                    $index = $this->model->insert( $init ) ;

                    if( $index )
                    {
                        return $this->success( $response , $this->model->get( $index->_key , [ 'queryFields' => $this->getFields() ] ) ) ;
                    }
                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'post' , $e->getMessage() ] , NULL , 500 );
                }
            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }

        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * Put new values for the item
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        $item = $this->model->get( $id , [ 'fields' => '_key,alternateName' ] ) ;

        if( is_null($item) )
        {
            return $this->formatError( $response , '404', [ 'put(' . $id . ')' ], NULL , 404 );
        }

        if( isset( $request ) )
        {
            $params = $request->getParsedBody() ;

            $init = [] ;
            $conditions = [] ;

            if( isset( $params['name'] ) )
            {
                $init['name'] = $this->filterLanguages( $params['name'] ) ;
            }

            if( isset( $params['description'] ) )
            {
                $init['description'] = $this->filterLanguages( $params['description'] ) ;
            }

            // -------- alternateName

            if( isset($params['alternateName']) )
            {
                if( $params['alternateName'] != "" )
                {
                    $init['alternateName'] = strtolower( Strings::latinize( $params['alternateName'] ) ) ;
                    $conditions['alternateName'] = [ $init['alternateName'] , 'alnumSlash|uniqueAlternateName(' . $item->alternateName . ')|max(150)' ] ;
                }
                else
                {
                    $init['alternateName'] = NULL ;
                }

            }

            // -------- colors

            if( isset( $params['color'] ) && $params['color'] != '' )
            {
                $init['color'] = $params['color'] ;
                $conditions['color'] = [ $params['color'] , 'min(3)|max(6)|isColor' ] ;
            }
            else
            {
                $init['color'] = NULL ;
            }

            if( isset( $params['bgcolor'] ) && $params['bgcolor'] != '' )
            {
                $init['bgcolor'] = $params['bgcolor'] ;
                $conditions['bgcolor'] = [ $params['bgcolor'] , 'min(3)|max(6)|isColor' ] ;
            }
            else
            {
                $init['bgcolor'] = NULL ;
            }

            if( isset( $params['pattern'] ) )
            {
                $init['pattern'] = $params['pattern'] ;
            }

            if( isset( $params['format'] ) )
            {
                $init['format'] = $params['format'] ;
            }

            if( isset( $params['validator'] ) )
            {
                $init['validator'] = $params['validator'] ;
            }

            $validator = new ThesaurusValidator( $this->container , $this->model ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {
                try
                {
                    $result = $this->model->update( $init ,$id ) ;

                    if( $result )
                    {
                        return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ) ;
                    }
                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'put' , $e->getMessage() ] , NULL , 500 );
                }
            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }

        }
    }

    /**
     * The default 'patchActive' methods options.
     */
    const ARGUMENTS_PATCH_ACTIVE_DEFAULT =
    [
        'id'   => NULL ,
        'bool' => NULL
    ] ;

    /**
     * Updates the activity of the item
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchActive( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_ACTIVE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patchActive(' . $id . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id ) ;

            if( is_null($item) )
            {
                return $this->formatError( $response , '404', [ 'patchActive(' . $id . ')' ], NULL , 404 );
            }

            if( is_string( $bool ) )
            {
                $bool = strtolower( $bool ) ;
            }

            if( $bool != 'true' && $bool != 'false' )
            {
                return $this->formatError( $response , '400', [ 'patchActive(' . $id . ',' . $bool . ')' ] );
            }

            $active = $bool == 'true' ;


            $result = $this->model->update( [ 'active' => (int) $active ] , $id ) ;

            if( $result )
            {
                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) );
            }
            else
            {
                return $this->error( $response , 'error' ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patchActive(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    /**
     * The default 'deleteImage' methods options.
     */
    const ARGUMENTS_DELETEIMAGE_DEFAULT =
    [
        'id'   => NULL
    ] ;

    public function deleteImage( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETEIMAGE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteImage(' . $id . ')' ) ;
        }

        try
        {

            $item = $this->model->get( $id ) ;

            if( is_null($item) )
            {
                return $this->formatError( $response , '404', [ 'deleteImage(' . $id . ')' ], NULL , 404 );
            }

            $flag = FALSE ;
            $icon = $this->getImagePath( $id ) ;

            if( file_exists($icon) )
            {
                $flag = unlink( $icon ) ;
            }

            $update = $this->model->update( [ "image" => false ] , $id ) ;

            return $this->success( $response , (int) $id ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteImage(' . $id . ')' , $e->getMessage() ], NULL , 500 );
        }
    }

    /**
     * The default 'postImage' methods options.
     */
    const ARGUMENTS_POST_IMAGE_DEFAULT =
    [
        'id'   => NULL
    ] ;

    public function postImage( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_IMAGE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' postImage(' . $id . ')' ) ;
        }

        $icons = $this->config['icons'] ;

        try
        {
            $item = $this->model->get( $id ) ;

            if( is_null($item) )
            {
                return $this->formatError( $response , '404', [ 'postImage(' . $id . ')' ], NULL , 404 );
            }

            if( $request )
            {
                $files = $request->getUploadedFiles() ;
                $params = $request->getParsedBody() ;

                $filename = NULL ;

                if ( empty( $files ) )
                {
                    return $this->error( $response , ' postImage(' . $id . ') failed, the $files is empty.' , "400" ) ;
                }
                else if ( !isset( $files['icon'] ) )
                {
                    return $this->error( $response , ' postImage(' . $id . ') failed, the $files["file"] is not set.' , "400" ) ;
                }
                else
                {
                    //$set = $this->getSettings() ;

                    $file = $files['icon'] ;
                    $error = $file->getError() ;

                    if( $error === UPLOAD_ERR_OK )
                    {
                        $filename = $file->getClientFilename() ;
                        $size  = $file->getSize() ;
                        $type  = $file->getClientMediaType() ;

                        // check filesize
                        if( array_key_exists( 'maxsize' , $icons ) )
                        {
                            // in bytes
                            $limitSize = $icons['maxsize'] * 1048576 ;
                            if( $size > $limitSize )
                            {
                                return $this->formatError( $response , '400', NULL , [ "message" => [ 'size' => 'The size of the file is too big.' ] ] ) ;
                            }
                        }

                        // Really check the type of the file
                        $finfo = finfo_open( FILEINFO_MIME_TYPE ) ;
                        $checkType = finfo_file( $finfo , $file->file ) ;
                        finfo_close( $finfo ) ;

                        if( $type !== $checkType )
                        {
                            return $this->formatError( $response , '400', NULL , [ "message" => [ 'type' => 'The uploaded file type mismatch.' ] ] ) ;
                        }

                        $typesAvailable = NULL ;

                        if( array_key_exists( 'formats' , $icons ) )
                        {
                            $typesAvailable = $icons['formats'] ;
                        }

                        $maxwidth  = 0 ;
                        $minwidth  = 0 ;

                        $maxheight = 0 ;
                        $minheight = 0 ;

                        if( array_key_exists( 'maxwidth' , $icons ) && array_key_exists( 'maxheight' , $icons ) )
                        {
                            $maxwidth = $icons[ 'maxwidth' ] ;
                            $maxheight = $icons[ 'maxheight' ] ;
                        }

                        if( array_key_exists( 'minwidth' , $icons ) && array_key_exists( 'minheight' , $icons ) )
                        {
                            $minwidth = $icons[ 'minwidth' ] ;
                            $minheight = $icons[ 'minheight' ] ;
                        }

                        if( $typesAvailable && in_array( $type , $typesAvailable ) )
                        {
                            // check resolution of the image
                            $imgUpload = new Imagick( $file->file ) ;

                            $width = $imgUpload->getImageWidth() ;
                            $height = $imgUpload->getImageHeight() ;

                            if( $width < $minwidth || $height < $minheight )
                            {
                                return $this->formatError( $response , '400' , NULL , [ 'message' => [ 'resolution' => 'The resolution of the file is too small.' ] ] );
                            }
                            else if( $width > $maxwidth || $height > $maxheight )
                            {
                                return $this->formatError( $response , '400' , NULL , [ 'message' => [ 'resolution' => 'The resolution of the file is too large.' ] ] );
                            }

                            if( $filename )
                            {
                                $output = $this->getImagePath( $id , FALSE , TRUE ) ; // lazy calling

                                $file->moveTo( $output ) ;
                                $update = $this->model->update( [ "image" => true ] , $id ) ;
                                return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) );

                            }
                            else
                            {
                                return $this->formatError( $response , '606' , [ 'patchImage(' . $id . ')' , 'unknow error' ] ) ;
                            }
                        }
                        else
                        {
                            return $this->formatError( $response , '400', NULL , [ "message" => [ 'type' => 'The uploaded file type mismatch.' ] ] ) ;
                        }
                    }
                    else
                    {
                        return $this->formatError( $response , '400', NULL , [ "message" => [ 'upload' => 'The uploaded file is not valid.' ] ] ) ;
                    }
                }
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'postImage(' . $id . ')' , $e->getMessage() ], NULL , 500 );
        }
    }


    /**
     * Sets the path of the controller.
     *
     * @param string $value
     */
    public function setPath( $value = NULL )
    {
        if( isset( $value ) )
        {
            $set = $this->container->settings ;
            $this->path     = $value ;
            $this->fullPath = '/thesaurus/' . $value ;
            $this->iconPath = $set['icons']['root'] . $value ;
            $this->pattern  = '/thesaurus/' . $value . '/%u' ;
        }
        else
        {
            $this->path     = '' ;
            $this->fullPath = '' ;
            $this->iconPath = '' ;
            $this->pattern  = '' ;
        }
    }
}


