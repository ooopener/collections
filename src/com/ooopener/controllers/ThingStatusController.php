<?php

namespace com\ooopener\controllers;

use com\ooopener\helpers\Status;
use com\ooopener\models\Model;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Container;

use Exception ;


/**
 * The status controller class.
 *
 * @OA\Schema(
 *     schema="successWithStatus",
 *     allOf={},
 *     @OA\Property(property="withStatus",ref="#/components/schemas/withStatus"),
 *     @OA\Property(property="modified",type="string",format="date-time"),
 *     @OA\Property(property="created",type="string",format="date-time"),
 *     @OA\Property(property="id",type="integer"),
 *     @OA\Property(property="url",type="string",format="uri")
 * )
 *
 * @OA\Schema(
 *     schema="withStatus",
 *     type="string",
 *     description="Status of the resource",
 *     enum={
 *         "anonymized",
 *         "archived",
 *         "draft",
 *         "published",
 *         "rejected",
 *         "unpublished",
 *         "under_review",
 *     },
 *     default="draft"
 * )
 *
 * @OA\Parameter(
 *     name="status",
 *     in="path",
 *     description="status of the resource",
 *     required=true,
 *     @OA\Schema(ref="#/components/schemas/withStatus")
 * )
 *
 */
class ThingStatusController extends Controller
{
    /**
     * Creates a new ThingStatus instance.
     *
     * @param Container $container
     * @param Model $model
     * @param string $path
     */
    public function __construct( Container $container , Model $model = NULL , $path = NULL )
    {
        parent::__construct( $container );
        $this->model = $model ;
        if( !empty($path) )
        {
            $this->path     = $path ;
            $this->fullPath = '/' . $path ;
        }
    }

    /**
     * The full path expression.
     */
    public $fullPath ;

    /**
     * The model reference.
     */
    public $model ;

    /**
     * The main route path expression.
     */
    public $path ;

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'   => NULL
    ] ;

    /**
     * Returns if the specific item is active.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id , [ 'fields' => '_key,withStatus,modified,created' ] ) ;

            if( !$item )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $item->id = $item->_key ; // set id
            unset( $item->_key ) ; // clean
            $item->url = $this->getFullPath() ; // set url

            // set ISO 8601 dates
            $item->created = $this->container->iso8601->formatTimeToISO8601( $item->created , TRUE ) ;
            $item->modified = $this->container->iso8601->formatTimeToISO8601( $item->modified , TRUE ) ;

            return $this->success( $response , $item ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'     => NULL ,
        'status' => NULL
    ] ;

    /**
     * Switch the activity of the specific item.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ',' . $status . ')' ) ;
        }

        try
        {
            $item = $this->model->get( $id , [ 'fields' => '_key,withStatus' ] ) ;

            if( is_null($item) )
            {
                return $this->formatError( $response , '404', [ 'patch(' . $id . ',' . $status . ')' ] , NULL , 404 );
            }

            $allowed = [] ;

            switch( $item->withStatus )
            {
                case Status::ANONYMIZED :
                    // can't modify
                    break ;
                case Status::ARCHIVED :
                    $allowed = [ Status::ANONYMIZED , Status::UNPUBLISHED , Status::UNDER_REVIEW ] ;
                    break ;
                case Status::DRAFT :
                    $allowed = [ Status::PUBLISHED , Status::UNDER_REVIEW ] ;
                    break ;
                case Status::PUBLISHED :
                    $allowed = [ Status::ANONYMIZED , Status::ARCHIVED , Status::DRAFT , Status::UNPUBLISHED ] ;
                    break ;
                case Status::REJECTED :
                    $allowed = [ Status::DRAFT , Status::UNDER_REVIEW ] ;
                    break ;
                case Status::UNDER_REVIEW :
                    $allowed = [ Status::DRAFT , Status::PUBLISHED , Status::REJECTED ] ;
                    break ;
                case Status::UNPUBLISHED :
                    $allowed = [ Status::ARCHIVED , Status::DRAFT , Status::PUBLISHED , Status::UNDER_REVIEW ] ;
                    break ;
                default :
                    $allowed = [ Status::DRAFT , Status::PUBLISHED , Status::UNDER_REVIEW ] ;
                    break ;
            }

            $withStatus = strtolower( $status ) ;

            if( !in_array( $withStatus , $allowed ) )
            {
                return $this->formatError( $response , '403', [ 'patch(' . $id . ',' . $status . ')' ] );
            }

            $result = $this->model->update( [ 'withStatus' => $withStatus ] , $id ) ;

            if( $result )
            {
                $obj = $this->model->get( $id , [ 'fields' => '_key,withStatus,modified,created' ] ) ;
                $obj->id = $obj->_key ; // set id
                unset( $obj->_key ) ; // clean
                $obj->url = $this->getFullPath() ; // set url

                // set ISO 8601 dates
                $obj->created = $this->container->iso8601->formatTimeToISO8601( $obj->created , TRUE ) ;
                $obj->modified = $this->container->iso8601->formatTimeToISO8601( $obj->modified , TRUE ) ;

                return $this->success( $response , $obj ) ;
            }

            return $this->error( $response , 'error' ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patch(' . $id . ',' . $status . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}


