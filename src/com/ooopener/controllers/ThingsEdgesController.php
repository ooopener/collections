<?php

namespace com\ooopener\controllers ;

use com\ooopener\things\Thing;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use Slim\Container;

use Exception ;

class ThingsEdgesController extends EdgesController
{
    /**
     * Creates a new LivestockNumbersController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
    }

    public $conditions ;

    public $item ;

    public $validator ;

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ]
    ];

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            // delete edge
            $del = $this->edge->delete( $this->model->table . '/' . $id ) ;

            // delete model
            $result = $this->model->delete( $id ) ;

            // update owner
            $this->owner->updateDate( $owner ) ;

            if( $response )
            {
                return $this->success( $response , (int) $result->_key );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'deleteAll' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'owner' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|bool
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $owner . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
            }

            // get
            $all = $this->edge->getEdge( NULL , $owner ) ;

            if( $all && $all->edge )
            {
                // delete them all
                foreach( $all->edge as $edge )
                {
                    // delete
                    $result = $this->delete( NULL , $response , [ 'owner' => $owner , 'id' => (string) $edge['id'] ] ) ;
                    if( $result instanceof Response && $result->getStatusCode() != 200 )
                    {
                        return $result ;
                    }
                }
            }

            if( $response )
            {
                return $this->success( $response , 'ok' );
            }
            else
            {
                return true ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $owner . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'    => NULL ,
        'lang'  => NULL ,
        'owner' => NULL ,
        'skin'  => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $owner . ',' . $id . ')' ) ;
        }

        $set = $this->container->settings[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'get(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $result = $this->model->get( $id , [ 'queryFields' => $this->getFields( $skin ) ] ) ;

            $result = $this->create( $result ) ;

            if( $response )
            {
                return $this->success( $response , $result );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'    => NULL
    ] ;

    /**
     * Post
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' ) ;
        }

        try
        {
            $ev = $this->owner->get( $id , [ 'fields' => '_key' ]  ) ;
            if( !$ev )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $id . ')' ] , NULL , 404 );
            }

            if( isset( $this->conditions ) && isset( $this->item ) && isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;

                if( $this->validator->passes() )
                {
                    // model

                    $new = $this->model->insert( $this->item ) ;

                    if( $new )
                    {
                        // edge
                        $newEdge = $this->edge->insertEdge( $new->_id , $this->owner->table . '/' . $id ) ;

                        // update owner
                        $this->owner->updateDate( $id ) ;

                        $item = $this->model->get( $new->_key , [ 'queryFields' => $this->getFields() ] ) ;
                        if( $item )
                        {
                            return $this->success
                            (
                                $response,
                                $item
                            ) ;
                        }
                    }
                }
                else
                {
                    $errors = [] ;

                    $err  = $this->validator->errors() ;
                    $keys = $err->keys() ;

                    foreach( $keys as $key )
                    {
                        $errors[$key] = $err->first($key) ;
                    }

                    return $this->error( $response , $errors , "400" ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * Put specific keyword
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $ev = $this->owner->get( $owner , [ 'fields' => '_key' ]  ) ;
            if( !$ev )
            {
                return $this->formatError( $response , '404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( isset( $this->conditions ) && isset( $this->item ) && isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;

                if( $this->validator->passes() )
                {

                    $success = $this->model->update( $this->item , $id ) ;

                    if( $success )
                    {
                        // update owner
                        $this->owner->updateDate( $owner ) ;

                        $item = $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) ;
                        if( $item )
                        {
                            return $this->success
                            (
                                $response,
                                $item
                            ) ;
                        }
                    }
                }
                else
                {
                    $errors = [] ;

                    $err  = $this->validator->errors() ;
                    $keys = $err->keys() ;

                    foreach( $keys as $key )
                    {
                        $errors[$key] = $err->first($key) ;
                    }

                    return $this->error( $response , $errors , '400' ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }
}
