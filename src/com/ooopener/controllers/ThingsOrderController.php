<?php

namespace com\ooopener\controllers ;

use com\ooopener\models\Collections;

use com\ooopener\validations\Validation;
use Exception ;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Container;

class ThingsOrderController extends CollectionsController
{
    /**
     * ThingsOrderController constructor.
     *
     * @param Container $container
     * @param Collections|NULL $model
     * @param Collections|NULL $owner
     * @param null $path
     */
    public function __construct( Container $container , Collections $model = NULL , Collections $owner = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $path ) ;

        $this->owner = $owner ;

        $this->validator = new Validation( $container ) ;
    }

    public $conditions ;

    /**
     * The owner model reference.
     */
    public $owner ;

    public $validator ;

    public function all( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ) ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();
        }

        $name = $this->owner->table.'Controller' ;
        if( isset( $this->container[$name] ) )
        {
            $ownerController = $this->container[$name] ;
            $result = $ownerController->get
            (
                NULL,
                NULL,
                [
                    'active' => NULL ,
                    'id'     => $id,
                    'skin'   => $this->path
                ]
            ) ;


            if( $response )
            {
                return $this->success( $response , $result->{$this->path}, $this->getFullPath( $params ) ) ;
            }
            return $result->{$this->path} ;
        }
    }

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|mixed
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'delete(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            // update in owner
            $result = $this->deleteInOwner( $id , $o ) ;

            if( $response )
            {
                return $this->success( $response , (int) $id );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'deleteAll' methods options.
     */
    const ARGUMENTS_DELETE_ALL_DEFAULT =
    [
        'owner' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|bool
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' deleteAll(' . $owner . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'deleteAll(' . $owner . ')' ] , NULL , 404 );
            }

            if( $o->{$this->path} )
            {
                // delete them all
                foreach( $o->{$this->path} as $id )
                {
                    // delete
                    $result = $this->delete( NULL , $response , [ 'owner' => $owner , 'id' => (string) $id ] ) ;
                    if( $result instanceof Response && $result->getStatusCode() != 200 )
                    {
                        return $result ;
                    }
                }
            }

            if( $response )
            {
                return $this->success( $response , 'ok' );
            }
            else
            {
                return true ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'deleteAll(' . $owner . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'get' methods options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'id'    => NULL ,
        'lang'  => NULL ,
        'skin'  => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( $params['lang'] , $api['languages'] ) )
                {
                    $params['lang'] = $lang = $params['lang'] ;
                }
                else if( $params['lang'] == 'all' )
                {
                    $lang = NULL ;
                }

            }

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'], $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            if( !$this->owner->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $extraQuery = '' ;
            $fields = $this->getFields( $skin ) ;

            // get position
            if( array_key_exists( 'position' , $fields ) )
            {
                $extraQuery = 'LET ' . $fields['position']['unique'] . ' = ( ' .
                                'FOR doc_coll IN ' . $this->model->table . ' ' .
                                'FILTER POSITION( doc_coll.' . $this->path . ' , TO_NUMBER( @value ) ) == true ' .
                                'RETURN POSITION( doc_coll.' . $this->path . ' , TO_NUMBER( @value ) , true ) ' .
                                ')[0]' ;
            }

            $result = $this->owner->get
            (
                $id ,
                [
                    'extraQuery'  => $extraQuery ,
                    'queryFields' => $fields ,
                    'lang'        => $lang
                ]
            ) ;

            $result = $this->create( $result ) ;

            if( $response )
            {
                $options = [] ;

                // get owner
                $ownerItem = $this->getOwner( $id ) ;
                if( $ownerItem )
                {
                    $options[ 'object' ] = $ownerItem ;
                }

                // add headers
                $response = $this->container->cache->withETag( $response , $result->modified );
                $response = $this->container->cache->withLastModified( $response , $result->modified );

                return $this->success( $response , $result , null , null , $options );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch position' methods options.
     */
    const ARGUMENTS_PATCH_POSITION_DEFAULT =
    [
        'id'       => NULL ,
        'owner'    => NULL ,
        'position' => NULL
    ] ;

    public function patchPosition( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_POSITION_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patchPosition(' . $owner . ' , ' . $id . ' , '  . $position . ')' ) ;
        }

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'patchPosition(' . $owner . ' , ' . $id . ' , '  . $position . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'patchPosition(' . $owner . ',' . $id . ' , '  . $position . ')' ] , NULL , 404 );
            }

            // check step in course
            if( in_array( (int)$id , $o->{$this->path} , TRUE ) === FALSE )
            {
                return $this->formatError( $response , '404', [ 'patchPosition(' . $owner . ',' . $id . ' , '  . $position . ')' ] , NULL , 404 );
            }

            $length = count( $o->{$this->path} ) ;

            // remove id in array
            $newArray = array_diff( $o->{$this->path} , [ (int)$id ] ) ;

            // check position
            if( $position < 0 )
            {
                $position = 0 ;
            }
            else if( $position > $length - 1 )
            {
                $position = $length - 1 ;
            }

            // put id in the new position
            array_splice( $newArray , $position , 0 , (int)$id ) ;

            // update
            $update = $this->owner->update( [ $this->path => $newArray ] , $owner ) ;

            $item = $this->all( NULL , NULL , [ 'id' => $owner ] ) ;
            return $this->success( $response , $item ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'patchPosition(' . $owner . ' , ' . $id . ' , '  . $position . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'       => NULL,
        'owner'    => NULL,
        'position' => NULL
    ] ;

    /**
     * Post
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $owner . ', ' . $id . ')' ) ;
        }

        $params = $request->getParsedBody();

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key,' . $this->path ]  ) ;
            if( !$o )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $owner . ', ' .  $id . ')' ] , NULL , 404 );
            }

            $item = $this->model->get( $id , [ 'fields' => '_key' ]  ) ;
            if( !$item )
            {
                return $this->formatError( $response , '404' , [ 'post(' . $owner . ', ' .  $id . ')' ] , NULL , 404 );
            }

            $this->conditions = [] ;

            if( isset( $params['position'] ) )
            {
                $position = (int) $params['position'] ;
                $this->conditions['position'] = [ $params['position'] , 'int|min(0,number)' ] ;
            }

            if( isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;

                if( $this->validator->passes() )
                {

                    // update owner
                    $this->insertInOwner( $item->_key , $o , $position ) ;

                    $result = $this->get( NULL , NULL , [ 'id' => $o->_key ] ) ;
                    if( $result )
                    {
                        return $this->success
                        (
                            $response,
                            $result
                        ) ;
                    }

                }
                else
                {
                    $errors = [] ;

                    $err  = $this->validator->errors() ;
                    $keys = $err->keys() ;

                    foreach( $keys as $key )
                    {
                        $errors[$key] = $err->first($key) ;
                    }

                    return $this->error( $response , $errors , "400" ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    /**
     * Put specific keyword
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $owner . ',' . $id . ')' ) ;
        }

        try
        {
            $ev = $this->owner->get( $owner , [ 'fields' => '_key' ]  ) ;
            if( !$ev )
            {
                return $this->formatError( $response , '404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( isset( $this->conditions ) && isset( $this->item ) && isset( $this->validator ) )
            {
                $this->validator->validate( $this->conditions ) ;

                if( $this->validator->passes() )
                {

                    $success = $this->model->update( $this->item , $id ) ;

                    if( $success )
                    {
                        // update owner
                        $this->owner->updateDate( $owner ) ;

                        $item = $this->get( NULL , NULL , [ 'id' => $id ] ) ;
                        if( $item )
                        {
                            return $this->success
                            (
                                $response,
                                $item
                            ) ;
                        }
                    }
                }
                else
                {
                    $errors = [] ;

                    $err  = $this->validator->errors() ;
                    $keys = $err->keys() ;

                    foreach( $keys as $key )
                    {
                        $errors[$key] = $err->first($key) ;
                    }

                    return $this->error( $response , $errors , '400' ) ;
                }
            }
            else
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , 'missing variables' ] , NULL , 500 );
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    //////////////////////////////////

    protected function deleteInOwner( $id , $owner )
    {
        $array = $owner->{$this->path} ;
        $pos = array_search( (int)$id , $array , TRUE ) ;
        if( $pos !== FALSE )
        {
            array_splice( $array , $pos , 1 ) ;
            return $this->owner->update( [ $this->path => $array ] , $owner->_key ) ;
        }

        return null ;
    }

    protected function getOwner( $modelId , $skin = NULL )
    {
        $name = $this->owner->table.'Controller' ;
        if( isset( $this->container[$name] ) )
        {
            $ownerController = $this->container[$name] ;
            $result = $this->owner->all
            ([
                'active'     => NULL ,
                'conditions' =>
                [
                    'POSITION( doc.' . $this->path . ' , ' . $modelId . ' ) == true'
                ],
                'queryFields' => $ownerController->getFields( $skin ),
                'limit'       => 1
            ]) ;

            if( is_array( $result ) && count( $result ) == 1 )
            {
                return $result[0] ;
            }
        }
        return null ;
    }

    protected function insertInOwner( $id , $owner , $position = NULL )
    {
        $array = $owner->{$this->path} ;
        $length = count( $array ) ;
        if( $position === NULL || $position > $length )
        {
            $position = $length ;
        }
        array_splice( $array , $position , 0 , (int)$id ) ;
        return $this->owner->update( [ $this->path => $array ] , $owner->_key ) ;
    }
}
