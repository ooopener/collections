<?php

namespace com\ooopener\controllers;

use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;
use com\ooopener\validations\WebsiteValidator;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

/**
 * The websites controller.
 */
class WebsitesController extends ThingsEdgesController
{
    /**
     * Creates a new WebsitesController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );

        $this->validator = new WebsiteValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="Website",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(property="additionalType",description="The additional type ",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="href",format="uri",description="The href value",format="uri"),
     *     @OA\Property(type="integer",property="order"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'             => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'           => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'href'           => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'order'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'        => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'alternateName'  => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'additionalType' => [ 'filter' => Thing::FILTER_JOIN      ]
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postWebsite",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="href",type="string",format="uri",description="The url of the website"),
     *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
     *             required={"href","additionalType"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="order",type="integer",description=""),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['order'] ) )
        {
            $item['order'] = $params['order'] ;
        }

        if( isset( $params['href'] ) )
        {
            $item['href'] = $params['href'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'url'            => [ $params['href']           , 'required|url'         ] ,
            'additionalType' => [ $params['additionalType'] , 'required|websiteType' ]
        ];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'min(2)|max(50)' ] ;
        }

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::post( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putWebsite",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="href",type="string",format="uri",description="The url of the website"),
     *             @OA\Property(property="additionalType",type="integer",description="The additional type "),
     *             required={"href","additionalType"},
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="order",type="integer",description=""),
     *             @OA\Property(property="alternateName",ref="#/components/schemas/text"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['order'] ) )
        {
            $item['order'] = $params['order'] ;
        }

        if( isset( $params['href'] ) )
        {
            $item['href'] = $params['href'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'url'            => [ $params['href']           , 'required|url'         ] ,
            'additionalType' => [ $params['additionalType'] , 'required|websiteType' ]
        ];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'min(2)|max(50)' ] ;
        }

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::put( $request , $response , $args ) ;
    }
}
