<?php

namespace com\ooopener\controllers\applications ;

use com\ooopener\models\Collections;
use com\ooopener\things\Application;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\TranslationController;
use com\ooopener\things\Thing;

use Slim\Container;

class ApplicationTranslationController extends TranslationController
{
    /**
     * ApplicationTranslationController constructor.
     *
     * @param Container $container
     * @param Collections $model
     * @param null $path
     * @param string $fields
     */
    public function __construct( Container $container , Collections $model = NULL , $path = NULL , $fields = 'description' )
    {
        parent::__construct( $container , $model , $path , $fields );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function alternativeHeadline( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Application::FILTER_ALTERNATIVEHEADLINE ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function headline( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Application::FILTER_HEADLINE ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function description( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_DESCRIPTION ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function notes( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_NOTES ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function slogan( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Application::FILTER_SLOGAN ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function text( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_TEXT ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchAlternativeHeadline( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Application::FILTER_ALTERNATIVEHEADLINE ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchHeadline( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Application::FILTER_HEADLINE ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchDescription( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Thing::FILTER_DESCRIPTION ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchNotes( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Thing::FILTER_NOTES ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchSlogan( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Application::FILTER_SLOGAN ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchText( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Thing::FILTER_TEXT ) ;
    }
}
