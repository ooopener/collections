<?php

namespace com\ooopener\controllers\articles;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Articles;

use com\ooopener\validations\ArticleValidator;
use Exception ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use com\ooopener\things\Thing;


class ArticlesController extends CollectionsController
{
    /**
     * Creates a new ArticlesController instance.
     *
     * @param Container $container
     * @param Articles $model
     * @param string $path
     */
    public function __construct( Container $container , Articles $model = NULL , $path = 'articles' )
    {
        parent::__construct( $container , $model , $path );
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="ArticleList",
     *     description="Article",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/headlineText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     * )
     *
     * @OA\Schema(
     *     schema="Article",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/ArticleList")},
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="hasPart",type="array",items=@OA\Items(ref="#/components/schemas/ArticleList")),
     *     @OA\Property(property="isPartOf",type="array",items=@OA\Items(ref="#/components/schemas/ArticleList")),
     *     @OA\Property(property="isRelatedTo",type="array",items=@OA\Items(ref="#/components/schemas/ArticleList")),
     *     @OA\Property(property="isSimilarTo",type="array",items=@OA\Items(ref="#/components/schemas/ArticleList")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website"))
     * )
     *
     * @OA\Response(
     *     response="articleResponse",
     *     description="Result of the article",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Article")
     *     )
     * )
     *
     * @OA\Response(
     *     response="articleListResponse",
     *     description="Result list of articles",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/ArticleList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'audio'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'additionalType'      => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'headline'              => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'alternativeHeadline'   => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'description'           => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,

        'numAudios'             => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'             => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'             => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'audios'                =>  [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'                =>  [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'                =>  [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'videos' ] ] ,

        'text'                  => [ 'filter' => Thing::FILTER_TRANSLATE , 'skins' => [ 'full' ] ] ,
        'notes'                 => [ 'filter' => Thing::FILTER_TRANSLATE , 'skins' => [ 'full' ] ] ,

        'hasPart'               => [ 'filter' => Thing::FILTER_EDGE , 'skins' => [ 'full' ] ] ,
        'isPartOf'              => [ 'filter' => Thing::FILTER_EDGE , 'skins' => [ 'full' ] ] ,

        'isRelatedTo'           => [ 'filter' => Thing::FILTER_EDGE , 'skins' => [ 'full' ] ] ,
        'isSimilarTo'           => [ 'filter' => Thing::FILTER_EDGE , 'skins' => [ 'full' ] ] ,

        'websites'              => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'compact' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        /*if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {

                }
            }
        }*/
        return $init ;
    }

    ///////////////////////////

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        //// remove all linked resources

        // remove websites
        $websites = $this->container->articleWebsitesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove course discover
        if( isset( $this->container->courseDiscoverController ) )
        {
            $discover = $this->container->courseDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        // remove stage discover
        if( isset( $this->container->stageDiscoverController ) )
        {
            $discover = $this->container->stageDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postArticle",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request , Response $response , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $item = [];

        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string)$params['additionalType'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']   , 'required|max(70)'         ] ,
            'additionalType' => [ $additionalType   , 'required|additionalType'  ] ,
        ] ;

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new ArticleValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $result = $this->model->insert( $item );

                // add additionalType edge
                $addTypeEdge = $this->container->articleArticlesTypes ;
                $ate = $addTypeEdge->insertEdge( $addTypeEdge->from['name'] . '/' . $additionalType , $result->_id ) ;

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ]) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchArticle",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request , Response $response , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType   = NULL ;

        $item = [];
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(70)' ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType' ] ;
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        // check if there is at least one param
        if( empty( $item ) && $additionalType === NULL )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        // check if resource exists
        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        ////// validator

        $validator = new ArticleValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $idTo = $this->model->table . '/' . $id ;

                // update edge
                if( $additionalType !== NULL )
                {
                    $addTypeEdge = $this->container->articleArticlesTypes ;

                    $idFrom = $addTypeEdge->from['name'] . '/' . $additionalType ;

                    if( !$addTypeEdge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $addTypeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $addTypeEdge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putArticle",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request , Response $response , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType = NULL ;

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string)$params['additionalType'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']   , 'required|max(70)'        ] ,
            'additionalType' => [ $additionalType   , 'required|additionalType' ]
        ] ;


        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new ArticleValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {

                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                // update edge
                $addTypeEdge = $this->container->articleArticlesTypes ;

                $idFrom = $addTypeEdge->from['name'] . '/' . $additionalType ;
                $idTo = $this->model->table . '/' . $id ;

                if( !$addTypeEdge->existEdge( $idFrom , $idTo ) )
                {
                    // delete all edges to be sure
                    $addTypeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    // add edge
                    $addTypeEdge->insertEdge( $idFrom , $idTo ) ;
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields('normal' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * Outputs all the elements.
     *
     * @param Request $request
     * @param Response $response
     * @param array $items
     * @param array $params
     * @param array $options
     *
     * @return Response|array
     */
    protected function outputAll( Request $request = NULL, Response $response = NULL , $items = NULL , $params = NULL , array $options = NULL )
    {
        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        $format    = $this->container['format'] ;
        $lang      = NULL ;
        $lang      = NULL ;
        $skin      = NULL ;

        if( !isset($params) )
        {
            $params = isset( $request ) ? $request->getQueryParams() : []  ;
        }

        // ----- lang

        if( !empty($params['lang']) )
        {
            if( in_array( strtolower($params['lang']) , $api['languages'] ) )
            {
                $params['lang'] = $lang = strtolower($params['lang']) ;
            }
            else if( strtolower($params['lang']) == 'all' )
            {
                $lang = NULL ;
            }
            else
            {
                unset($params['lang']) ;
            }
        }

        // ----- skin

        if( !isset($skin) && array_key_exists( 'skin_default' ,$set ) )
        {
            if( array_key_exists( 'skin_all', $set)  )
            {
                $skin = $set['skin_all'] ;
            }
            else if( array_key_exists( 'skin_default', $set)  )
            {
                $skin = $set['skin_default'] ;
            }
        }

        if( !empty($params['skin']) )
        {
            if( in_array( strtolower($params['skin']) , $set['skins'] ) )
            {
                $params['skin'] = $skin = strtolower($params['skin']) ;
            }
            else
            {
                unset($params['skin']) ;
            }
        }

        if( $skin == 'main' || !in_array( strtolower($skin) , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        if( $items )
        {
            foreach( $items as $key => $value )
            {
                $items[$key] = $this->create( $value , $lang , $skin , $params ) ;
            }
        }

        if( $response )
        {
            switch( $format )
            {
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response,
                        $items,
                        $this->getFullPath( $params ) ,
                        is_array($items) ? count($items) : NULL,
                        $options
                    );
                }
            }
        }

        return $items ;
    }
}


