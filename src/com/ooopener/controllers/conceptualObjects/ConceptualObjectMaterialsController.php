<?php

namespace com\ooopener\controllers\conceptualObjects;

use com\ooopener\controllers\ThingsEdgesController;
use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;
use com\ooopener\validations\ConceptualObjectMaterialValidator;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

/**
 * The object materials controller.
 */
class ConceptualObjectMaterialsController extends ThingsEdgesController
{
    /**
     * Creates a new ConceptualObjectMaterialsController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );

        $this->validator = new ConceptualObjectMaterialValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="ConceptualObjectMaterial",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="description",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="material",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="technique",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID        ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_TRANSLATE ] ,
        'description'   => [ 'filter' =>  Thing::FILTER_DEFAULT   ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME  ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME  ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'material'      => [ 'filter' =>  Thing::FILTER_JOIN     ] ,
        'technique'     => [ 'filter' =>  Thing::FILTER_JOIN     ]
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postConceptualObjectMaterial",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(type="integer",property="material"),
     *             @OA\Property(type="integer",property="technique"),
     *             required={"material","technique"},
     *             @OA\Property(type="string",property="name",description="The name of the resource"),
     *             @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
     *             @OA\Property(type="string",property="description",ref="#/components/schemas/text"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
       $set = $this->config['conceptualObjects-materials'];

        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['material'] ) )
        {
            $item['material'] = (int) $params['material'] ;
        }

        if( isset( $params['technique'] ) )
        {
            $item['technique'] = (int) $params['technique'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['description'] ) )
        {
            $item['description'] = $this->filterLanguages( $params['description'] ) ;
        }

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'material'    => [ $params['material']    , 'int|material' ] ,
            'technique'   => [ $params['technique']   , 'int|technique' ] ,
            'name'        => [ $params['name']        , 'min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ]
        ];

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::post( $request , $response , $args ) ;

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putConceptualObjectMaterial",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(type="integer",property="material"),
     *             @OA\Property(type="integer",property="technique"),
     *             required={"material","technique"},
     *             @OA\Property(type="string",property="name",description="The name of the resource"),
     *             @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
     *             @OA\Property(type="string",property="description",ref="#/components/schemas/text"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $set = $this->config['conceptualObjects-materials'];

        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['material'] ) )
        {
            $item['material'] = (int) $params['material'] ;
        }

        if( isset( $params['technique'] ) )
        {
            $item['technique'] = (int) $params['technique'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['description'] ) )
        {
            $item['description'] = $this->filterLanguages( $params['description'] ) ;
        }

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'material'    => [ $params['material']    , 'int|material' ] ,
            'technique'   => [ $params['technique']   , 'int|technique' ] ,
            'name'        => [ $params['name']        , 'min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ]
        ];

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::put( $request , $response , $args ) ;
    }
}
