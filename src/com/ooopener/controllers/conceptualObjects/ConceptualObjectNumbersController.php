<?php

namespace com\ooopener\controllers\conceptualObjects;

use com\ooopener\controllers\ThingsEdgesController;
use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;
use com\ooopener\validations\ConceptualObjectNumberValidator;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

/**
 * The object numbers controller.
 */
class ConceptualObjectNumbersController extends ThingsEdgesController
{
    /**
     * Creates a new ConceptualObjectNumbersController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );

        $this->validator = new ConceptualObjectNumberValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="ConceptualObjectNumber",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
     *     @OA\Property(type="string",property="additionalType",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="string",property="value",description="The value of the resource"),
     *     @OA\Property(type="string",property="date",description="The date of the resource"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'value'         => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'date'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'alternateName'  => [ 'filter' =>  Thing::FILTER_TRANSLATE ] ,
        'additionalType' => [ 'filter' =>  Thing::FILTER_JOIN      ]
    ];


    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postConceptualObjectNumber",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(type="integer",property="additionalType"),
     *             @OA\Property(type="string",property="date"),
     *             required={"additionalType","date"},
     *             @OA\Property(type="string",property="name",description="The name of the resource"),
     *             @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
     *             @OA\Property(type="string",property="value",description=""),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $set = $this->config['conceptualObjects-numbers'];

        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $init['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['value'] ) )
        {
            $item['value'] = $params['value'] ;
        }

        if( isset( $params['date'] ) )
        {
            $item['date'] = $params['date'] ;
        }

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']           , 'min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ] ,
            'additionalType' => [ $params['additionalType'] , 'int|additionalType'                                        ] ,
            'date'           => [ $params['date']           , 'simpleDate(true)'                                          ]
        ];

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::post( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putConceptualObjectNumber",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(type="integer",property="additionalType"),
     *             @OA\Property(type="string",property="date"),
     *             required={"additionalType","date"},
     *             @OA\Property(type="string",property="name",description="The name of the resource"),
     *             @OA\Property(type="string",property="alternateName",ref="#/components/schemas/text"),
     *             @OA\Property(type="string",property="value",description=""),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $set = $this->config['conceptualObjects-numbers'];

        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $init['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['value'] ) )
        {
            $item['value'] = $params['value'] ;
        }

        if( isset( $params['date'] ) )
        {
            $item['date'] = $params['date'] ;
        }

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']           , 'min(' . $set['minName'] . ')|max(' . $set['maxName'] . ')' ] ,
            'additionalType' => [ $params['additionalType'] , 'int|additionalType'                                        ] ,
            'date'           => [ $params['date']           , 'simpleDate(true)'                                          ]
        ];

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::put( $request , $response , $args ) ;
    }
}
