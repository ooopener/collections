<?php

namespace com\ooopener\controllers\conceptualObjects;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Collections;
use com\ooopener\validations\ConceptualObjectValidator;

use Exception ;

use Slim\Container ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\things\ConceptualObject;
use com\ooopener\things\Thing;

/**
 * The object collection controller.
 */
class ConceptualObjectsController extends CollectionsController
{
    /**
     * Creates a new ConceptualObjectsController instance.
     *
     * @param Container $container
     * @param Collections $model
     * @param string $path
     */
    public function __construct( Container $container , Collections $model = NULL , $path = 'conceptualObjects' )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="ConceptualObjectList",
     *     description="A conceptual object like artwork, sculture, painting, etc",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="location",ref="#/components/schemas/PlaceList"),
     *
     *     @OA\Property(property="category",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="movement",ref="#/components/schemas/Thesaurus"),
     * )
     *
     * @OA\Schema(
     *     schema="ConceptualObject",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/ConceptualObjectList")},
     *     @OA\Property(property="temporal",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="marks",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectMark")),
     *     @OA\Property(property="materials",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectMaterial")),
     *     @OA\Property(property="measurements",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectMeasurement")),
     *     @OA\Property(property="numbers",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectNumber")),
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="productions",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website"))
     * )
     *
     * @OA\Response(
     *     response="conceptualObjectResponse",
     *     description="Result of the conceptualObject",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/ConceptualObject")
     *     )
     * )
     *
     * @OA\Response(
     *     response="conceptualObjectListResponse",
     *     description="Result list of conceptualObjects",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        =>  [ 'filter' => Thing::FILTER_BOOL          ] ,
        'withStatus'    =>  [ 'filter' => Thing::FILTER_DEFAULT       ] ,
        'id'            =>  [ 'filter' => Thing::FILTER_ID            ] ,
        'name'          =>  [ 'filter' => Thing::FILTER_DEFAULT       ] ,
        'url'           =>  [ 'filter' => Thing::FILTER_URL           ] ,
        'created'       =>  [ 'filter' => Thing::FILTER_DATETIME      ] ,
        'modified'      =>  [ 'filter' => Thing::FILTER_DATETIME      ] ,

        'isBasedOn'     =>  [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'audio'         =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE   ] ,
        'image'         =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE   ] ,
        'video'         =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE   ] ,

        'alternateName' =>  [ 'filter' => Thing::FILTER_TRANSLATE                 ] ,
        'description'   =>  [ 'filter' => Thing::FILTER_TRANSLATE                 ] ,
        'text'          =>  [ 'filter' => Thing::FILTER_TRANSLATE                 ] ,
        'location'      =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE               ] ,

        'category'      =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE               ] ,
        'movement'      =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE               ] ,

        'numAudios'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'temporal'      =>  [ 'filter' => Thing::FILTER_EDGE_SINGLE            , 'skins' => [ 'full' , 'list' , 'extend' ]] ,

        'marks'         =>  [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'materials'     =>  [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'measurements'  =>  [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'numbers'       =>  [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'keywords'      =>  [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ] ,
        'audios'        =>  [ 'filter' => Thing::FILTER_JOIN_ARRAY             , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'        =>  [ 'filter' => Thing::FILTER_JOIN_ARRAY             , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'        =>  [ 'filter' => Thing::FILTER_JOIN_ARRAY             , 'skins' => [ 'full' , 'videos' ] ] ,
        'productions'   =>  [ 'filter' => ConceptualObject::FILTER_PRODUCTIONS , 'skins' => [ 'full' , 'extend' ] ] ,
        'notes'         =>  [ 'filter' => Thing::FILTER_TRANSLATE              , 'skins' => [ 'full' , 'extend' ] ] ,
        'websites'      =>  [ 'filter' => Thing::FILTER_EDGE                   , 'skins' => [ 'full' , 'extend' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init An object to initialize a new Object instance.
     * @param string $lang The lang of the object elements.
     * @param string $skin The optional skin mode.
     * @param object $params The optional params object.
     *
     * @return ConceptualObject|object|null
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL  , $params = NULL )
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case ConceptualObject::FILTER_LOCATION :
                    {
                        // get image
                        if( property_exists( $init , ConceptualObject::FILTER_LOCATION ) && $init->{ ConceptualObject::FILTER_LOCATION } != NULL )
                        {
                            $init->{ $key } = $this->container->placesController->create( (object) $init->{ ConceptualObject::FILTER_LOCATION } , $lang ) ;
                        }
                        break ;
                    }
                    case ConceptualObject::FILTER_PRODUCTIONS :
                    {
                        if( property_exists( $init , ConceptualObject::FILTER_PRODUCTIONS ) )
                        {
                            // get people edges
                            $productions = $this->container->conceptualObjectProductionsController->all(NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ]);

                            $init->{$key} = $productions;
                        }
                        break;
                    }
                }
            }
        }
        return $init ;
    }

    ///////////////////////////


    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
        }

        //// remove all linked resources

        // remove keywords
        $keywords = $this->container->conceptualObjectKeywordsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove marks
        $marks = $this->container->conceptualObjectMarksController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove materials
        $materials = $this->container->conceptualObjectMaterialsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove measurements
        $measurements = $this->container->conceptualObjectMeasurementsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove numbers
        $numbers = $this->container->conceptualObjectNumbersController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove productions
        $productions = $this->container->conceptualObjectProductionsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove category
        $category = $this->container->conceptualObjectConceptualObjectsCategoriesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove movement
        $movement = $this->container->conceptualObjectArtMovementsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove temporal
        $temporal = $this->container->conceptualObjectArtTemporalEntitiesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove object place
        $place = $this->container->conceptualObjectPlacesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove websites
        $websites = $this->container->conceptualObjectWebsitesController->deleteAll( NULL, NULL , [ 'owner' => $id ] ) ;

        // remove course discover
        if( isset( $this->container->courseDiscoverController ) )
        {
            $discover = $this->container->courseDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        // remove stage discover
        if( isset( $this->container->stageDiscoverController ) )
        {
            $discover = $this->container->stageDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postConceptualObject",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"},
     *             @OA\Property(property="category",type="integer",description="The category ID"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $category = NULL ;
        $location = NULL ;

        $item = [];

        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'name' => [ $params['name'] , 'required|min(2)|max(255)' ]
        ] ;

        if( isset( $params['category'] ) )
        {
            $category = $params['category'] ;
            $conditions['category'] = [ $params['category'] , 'category' ] ;
        }

        if( isset( $params['location'] ) )
        {
            $location = $params['location'] ;
            $conditions['location'] = [ $params['location'] , 'location' ] ;
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new ConceptualObjectValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $result = $this->model->insert( $item );

                // insert category edge
                $this->insertEdge( $category , $result->_id , $this->container->conceptualObjectConceptualObjectsCategories ) ;

                // insert location edge
                $this->insertEdge( $location , $result->_id , $this->container->conceptualObjectPlaces ) ;

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchConceptualObject",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="category",type="integer",description="The category ID"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="movement",type="integer",description="The place ID"),
     *             @OA\Property(property="temporal",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $category = NULL ;
        $location = NULL ;
        $movement = NULL ;
        $temporal = NULL ;

        $item = [] ;
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(255)' ] ;
        }

        if( isset( $params['location'] ) )
        {
            if( $params['location'] != '' )
            {
                $location = $params[ 'location' ];
                $conditions[ 'location' ] = [ $params[ 'location' ] , 'location' ];
            }
            else
            {
                $location = FALSE ;
            }
        }

        if( isset( $params['category'] ) )
        {
            if( $params['category'] != '' )
            {
                $category = $params['category'] ;
                $conditions['category'] = [ $params['category'] , 'category' ] ;
            }
            else
            {
                $category = FALSE ;
            }
        }

        if( isset( $params['movement'] ) )
        {
            if( $params['movement'] != '' )
            {
                $movement = $params['movement'] ;
                $conditions['movement'] = [ $params['movement'] , 'movement' ] ;
            }
            else
            {
                $movement = FALSE ;
            }
        }

        if( isset( $params['temporal'] ) )
        {
            if( $params['temporal'] != '' )
            {
                $temporal = $params['temporal'] ;
                $conditions['temporal'] = [ $params['temporal'] , 'temporal' ] ;
            }
            else
            {
                $temporal = FALSE ;
            }
        }


        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        // check if there is at least one param
        if( empty( $item ) && $category === NULL && $location === NULL && $movement === NULL && $temporal == NULL )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        // check if resource exists
        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        ////// validator

        $validator = new ConceptualObjectValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $idTo = $this->model->table . '/' . $id ;

                // update edges

                // update place edge
                $this->updateEdge( $location , $idTo , $this->container->conceptualObjectPlaces ) ;

                // add conceptualObject category edge
                $this->updateEdge( $category , $idTo , $this->container->conceptualObjectConceptualObjectsCategories ) ;

                // add conceptualObject movement
                $this->updateEdge( $movement , $idTo , $this->container->conceptualObjectArtMovements ) ;

                // add conceptualObject temporal
                $this->updateEdge( $temporal , $idTo , $this->container->conceptualObjectArtTemporalEntities ) ;


                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields('extend') ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putConceptualObject",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"},
     *             @OA\Property(property="category",type="integer",description="The category ID"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="movement",type="integer",description="The place ID"),
     *             @OA\Property(property="temporal",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $category = NULL ;
        $location = NULL ;
        $movement = NULL ;
        $temporal = NULL ;

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        $conditions =
        [
            'name' => [ $params['name'] , 'required|min(2)|max(255)' ]
        ] ;

        if( isset( $params['category'] ) && $params['category'] != '' )
        {
            $category = $params['category'] ;
            $conditions[ 'category' ] = [ $params[ 'category' ] , 'category' ] ;
        }
        else
        {
            $category = FALSE ;
        }

        if( isset( $params['location'] ) && $params['location'] != '' )
        {
            $location = $params[ 'location' ] ;
            $conditions[ 'location' ] = [ $params[ 'location' ] , 'location' ] ;
        }
        else
        {
            $location = FALSE ;
        }

        if( isset( $params['movement'] ) )
        {
            $movement = $params['movement'] ;
            $conditions[ 'movement' ] = [ $params[ 'movement' ] , 'movement' ] ;
        }
        else
        {
            $movement = FALSE ;
        }

        if( isset( $params['temporal'] ) )
        {
            $temporal = $params['temporal'] ;
            $conditions[ 'temporal' ] = [ $params[ 'temporal' ] , 'temporal' ] ;
        }
        else
        {
            $temporal = FALSE ;
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new ConceptualObjectValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                // update edges

                $idTo = $this->model->table . '/' . $id ;

                // update place edge
                $this->updateEdge( $location , $idTo , $this->container->conceptualObjectPlaces ) ;

                // add conceptualObject category edge
                $this->updateEdge( $category , $idTo , $this->container->conceptualObjectConceptualObjectsCategories ) ;

                // add conceptualObject movement
                $this->updateEdge( $movement , $idTo , $this->container->conceptualObjectArtMovements ) ;

                // add conceptualObject temporal
                $this->updateEdge( $temporal , $idTo , $this->container->conceptualObjectArtTemporalEntities ) ;


                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields('full') ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

}


