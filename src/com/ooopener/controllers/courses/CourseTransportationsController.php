<?php

namespace com\ooopener\controllers\courses;

use com\ooopener\controllers\ThingsEdgesController;
use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;
use com\ooopener\validations\TransportationValidator;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;

class CourseTransportationsController extends ThingsEdgesController
{
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , string $path = NULL )
    {
        parent::__construct($container , $model , $owner , $edge , $path);

        $this->validator = new TransportationValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="CourseTransportation",
     *     type="object",
     *     @OA\Property(type="integer",property="id",description="Resource identification"),
     *     @OA\Property(type="string",property="name",description="The name of the resource"),
     *     @OA\Property(type="string",property="additionalType",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(type="integer",property="duration",description="The duration (in minutes) of the tranportation"),
     *     @OA\Property(type="string",property="created",format="date-time",description="Resource date created"),
     *     @OA\Property(type="string",property="modified",format="date-time",description="Resource date modified")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'             => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'           => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'duration'       => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'comment'        => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'        => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'additionalType' => [ 'filter' =>  Thing::FILTER_JOIN    ]
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postCourseTransportation",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(type="integer",property="additionalType"),
     *             @OA\Property(type="integer",property="duration"),
     *             required={"additionalType","language","technique"},
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['duration'] ) )
        {
            $item['duration'] = (int) $params['duration'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'duration'       => [ $params['duration']       , 'required|int'            ] ,
            'additionalType' => [ $params['additionalType'] , 'required|additionalType' ]
        ];

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::post( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putCourseTransportation",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(type="integer",property="additionalType"),
     *             @OA\Property(type="integer",property="duration"),
     *             required={"additionalType","language","technique"},
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['duration'] ) )
        {
            $item['duration'] = (int) $params['duration'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'duration'       => [ $params['duration']       , 'required'                ] ,
            'additionalType' => [ $params['additionalType'] , 'required|additionalType' ]
        ];

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::put( $request , $response , $args ) ;
    }
}
