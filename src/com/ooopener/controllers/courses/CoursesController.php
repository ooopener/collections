<?php

namespace com\ooopener\controllers\courses ;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Courses;
use com\ooopener\things\Course;
use com\ooopener\things\Thing;
use com\ooopener\validations\CourseValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container;

use Exception ;

class CoursesController extends CollectionsController
{
    public function __construct( Container $container , Courses $model , $path = 'courses' )
    {
        parent::__construct( $container , $model , $path );
    }


    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="CourseList",
     *     description="Course",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/headlineText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="additionalType",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="distance",type="string",description="The total course distance in meters"),
     *     @OA\Property(property="elevation",type="object",description="The course elevation"),
     *     @OA\Property(property="level",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="status",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="numSteps",type="number",description="Number of steps"),
     * )
     *
     * @OA\Schema(
     *     schema="Course",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/CourseList")},
     *     @OA\Property(property="openingHoursSpecification",type="array",items=@OA\Items(ref="#/components/schemas/OpeningHoursSpecification")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="audience",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="path",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="steps",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="transportation",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="discover",type="array",items=@OA\Items(ref="#/components/schemas/discovers")),
     * )
     *
     * @OA\Response(
     *     response="courseResponse",
     *     description="Result of the course",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Course")
     *     )
     * )
     *
     * @OA\Response(
     *     response="courseListResponse",
     *     description="Result list of courses",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/CourseList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'audio'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'additionalType' => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'distance'       => [ 'filter' => Thing::FILTER_DEFAULT     ] ,
        'elevation'      => [ 'filter' => Thing::FILTER_DEFAULT     ] ,

        'alternativeHeadline'       => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'description'               => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'headline'                  => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'level'                     => [ 'filter' => Thing::FILTER_EDGE_SINGLE  ] ,
        'status'                    => [ 'filter' => Thing::FILTER_EDGE_SINGLE  ] ,

        'openingHoursSpecification' => [ 'filter' => Thing::FILTER_EDGE        ] ,

        'numSteps'                  => [ 'filter' => Thing::FILTER_JOIN_COUNT  ],

        'numAudios'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'notes'                     => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' , 'compact' ] ] ,
        'text'                      => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' , 'compact' ] ] ,
        'audios'                    => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'                    => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'                    => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'videos' ] ] ,
        'audience'                  => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' , 'normal' , 'compact' ] ],
        'path'                      => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' , 'normal' ] ],
        'steps'                     => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'steps' , 'compact' ] ],
        'transportations'           => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'compact' ] ],

        'discover'             => [ 'filter' => Thing::FILTER_DEFAULT    , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,

        'articles'            => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'conceptualObjects'   => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'courses'             => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'events'              => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'organizations'       => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'people'              => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'places'              => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'stages'              => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Course::FILTER_DISCOVER :
                    {

                        // regroup events, conceptualObjects, organizations, people, places, stages, courses
                        if( property_exists( $init , Course::FILTER_DISCOVER ) )
                        {
                            $items = [] ;

                            if( property_exists( $init , Course::FILTER_ARTICLES ) && is_array( $init->{ Course::FILTER_ARTICLES } ) && count( $init->{ Course::FILTER_ARTICLES } ) > 0 )
                            {
                                foreach( $init->{ Course::FILTER_ARTICLES } as $item )
                                {
                                    array_push( $items , $this->container->articlesController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Course::FILTER_ARTICLES } ) ;

                            if( property_exists( $init , Course::FILTER_EVENTS ) && is_array( $init->{ Course::FILTER_EVENTS } ) && count( $init->{ Course::FILTER_EVENTS } ) > 0 )
                            {
                                foreach( $init->{ Course::FILTER_EVENTS } as $item )
                                {
                                    array_push( $items , $this->container->eventsController->create( (object) $item ) ) ;
                                }

                            }

                            // remove property
                            unset( $init->{ Course::FILTER_EVENTS } ) ;

                            if( property_exists( $init , Course::FILTER_CONCEPTUAL_OBJECTS ) && is_array( $init->{ Course::FILTER_CONCEPTUAL_OBJECTS } ) && count( $init->{ Course::FILTER_CONCEPTUAL_OBJECTS } ) > 0 )
                            {
                                foreach( $init->{ Course::FILTER_CONCEPTUAL_OBJECTS } as $item )
                                {
                                    array_push( $items , $this->container->conceptualObjectsController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Course::FILTER_CONCEPTUAL_OBJECTS } ) ;

                            if( property_exists( $init , Course::FILTER_ORGANIZATIONS ) && is_array( $init->{ Course::FILTER_ORGANIZATIONS } ) && count( $init->{ Course::FILTER_ORGANIZATIONS } ) > 0 )
                            {
                                foreach( $init->{ Course::FILTER_ORGANIZATIONS } as $item )
                                {
                                    array_push( $items , $this->container->organizationsController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Course::FILTER_ORGANIZATIONS } ) ;

                            if( property_exists( $init , Course::FILTER_PEOPLE ) && is_array( $init->{ Course::FILTER_PEOPLE } ) && count( $init->{ Course::FILTER_PEOPLE } ) > 0 )
                            {
                                foreach( $init->{ Course::FILTER_PEOPLE } as $item )
                                {
                                    array_push( $items , $this->container->peopleController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Course::FILTER_PEOPLE } ) ;

                            if( property_exists( $init , Course::FILTER_PLACES ) && is_array( $init->{ Course::FILTER_PLACES } ) && count( $init->{ Course::FILTER_PLACES } ) > 0 )
                            {
                                foreach( $init->{ Course::FILTER_PLACES } as $item )
                                {
                                    array_push( $items , $this->container->placesController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Course::FILTER_PLACES } ) ;

                            if( property_exists( $init , Course::FILTER_STAGES ) && is_array( $init->{ Course::FILTER_STAGES } ) && count( $init->{ Course::FILTER_STAGES } ) > 0 )
                            {
                                foreach( $init->{ Course::FILTER_STAGES } as $item )
                                {
                                    array_push( $items , $this->container->stagesController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Course::FILTER_STAGES } ) ;

                            if( property_exists( $init , Course::FILTER_COURSES ) && is_array( $init->{ Course::FILTER_COURSES } ) && count( $init->{ Course::FILTER_COURSES } ) > 0 )
                            {
                                foreach( $init->{ Course::FILTER_COURSES } as $item )
                                {
                                    array_push( $items , $this->container->coursesController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Course::FILTER_COURSES } ) ;

                            // order result
                            $order = $init->{ Course::FILTER_DISCOVER } ;

                            if( is_array( $order ) && count( $order ) > 0 )
                            {
                                $itemsOrdered = [] ;

                                // sorting
                                $position = 0;
                                foreach( $order as $ord )
                                {
                                    $found = NULL ;

                                    foreach( $items as $item )
                                    {
                                        if( $this->endsWith( $item->url , $ord ) )
                                        {
                                            $found = $item ;
                                            break ;
                                        }
                                    }

                                    if( $found !== NULL  )
                                    {
                                        $found->position = $position ;
                                        array_push( $itemsOrdered , $found );

                                        $position++;
                                    }
                                }

                                $init->{ $key } = $itemsOrdered ;
                            }
                            else
                            {
                                $init->{ $key } = $items ;
                            }
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    ///////////////////////////

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
        }

        //// remove all linked resources

        // remove audiences
        $audiences = $this->container->courseCoursesAudiencesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove levels
        $levels = $this->container->courseCoursesLevelsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove openingHours
        $openingHours = $this->container->courseOpeningHoursController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove paths
        $paths = $this->container->courseCoursesPathsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove status
        $status = $this->container->courseCoursesStatusController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove steps
        $steps = $this->container->stepsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove transportations
        $transportations = $this->container->courseTransportationsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove additionalType
        $additionalType = $this->container->courseCoursesTypesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        //// discover

        // remove conceptualObjects
        $objects = $this->container->courseConceptualObjectsController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove courses
        $courses = $this->container->courseCoursesController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove events
        $events = $this->container->courseEventsController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove organizations
        $organizations = $this->container->courseOrganizationsController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove people
        $people = $this->container->coursePeopleController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove places
        $places = $this->container->coursePlacesController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove stages
        $stages = $this->container->courseStagesController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove course discover
        if( isset( $this->container->courseDiscoverController ) )
        {
            $discover = $this->container->courseDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        // remove stage discover
        if( isset( $this->container->stageDiscoverController ) )
        {
            $discover = $this->container->stageDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchCourse",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="status",type="integer",description="The id of the stage status"),
     *             required={"name","location","status"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_PATCH_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' patch(' . $id . ')');
        }

        // check
        $params = $request->getParsedBody();

        $additionalType   = NULL ;

        $audience = NULL ;
        $level    = NULL ;
        $path     = NULL ;
        $status   = NULL ;

        $item = [];
        $conditions = [] ;

        $elevation = [];

        if( isset($params[ 'name' ]) )
        {
            $item[ 'name' ] = $params[ 'name' ];
            $conditions['name'] = [ $params['name'] , 'required|max(70)'  ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'additionalType' ] ;
        }

        if( isset($params[ 'distance' ]) )
        {
            if( $params['distance'] != '' )
            {
                $item[ 'distance' ] = (int) $params[ 'distance' ];
                $conditions['distance'] = [ $params['distance'] , 'int'  ] ;
            }
            else
            {
                $item['distance'] = null ;
            }
        }

        if( isset($params[ 'minValue' ]) )
        {
            if(  $params['minValue'] != '' )
            {
                $elevation[ 'minValue' ] = (int) $params[ 'minValue' ];
                $conditions['minValue'] = [ $params['minValue'] , 'int'  ] ;
            }
            else
            {
                $elevation['minValue'] = null ;
            }
        }

        if( isset($params[ 'maxValue' ]) )
        {
            if( $params['maxValue'] != '' )
            {
                $elevation[ 'maxValue' ] = (int) $params[ 'maxValue' ];
                $conditions['maxValue'] = [ $params['maxValue'] , 'int'  ] ;
            }
            else
            {
                $elevation['maxValue'] = null ;
            }
        }

        if( isset($params[ 'gain' ]) )
        {
            if( $params['gain'] != '' )
            {
                $elevation[ 'gain' ] = (int) $params[ 'gain' ];
                $conditions['gain'] = [ $params['gain'] , 'int'  ] ;
            }
            else
            {
                $elevation['gain'] = null ;
            }
        }

        if( isset($params[ 'loss' ]) )
        {
            if( $params['loss'] != '' )
            {
                $elevation[ 'loss' ] = (int) $params[ 'loss' ];
                $conditions['loss'] = [ $params['loss'] , 'int'  ] ;
            }
            else
            {
                $elevation['loss'] = null ;
            }
        }

        if( isset( $params['audience'] ) )
        {
            $audience = $params['audience'] ;
            $conditions['audience'] = [ $params['audience'] , 'audience'  ] ;
        }

        if( isset( $params['level'] ) )
        {
            $level = $params['level'] ;
            $conditions['level'] = [ $params['level'] , 'level'  ] ;
        }

        if( isset( $params['path'] ) )
        {
            $path = $params['path'] ;
            $conditions['path'] = [ $params['path'] , 'path'  ] ;
        }

        if( isset( $params['status'] ) )
        {
            $status = $params['status'] ;
            $conditions['status'] = [ $params['status'] , 'required|status'  ] ;
        }

        ////// validator

        $validator = new CourseValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                // update edge

                if( $additionalType !== NULL )
                {
                    $edge = $this->container->courseCoursesTypes ;

                    $idFrom = $edge->from['name'] . '/' . $additionalType ;

                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }
                else
                {
                    $edge = $this->container->courseCoursesTypes ;

                    $idFrom = $edge->from['name'] . '/' . $additionalType ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                }

                // update edges
                if( $audience )
                {
                    $edge = $this->container->courseCoursesAudiences ;

                    $idFrom = $edge->from['name'] . '/' . $audience ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( $level )
                {
                    $edge = $this->container->courseCoursesLevels ;

                    $idFrom = $edge->from['name'] . '/' . $level ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( $path )
                {
                    $edge = $this->container->courseCoursesPaths ;

                    $idFrom = $edge->from['name'] . '/' . $path ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( $status )
                {
                    $edge = $this->container->courseCoursesStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                $item['elevation'] = $elevation ;

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postCourse",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="status",type="integer",description="The id of the stage status"),
     *             required={"name","location","status"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug($this . ' post()');
        }

        // check
        $params = $request->getParsedBody();

        $additionalType   = NULL ;

        $item = [];

        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path ;
        $item['steps'] = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'additionalType' ] ;
        }

        // get open status
        $status = $this->container->coursesStatus->get( 'open' , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;
        if( $status )
        {
            $status = $status->_key ;
        }

        $conditions =
        [
            'name'     => [ $params['name'] , 'required|max(70)' ],
            'status'   => [ $status         , 'required|status'  ]
        ] ;

        ////// validator

        $validator = new CourseValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {
                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                // set empty array discover
                $item['discover'] = [] ;

                $result = $this->model->insert( $item );

                // add  edge

                // status edge
                if( $additionalType !== NULL )
                {
                    $edge = $this->container->courseCoursesTypes ;

                    $idFrom = $edge->from['name'] . '/' . $additionalType ;

                    // add edge
                    $edge->insertEdge( $idFrom , $result->_id ) ;

                }

                if( $status )
                {
                    $edge = $this->container->courseCoursesStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // add edge
                    $edge->insertEdge( $idFrom , $result->_id ) ;
                }

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ]) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putCourse",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="status",type="integer",description="The id of the stage status"),
     *             required={"name","location","status"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract(array_merge(self::ARGUMENTS_PUT_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' put(' . $id . ')');
        }

        // check
        $params = $request->getParsedBody();

        $additionalType   = NULL ;

        $audience = NULL ;
        $level    = NULL ;
        $path     = NULL ;
        $status   = NULL ;

        $item = [];

        $elevation = [];


        if( isset($params[ 'name' ]) )
        {
            $item[ 'name' ] = $params[ 'name' ];
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'additionalType' ] ;
        }

        if( isset($params[ 'distance' ]) )
        {
            if( $params['distance'] != '' )
            {
                $item[ 'distance' ] = (int) $params[ 'distance' ];
            }
            else
            {
                $item['distance'] = null ;
            }
        }

        if( isset( $params['audience'] ) )
        {
            $audience = $params['audience'] ;
        }

        if( isset( $params['level'] ) )
        {
            $level = $params['level'] ;
        }

        if( isset( $params['path'] ) )
        {
            $path = $params['path'] ;
        }

        if( isset( $params['status'] ) )
        {
            $status = $params['status'] ;
        }

        $conditions =
        [
            'name'     => [ $params['name']     , 'required|max(70)' ] ,
            'audience' => [ $audience           , 'audience'         ] ,
            'level'    => [ $level              , 'level'            ] ,
            'path'     => [ $path               , 'path'             ] ,
            'status'   => [ $status             , 'required|status'  ] ,
            'distance' => [ $params['distance'] , 'int'              ]
        ] ;

        if( isset($params[ 'minValue' ]) )
        {
            if(  $params['minValue'] != '' )
            {
                $elevation[ 'minValue' ] = (int) $params[ 'minValue' ];
                $conditions['minValue'] = [ $params['minValue'] , 'int'  ] ;
            }
            else
            {
                $elevation['minValue'] = null ;
            }
        }

        if( isset($params[ 'maxValue' ]) )
        {
            if( $params['maxValue'] != '' )
            {
                $elevation[ 'maxValue' ] = (int) $params[ 'maxValue' ];
                $conditions['maxValue'] = [ $params['maxValue'] , 'int'  ] ;
            }
            else
            {
                $elevation['maxValue'] = null ;
            }
        }

        if( isset($params[ 'gain' ]) )
        {
            if( $params['gain'] != '' )
            {
                $elevation[ 'gain' ] = (int) $params[ 'gain' ];
                $conditions['gain'] = [ $params['gain'] , 'int'  ] ;
            }
            else
            {
                $elevation['gain'] = null ;
            }
        }

        if( isset($params[ 'loss' ]) )
        {
            if( $params['loss'] != '' )
            {
                $elevation[ 'loss' ] = (int) $params[ 'loss' ];
                $conditions['loss'] = [ $params['loss'] , 'int'  ] ;
            }
            else
            {
                $elevation['loss'] = null ;
            }
        }

        ////// validator

        $validator = new CourseValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                // update edges

                if( $additionalType !== NULL )
                {
                    $edge = $this->container->courseCoursesTypes ;

                    $idFrom = $edge->from['name'] . '/' . $additionalType ;

                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }
                else
                {
                    $edge = $this->container->courseCoursesTypes ;

                    $idFrom = $edge->from['name'] . '/' . $additionalType ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                }

                if( $audience )
                {
                    $edge = $this->container->courseCoursesAudiences ;

                    $idFrom = $edge->from['name'] . '/' . $audience ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }
                else
                {
                    $edge = $this->container->courseCoursesAudiences ;

                    $idFrom = $edge->from['name'] . '/' . $audience ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                }

                if( $level )
                {
                    $edge = $this->container->courseCoursesLevels ;

                    $idFrom = $edge->from['name'] . '/' . $level ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }
                else
                {
                    $edge = $this->container->courseCoursesLevels ;

                    $idFrom = $edge->from['name'] . '/' . $level ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                }

                if( $path )
                {
                    $edge = $this->container->courseCoursesPaths ;

                    $idFrom = $edge->from['name'] . '/' . $path ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }
                else
                {
                    $edge = $this->container->courseCoursesPaths ;

                    $idFrom = $edge->from['name'] . '/' . $path ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                }

                if( $status )
                {
                    $edge = $this->container->courseCoursesStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( !empty( $elevation ) )
                {
                    $item['elevation'] = $elevation ;
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    private function endsWith( $haystack , $needle )
    {
        return substr( $haystack , -strlen( $needle ) ) === $needle ;
    }
}

