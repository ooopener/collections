<?php

namespace com\ooopener\controllers\creativeWork\collection;

use com\ooopener\controllers\CollectionsController;

use com\ooopener\helpers\Status;
use com\ooopener\models\Collections;

use com\ooopener\things\creativeWork\CreativeWorkCollection;
use com\ooopener\things\Thing;

use com\ooopener\validations\creativeWork\CreativeWorkCollectionValidator;

use Exception;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Container;

/**
 * The creativeWorkCollection controller.
 */
class CreativeWorkCollectionsController extends CollectionsController
{
    /**
     * Creates a new CreativeWorkCollectionsController instance.
     *
     * @param Container $container
     * @param Collections $model
     * @param string $path
     */
    public function __construct( Container $container , Collections $model = NULL , $path = 'creativeWorkCollection' )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'startDate'     => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'endDate'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'image'         => [ 'filter' => Thing::FILTER_DEFAULT  ] ,

        'additionalType'      => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'alternativeHeadline' => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'description'         => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'headline'            => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'text'                => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,

        'notes'          => [ 'filter' => Thing::FILTER_TRANSLATE               , 'skins' => [ 'full' , 'normal' ] ] ,
        'keywords'       => [ 'filter' => Thing::FILTER_EDGE                    , 'skins' => [ 'full' ] ] ,
        'photos'         => [ 'filter' => CreativeWorkCollection::FILTER_PHOTOS , 'skins' => [ 'full' , 'photos' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case CreativeWorkCollection::FILTER_HAS_PART :
                    {
                        if( property_exists( $init , CreativeWorkCollection::FILTER_HAS_PART ) && is_array( $init->{ CreativeWorkCollection::FILTER_HAS_PART } ) && count( $init->{ CreativeWorkCollection::FILTER_HAS_PART } ) > 0 )
                        {
                            $hasPart = [] ;
                            foreach( $init->{ CreativeWorkCollection::FILTER_HAS_PART } as $item )
                            {
                                array_push( $hasPart , $this->create( (object) $item , $lang , 'normal' ) ) ;
                            }
                            $init->{ $key } = $hasPart ;
                        }
                        break ;
                    }
                    case CreativeWorkCollection::FILTER_IS_PART_OF :
                    {
                        if( property_exists( $init , CreativeWorkCollection::FILTER_IS_PART_OF ) && is_array( $init->{ CreativeWorkCollection::FILTER_IS_PART_OF } ) && count( $init->{ CreativeWorkCollection::FILTER_IS_PART_OF } ) > 0 )
                        {
                            $isPartOf = [] ;
                            foreach( $init->{ CreativeWorkCollection::FILTER_IS_PART_OF } as $item )
                            {
                                array_push( $isPartOf , $this->create( (object) $item , $lang , 'normal' ) ) ;
                            }
                            $init->{ $key } = $isPartOf ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    ///////////////////////////

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        //// remove all linked resources

        // remove hasPart
        $hasPart = $this->container->creativeWorkCollectionHasPartsController->deleteALL( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove isPartOf
        $isPartOf = $this->container->creativeWorkCollectionHasPartsController->deleteALL( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE ] ) ;

        return parent::delete( $request , $response , $args ) ;
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType = NULL ;

        $item = [];

        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string)$params['additionalType'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']        , 'required|min(2)|max(140)' ] ,
            'additionalType' => [ $additionalType        , 'required|additionalType'  ]
        ] ;

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new CreativeWorkCollectionValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $result = $this->model->insert( $item ) ;

                // add additionalType edge
                $addTypeEdge = $this->container->creativeWorkCollectionCreativeWorkCollectionsTypes ;
                $ate = $addTypeEdge->insertEdge( $addTypeEdge->from['name'] . '/' . $additionalType , $result->_id ) ;

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        $old = $this->model->get( $id , [ 'fields' => 'endDate,startDate' ] ) ;
        if( !$old )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType = NULL ;

        $testEndDate   = $old->endDate ;
        $testStartDate = $old->startDate ;

        $item = [];
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(140)' ] ;
        }

        if( isset( $params['endDate'] ) )
        {
            if( $params['endDate'] != '' )
            {
                $item['endDate'] = $testEndDate = $params['endDate'] ;
                $conditions['endDate'] = [ $params['endDate'] , 'datetime' ] ;
            }
            else
            {
                $item['endDate'] = $testEndDate = NULL ;
            }
        }

        if( isset( $params['startDate'] ) )
        {
            $item['startDate'] = $testStartDate = $params['startDate'] ;
            $conditions['startDate'] = [ $params['startDate'] , 'required|datetime' ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType' ] ;
        }

        /// check dates

        if( $testStartDate && $testEndDate )
        {
            $after = (boolean) (strtotime($testStartDate) < strtotime($testEndDate)) ;
            $conditions['after']  = [ $after , 'true' ] ;
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        // check if there is at least one param
        if( empty( $item ) && $additionalType === NULL )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        ////// validator

        $validator = new CreativeWorkCollectionValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $additionalType !== NULL )
                {
                    $addTypeEdge = $this->container->eventEventsTypes ;

                    $idFrom = $addTypeEdge->from['name'] . '/' . $additionalType ;

                    if( !$addTypeEdge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $addTypeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $addTypeEdge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response ,$this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }
}
