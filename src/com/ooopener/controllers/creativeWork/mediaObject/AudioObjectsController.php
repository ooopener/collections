<?php

namespace com\ooopener\controllers\creativeWork\mediaObject;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\models\Collections;
use com\ooopener\things\Thing;



use Slim\Container;

class AudioObjectsController extends CollectionsController
{
    /**
     * Creates a new AudioObjectsController instance.
     *
     * @param Container $container
     * @param Collections $model
     * @param string $path
     */
    public function __construct( Container $container , Collections $model = NULL , $path = 'imageObject' )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * @OA\Schema(
     *     schema="AudioObject",
     *     allOf={@OA\Schema(ref="#/components/schemas/MediaObject")},
     *     @OA\Property(type="string",property="caption",description="The caption for this object"),
     *     @OA\Property(type="string",property="transcript",description="If this MediaObject is an AudioObject or VideoObject, the transcript of that object")
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'            =>  [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          =>  [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           =>  [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       =>  [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      =>  [ 'filter' => Thing::FILTER_DATETIME ] ,

        'position'      => [ 'filter' => Thing::FILTER_UNIQUE_NAME , 'skins' => [ 'extend' ] ] ,

        'headline'            =>  [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'alternativeHeadline' =>  [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'description'         =>  [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'text'                =>  [ 'filter' => Thing::FILTER_TRANSLATE ] ,

        'author'              =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'encoding'            =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'encodingFormat'      =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'inLanguage'          =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'keywords'            =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'license'             =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'mentions'            =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'publisher'           =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'review'              =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'source'              =>  [ 'filter' => Thing::FILTER_MEDIA_SOURCE ] ,
        'thumbnail'           =>  [ 'filter' => Thing::FILTER_MEDIA_THUMBNAIL ] ,
        'thumbnailUrl'        =>  [ 'filter' => Thing::FILTER_DEFAULT ] ,

        'bitrate'             =>  [ 'filter' => Thing::FILTER_INT       ] ,
        'contentSize'         =>  [ 'filter' => Thing::FILTER_INT       ] ,
        'contentUrl'          =>  [ 'filter' => Thing::FILTER_MEDIA_URL ] ,
        'duration'            =>  [ 'filter' => Thing::FILTER_FLOAT     ] ,
        'editor'              =>  [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'embedUrl'            =>  [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'playerType'          =>  [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'width'               =>  [ 'filter' => Thing::FILTER_INT       ] ,
        'height'              =>  [ 'filter' => Thing::FILTER_INT       ] ,

        'audioCodec'          =>  [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'bitsPerSample'       =>  [ 'filter' => Thing::FILTER_INT       ] ,
        'channels'            =>  [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'sampleRate'          =>  [ 'filter' => Thing::FILTER_INT       ] ,
        'transcript'          =>  [ 'filter' => Thing::FILTER_TRANSLATE , 'skins' => [ 'full' ]  ]
    ];
}
