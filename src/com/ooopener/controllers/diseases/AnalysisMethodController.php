<?php

namespace com\ooopener\controllers\diseases;

use com\ooopener\controllers\ThingsEdgesController;
use com\ooopener\models\Collections;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;

use com\ooopener\validations\DiseaseAnalysisMethodValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container;

/**
 * The AnalysisMethodController controller.
 */
class AnalysisMethodController extends ThingsEdgesController
{
    /**
     * Creates a new AnalysisMethodController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param Edges|NULL $edge
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );

        $this->validator = new DiseaseAnalysisMethodValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'id'             => [ 'filter' =>  Thing::FILTER_ID      ] ,

        'created'        => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'additionalType' => [ 'filter' => Thing::FILTER_JOIN     ],

        'mandatory'      => [ 'filter' => Thing::FILTER_BOOL     ]
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['mandatory'] ) )
        {
            $item['mandatory'] = (int) $params['mandatory'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'additionalType' => [ $params['additionalType'] , 'required|additionalType' ]
        ];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'min(2)|max(50)' ] ;
        }

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::post( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['mandatory'] ) )
        {
            $item['mandatory'] = (int) $params['mandatory'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
        }

        $conditions =
        [
            'additionalType' => [ $params['additionalType'] , 'required|additionalType' ]
        ];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'min(2)|max(50)' ] ;
        }

        // set
        $this->conditions = $conditions ;
        $this->item = $item ;

        return parent::put( $request , $response , $args ) ;
    }
}
