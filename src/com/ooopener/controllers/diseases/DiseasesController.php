<?php

namespace com\ooopener\controllers\diseases;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Collections;
use com\ooopener\things\Thing;

use com\ooopener\validations\DiseaseValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container;

use Exception;

class DiseasesController extends CollectionsController
{
    public function __construct( Container $container , Collections $model , $path = 'diseases' )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'additionalType'  => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,

        'level'          => [ 'filter' => Thing::FILTER_EDGE_SINGLE ],

        'notes'         => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,
        'text'          => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,

        'analysisMethod'     => [ 'filter' => Thing::FILTER_EDGE   , 'skins' => [ 'full' ] ] ,
        'analysisSampling'   => [ 'filter' => Thing::FILTER_EDGE   , 'skins' => [ 'full' ] ] ,
        'transmissionMethod' => [ 'filter' => Thing::FILTER_EDGE   , 'skins' => [ 'full' ] ]
    ];

    ///////////////////////////

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
        }

        //// remove all linked resources

        // remove additionalType
        $additionalType = $this->container->diseaseDiseasesTypesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove level
        $level = $this->container->diseaseDiseasesLevelsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove analysisMethod
        $analysisMethod = $this->container->diseaseAnalysisMethodController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove analysisSampling
        $analysisSampling = $this->container->diseaseAnalysisSamplingController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove transmissionMethod
        $transmissionMethod = $this->container->diseaseTransmissionsMethodsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_PATCH_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' patch(' . $id . ')');
        }

        // check
        $params = $request->getParsedBody();

        $additionalType = NULL ;
        $level   = NULL ;

        $item = [];
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(70)' ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            if( $additionalType != '' )
            {
                $conditions['additionalType'] = [ $params['additionalType'] , 'additionalType' ] ;
            }
        }

        if( isset( $params['level'] ) )
        {
            $level = $params['level'] ;
            if( $level != '' )
            {
                $conditions['level'] = [ $params['level'] , 'level' ] ;
            }
        }

        // check if there is at least one param
        if( empty( $item ) && $additionalType === NULL && $level === NULL )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        // check if resource exists
        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        ////// validator

        $validator = new DiseaseValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $additionalType !== NULL )
                {
                    $addTypeEdge = $this->container->diseaseDiseasesTypes ;

                    $idFrom = $addTypeEdge->from['name'] . '/' . $additionalType ;

                    if( !$addTypeEdge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $addTypeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        if( $additionalType != '' )
                        {
                            $addTypeEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                if( $level !== NULL )
                {
                    $levelEdge = $this->container->diseaseDiseasesLevels ;

                    $idFrom = $levelEdge->from['name'] . '/' . $level ;

                    if( !$levelEdge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $levelEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        if( $level != '' )
                        {
                            $levelEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                $result = $this->model->update( $item , $id ) ;

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'extend' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug($this . ' post()');
        }

        // check
        $params = $request->getParsedBody();

        $additionalType = NULL ;
        $level = NULL ;

        $item = [];

        $item[ 'active' ] = 1;
        $item['withStatus'] = Status::DRAFT ;
        $item[ 'path' ] = $this->path;

        if( isset($params[ 'name' ]) )
        {
            $item[ 'name' ] = $params[ 'name' ];
        }

        $conditions =
        [
            'name'     => [ $params['name'] , 'required|max(70)' ]
        ] ;

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string)$params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'additionalType' ] ;
        }

        if( isset( $params['level'] ) )
        {
            $level = (string)$params['level'] ;
            $conditions['level'] = [ $params['level'] , 'level' ] ;
        }

        ////// validator

        $validator = new DiseaseValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {
                $result = $this->model->insert( $item );

                // add  edge

                // add additionalType edge
                if( $additionalType != NULL )
                {
                    $addTypeEdge = $this->container->diseaseDiseasesTypes ;
                    $ate = $addTypeEdge->insertEdge( $addTypeEdge->from['name'] . '/' . $additionalType , $result->_id ) ;
                }

                // add level edge
                if( $level != NULL )
                {
                    $levelEdge = $this->container->diseaseDiseasesLevels ;
                    $le = $levelEdge->insertEdge( $levelEdge->from['name'] . '/' . $level , $result->_id ) ;
                }

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ]) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }
}
