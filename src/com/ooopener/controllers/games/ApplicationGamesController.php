<?php

namespace com\ooopener\controllers\games;

use com\ooopener\helpers\Status;
use com\ooopener\things\Game;
use com\ooopener\things\Thing;
use com\ooopener\validations\GameValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\CollectionsController;

use com\ooopener\models\Games;
use Slim\Container;

use Exception ;

class ApplicationGamesController extends CollectionsController
{
    public function __construct( Container $container , Games $model , $path = 'games' )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        //'image'         => [ 'filter' => Thing::FILTER_DEFAULT ] ,

        //'about'           => [ 'filter' => Thing::FILTER_JOIN  ] ,
        'additionalType'  => [ 'filter' => Thing::FILTER_JOIN  ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,

        'item'          => [ 'filter' => Thing::FILTER_DEFAULT     , 'skins' => [ 'full' , 'normal' ] ] ,
        'notes'         => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,
        'text'          => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,

        'quest'          => [ 'filter' => Thing::FILTER_EDGE   , 'skins' => [ 'full' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        /*if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {

                }
            }
        }*/
        return $init ;
    }

    ///////////////////////////

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        //// remove all linked resources

        // delete gameCourses
        $gameCourses = $this->container->coursesGamesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // update applications
        $this->updateApplications( $id ) ;

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
        [
            'id' => NULL
        ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_PATCH_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' patch(' . $id . ')');
        }

        // check
        $params = $request->getParsedBody();

        $item = [];
        $conditions = [] ;

        if( isset($params[ 'name' ]) )
        {
            $item[ 'name' ] = $params[ 'name' ];
            $conditions['name'] = [ $params['name'] , 'required|max(70)'  ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType'  ] ;
        }

        ////// validator

        $validator = new GameValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    // update applications
                    $this->updateApplications( $id ) ;

                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug($this . ' post()');
        }

        // check
        $params = $request->getParsedBody();

        $item = [];

        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        // get additionalType
        $additionalType = $this->container->gamesTypes->get( 'application' , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;
        if( $additionalType )
        {
            $item['additionalType'] = (int) $additionalType->_key ;
        }

        $conditions =
        [
            'name'     => [ $params['name'] , 'required|max(70)' ]
        ] ;

        ////// validator

        $validator = new GameValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {
                $result = $this->model->insert( $item );

                // add  edge

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ]) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    public function update( $id )
    {
        $this->model->updateDate( $id ) ;

        $this->updateApplications( $id ) ;
    }

    private function updateApplications( $id )
    {
        if( isset( $this->container->applications ) )
        {
            $this->container->applications->updateDate( $id , [ 'key' => 'setting.game' ] );
        }
    }
}
