<?php

namespace com\ooopener\controllers\games;

use com\ooopener\controllers\Controller;
use com\ooopener\controllers\ThingsEdgesController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;
use com\ooopener\validations\GameValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container;

use Exception ;

class QuestionGamesController extends ThingsEdgesController
{
    /**
     * Creates a new QuestionGamesController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Model|NULL $owner
     * @param Edges|NULL $edge
     * @param Controller|NULL $controller
     * @param string $path
     */
    public function __construct( Container $container , Model $model = NULL , Model $owner = NULL , Edges $edge = NULL , Controller $controller = NULL , $path = NULL )
    {
        $this->controller = $controller ;

        parent::__construct( $container , $model , $owner , $edge , $path );
    }

    public $controller ;

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        //'image'         => [ 'filter' => Thing::FILTER_DEFAULT ] ,

        'about'           => [ 'filter' => Thing::FILTER_JOIN  ] ,
        'additionalType'  => [ 'filter' => Thing::FILTER_JOIN  ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE    ] ,

        'item'          => [ 'filter' => Thing::FILTER_DEFAULT     , 'skins' => [ 'full' , 'normal' ] ] ,
        'notes'         => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,
        'text'          => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'normal' ] ] ,

        'quest'         => [ 'filter' => Thing::FILTER_DEFAULT   , 'skins' => [ 'full' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        /*if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {

                }
            }
        }*/
        return $init ;
    }

    ///////////////////////////

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        //// remove all linked resources

        // get owner
        $edgeOwner = $this->edge->getEdge( $id ) ;
        if( $edgeOwner && property_exists( $edgeOwner , Thing::FILTER_EDGE ) && is_array( $edgeOwner->edge ) && count( $edgeOwner->edge ) == 1 )
        {
            $args['owner'] = (string)$edgeOwner->edge[0]['id'] ;
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $set = $this->container->settings[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $result = $this->model->get( $id , [ 'queryFields' => $this->getFields( $skin ) ] ) ;

            $result = $this->create( $result ) ;

            if( $response )
            {
                // get courseGame
                $courseGame = null ;
                $edgeCourseGame = $this->edge->getEdge( (string) $result->id ) ;
                if( $edgeCourseGame && property_exists( $edgeCourseGame , Thing::FILTER_EDGE ) && is_array( $edgeCourseGame->edge ) && count( $edgeCourseGame->edge ) == 1 )
                {
                    $courseGame = $edgeCourseGame->edge[0] ;
                }
                $options[ 'object' ] = $courseGame ;

                return $this->success( $response , $result , null , null , $options );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_PATCH_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' patch(' . $id . ')');
        }

        // check
        $params = $request->getParsedBody();

        $item = [];
        $conditions = [] ;

        if( isset($params[ 'name' ]) )
        {
            $item[ 'name' ] = $params[ 'name' ];
            $conditions['name'] = [ $params['name'] , 'required|max(70)'  ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $item['additionalType'] = (int) $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType'  ] ;
        }

        if( isset( $params['quest'] ) )
        {
            try
            {
                $item['quest'] = json_decode( $params['quest'] , true ) ;
            }
            catch(Exception $e)
            {
                return $this->formatError( $response , '400', [ 'patch(' . $id . ')' ] , NULL , 400 );
            }
        }

        ////// validator

        $validator = new GameValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {

                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
                }

                if( array_key_exists( 'quest' , $item ) )
                {
                    $result = $this->model->update( [ 'quest' => '' ] , $id );
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {

                    // get owner
                    $owner = $this->edge->getEdge( $id );

                    if( $owner && property_exists( $owner , 'edge' ) && is_array($owner->edge) && count($owner->edge) == 1 )
                    {
                        $owner = (object) $owner->edge[0] ;
                        // update owner
                        $this->controller->update( (string)$owner->id );
                    }

                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'full' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_POST_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' post(' . $id . ')');
        }

        // check
        $params = $request->getParsedBody();

        try
        {
            $ev = $this->owner->get($id , [ 'fields' => '_id' ]);
            if( !$ev )
            {
                return $this->formatError($response , '404' , [ 'post(' . $id . ')' ] , NULL , 404);
            }

            $item = [];

            $item['active'] = 1 ;
            $item['withStatus'] = Status::DRAFT ;
            $item['path'] = $this->path ;

            if( isset( $params['about'] ) )
            {
                $item['about'] = (int) $params['about'] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            // get additionalType
            $additionalType = $this->container->gamesTypes->get( 'question' , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;
            if( $additionalType )
            {
                $item['additionalType'] = (int) $additionalType->_key ;
            }

            $conditions =
            [
                'name'     => [ $params['name']  , 'required|max(70)' ] ,
                'about'    => [ $params['about'] , 'required|step|uniqueStep(' . $id . ',' . $this->edge->table . ')'    ]
            ] ;

            ////// validator

            $validator = new GameValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {

                $result = $this->model->insert( $item );

                if( $result )
                {
                    // save questions courses edge
                    $qce = $this->edge->insertEdge( $result->_id , $ev->_id ) ;

                    // update owner
                    $this->controller->update( $id ) ;

                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ]) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }

            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
        }
    }

    public function update( $id )
    {
        $this->model->updateDate( $id ) ;

        // get owner
        $owner = $this->edge->getEdge( $id );

        if( $owner && key_exists( 'edge' , $owner ) && is_array($owner->edge) && count($owner->edge) == 1 )
        {
            $owner = (object) $owner->edge[0] ;
            // update owner
            $this->controller->update( (string)$owner->id );
        }
    }
}
