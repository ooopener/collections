<?php

namespace com\ooopener\controllers\livestocks ;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Collections;

use com\ooopener\things\Livestock;
use com\ooopener\things\Thing;
use com\ooopener\validations\LivestockValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Slim\Container ;

class LivestocksController extends CollectionsController
{
    /**
     * Creates a new LivestocksController instance.
     *
     * @param Container $container
     * @param Collections $model
     * @param string $path
     */
    public function __construct( Container $container , Collections $model = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $path );
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'organization'  => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,
        'numbers'       => [ 'filter' =>  Thing::FILTER_EDGE        ] ,

        'technicians'   => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'full' ] ] ,
        'veterinarians' => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'full' ] ] ,
        'workshops'     => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'list' , 'full' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Livestock::FILTER_ORGANIZATION :
                    {
                        if( property_exists( $init , Livestock::FILTER_ORGANIZATION ) && $init->{ Livestock::FILTER_ORGANIZATION } != NULL )
                        {
                            $init->{ $key } = $this->container->organizationsController->create( (object) $init->{ Livestock::FILTER_ORGANIZATION } ) ;
                        }
                        break ;
                    }
                    case Livestock::FILTER_TECHNICIANS :
                    {
                        if( property_exists( $init , Livestock::FILTER_TECHNICIANS ) && is_array( $init->{ Livestock::FILTER_TECHNICIANS } ) && count( $init->{ Livestock::FILTER_TECHNICIANS } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Livestock::FILTER_TECHNICIANS } as $item )
                            {
                                array_push( $sub , $this->container->techniciansController->create( (object) $item ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Livestock::FILTER_VETERINARIANS :
                    {
                        if( property_exists( $init , Livestock::FILTER_VETERINARIANS ) && is_array( $init->{ Livestock::FILTER_VETERINARIANS } ) && count( $init->{ Livestock::FILTER_VETERINARIANS } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Livestock::FILTER_VETERINARIANS } as $item )
                            {
                                array_push( $sub , $this->container->veterinariansController->create( (object) $item ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        try
        {

            // remove children
            $deleteChildren  = $this->container->workshopsController->deleteAll( NULL , NULL , [ 'owner' =>  $id ] ) ;

            // remove edges

            // remove organizations
            $organizations = $this->container->livestockOrganizationsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

            // remove technicians
            $technicians = $this->container->livestockTechniciansController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

            // remove veterinarians
            $veterinarians = $this->container->livestockVeterinariansController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

            // remove workshops breedingTypes edge
            $this->container['livestocksLivestocksNumbers']->delete( $this->container['livestocksLivestocksNumbers']->to['table'] . '/' . $id , [ 'key' => '_to' ] ) ;


        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        return parent::delete( $request , $response , $args ) ;
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $organization = NULL ;

        $item = [];
        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path ;

        if( isset( $params['organization'] ) )
        {
            $organization = $params['organization']  ;
        }

        $conditions =
        [
            'organization' => [ $organization       , 'required|organization' ]
        ] ;

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new LivestockValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $new = $this->model->insert( $item );

                // organization edge
                if( $organization )
                {
                    $edge = $this->container->livestockOrganizations ;

                    $idFrom = $edge->from['name'] . '/' . $organization ;

                    // add edge
                    $edge->insertEdge( $idFrom , $new->_id ) ;
                }

                if( $new )
                {
                    return $this->success( $response , $this->model->get( $new->_key , [ 'queryFields' => $this->getFields() ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' );
        }

        // check
        $params = $request->getParsedBody();

        $organization = NULL;

        $item = [];

        if( isset( $params['organization'] ) )
        {
            $organization = $params['organization']  ;
        }

        $conditions =
        [
            'organization' => [ $organization   , 'required|organization' ]
        ] ;


        ////// validator

        $validator = new LivestockValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $organization )
                {
                    $edge = $this->container->livestockOrganizations ;

                    $idFrom = $edge->from['name'] . '/' . $organization ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

}
