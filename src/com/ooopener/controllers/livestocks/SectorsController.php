<?php

namespace com\ooopener\controllers\livestocks ;

use com\ooopener\controllers\ThingsEdgesController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;
use com\ooopener\validations\SectorValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Slim\Container ;

class SectorsController extends ThingsEdgesController
{
    /**
     * Creates a new WorkshopsController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Model|NULL $owner
     * @param Edges|NULL $edge
     * @param string $path
     */
    public function __construct( Container $container , Model $model = NULL , Model $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'capacity'          => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'numAttendee'       => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'remainingAttendee' => [ 'filter' => Thing::FILTER_DEFAULT   ] ,

        'additionalType' => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ]
    ];

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        try
        {
            // remove edges

            // remove sectors sectorsTypes
            $sectorTypes = $this->container->sectorSectorsTypesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ] ;

    /**
     * The post request
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_POST_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        try
        {
            $workplace = $this->owner->get($id , [ 'fields' => '_id,_key' ]);
            if( !$workplace )
            {
                return $this->formatError($response , '404' , [ 'post(' . $id . ')' ] , NULL , 404);
            }

            $workshop = $this->container->workshops->get($owner , [ 'fields' => '_id' ]);
            if( !$workshop )
            {
                return $this->formatError($response , '404' , [ 'post(' . $id . ')' ] , NULL , 404);
            }

            $item = [] ;

            $item['active'] = 1 ;
            $item['withStatus'] = Status::DRAFT ;

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            if( isset( $params['additionalType'] ) )
            {
                $item['additionalType'] = (int)$params['additionalType'] ;
            }

            $conditions =
            [
                'name'           => [ $params['name']           , 'required|max(70)' ] ,
                'additionalType' => [ $params['additionalType'] , 'required|additionalType' ]
            ] ;

            if( isset( $params['capacity'] ) )
            {
                $item['capacity'] = (int) $params['capacity'] ;
                $conditions['capacity'] = [ $params['capacity'] , 'int' ] ;
            }

            if( isset( $params['numAttendee'] ) )
            {
                $item['numAttendee'] = (int) $params['numAttendee'] ;
                $conditions['numAttendee'] = [ $params['numAttendee'] , 'int' ] ;
            }

            if( isset( $params['remainingAttendee'] ) )
            {
                $item['remainingAttendee'] = (int) $params['remainingAttendee'] ;
                $conditions['remainingAttendee'] = [ $params['remainingAttendee'] , 'int' ] ;
            }

            ////// validator

            $validator = new SectorValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {

                $item['active'] = 1 ;
                $item['path'] = 'livestocks/' . $workshop->_id . '/' . $workplace->_id . '/' . $this->path ;

                // save
                $result = $this->model->insert( $item );

                if( $result )
                {
                    // save workplaces sectors edges
                    $wse = $this->edge->insertEdge( $result->_id , $workplace->_id ) ;

                    // save sectors sectorsTypes edge
                    $sst = $this->container->sectorsSectorsTypes->insertEdge( $this->container->sectorsSectorsTypes->from['name'] . '/' . $params['additionalType'] , $result->_id ) ;

                    return $this->success( $response , $this->get( NULL , NULL , [ 'owner' => $id , 'id' => $result->_key ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }

            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_POST_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' put(' . $owner . ',' . $id . ')');
        }

        $params = $request->getParsedBody();

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $item = [];

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            if( isset( $params['additionalType'] ) )
            {
                $item['additionalType'] = (int)$params['additionalType'] ;
            }

            $conditions =
            [
                'name'           => [ $params['name']           , 'required|max(70)' ] ,
                'additionalType' => [ $params['additionalType'] , 'required|additionalType' ]
            ];

            if( isset( $params['capacity'] ) )
            {
                $item['capacity'] = (int) $params['capacity'] ;
                $conditions['capacity'] = [ $params['capacity'] , 'int' ] ;
            }

            if( isset( $params['numAttendee'] ) )
            {
                $item['numAttendee'] = (int) $params['numAttendee'] ;
                $conditions['numAttendee'] = [ $params['numAttendee'] , 'int' ] ;
            }

            if( isset( $params['remainingAttendee'] ) )
            {
                $item['remainingAttendee'] = (int) $params['remainingAttendee'] ;
                $conditions['remainingAttendee'] = [ $params['remainingAttendee'] , 'int' ] ;
            }

            $validator = new SectorValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {
                $result = $this->model->update( $item , $id ) ;

                // update edges

                // save sectors sectorsTypes
                $existSST = $this->container->sectorsSectorsTypes->existEdge( $this->container->sectorsSectorsTypes->from['name'] . '/' . $params['additionalType'] , $this->container->sectorsSectorsTypes->to['name'] . '/' . $id ) ;
                if( !$existSST )
                {
                    $this->container->sectorsSectorsTypes->delete( $this->container->sectorsSectorsTypes->to['name'] . '/' . $id ) ;
                    $sst = $this->container->sectorsSectorsTypes->insertEdge( $this->container->sectorsSectorsTypes->from['name'] . '/' . $params['additionalType'] , $this->container->sectorsSectorsTypes->to['name'] . '/' . $id ) ;
                }
                return $this->success( $response , $this->get( NULL , NULL , [ 'owner' => $owner , 'id' => $id ] ) ) ;

                //////
            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
