<?php

namespace com\ooopener\controllers\livestocks ;

use com\ooopener\controllers\ThingsEdgesController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Edges;
use com\ooopener\models\Model;

use com\ooopener\things\Thing;
use com\ooopener\validations\WorkplaceValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Slim\Container ;

class WorkplacesController extends ThingsEdgesController
{
    /**
     * Creates a new WorkshopsController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Model|NULL $owner
     * @param Edges|NULL $edge
     * @param string $path
     */
    public function __construct( Container $container , Model $model = NULL , Model $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'       => [ 'filter' =>  Thing::FILTER_BOOL     ] ,
        'withStatus'   => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'           => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'         => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'identifier'   => [ 'filter' =>  Thing::FILTER_JOIN     ] ,
        'url'          => [ 'filter' =>  Thing::FILTER_URL      ] ,
        'created'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'     => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'capacity'          => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'numAttendee'       => [ 'filter' => Thing::FILTER_DEFAULT ] ,
        'remainingAttendee' => [ 'filter' => Thing::FILTER_DEFAULT ] ,

        //'location'      => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE    ] ,
        'sectors'      => [ 'filter' =>  Thing::FILTER_EDGE    ]
    ];

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        try
        {
            // delete sectors
            $deleteChildren  = $this->container->sectorsController->deleteAll( NULL , NULL , [ 'owner' =>  $id ] ) ;

            // remove edges


        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'    => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_POST_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' post(' . $id . ')');
        }

        $params = $request->getParsedBody();

        try
        {
            $ev = $this->owner->get($id , [ 'fields' => '_id' ]);
            if( !$ev )
            {
                return $this->formatError($response , '404' , [ 'post(' . $id . ')' ] , NULL , 404);
            }

            $item = [];

            $item['active'] = 1 ;
            $item['withStatus'] = Status::DRAFT ;

            if( isset( $params['identifier'] ) )
            {
                $item['identifier'] = (int) $params['identifier'] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            $conditions =
            [
                'name'       => [ $params['name']       , 'required|max(70)'        ] ,
                'identifier' => [ $params['identifier'] , 'required|int|identifier' ]
            ];

            if( isset( $params['capacity'] ) )
            {
                $item['capacity'] = (int) $params['capacity'] ;
                $conditions['capacity'] = [ $params['capacity'] , 'int' ] ;
            }

            if( isset( $params['numAttendee'] ) )
            {
                $item['numAttendee'] = (int) $params['numAttendee'] ;
                $conditions['numAttendee'] = [ $params['numAttendee'] , 'int' ] ;
            }

            if( isset( $params['remainingAttendee'] ) )
            {
                $item['remainingAttendee'] = (int) $params['remainingAttendee'] ;
                $conditions['remainingAttendee'] = [ $params['remainingAttendee'] , 'int' ] ;
            }

            $validator = new WorkplaceValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {

                $item['active'] = 1 ;
                $item['path'] = 'livestocks/' . $ev->_id . '/' . $this->path ;

                // save workshop
                $result = $this->model->insert( $item ) ;

                if( $result )
                {
                    // save workshops workplaces edge
                    $wwe = $this->edge->insertEdge( $result->_id , $ev->_id ) ;

                    return $this->success( $response , $this->get( NULL , NULL , [ 'owner' => $id , 'id' => $result->_key ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error') ;
                }
            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_POST_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' put(' . $owner . ',' . $id . ')');
        }

        $params = $request->getParsedBody();

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $item = [];

            if( isset( $params['identifier'] ) )
            {
                $item['identifier'] = (int) $params['identifier'] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            $conditions =
            [
                'name'       => [ $params['name']       , 'required|max(70)'        ] ,
                'identifier' => [ $params['identifier'] , 'required|int|identifier' ]
            ];

            if( isset( $params['capacity'] ) )
            {
                $item['capacity'] = (int) $params['capacity'] ;
                $conditions['capacity'] = [ $params['capacity'] , 'int' ] ;
            }

            if( isset( $params['numAttendee'] ) )
            {
                $item['numAttendee'] = (int) $params['numAttendee'] ;
                $conditions['numAttendee'] = [ $params['numAttendee'] , 'int' ] ;
            }

            if( isset( $params['remainingAttendee'] ) )
            {
                $item['remainingAttendee'] = (int) $params['remainingAttendee'] ;
                $conditions['remainingAttendee'] = [ $params['remainingAttendee'] , 'int' ] ;
            }

            $validator = new WorkplaceValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {
                $result = $this->model->update( $item , $id ) ;

                return $this->success( $response , $this->get( NULL , NULL , [ 'owner' => $owner , 'id' => $id ] ) ) ;

                //////
            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

}
