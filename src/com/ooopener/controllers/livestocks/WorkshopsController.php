<?php

namespace com\ooopener\controllers\livestocks ;

use com\ooopener\controllers\ThingsEdgesController;
use com\ooopener\helpers\Status;
use com\ooopener\validations\WorkshopValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;


use com\ooopener\models\Edges;
use com\ooopener\models\Model;
use com\ooopener\things\Thing;

use Slim\Container ;

use Exception ;

class WorkshopsController extends ThingsEdgesController
{
    /**
     * Creates a new WorkshopsController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Model|NULL $owner
     * @param Edges|NULL $edge
     * @param string $path
     */
    public function __construct( Container $container , Model $model = NULL , Model $owner = NULL , Edges $edge = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $edge , $path );
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'identifier'    => [ 'filter' =>  Thing::FILTER_JOIN        ],
        'breeding'      => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ],
        'production'    => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ],
        'water'         => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ],

        'workplaces'    => [ 'filter' =>  Thing::FILTER_EDGE        ]
    ];

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        try
        {
            // delete workplaces
            $deleteChildren  = $this->container->workplacesController->deleteAll( NULL , NULL , [ 'owner' =>  $id ] ) ;


            // remove edges

            // remove workshops breedingTypes edge
            $this->container->workshopsBreedingsTypesController->deleteAll(  NULL , NULL , [ 'owner' => $id ]  ) ;

            // remove workshops productionTypes edge
            $this->container->workshopsProductionsTypesController->deleteAll(  NULL , NULL , [ 'owner' => $id ]  ) ;

            // remove workshops waterOrigins edge
            $this->container->workshopsWaterOriginsController->deleteAll(  NULL , NULL , [ 'owner' => $id ]  ) ;


        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $set = $this->container->settings[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $result = $this->model->get( $id , [ 'queryFields' => $this->getFields( $skin ) ] ) ;

            $result = $this->create( $result ) ;

            if( $response )
            {
                // get livestock
                $livestock = null ;
                $edgeLivestock = $this->edge->getEdge( (string) $result->id ) ;
                if( $edgeLivestock && property_exists( $edgeLivestock , Thing::FILTER_EDGE ) && is_array( $edgeLivestock->edge ) && count( $edgeLivestock->edge ) == 1 )
                {
                    $livestock = $edgeLivestock->edge[0] ;
                }
                $options[ 'object' ] = $livestock ;

                return $this->success( $response , $result , null , null , $options );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'    => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_POST_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' post(' . $id . ')');
        }

        $params = $request->getParsedBody();

        try
        {
            $ev = $this->owner->get($id , [ 'fields' => '_id' ]);
            if( !$ev )
            {
                return $this->formatError($response , '404' , [ 'post(' . $id . ')' ] , NULL , 404);
            }

            $item = [];

            $item['active'] = 1 ;
            $item['withStatus'] = Status::DRAFT ;

            if( isset( $params['identifier'] ) )
            {
                $item['identifier'] = (int) $params['identifier'] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            $conditions =
            [
                'name'       => [ $params['name']       , 'required|max(70)' ] ,
                'breeding'   => [ $params['breeding']   , 'required|int|breeding' ] ,
                'production' => [ $params['production'] , 'required|int|production' ] ,
                'identifier' => [ $params['identifier'] , 'required|int|identifier' ] ,
                'water'      => [ $params['water']      , 'required|int|water' ]
            ];

            $validator = new WorkshopValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {
                // check if breedingTypes productionTypes exists
                $check = $this->container['breedingsTypesProductionsTypesController']->exists( NULL , NULL , [ 'from' => $params['production'] , 'to' => $params['breeding'] ] ) ;

                if( !$check )
                {
                    return $this->error( $response , 'breedings types and productions types are not linked' , '400' );
                }

                $item['active'] = 1 ;
                $item['path'] = $this->owner->table . '/' . $this->path ;

                // save workshop
                $result = $this->model->insert( $item ) ;

                if( $result )
                {
                    // save workshops livestocks edge
                    $wle = $this->edge->insertEdge( $result->_id , $ev->_id ) ;

                    // save workshops breedingTypes edge
                    $wbte = $this->container['workshopsBreedingsTypes']->insertEdge( $this->container['workshopsBreedingsTypes']->from['name'] . '/' . $params['breeding'] , $result->_id ) ;

                    // save workshops productionTypes edge
                    $wpte = $this->container['workshopsProductionsTypes']->insertEdge( $this->container['workshopsProductionsTypes']->from['name'] . '/' . $params['production'] , $result->_id ) ;

                    // save workshops waterOrigins edge
                    $wwoe = $this->container['workshopsWaterOrigins']->insertEdge( $this->container['workshopsWaterOrigins']->from['name'] . '/' . $params['water'] , $result->_id ) ;

                    return $this->success( $response , $this->get( NULL , NULL , [ 'owner' => $id , 'id' => $result->_key ] ) ) ;
                }
                else
                {
                    return $this->error( $response ) ;
                }
            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'    => NULL,
        'owner' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract(array_merge(self::ARGUMENTS_POST_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' put(' . $owner . ',' . $id . ')');
        }

        $params = $request->getParsedBody();

        try
        {
            $o = $this->owner->get( $owner , [ 'fields' => '_key' ] ) ;
            if( !$o )
            {
                return $this->formatError( $response ,'404' , [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'put(' . $owner . ',' . $id . ')' ] , NULL , 404 );
            }

            $item = [];

            $conditions =
            [
                'name'       => [ $params['name']       , 'required|max(70)' ],
                'breeding'   => [ $params['breeding']   , 'required|int|breeding' ] ,
                'production' => [ $params['production'] , 'required|int|production' ]
            ];

            if( isset( $params['identifier'] ) )
            {
                $item['identifier'] = (int) $params['identifier'] ;
                $conditions['identifier'] = [ $params['identifier'] , 'required|int|identifier' ] ;
            }

            if( isset( $params['name'] ) )
            {
                $item['name'] = $params['name'] ;
            }

            if( isset( $params['water'] ) )
            {
                $conditions['water'] = [ $params['water'] , 'required|int|water' ] ;
            }


            $validator = new WorkshopValidator( $this->container ) ;

            $validator->validate( $conditions ) ;

            if( $validator->passes() )
            {
                // check if breedingTypes productionTypes exists
                if( isset( $params['breeding'] ) && isset( $params['production'] ) )
                {
                    $check = $this->container['breedingsTypesProductionsTypesController']->exists( NULL , NULL , [ 'from' => $params['production'] , 'to' => $params['breeding'] ] ) ;

                    if( !$check )
                    {
                        return $this->error( $response , 'breedings types and productions types are not linked' , '400' );
                    }
                }

                $result = $this->model->update( $item , $id ) ;

                // update edges

                if( isset( $params['breeding'] ) && isset( $params['production'] ) )
                {
                    // save workshops breedingTypes edge
                    $existWBTE = $this->container['workshopsBreedingsTypes']->existEdge( $this->container['workshopsBreedingsTypes']->from['name'] . '/' . $params['breeding'] , $this->container['workshopsBreedingsTypes']->to['name'] . '/' . $id ) ;
                    if( !$existWBTE )
                    {
                        $this->container['workshopsBreedingsTypes']->delete( $this->container['workshopsBreedingsTypes']->to['name'] . '/' . $id , [ 'key' => '_to' ] ) ;
                        $wbte = $this->container['workshopsBreedingsTypes']->insertEdge( $this->container['workshopsBreedingsTypes']->from['name'] . '/' . $params['breeding'] , $this->container['workshopsBreedingsTypes']->to['name'] . '/' . $id ) ;
                    }

                    // save workshops productionTypes edge
                    $existWPTE = $this->container['workshopsProductionsTypes']->existEdge( $this->container['workshopsProductionsTypes']->from['name'] . '/' . $params['production'] , $this->container['workshopsBreedingsTypes']->to['name'] . '/' . $id ) ;
                    if( !$existWPTE )
                    {
                        $this->container['workshopsProductionsTypes']->delete( $this->container['workshopsProductionsTypes']->to['name'] . '/' . $id , [ 'key' => '_to' ] ) ;
                        $wpte = $this->container['workshopsProductionsTypes']->insertEdge( $this->container['workshopsProductionsTypes']->from['name'] . '/' . $params['production'] , $this->container['workshopsBreedingsTypes']->to['name'] . '/' . $id ) ;
                    }
                }

                if( isset( $params['water'] ) )
                {
                    // save workshops waterOrigins edge
                    $existWWOE = $this->container['workshopsWaterOrigins']->existEdge( $this->container['workshopsWaterOrigins']->from['name'] . '/' . $params['water'] , $this->container['workshopsWaterOrigins']->to['name'] . '/' . $id ) ;
                    if( !$existWWOE )
                    {
                        $this->container['workshopsWaterOrigins']->delete( $this->container['workshopsWaterOrigins']->to['name'] . '/' . $id , [ 'key' => '_to' ] ) ;
                        $wwoe = $this->container['workshopsWaterOrigins']->insertEdge( $this->container['workshopsWaterOrigins']->from['name'] . '/' . $params['water'] , $this->container['workshopsWaterOrigins']->to['name'] . '/' . $id ) ;
                    }
                }

                return $this->success( $response , $this->get( NULL , NULL , [ 'owner' => $owner , 'id' => $id ] ) ) ;

                //////
            }
            else
            {
                $errors = [] ;

                $err  = $validator->errors() ;
                $keys = $err->keys() ;

                foreach( $keys as $key )
                {
                    $errors[$key] = $err->first($key) ;
                }

                return $this->error( $response , $errors , "400" ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }
}
