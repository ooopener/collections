<?php

namespace com\ooopener\controllers\livestocks\observations;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Collections;
use com\ooopener\things\Observation;
use com\ooopener\things\Thing;
use com\ooopener\validations\ObservationValidator;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;

use DateTime;

use Exception;

class ObservationsController extends CollectionsController
{
    public function __construct( Container $container , Collections $model = NULL , string $path = NULL )
    {
        parent::__construct( $container , $model , $path );
    }


    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'startDate'     => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'endDate'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'image'         => [ 'filter' => Thing::FILTER_DEFAULT  ] ,

        'alternateName' => [ 'filter' =>  Thing::FILTER_TRANSLATE  ] ,
        'description'   => [ 'filter' =>  Thing::FILTER_TRANSLATE  ] ,
        'notes'         => [ 'filter' =>  Thing::FILTER_TRANSLATE  ] ,
        'text'          => [ 'filter' =>  Thing::FILTER_TRANSLATE  ] ,

        'about'          => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE  ] ,
        'subject'        => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE  ] ,

        'additionalType' => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE  ] ,
        'eventStatus'    => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE  ] ,

        'actor'         => [ 'filter' =>  Thing::FILTER_EDGE ] ,
        'attendee'      => [ 'filter' =>  Thing::FILTER_EDGE ] ,

        'authority'     => [ 'filter' =>  Observation::FILTER_AUTHORITY ] ,

        'location'      => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,

        'owner'         => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,

        'review'        => [ 'filter' => Thing::FILTER_DEFAULT ]
    ];

    public function allLivestock( Request $request = NULL , Response $response = NULL , array $args = [] )
    {

        $args['facets'] = [ 'livestock' => $args['id'] ] ;

        return parent::all( $request , $response , $args );
    }

    public function allWorkshop( Request $request = NULL , Response $response = NULL , array $args = [] )
    {

        $args['facets'] = [ 'workshop' => $args['id'] ] ;

        return parent::all( $request , $response , $args );
    }

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Observation::FILTER_AUTHORITY :
                    {
                        // get authority
                        $authority = $this->container->observationAuthorityController->all( NULL , NULL , [ 'id' => (string) $init->id ] ) ;

                        $init->{ $key } = $authority ;
                        break;
                    }
                    case Observation::FILTER_FIRST_OWNER :
                    {
                        if( property_exists( $init , Observation::FILTER_FIRST_OWNER) )
                        {
                            $init->{ $key } = $this->container->usersController->create( (object)$init->{ Observation::FILTER_FIRST_OWNER } ) ;
                        }
                        break;
                    }
                    case Observation::FILTER_OWNER :
                    {
                        if( property_exists( $init , Observation::FILTER_OWNER) )
                        {
                            $init->{ $key } = $this->container->usersController->create( (object)$init->{ Observation::FILTER_OWNER } ) ;
                        }
                        break;
                    }
                }
            }
        }
        return $init ;
    }

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        // check observation
        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'delete(' . $id . ')' ] , NULL , 404 );
        }

        //// remove children

        // delete actors edge
        $actors = $this->container->observationActorsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // delete attendees edge
        $attendees = $this->container->observationAttendeesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // delete authority edge
        $authority = $this->container->observationAuthorityController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // delete livestocks edge
        $livestocks = $this->container->observationLivestocksController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // delete workshops edge
        $workshops = $this->container->observationWorkshopsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // delete location edge
        $location = $this->container->observationPlacesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // delete status edge
        $status = $this->container->observationObservationsStatusController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // delete types edge
        $types = $this->container->observationObservationsTypesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // delete owner edge
        $owner = $this->container->observationOwnersController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // delete people edge
        $people = $this->container->observationPeopleController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;


        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id'    => NULL
    ] ;

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' );
        }

        $old = $this->model->get( $id , [ 'fields' => 'endDate,startDate' ] ) ;
        if( !$old )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        // check
        $params = $request->getParsedBody();

        $location = NULL ;
        $status = NULL ;

        $testEndDate   = $old->endDate ;
        $testStartDate = $old->startDate ;

        $item = [] ;
        $conditions = [] ;

        if( isset( $params['review'] ) )
        {
            try
            {
                $item['review'] = json_decode( $params['review'] , true ) ;
            }
            catch(Exception $e)
            {
                return $this->formatError( $response , '400', [ 'patch(' . $id . ')' ] , NULL , 400 );
            }
        }

        if( isset( $params['endDate'] ) )
        {
            if( $params['endDate'] != '' )
            {
                $item['endDate'] = $testEndDate = $params['endDate'] ;
                $conditions['endDate'] = [ $params['endDate'] , 'datetime' ] ;
            }
            else
            {
                $item['endDate'] = $testEndDate = NULL ;
            }
        }

        if( isset( $params['startDate'] ) )
        {
            $item['startDate'] = $testStartDate = $params['startDate'] ;
            $conditions['startDate'] = [ $params['startDate'] , 'datetime' ] ;
        }

        if( isset( $params['eventStatus'] ) )
        {
            if( $params['eventStatus'] != '' )
            {
                $status = $params['eventStatus'] ;
                $conditions['eventStatus'] = [ $params['eventStatus'] , 'required|eventStatus' ] ;
            }
        }

        if( isset( $params['location'] ) )
        {
            if( $params['location'] != '' )
            {
                $location = $params['location'] ;
                $conditions['location'] = [ $params['location'] , 'location' ] ;
            }
            else
            {
                $location = FALSE ;
            }
        }

        /// check dates

        if( $testStartDate && $testEndDate )
        {
            $after = (boolean) (strtotime($testStartDate) < strtotime($testEndDate)) ;
            $conditions['after']  = [ $after , 'true' ] ;
        }

        ////// validator

        $validator = new ObservationValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $status !== NULL )
                {
                    $edge = $this->container->observationObservationsStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( $location !== NULL )
                {
                    $locEdge = $this->container->observationPlaces ;

                    $idFrom = $locEdge->from['name'] . '/' . $location ;

                    if( !$locEdge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $locEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $locEdge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( array_key_exists( 'review' , $item ) )
                {
                    $result = $this->model->update( [ 'review' => '' ] , $id );
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->create( $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id'    => NULL ,
        'owner' => NULL
    ] ;

    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' );
        }

        // check workshop
        $w = $this->container->workshopsController->get( NULL , NULL , [ 'id' => $id , 'skin' => 'full' ] ) ;
        if( !$w )
        {
            return $this->formatError( $response , '404', [ 'post(' . $id . ')' ] , NULL , 404 );
        }

        // check livestock
        $o = $this->container->livestocksWorkshops->getEdge( (string)$w->id ) ;
        if( !$o && !property_exists( 'edge' , $o ) && !is_array( $o->edge ) && ( count( $o->edge ) != 1 ) )
        {
            return $this->formatError( $response , '404', [ 'post(' . $id . ')' ] , NULL , 404 );
        }
        $o = (object)$o->edge[0] ;

        $params = $request->getParsedBody();

        $type = NULL ;

        $item = [];
        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = 'livestocks/' . $this->path ;

        if( isset( $params['additionalType'] ) )
        {
            $type = $params['additionalType'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $this->filterLanguages( $params['alternateName'] ) ;
        }

        if( isset( $params['review'] ) )
        {
            try
            {
                $item['review'] = json_decode( $params['review'] , true ) ;
            }
            catch(Exception $e)
            {
                return $this->formatError( $response , '400', [ 'post(' . $id . ')' ] , NULL , 400 );
            }
        }
        else
        {
            try
            {
                $item['review'] = [ 'about' => json_decode( json_encode( $w ) , true ) ] ;
            }
            catch(Exception $e)
            {
                return $this->formatError( $response , '400', [ 'post(' . $id . ')' ] , NULL , 400 );
            }
        }

        // get open status
        $status = $this->container->observationsStatus->get( 'open' , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;
        if( $status )
        {
            $status = $status->_key ;
        }

        $conditions =
        [
            'eventStatus'    => [ $status  , 'required'                ] ,
            'additionalType' => [ $type    , 'required|additionalType' ]
        ] ;

        ////// validator

        $validator = new ObservationValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {

                $user = $this->container->auth ;

                $item['firstOwner'] = (int)$user->id ;

                ///// TODO check if user is a veterinarian and authorized to post observation

                $new = $this->model->insert( $item );

                ///// set name

                // get type name
                $observationTpe = $this->container->observationsTypesController->get( NULL , NULL , [ 'id' => $type , 'fields' => 'name' ] ) ;

                $date = new DateTime();
                $name = strtoupper( str_replace( '/' , '-' , $observationTpe->alternateName ) ) . '-' . $date->format( 'Ymd' ) . '-' . $new->_key ;

                $update = $this->model->update( [ 'name' => $name ] , $new->_key ) ;

                ///// edges

                // livestocks edge
                $edge = $this->container->observationLivestocks ;

                // add edge
                $edge->insertEdge( 'livestocks/' . $o->id , $new->_id ) ;

                // workshops edge
                $edge = $this->container->observationWorkshops ;

                // add edge
                $edge->insertEdge( 'workshops/' . $w->id , $new->_id ) ;


                // status edge
                if( $status )
                {
                    $edge = $this->container->observationObservationsStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // add edge
                    $edge->insertEdge( $idFrom , $new->_id ) ;
                }

                // type edge
                if( $type )
                {
                    $edge = $this->container->observationObservationsTypes ;

                    $idFrom = $edge->from['name'] . '/' . $type ;

                    // add edge
                    $edge->insertEdge( $idFrom , $new->_id ) ;
                }

                // save owner
                $edge = $this->container->observationOwners ;

                $idFrom = $edge->from['name'] . '/' . $user->id ;

                // add edge
                $edge->insertEdge( $idFrom , $new->_id ) ;

                // location edge
                if( property_exists( $o , 'organization' ) && array_key_exists( 'location' , $o->organization ) && $o->organization['location'] != null )
                {
                    $edge = $this->container->observationPlaces ;

                    // add edge
                    $edge->insertEdge( 'places/' . $o->organization['location']['id'] , $new->_id ) ;
                }

                if( $new )
                {
                    return $this->success( $response , $this->model->get( $new->_key , [ 'queryFields' => $this->getFields() ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id'       => NULL ,
        'owner'    => NULL ,
        'workshop' => NULL
    ] ;

    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $owner . ',' . $workshop . ',' . $id . ')' );
        }

        // check
        $params = $request->getParsedBody();

        $status = NULL ;

        $item = [];

        if( isset( $params['endDate'] ) && !empty( $params['endDate'] ) )
        {
            $item['endDate'] = $params['endDate'] ;
        }

        if( isset( $params['startDate'] ) && !empty( $params['startDate'] ) )
        {
            $item['startDate'] = $params['startDate'] ;
        }

        if( isset( $params['eventStatus'] ) )
        {
            $status = $params['eventStatus'] ;
        }

        if( isset( $params['review'] ) )
        {
            try
            {
                $item['review'] = json_decode( $params['review'] , true ) ;
            }
            catch(Exception $e)
            {
                return $this->formatError( $response , '400', [ 'put(' . $id . ')' ] , NULL , 400 );
            }
        }

        $conditions =
        [
            'name'         => [ $params['name']        , 'required|max(70)'    ] ,
            'endDate'      => [ $params['endDate']     , 'simpleDate'          ] ,
            'startDate'    => [ $params['startDate']   , 'simpleDate' ] ,
            'status'       => [ $status                , 'required|status'     ]
        ] ;

        if( isset( $params['startDate'] ) && isset( $params['endDate'] ) )
        {
            $after = (boolean) (strtotime($params['startDate']) <= strtotime($params['endDate'])) ;
            $conditions['after']  = [ $after , 'after' ] ;
        }

        ////// validator

        $validator = new ObservationValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                // check livestock
                $o = $this->container->livestocks->get( $owner , [ 'fields' => '_id' ] ) ;
                if( !$o )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $owner . ',' . $workshop . ',' . $id . ')' ] , NULL , 404 );
                }

                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $owner . ',' . $workshop . ',' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $status )
                {
                    $edge = $this->container->observationObservationsStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->create( $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $owner . ',' . $workshop . ',' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }
}
