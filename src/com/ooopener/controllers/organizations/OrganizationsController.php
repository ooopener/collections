<?php

namespace com\ooopener\controllers\organizations;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\helpers\Status;
use com\ooopener\validations\OrganizationValidator;

use Exception ;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use core\Maths ;

use com\ooopener\things\GeoCoordinates;
use com\ooopener\models\Organizations;
use com\ooopener\things\Organization;
use com\ooopener\things\Thing;

/**
 * The organization collection controller.
 */
class OrganizationsController extends CollectionsController
{
    /**
     * Creates a new OrganizationsController instance.
     *
     * @param Container $container
     * @param Organizations $model
     * @param string $path
     */
    public function __construct( Container $container , Organizations $model = NULL , $path = 'organizations' )
    {
        parent::__construct( $container , $model , $path );
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="OrganizationList",
     *     description="An organization such as a school, NGO, corporation, club, etc",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="slogan",ref="#/components/schemas/text"),
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="logo",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="additionalType",ref="#/components/schemas/Thesaurus"),
     *
     *     @OA\Property(property="location",ref="#/components/schemas/PlaceList"),
     *
     *     @OA\Property(property="taxID",type="string"),
     *     @OA\Property(property="vatID",type="string"),
     * )
     *
     * @OA\Schema(
     *     schema="Organization",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/OrganizationList")},
     *     @OA\Property(property="address",ref="#/components/schemas/PostalAddress"),
     *     @OA\Property(property="dissolutionDate",type="string",format="date"),
     *     @OA\Property(property="foundingDate",type="string",format="date"),
     *     @OA\Property(property="foundingLocation",ref="#/components/schemas/PlaceList"),
     *     @OA\Property(property="legalForm",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="ape",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="numbers",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationNumber")),
     *     @OA\Property(property="email",type="array",items=@OA\Items(ref="#/components/schemas/Email")),
     *     @OA\Property(property="telephone",type="array",items=@OA\Items(ref="#/components/schemas/PhoneNumber")),
     *     @OA\Property(property="employees",type="array",items=@OA\Items(ref="#/components/schemas/PersonList")),
     *     @OA\Property(property="members",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="providers",type="array",items=@OA\Items(ref="#/components/schemas/agent")),
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="ows",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList")),
     *     @OA\Property(property="subOrganization",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="parentOrganization",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="department",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="memberOf",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website"))
     * )
     *
     * @OA\Response(
     *     response="organizationResponse",
     *     description="Result of the organization",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Organization")
     *     )
     * )
     *
     * @OA\Response(
     *     response="organizationListResponse",
     *     description="Result list of organizations",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/OrganizationList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'identifier'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'legalName'     => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'logo'          => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'audio'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'additionalType'     => [ 'filter' => Thing::FILTER_EDGE_SINGLE      ] ,
        'alternateName'      => [ 'filter' => Thing::FILTER_TRANSLATE        ] ,
        'description'        => [ 'filter' => Thing::FILTER_TRANSLATE        ] ,
        'slogan'             => [ 'filter' => Thing::FILTER_TRANSLATE        ] ,
        'text'               => [ 'filter' => Thing::FILTER_TRANSLATE        ] ,
        'location'           => [ 'filter' => Thing::FILTER_EDGE_SINGLE      ] ,

        'taxID'              => [ 'filter' => Thing::FILTER_DEFAULT          ] ,
        'vatID'              => [ 'filter' => Thing::FILTER_DEFAULT          ] ,

        'numAudios'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'numbers'            => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'full' ] ] ,

        'address'            => [ 'filter' => Thing::FILTER_DEFAULT            ] ,
        'email'              => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'normal' ] ] ,
        'telephone'          => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'normal' ] ] ,

        'notes'              => [ 'filter' => Thing::FILTER_TRANSLATE        , 'skins' => [ 'full' , 'compact' ] ] ,
        'department'         => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'dissolutionDate'    => [ 'filter' => Thing::FILTER_DATETIME         , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'founder'            => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'foundingDate'       => [ 'filter' => Thing::FILTER_DATETIME         , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'foundingLocation'   => [ 'filter' => Thing::FILTER_EDGE_SINGLE      , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'legalForm'          => [ 'filter' => Thing::FILTER_EDGE_SINGLE      , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'ape'                => [ 'filter' => Thing::FILTER_EDGE_SINGLE      , 'skins' => [ 'full' , 'extend' , 'compact' ] ] ,
        'employees'          => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'keywords'           => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'members'            => [ 'filter' => Organization::FILTER_MEMBERS   , 'skins' => [ 'full' , 'compact' ] ] ,
        'audios'             => [ 'filter' => Thing::FILTER_JOIN_ARRAY       , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'             => [ 'filter' => Thing::FILTER_JOIN_ARRAY       , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'             => [ 'filter' => Thing::FILTER_JOIN_ARRAY       , 'skins' => [ 'full' , 'videos' ] ] ,
        'providers'          => [ 'filter' => Organization::FILTER_PROVIDERS , 'skins' => [ 'full' , 'compact' ] ] ,
        'owns'               => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'subOrganization'    => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'parentOrganization' => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ] ,
        'memberOf'           => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ],
        //'sponsor'                   => [ 'filter' => Organization::FILTER_SPONSOR   , 'skins' => [ 'full' ] ]
        'websites'           => [ 'filter' => Thing::FILTER_EDGE             , 'skins' => [ 'full' , 'compact' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Organization::FILTER_EMPLOYEES :
                    {
                        if( property_exists( $init , Organization::FILTER_EMPLOYEES ) && is_array( $init->{ Organization::FILTER_EMPLOYEES } ) && count( $init->{ Organization::FILTER_EMPLOYEES } ) > 0 )
                        {
                            $employees = [] ;
                            foreach( $init->{ Organization::FILTER_EMPLOYEES } as $item )
                            {
                                array_push( $employees , $this->container->peopleController->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $employees ;
                        }
                        break ;
                    }
                    case Organization::FILTER_FOUNDER :
                    {
                        if( property_exists( $init , Organization::FILTER_FOUNDER ) && is_array( $init->{ Organization::FILTER_FOUNDER } ) && count( $init->{ Organization::FILTER_FOUNDER } ) > 0 )
                        {
                            $founder = [] ;
                            foreach( $init->{ Organization::FILTER_FOUNDER } as $item )
                            {
                                array_push( $founder , $this->container->peopleController->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $founder ;
                        }
                        break ;
                    }
                    case Organization::FILTER_FOUNDING_LOCATION :
                    {
                        // get image
                        if( property_exists( $init , Organization::FILTER_FOUNDING_LOCATION ) && $init->{ Organization::FILTER_FOUNDING_LOCATION } != NULL )
                        {
                            $init->{ $key } = $this->container->placesController->create( (object) $init->{ Organization::FILTER_FOUNDING_LOCATION } , $lang ) ;
                        }
                        break ;
                    }
                    case Organization::FILTER_LOCATION :
                    {
                        // get image
                        if( property_exists( $init , Organization::FILTER_LOCATION ) && $init->{ Organization::FILTER_LOCATION } != NULL )
                        {
                            $init->{ $key } = $this->container->placesController->create( (object) $init->{ Organization::FILTER_LOCATION } , $lang ) ;
                        }
                        break ;
                    }
                    case Organization::FILTER_MEMBER_OF :
                    {
                        if( property_exists( $init , Organization::FILTER_MEMBER_OF ) && is_array( $init->{ Organization::FILTER_MEMBER_OF } ) && count( $init->{ Organization::FILTER_MEMBER_OF } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Organization::FILTER_MEMBER_OF } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Organization::FILTER_MEMBERS :
                    {
                        if( property_exists( $init , Organization::FILTER_MEMBERS ) )
                        {
                            // get members
                            $members = $this->container->organizationMembersController->all( NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ] ) ;

                            $init->{ $key } = $members ;
                        }
                        break;
                    }
                    case Organization::FILTER_OWNS :
                    {
                        if( property_exists( $init , Organization::FILTER_OWNS ) && is_array( $init->{ Organization::FILTER_OWNS } ) && count( $init->{ Organization::FILTER_OWNS } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Organization::FILTER_OWNS } as $item )
                            {
                                array_push( $sub , $this->container->conceptualObjectsController->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Organization::FILTER_PARENT_ORGANIZATION :
                    {
                        if( property_exists( $init , Organization::FILTER_PARENT_ORGANIZATION ) && is_array( $init->{ Organization::FILTER_PARENT_ORGANIZATION } ) && count( $init->{ Organization::FILTER_PARENT_ORGANIZATION } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Organization::FILTER_PARENT_ORGANIZATION } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Organization::FILTER_PROVIDERS :
                    {
                        if( property_exists( $init , Organization::FILTER_PROVIDERS ) )
                        {
                            // get providers
                            $providers = $this->container->organizationProvidersController->all( NULL , NULL , [ 'id' => (string) $init->id , 'lang' => $lang ] ) ;

                            $init->{ $key } = $providers ;
                        }
                        break;
                    }
                    case Organization::FILTER_SUB_ORGANIZATION :
                    {
                        if( property_exists( $init , Organization::FILTER_SUB_ORGANIZATION ) && is_array( $init->{ Organization::FILTER_SUB_ORGANIZATION } ) && count( $init->{ Organization::FILTER_SUB_ORGANIZATION } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Organization::FILTER_SUB_ORGANIZATION } as $item )
                            {
                                array_push( $sub , $this->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    ///////////////////////////


    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
        }

        //// remove all linked resources

        // remove keywords
        $keywords = $this->container->organizationKeywordsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove email
        $emails = $this->container->organizationEmailsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove telephone
        $telephone = $this->container->organizationPhoneNumbersController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove employess
        $employees = $this->container->organizationEmployeesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove founder
        $founder = $this->container->organizationFounderController->deleteAll( NULL ,NULL , [ 'owner' => $id ] ) ;

        // remove founding location
        $foundingLocation = $this->container->organizationFoundingLocationController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove legalForm
        $legalForm = $this->container->organizationLegalFormController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove members
        $members = $this->container->organizationMembersController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove memberOf
        $memberOf = $this->container->organizationMembersController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE , 'type' => 'organization' ] ) ;

        // remove ape
        $ape = $this->container->organizationApeController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove numbers
        $numbers = $this->container->organizationNumbersController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove organizersOf
        $organizersOf = $this->container->eventOrganizersController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE , 'type' => 'organization' ] ) ;

        // remove owns
        $owns = $this->container->conceptualObjectProductionsController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE , 'type' => 'organization' ] ) ;

        // remove providers
        $providers = $this->container->organizationProvidersController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove providersOf
        $providersOf = $this->container->organizationProvidersController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE , 'type' => 'organization' ] ) ;

        // remove organization place
        $place = $this->container->organizationPlacesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove organization type
        $type = $this->container->organizationOrganizationsTypesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove sub organization
        $subOrganization = $this->container->organizationSubOrganizationsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove parent organization
        $parentOrganization = $this->container->organizationSubOrganizationsController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE ] ) ;

        // remove websites
        $websites = $this->container->organizationWebsitesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove course discover
        if( isset( $this->container->courseDiscoverController ) )
        {
            $discover = $this->container->courseDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        // remove stage discover
        if( isset( $this->container->stageDiscoverController ) )
        {
            $discover = $this->container->stageDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postOrganization",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             required={"name","additionalType"},
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="identifier",type="string",description="The identifier"),
     *             @OA\Property(property="legalName",type="string",description="The legal name"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType = NULL ;
        $location = NULL ;

        $item = [];

        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = (string)$params['additionalType'] ;
        }

        if( isset( $params['identifier'] ) )
        {
            if( $params['identifier'] == '' ) $params['identifier'] = NULL ;

            $item['identifier'] = $params['identifier'] ;
        }

        if( isset( $params['location'] ) )
        {
            $location = $params['location'] != '' ? (string)$params['location'] : NULL  ;
        }

        $conditions =
        [
            'name'            => [ $params['name']       , 'required|max(70)'        ] ,
            'additionalType'  => [ $additionalType       , 'required|additionalType' ] ,
            'location'        => [ $location             , 'location'                ]
        ] ;

        if( isset( $params['legalName'] ) )
        {
            $item['legalName'] = $params['legalName'] ;
            $conditions['legalName'] = [ $params['legalName'] , 'max(70)' ] ;
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new OrganizationValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $result = $this->model->insert( $item );

                // add additionalType edge
                $addTypeEdge = $this->container->organizationOrganizationsTypes ;
                $ate = $addTypeEdge->insertEdge( $addTypeEdge->from['name'] . '/' . $additionalType , $result->_id ) ;

                // add location edge
                if( $location != NULL )
                {
                    $locEdge = $this->container->organizationPlaces ;
                    $iloc = $locEdge->insertEdge( $locEdge->from['name'] . '/' . $location , $result->_id ) ;
                }

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchOrganization",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="identifier",type="string",description="The identifier"),
     *             @OA\Property(property="legalName",type="string",description="The legal name"),
     *
     *             @OA\Property(property="legalForm",type="integer",description="The legal form ID"),
     *             @OA\Property(property="ape",type="integer",description="The APE ID"),
     *
     *             @OA\Property(property="taxID",type="string",description="The tax ID"),
     *             @OA\Property(property="vatID",type="string",description="The vat ID"),
     *             @OA\Property(property="dissolutionDate",type="string",description="The dissolution date",format="date"),
     *             @OA\Property(property="foundingDate",type="string",description="The founding date",format="date"),
     *             @OA\Property(property="foundingLocation",type="integer",description="The place ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType   = NULL ;
        $foundingLocation = NULL ;
        $legalForm        = NULL ;
        $location         = NULL ;
        $ape              = NULL ;

        $item = [];
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(70)' ] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
            $conditions['additionalType'] = [ $params['additionalType'] , 'required|additionalType' ] ;
        }

        if( isset( $params['identifier'] ) )
        {
            $item['identifier'] = $params['identifier'] ;
        }

        if( isset( $params['location'] ) )
        {
            if( $params['location'] != '' )
            {
                $location = $params['location'] ;
                $conditions['location'] = [ $params['location'] , 'location' ] ;
            }
            else
            {
                $location = FALSE ;
            }
        }

        // legal

        if( isset( $params['legalName'] ) )
        {
            $item['legalName'] = $params['legalName'] ;
            $conditions['legalName'] = [ $params['legalName'] , 'max(70)' ] ;
        }

        if( isset( $params['legalForm'] ) )
        {
            if( $params['legalForm'] != '' )
            {
                $legalForm = $params['legalForm'] ;
                $conditions['legalForm'] = [ $params['legalForm'] , 'legalForm' ] ;
            }
            else
            {
                $legalForm = FALSE ;
            }
        }

        if( isset( $params['ape'] ) )
        {
            if( $params['ape'] != '' )
            {
                $ape = $params['ape'] ;
                $conditions['ape'] = [ $params['ape'] , 'ape' ] ;
            }
            else
            {
                $ape = FALSE ;
            }
        }

        if( isset( $params['taxID'] ) )
        {
            $item['taxID'] = $params['taxID'] ;
        }

        if( isset( $params['vatID'] ) )
        {
            $item['vatID'] = $params['vatID'] ;
        }

        if( isset( $params['dissolutionDate'] ) )
        {
            if( $params['dissolutionDate'] != '' )
            {
                $item['dissolutionDate'] = $params['dissolutionDate'] ;
                $conditions['dissolutionDate'] = [ $params['dissolutionDate'] , 'date' ] ;
            }
            else
            {
                $item['dissolutionDate'] = null ;
            }
        }

        if( isset( $params['foundingDate'] ) )
        {
            if( $params['foundingDate'] != '' )
            {
                $item['foundingDate'] = $params['foundingDate'] ;
                $conditions['foundingDate'] = [ $params['foundingDate'] , 'date' ] ;
            }
            else
            {
                $item['foundingDate'] = null ;
            }
        }

        if( isset( $params['foundingLocation'] ) )
        {
            if( $params['foundingLocation'] != '' )
            {
                $foundingLocation = $params['foundingLocation'] ;
                $conditions['foundingLocation'] = [ $params['foundingLocation'] , 'location' ] ;
            }
            else
            {
                $foundingLocation = FALSE ;
            }
        }


        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        // check if there is at least one param
        if( empty( $item ) && $additionalType === NULL && $legalForm === NULL && $location === NULL && $foundingLocation === NULL && $ape === NULL )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        // check if resource exists
        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        ////// validator

        $validator = new OrganizationValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $additionalType !== NULL )
                {
                    $addTypeEdge = $this->container->organizationOrganizationsTypes ;

                    $idFrom = $addTypeEdge->from['name'] . '/' . $additionalType ;

                    if( !$addTypeEdge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $addTypeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $addTypeEdge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( $foundingLocation !== NULL )
                {
                    $fLocEdge = $this->container->organizationFoundingLocation ;

                    if( $foundingLocation === FALSE )
                    {
                        // delete all edges
                        $fLocEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                    else
                    {
                        $idFrom = $fLocEdge->from['name'] . '/' . $foundingLocation ;

                        if( !$fLocEdge->existEdge( $idFrom , $idTo ) )
                        {
                            // delete all edges to be sure
                            $fLocEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                            // add edge
                            $fLocEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                if( $legalForm !== NULL )
                {
                    $legalFormEdge = $this->container->organizationLegalForm ;

                    if( $legalForm === FALSE )
                    {
                        // delete all edges
                        $legalFormEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                    else
                    {
                        $idFrom = $legalFormEdge->from['name'] . '/' . $legalForm ;

                        if( !$legalFormEdge->existEdge( $idFrom , $idTo ) )
                        {
                            // delete all edges to be sure
                            $legalFormEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                            // add edge
                            $legalFormEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                if( $location !== NULL )
                {
                    $locEdge = $this->container->organizationPlaces ;

                    if( $location === FALSE )
                    {
                        // delete all edges
                        $locEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                    else
                    {
                        $idFrom = $locEdge->from['name'] . '/' . $location ;

                        if( !$locEdge->existEdge( $idFrom , $idTo ) )
                        {
                            // delete all edges to be sure
                            $locEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                            // add edge
                            $locEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                if( $ape !== NULL )
                {
                    $apeEdge = $this->container->organizationApe ;

                    if( $ape === FALSE )
                    {
                        // delete all edges
                        $apeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    }
                    else
                    {
                        $idFrom = $apeEdge->from['name'] . '/' . $ape ;

                        if( !$apeEdge->existEdge( $idFrom , $idTo ) )
                        {
                            // delete all edges to be sure
                            $apeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                            // add edge
                            $apeEdge->insertEdge( $idFrom , $idTo ) ;
                        }
                    }
                }

                $result = $this->model->update( $item , $id ) ;

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'extend' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putOrganization",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="additionalType",type="integer",description="The id of the additionalType"),
     *             required={"name","additionalType"},
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="identifier",type="string",description="The identifier"),
     *             @OA\Property(property="legalName",type="string",description="The legal name"),
     *
     *             @OA\Property(property="taxID",type="string",description="The tax ID"),
     *             @OA\Property(property="vatID",type="string",description="The vat ID"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $additionalType = NULL ;
        $location = NULL ;

        $item = [];

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['legalName'] ) )
        {
            $item['legalName'] = $params['legalName'] ;
        }

        if( isset( $params['additionalType'] ) )
        {
            $additionalType = $params['additionalType'] ;
        }

        if( isset( $params['identifier'] ) )
        {
            if( $params['identifier'] == '' ) $params['identifier'] = NULL ;

            $item['identifier'] = $params['identifier'] ;
        }

        if( isset( $params['taxID'] ) )
        {
            $item['taxID'] = $params['taxID'] ;
        }

        if( isset( $params['vatID'] ) )
        {
            $item['vatID'] = $params['vatID'] ;
        }

        $conditions =
        [
            'name'           => [ $params['name']       , 'required|max(70)'        ] ,
            'legalName'      => [ $params['legalName']  , 'max(70)'                 ] ,
            'additionalType' => [ $additionalType       , 'required|additionalType' ]
        ] ;

        if( isset( $params['location'] ) && $params['location'] != '' )
        {
            $location = $params[ 'location' ] ;
            $conditions[ 'location' ] = [ $params[ 'location' ] , 'location' ] ;
        }
        else
        {
            $location = FALSE ;
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new OrganizationValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                // update edges
                $addTypeEdge = $this->container->organizationOrganizationsTypes ;

                $idFrom = $addTypeEdge->from['name'] . '/' . $additionalType ;
                $idTo = $this->model->table . '/' . $id ;

                if( !$addTypeEdge->existEdge( $idFrom , $idTo ) )
                {
                    // delete all edges to be sure
                    $addTypeEdge->delete( $idTo , [ 'key' => '_to' ] ) ;
                    // add edge
                    $addTypeEdge->insertEdge( $idFrom , $idTo ) ;
                }

                $this->updateEdge( $location , $idTo , $this->container->organizationPlaces ) ;

                $result = $this->model->update( $item , $id ) ;

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields() ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * Outputs all the elements.
     *
     * @param Request $request
     * @param Response $response
     * @param array $items
     * @param array $params
     * @param array $options
     *
     * @return Response|array
     */
    protected function outputAll( Request $request = NULL, Response $response = NULL , $items = NULL , $params = NULL , array $options = NULL )
    {
        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        $format = $this->container['format'] ;
        $lang      = NULL ;
        $latitude  = NULL ;
        $longitude = NULL ;
        $lang      = NULL ;
        $marker    = FALSE ;
        $provider  = NULL ;
        $skin      = NULL ;

        if( !isset($params) )
        {
            $params = isset( $request ) ? $request->getQueryParams() : []  ;
        }

        // ----- lang

        if( !empty($params['lang']) )
        {
            if( in_array( strtolower($params['lang']) , $api['languages'] ) )
            {
                $params['lang'] = $lang = strtolower($params['lang']) ;
            }
            else if( strtolower($params['lang']) == 'all' )
            {
                $lang = NULL ;
            }
        }

        // ----- skin

        if( !isset($skin) && array_key_exists( 'skin_default' ,$set ) )
        {
            if( array_key_exists( 'skin_all', $set)  )
            {
                $skin = $set['skin_all'] ;
            }
            else if( array_key_exists( 'skin_default', $set)  )
            {
                $skin = $set['skin_default'] ;
            }
        }

        if( !empty($params['skin']) )
        {
            if( in_array( $params['skin'] , $set['skins'] ) )
            {
                $params['skin'] = $skin = $params['skin'] ;
            }
        }

        if( $skin == 'main' || !in_array( $skin , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        if( $format == 'map' )
        {
            $skin = 'map' ;
        }

        if( $items )
        {
            foreach( $items as $key => $value )
            {
                $items[$key] = $this->create( $value , $lang , $skin , $params ) ;
            }
        }

        if( $response )
        {
            switch( $format )
            {
                case "map" :
                {
                    $app = $this->container->settings['application'] ;
                    $map = $this->container->settings['map'] ;

                    // ----- marker

                    if( isset($params['marker']) )
                    {
                        if( $params['marker'] == 'true' || $params['marker'] == 'TRUE' )
                        {
                            $params['marker'] = $marker = TRUE ;
                        }
                    }

                    // ----- provider

                    $provider = $map['provider_default'] ;
                    if( isset($params['provider']) && !empty($params['provider']) )
                    {
                        if( in_array( $params['provider'] , $map['providers'] ) )
                        {
                            $params['provider'] = $provider = $params['provider'] ;
                        }
                    }

                    // ----- latitude and longitude

                    if( is_array($options)  )
                    {
                        if( array_key_exists('latitude',$options) )
                        {
                            $latitude = $options['latitude'] ;
                            unset($options['latitude']) ;
                        }

                        if( array_key_exists('longitude',$options) )
                        {
                            $longitude = $options['longitude'] ;
                            unset($options['longitude']) ;
                        }
                    }

                    $center = $this->getCenter( $items ) ;

                    if( !isset($latitude) && !isset($longitude) )
                    {
                        if( $center )
                        {
                            $latitude  = $center->latitude ;
                            $longitude = $center->longitude ;
                        }
                        else
                        {
                            $latitude  = $api['latitude_default'] ;
                            $longitude = $api['longitude_default'] ;
                        }
                    }

                    $zoom = $this->getMapZoomLevel
                    (
                        $this->getMaximum( $center , $items ) , $map['zoom_default']
                    );

                    $settings =
                    [
                        'api'         => $this->container->settings['app']['url'] ,
                        'analytics'   => (bool) $this->container->settings['useAnalytics'] ,
                        'extra'       => TRUE , // indicates if the extra mode is enabled (gds application only)
                        'interactive' => TRUE ,
                        'latitude'    => $latitude,
                        'longitude'   => $longitude,
                        'distance'    => isset( $options['distance'] ) ? $options['distance'] : 0 ,
                        'marker'      => $marker,
                        'organizations'      => $items,
                        'provider'    => $provider,
                        'version'     => $this->container->settings['version'],
                        'zoom'        => $zoom
                    ] ;

                    if( isset($options) )
                    {
                        $settings = array_merge( $settings , $options ) ;
                    }

                    return $this->render( $response , $map['template'] , $settings );
                }
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response,
                        $items,
                        $this->getFullPath( $params ) ,
                        is_array($items) ? count($items) : NULL,
                        $options
                    );
                }
            }
        }

        return $items ;
    }


    /**
     * Determinates the center position of all passed-in organizations.
     *
     * @param $organizations
     *
     * @return GeoCoordinates|FALSE
     */
    private function getCenter( $organizations )
    {
        if ( !is_array($organizations) )
        {
            return FALSE;
        }

        $count = count($organizations);

        $X = 0.0;
        $Y = 0.0;
        $Z = 0.0;

        foreach( $organizations as $organization )
        {
            $lat = deg2rad( $organization->geo->latitude ) ;
            $lon = deg2rad( $organization->geo->longitude );

            $a = cos($lat) * cos($lon);
            $b = cos($lat) * sin($lon);
            $c = sin($lat);

            $X += $a;
            $Y += $b;
            $Z += $c;
        }

        $X /= $count;
        $Y /= $count;
        $Z /= $count;

        $lon = atan2($Y, $X);
        $hyp = sqrt($X * $X + $Y * $Y);
        $lat = atan2($Z, $hyp);

        $geo = new GeoCoordinates() ;

        $geo->latitude  = rad2deg($lat) ;
        $geo->longitude = rad2deg($lon) ;

        return $geo ;
    }


    /**
     * Determinates the maximum organization distance.
     *
     * @param $center
     * @param $organizations
     *
     * @return float
     */
    private function getMaximum( $center , $organizations )
    {
        if ( !is_array($organizations) )
        {
            return FALSE;
        }

        $distances = [0];

        foreach( $organizations as $organization )
        {
            $distances[] = Maths::haversine( $center->latitude , $center->longitude , $organization->geo->latitude , $organization->geo->longitude ) ;
        }

        return max( $distances ) ;
    }

    /**
     * Indicates the zoom level with a specific distance.
     *
     * @param $distance
     * @param $defaultZoom
     *
     * @return integer
     */
    private function getMapZoomLevel( $distance , $defaultZoom = 0 )
    {
        $max = $distance / 1000 ; // km

        $zoom = $defaultZoom ;

        if( $max < 50 )
        {
            $zoom = 10 ;
        }
        else if( ($max > 50) && ($max <= 200) )
        {
            $zoom = 9 ;
        }
        else if( ($max > 200) && ($max <= 600) )
        {
            $zoom = 8 ;
        }
        else if( ($max > 600) && ($max <= 1000) )
        {
            $zoom = 6 ;
        }
        else if( $max > 1000 )
        {
            $zoom = 5 ;
        }

        return $zoom;
    }
}


