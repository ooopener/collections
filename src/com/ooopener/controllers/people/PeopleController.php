<?php

namespace com\ooopener\controllers\people;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Collections;
use com\ooopener\validations\PersonValidator;



use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception;

use Slim\Container ;

use com\ooopener\things\Person;
use com\ooopener\things\Thing;

/**
 * The people collection controller.
 */
class PeopleController extends CollectionsController
{
    /**
     * Creates a new PeopleController instance.
     *
     * @param Container $container
     * @param Collections $model
     * @param string $path
     */
    public function __construct( Container $container , Collections $model = NULL , $path = 'people' )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Schema(
     *     schema="PersonList",
     *     description="A person (alive, dead, undead, or fictional)",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *
     *     @OA\Property(property="taxID",type="string"),
     *     @OA\Property(property="vatID",type="string"),
     *
     *     @OA\Property(property="givenName",type="string"),
     *     @OA\Property(property="familyName",type="string"),
     *
     *     @OA\Property(property="honorificPrefix",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="honorificSuffix",type="string"),
     *
     *     @OA\Property(property="nationality",type="string"),
     *     @OA\Property(property="birthDate",type="string"),
     *     @OA\Property(property="birthPlace",type="string"),
     *     @OA\Property(property="deathDate",type="string"),
     *     @OA\Property(property="deathPlace",type="string"),
     * )
     *
     * @OA\Schema(
     *     schema="Person",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/PersonList")},
     *     @OA\Property(property="gender",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="address",ref="#/components/schemas/PostalAddress"),
     *     @OA\Property(property="email",type="array",items=@OA\Items(ref="#/components/schemas/Email")),
     *     @OA\Property(property="telephone",type="array",items=@OA\Items(ref="#/components/schemas/PhoneNumber")),
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="job",type="array",items=@OA\Items(ref="#/components/schemas/Thesaurus")),
     *     @OA\Property(property="memberOf",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList")),
     *     @OA\Property(property="ows",type="array",items=@OA\Items(ref="#/components/schemas/ConceptualObjectList")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website")),
     *     @OA\Property(property="worksFor",type="array",items=@OA\Items(ref="#/components/schemas/OrganizationList"))
     * )
     *
     * @OA\Response(
     *     response="personResponse",
     *     description="Result of the person",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Person")
     *     )
     * )
     *
     * @OA\Response(
     *     response="personListResponse",
     *     description="Result list of people",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/PersonList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL        ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT      ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID          ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT     ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL         ] ,

        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME    ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME    ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'audio'         => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,
        'image'         => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,
        'video'         => [ 'filter' =>  Thing::FILTER_EDGE_SINGLE ] ,

        'givenName'     => [ 'filter' =>  Thing::FILTER_DEFAULT     ] ,
        'familyName'    => [ 'filter' =>  Thing::FILTER_DEFAULT     ] ,

        'honorificPrefix' => [ 'filter' =>  Thing::FILTER_JOIN     ] ,
        'honorificSuffix' => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'text'          => [ 'filter' => Thing::FILTER_TRANSLATE ] ,

        'taxID'         => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'vatID'         => [ 'filter' => Thing::FILTER_DEFAULT   ] ,

        'address'       => [ 'filter' => Thing::FILTER_DEFAULT   ] ,

        'gender'        => [ 'filter' => Thing::FILTER_JOIN      ] ,

        'nationality'   => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'birthDate'     => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'birthPlace'    => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'deathDate'     => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'deathPlace'    => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,

        'email'         => [ 'filter' => Thing::FILTER_EDGE      ] ,
        'telephone'     => [ 'filter' => Thing::FILTER_EDGE      ] ,

        'numAudios'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'notes'         => [ 'filter' => Thing::FILTER_TRANSLATE  , 'skins' => [ 'full' , 'compact' ] ] ,
        'websites'      => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' , 'compact' ] ] ,
        'job'           => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' , 'list' , 'compact' ] ] ,
        'owns'          => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full', 'compact' ] ],
        'audios'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'audios' ] ],
        'photos'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'photos' ] ],
        'videos'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY , 'skins' => [ 'full' , 'videos' ] ],
        'memberOf'      => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' , 'compact' ] ],
        'worksFor'      => [ 'filter' => Thing::FILTER_EDGE       , 'skins' => [ 'full' , 'compact' ] ]

    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param object $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Person::FILTER_MEMBER_OF :
                    {
                        if( property_exists( $init , Person::FILTER_MEMBER_OF ) && is_array( $init->{ Person::FILTER_MEMBER_OF } ) && count( $init->{ Person::FILTER_MEMBER_OF } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Person::FILTER_MEMBER_OF } as $item )
                            {
                                array_push( $sub , $this->container->organizationsController->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Person::FILTER_OWNS :
                    {
                        if( property_exists( $init , Person::FILTER_OWNS ) && is_array( $init->{ Person::FILTER_OWNS } ) && count( $init->{ Person::FILTER_OWNS } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Person::FILTER_OWNS } as $item )
                            {
                                array_push( $sub , $this->container->conceptualObjectsController->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Person::FILTER_WORKS_FOR :
                    {
                        if( property_exists( $init , Person::FILTER_WORKS_FOR ) && is_array( $init->{ Person::FILTER_WORKS_FOR } ) && count( $init->{ Person::FILTER_WORKS_FOR } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Person::FILTER_WORKS_FOR } as $item )
                            {
                                array_push( $sub , $this->container->organizationsController->create( (object) $item , $lang ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    ///////////////////////////

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
        }

        //// remove all linked resources

        // remove jobs
        $jobs = $this->container->peopleJobsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove keywords
        $keywords = $this->container->peopleKeywordsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove emails
        $emails = $this->container->peopleEmailsController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove phoneNumbers
        $phoneNumbers = $this->container->peoplePhoneNumbersController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove memberOf
        $memberOf = $this->container->organizationMembersController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE , 'type' => 'person' ] ) ;

        // remove organizersOf
        $organizersOf = $this->container->eventOrganizersController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE , 'type' => 'person' ] ) ;

        // remove owns
        $owns = $this->container->conceptualObjectProductionsController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE , 'type' => 'person' ] ) ;

        // remove providersOf
        $providersOf = $this->container->organizationProvidersController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE , 'type' => 'person' ] ) ;

        // remove websites
        $websites = $this->container->peopleWebsitesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        // remove worksFor
        $worksFor = $this->container->organizationEmployeesController->deleteAll( NULL , NULL , [ 'owner' => $id , 'reverse' => TRUE ] ) ;

        // remove course discover
        if( isset( $this->container->courseDiscoverController ) )
        {
            $discover = $this->container->courseDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        // remove stage discover
        if( isset( $this->container->stageDiscoverController ) )
        {
            $discover = $this->container->stageDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postPeople",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"},
     *             @OA\Property(property="gender",type="integer",description="The id of the gender"),
     *             @OA\Property(property="alternateName",type="string",description="The alternate name"),
     *             @OA\Property(property="givenName",type="string",description="The given name"),
     *             @OA\Property(property="familyName",type="string",description="The family name"),
     *          )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $item = [];

        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['gender'] ) && !empty( $params['gender'] ) && $this->container->genders->exist( $params['gender'] ) )
        {
            $item['gender'] = (int) $params['gender'] ;
        }

        $conditions =
        [
            'name'          => [ $params['name']          , 'required|max(70)' ]
        ] ;

        if( isset( $params['alternateName'] ) )
        {
            $item['alternateName'] = $params['alternateName'] ;
            $conditions['alternateName'] = [ $params['alternateName'] , 'max(55)' ] ;
        }

        if( isset( $params['givenName'] ) )
        {
            $item['givenName'] = $params['givenName'] ;
            $conditions['givenName'] = [ $params['givenName'] , 'max(55)' ] ;
        }

        if( isset( $params['familyName'] ) )
        {
            $item['familyName'] = $params['familyName'] ;
            $conditions['familyName'] = [ $params['familyName'] , 'max(55)' ] ;
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new PersonValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $result = $this->model->insert( $item );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchPeople",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             required={"name"},
     *             @OA\Property(property="gender",type="integer",description="The id of the gender"),
     *             @OA\Property(property="alternateName",type="string",description="The alternate name"),
     *             @OA\Property(property="givenName",type="string",description="The given name"),
     *             @OA\Property(property="familyName",type="string",description="The family name"),
     *
     *             @OA\Property(property="nationality",type="string",description="The nationality"),
     *             @OA\Property(property="birthDate",type="string",description="The birth date"),
     *             @OA\Property(property="birthPlace",type="string",description="The birth place"),
     *             @OA\Property(property="deathDate",type="string",description="The death date"),
     *             @OA\Property(property="deathPlace",type="string",description="The death place"),
     *
     *             @OA\Property(property="honorificPrefix",type="integer",description="The honorific prefix ID"),
     *             @OA\Property(property="honorificSuffix",type="string",description="The honorific suffix"),
     *
     *             @OA\Property(property="taxID",type="string",description="The tax ID"),
     *             @OA\Property(property="vatID",type="string",description="The vat ID"),
     *          )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $item = [] ;
        $conditions = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
            $conditions['name'] = [ $params['name'] , 'required|min(2)|max(70)' ] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            if( $params['alternateName'] != '' )
            {
                $item['alternateName'] = $params['alternateName'] ;
                $conditions['alternateName'] = [ $params['alternateName'] , 'max(55)' ] ;
            }
            else
            {
                $item['alternateName'] = NULL ;
            }
        }

        if( isset( $params['nationality'] ) )
        {
            if( $params['nationality'] != '' )
            {
                $item['nationality'] = $params['nationality'] ;
                $conditions['nationality'] = [ $params['nationality'] , 'max(255)' ] ;
            }
            else
            {
                $item['nationality'] = NULL ;
            }
        }

        if( isset( $params['birthDate'] ) )
        {
            if( $params['birthDate'] != '' )
            {
                $item['birthDate'] = $params['birthDate'] ;
                $conditions['birthDate'] = [ $params['birthDate'] , 'simpleDate(true)' ] ;
            }
            else
            {
                $item['birthDate'] = NULL ;
            }
        }

        if( isset( $params['birthPlace'] ) )
        {
            if( $params['birthPlace'] != '' )
            {
                $item['birthPlace'] = $params['birthPlace'] ;
                $conditions['birthPlace'] = [ $params['birthPlace'] , 'max(255)' ] ;
            }
            else
            {
                $item['birthPlace'] = NULL ;
            }
        }

        if( isset( $params['deathDate'] ) )
        {
            if( $params['deathDate'] != '' )
            {
                $item['deathDate'] = $params['deathDate'] ;
                $conditions['deathDate'] = [ $params['deathDate'] , 'simpleDate(true)' ] ;
            }
            else
            {
                $item['deathDate'] = NULL ;
            }
        }

        if( isset( $params['deathPlace'] ) )
        {
            if( $params['deathPlace'] != '' )
            {
                $item['deathPlace'] = $params['deathPlace'] ;
                $conditions['deathPlace'] = [ $params['deathPlace'] , 'max(255)' ] ;
            }
            else
            {
                $item['deathPlace'] = NULL ;
            }
        }

        if( isset( $params['familyName'] ) )
        {
            if( $params['familyName'] != '' )
            {
                $item['familyName'] = $params['familyName'] ;
                $conditions['familyName'] = [ $params['familyName'] , 'max(55)' ] ;
            }
            else
            {
                $item['familyName'] = NULL ;
            }
        }

        if( isset( $params['givenName'] ) )
        {
            if( $params['givenName'] != '' )
            {
                $item['givenName'] = $params['givenName'] ;
                $conditions['givenName'] = [ $params['givenName'] , 'max(55)' ] ;
            }
            else
            {
                $item['givenName'] = NULL ;
            }
        }

        if( isset( $params['honorificPrefix'] ) )
        {
            if( $params['honorificPrefix'] != '' )
            {
                $item['honorificPrefix'] = (int)$params['honorificPrefix'] ;
                $conditions['honorificPrefix'] = [ $params['honorificPrefix'] , 'prefix' ] ;
            }
            else
            {
                $item['honorificPrefix'] = NULL ;
            }
        }

        if( isset( $params['honorificSuffix'] ) )
        {
            $item['honorificSuffix'] = $params['honorificSuffix'] ;
        }

        if( isset( $params['gender'] ) && $this->container->genders->exist( $params['gender'] ) )
        {
            $item['gender'] = (int) $params['gender'] ;
        }

        if( isset( $params['taxID'] ) )
        {
            if( $params['taxID'] != '' )
            {
                $item['taxID'] = $params['taxID'] ;
            }
            else
            {
                $item['taxID'] = NULL ;
            }
        }

        if( isset( $params['vatID'] ) )
        {
            if( $params['vatID'] != '' )
            {
                $item['vatID'] = $params['vatID'] ;
            }
            else
            {
                $item['vatID'] = NULL ;
            }
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        // check if there is at least one param
        if( empty( $item ) )
        {
            return $this->error( $response , 'no params' , '400' ) ;
        }

        // check if resource exists
        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
        }

        ////// validator

        $validator = new PersonValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields('extend') ] ) );
                }
                else
                {
                    return $this->error( $response ,'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putPeople",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="gender",type="integer",description="The id of the gender"),
     *             @OA\Property(property="alternateName",type="string",description="The alternate name"),
     *             @OA\Property(property="givenName",type="string",description="The given name"),
     *             @OA\Property(property="familyName",type="string",description="The family name"),
     *
     *             @OA\Property(property="nationality",type="string",description="The nationality"),
     *             @OA\Property(property="birthDate",type="string",description="The birth date"),
     *             @OA\Property(property="birthPlace",type="string",description="The birth place"),
     *             @OA\Property(property="deathDate",type="string",description="The death date"),
     *             @OA\Property(property="deathPlace",type="string",description="The death place"),
     *
     *             @OA\Property(property="honorificPrefix",type="integer",description="The honorific prefix ID"),
     *             @OA\Property(property="honorificSuffix",type="string",description="The honorific suffix"),
     *
     *             @OA\Property(property="taxID",type="string",description="The tax ID"),
     *             @OA\Property(property="vatID",type="string",description="The vat ID"),
     *          )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $item = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['alternateName'] ) )
        {
            if( $params['alternateName'] != '' )
            {
                $item['alternateName'] = $params['alternateName'] ;
            }
            else
            {
                $item['alternateName'] = NULL ;
            }
        }

        if( isset( $params['familyName'] ) )
        {
            if( $params['familyName'] != '' )
            {
                $item['familyName'] = $params['familyName'] ;
            }
            else
            {
                $item['familyName'] = NULL ;
            }
        }

        if( isset( $params['givenName'] ) )
        {
            if( $params['givenName'] != '' )
            {
                $item['givenName'] = $params['givenName'] ;
            }
            else
            {
                $item['givenName'] = NULL ;
            }
        }

        if( isset( $params['gender'] ) && $this->container->genders->exist( $params['gender'] ) )
        {
            $item['gender'] = (int) $params['gender'] ;
        }

        if( isset( $params['taxID'] ) )
        {
            if( $params['taxID'] != '' )
            {
                $item['taxID'] = $params['taxID'] ;
            }
            else
            {
                $item['taxID'] = NULL ;
            }
        }

        if( isset( $params['vatID'] ) )
        {
            if( $params['vatID'] != '' )
            {
                $item['vatID'] = $params['vatID'] ;
            }
            else
            {
                $item['vatID'] = NULL ;
            }
        }

        $conditions =
        [
            'name'          => [ $params['name']          , 'required|max(70)' ] ,
            'alternateName' => [ $params['alternateName'] , 'max(55)'          ] ,
            'familyName'    => [ $params['familyName']    , 'max(55)'          ] ,
            'givenName'     => [ $params['givenName']     , 'max(55)'          ]
        ] ;

        if( isset( $params['nationality'] ) )
        {
            if( $params['nationality'] != '' )
            {
                $item['nationality'] = $params['nationality'] ;
                $conditions['nationality'] = [ $params['nationality'] , 'max(255)' ] ;
            }
            else
            {
                $item['nationality'] = NULL ;
            }
        }

        if( isset( $params['birthDate'] ) )
        {
            if( $params['birthDate'] != '' )
            {
                $item['birthDate'] = $params['birthDate'] ;
                $conditions['birthDate'] = [ $params['birthDate'] , 'simpleDate(true)' ] ;
            }
            else
            {
                $item['birthDate'] = NULL ;
            }
        }

        if( isset( $params['birthPlace'] ) )
        {
            if( $params['birthPlace'] != '' )
            {
                $item['birthPlace'] = $params['birthPlace'] ;
                $conditions['birthPlace'] = [ $params['birthPlace'] , 'max(255)' ] ;
            }
            else
            {
                $item['birthPlace'] = NULL ;
            }
        }

        if( isset( $params['deathDate'] ) )
        {
            if( $params['deathDate'] != '' )
            {
                $item['deathDate'] = $params['deathDate'] ;
                $conditions['deathDate'] = [ $params['deathDate'] , 'simpleDate(true)' ] ;
            }
            else
            {
                $item['deathDate'] = NULL ;
            }
        }

        if( isset( $params['deathPlace'] ) )
        {
            if( $params['deathPlace'] != '' )
            {
                $item['deathPlace'] = $params['deathPlace'] ;
                $conditions['deathPlace'] = [ $params['deathPlace'] , 'max(255)' ] ;
            }
            else
            {
                $item['deathPlace'] = NULL ;
            }
        }

        if( isset( $params['honorificPrefix'] ) )
        {
            if( $params['honorificPrefix'] != '' )
            {
                $item['honorificPrefix'] = (int)$params['honorificPrefix'] ;
                $conditions['honorificPrefix'] = [ $params['honorificPrefix'] , 'prefix' ] ;
            }
            else
            {
                $item['honorificPrefix'] = NULL ;
            }
        }

        if( isset( $params['honorificSuffix'] ) )
        {
            $item['honorificSuffix'] = $params['honorificSuffix'] ;
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new PersonValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                // check if resource exists
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ] , NULL , 404 );
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields('full') ] ) );
                }
                else
                {
                    return $this->error( $response ,'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

}


