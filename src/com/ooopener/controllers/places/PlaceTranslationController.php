<?php

namespace com\ooopener\controllers\places ;

use com\ooopener\models\Collections;
use com\ooopener\things\Place;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\TranslationController;
use com\ooopener\things\Thing;

use Slim\Container;

class PlaceTranslationController extends TranslationController
{
    /**
     * PlaceTranslationController constructor.
     *
     * @param Container $container
     * @param Collections $model
     * @param null $path
     * @param string $fields
     */
    public function __construct( Container $container , Collections $model = NULL , $path = NULL , $fields = 'description' )
    {
        parent::__construct( $container , $model , $path , $fields );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function alternateName( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_ALTERNATE_NAME ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function description( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_DESCRIPTION ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function notes( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_NOTES ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function slogan( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Place::FILTER_SLOGAN ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function text( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Place::FILTER_TEXT ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchAlternateName( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Thing::FILTER_ALTERNATE_NAME ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchDescription( Request $request , Response $response , array $args = [] )
    {
        $args['html'] = TRUE ;
        return $this->patchElement( $request , $response , $args , Thing::FILTER_DESCRIPTION ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchNotes( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Thing::FILTER_NOTES ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchSlogan( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Place::FILTER_SLOGAN ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchText( Request $request , Response $response , array $args = [] )
    {
        $args['html'] = TRUE ;
        return $this->patchElement( $request , $response , $args , Place::FILTER_TEXT ) ;
    }
}
