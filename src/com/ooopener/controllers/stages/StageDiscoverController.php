<?php

namespace com\ooopener\controllers\stages ;

use com\ooopener\controllers\CollectionsController;

use com\ooopener\models\Collections;

use com\ooopener\validations\StageValidator;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Container;

use Exception ;

/**
 * Class StageDiscoverController
 *
 * @OA\Schema(
 *     schema="discover",
 *     description="The discover - ConceptualObject or Course or Event or Organization or Person or Place or Stage",
 *     type="object",
 *     allOf={},
 *     anyOf={
 *         @OA\Schema(ref="#/components/schemas/ConceptualObjectList"),
 *         @OA\Schema(ref="#/components/schemas/CourseList"),
 *         @OA\Schema(ref="#/components/schemas/EventList"),
 *         @OA\Schema(ref="#/components/schemas/OrganizationList"),
 *         @OA\Schema(ref="#/components/schemas/PersonList"),
 *         @OA\Schema(ref="#/components/schemas/PlaceList"),
 *         @OA\Schema(ref="#/components/schemas/StageList")
 *     }
 * )
 *
 * @OA\Schema(
 *     schema="discovers",
 *     description="List of discover",
 *     type="array",
 *     items=@OA\Items(ref="#/components/schemas/discover")
 * )
 *
 * @OA\Response(
 *     response="discoverResponse",
 *     description="Result of the discover",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="status", type="string",description="The request status",example="success"),
 *         @OA\Property(property="result",ref="#/components/schemas/discovers")
 *     )
 * )
 *
 */
class StageDiscoverController extends CollectionsController
{
    public function __construct( Container $container , Collections $model = NULL , $path = NULL )
    {
        parent::__construct($container , $model , $path);

        $this->validator = new StageValidator( $container ) ;
    }

    public $validator;

    /**
     * The default 'delete' methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return mixed|Response
     *
     * @OA\RequestBody(
     *     request="deleteStageDiscover",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="url",type="string",description="The url of the resource"),
     *             required={"url"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' delete(' . $id . ')' );
        }

        // check
        $params = $request->getParsedBody();

        $url = NULL ;

        if( isset( $params['url'] ) )
        {
            $url = $params['url'] ;
        }

        $conditions =
        [
            'url'      => [ $params['url']      , 'required|url' ]
        ] ;

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $appUrl = $this->config['app']['url'] ;
                // get url path
                $urlPath = substr( $url , strlen( $appUrl ) ) ;

                $item = $this->model->get( $id ) ;
                if( !$item || !property_exists( $item , $this->path ) ||
                    !is_array( $item->{ $this->path } ) || count( $item->{ $this->path } ) == 0 ||
                    !in_array( $urlPath , $item->{ $this->path } ) )
                {
                    return $this->formatError($response , '404' , [ 'delete(' . $id . ')' ] , NULL , 404) ;
                }

                // remove ref
                $newArray = $item->{ $this->path } ;
                $pos = array_search( $urlPath , $newArray , TRUE ) ;
                if( $pos !== FALSE )
                {
                    array_splice( $newArray , $pos , 1 ) ;
                    $update = $this->model->update( [ $this->path => $newArray ] , $id ) ;
                }

                // check valid url path
                $split = explode( '/' , $urlPath ) ;

                $result = null ;

                switch( $split[0] )
                {
                    case 'articles' :
                        $result = $this->container->stageArticlesController->delete( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'conceptualObjects' :
                        $result = $this->container->stageConceptualObjectsController->delete( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'courses' :
                        $result = $this->container->stageCoursesController->delete( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'events' :
                        $result = $this->container->stageEventsController->delete( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'organizations' :
                        $result = $this->container->stageOrganizationsController->delete( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'people' :
                        $result = $this->container->stagePeopleController->delete( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'places' :
                        $result = $this->container->stagePlacesController->delete( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'stages' :
                        $result = $this->container->stageStagesController->delete( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                }

                if( !$result )
                {
                    return $this->formatError($response , '500' , [ 'delete(' . $id . ')' ] , NULL , 500);
                }

                $item = $this->get( NULL , NULL , [ 'id' => $id ] ) ;
                return $this->success( $response , $item ) ;

            }
            catch( Exception $e )
            {
                return $this->formatError($response , '500' , [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500);
            }
        }
        else
        {
            $errors = [] ;

            $err  = $this->validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'deleteReverse' methods options.
     */
    const ARGUMENTS_DELETE_REVERSE_DEFAULT =
    [
        'owner' => NULL
    ] ;

    public function deleteReverse( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_REVERSE_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' deleteReverse(' . $owner . ')' );
        }

        try
        {
            $delete = $this->model->deleteDiscoverReverse( $owner ) ;

            if( $delete )
            {
                if( $response )
                {
                    return $this->success( $response , $owner ) ;
                }

                return $delete ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError($response , '500' , [ 'deleteReverse(' . $owner . ')' , $e->getMessage() ] , NULL , 500);
        }

        return NULL ;
    }

    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $args['active'] = NULL ;
        $args['skin'] = $this->path ;

        $result = $this->container->stagesController->get( NULL , NULL , $args ) ;

        if( $result && property_exists( $result , $this->path ) )
        {
            $result = $result->{ $this->path } ;
        }

        if( $response )
        {
            return $this->success( $response , $result , '' , count( $result ) ) ;
        }

        return $result ;
    }

    /**
     * The default 'post' methods options.
     */
    const ARGUMENTS_POST_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postStageDiscover",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="url",type="string",description="The url of the resource"),
     *             required={"url"},
     *             @OA\Property(property="position",type="integer",description="The position of stage discover"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' );
        }

        // check
        $params = $request->getParsedBody();

        $position = NULL ;
        $url = NULL ;

        if( isset( $params['url'] ) )
        {
            $url = $params['url'] ;
        }

        $conditions =
        [
            'url'      => [ $params['url']      , 'required|url' ]
        ] ;

        if( isset( $params['position'] ) )
        {
            $position = (int) $params['position'] ;
            $conditions['position'] = [ $params['position'] , 'int|min(0)'   ] ;
        }

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $appUrl = $this->config['app']['url'] ;
                // get url path
                $urlPath = substr( $url , strlen( $appUrl ) ) ;

                $item = $this->model->get( $id ) ;
                if( !$item || !property_exists( $item , $this->path ) ||
                    !is_array( $item->{ $this->path } ) )
                {
                    return $this->formatError($response , '404' , [ 'post(' . $id . ')' ] , NULL , 404) ;
                }

                // check if already present
                if( $urlPath == '' || in_array( $urlPath , $item->{ $this->path } ) )
                {
                    return $this->formatError($response , '400' , [ 'post(' . $id . ')' ] , NULL , 400) ;
                }

                // check valid url path
                $split = explode( '/' , $urlPath ) ;

                $result = null ;

                switch( $split[0] )
                {
                    case 'articles' :
                        $result = $this->container->stageArticlesController->post( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'conceptualObjects' :
                        $result = $this->container->stageConceptualObjectsController->post( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'courses' :
                        $result = $this->container->stageCoursesController->post( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'events' :
                        $result = $this->container->stageEventsController->post( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'organizations' :
                        $result = $this->container->stageOrganizationsController->post( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'people' :
                        $result = $this->container->stagePeopleController->post( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'places' :
                        $result = $this->container->stagePlacesController->post( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                    case 'stages' :
                        $result = $this->container->stageStagesController->post( NULL , NULL , [ 'id' => $split[1] , 'owner' => $id ] ) ;
                        break ;
                }

                if( !$result )
                {
                    return $this->formatError($response , '500' , [ 'post(' . $id . ')' ] , NULL , 500);
                }

                $array = $item->{ $this->path } ;
                $length = count( $array ) ;

                if( $position === NULL || $position > $length )
                {
                    $position = $length ;
                }

                array_splice( $array , $position , 0 , $urlPath ) ;

                // update
                $update = $this->model->update( [ $this->path => $array ] , $id ) ;

                $item = $this->get( NULL , NULL , [ 'id' => $id ] ) ;
                return $this->success( $response , $item ) ;

            }
            catch( Exception $e )
            {
                return $this->formatError($response , '500' , [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500);
            }
        }
        else
        {
            $errors = [] ;

            $err  = $this->validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }

    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchStageDiscover",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="url",type="string",description="The url of the resource"),
     *             @OA\Property(property="position",type="integer",description="The position of stage discover"),
     *             required={"url","position"},
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' );
        }

        // check
        $params = $request->getParsedBody();

        $position = NULL ;
        $url = NULL ;

        if( isset( $params['url'] ) )
        {
            $url = $params['url'] ;
        }

        if( isset( $params['position'] ) )
        {
            $position = (int) $params['position'] ;
        }

        $conditions =
        [
            'url'      => [ $params['url']      , 'required|url' ] ,
            'position' => [ $params['position'] , 'int|min(0)'   ]
        ] ;

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $appUrl = $this->config['app']['url'] ;
                // get url path
                $urlPath = substr( $url , strlen( $appUrl ) ) ;


                // check if url is present in older list

                $item = $this->model->get( $id ) ;
                if( !$item || !property_exists( $item , $this->path ) ||
                    !is_array( $item->{ $this->path } ) || count( $item->{ $this->path } ) == 0 ||
                    !in_array( $urlPath , $item->{ $this->path } ) )
                {
                    return $this->formatError($response , '404' , [ 'patch(' . $id . ')' ] , NULL , 404) ;
                }

                $length = count( $item->{ $this->path } ) ;

                // remove ref
                $newArray = array_diff( $item->{ $this->path } , [ $urlPath ] ) ;

                // check position
                if( $position < 0 )
                {
                    $position = 0 ;
                }
                else if( $position > $length - 1 )
                {
                    $position = $length - 1 ;
                }

                // put ref in the new position
                array_splice( $newArray , $position , 0 , $urlPath ) ;

                // update
                $update = $this->model->update( [ $this->path => $newArray ] , $id ) ;

                $item = $this->get( NULL , NULL , [ 'id' => $id ] ) ;
                return $this->success( $response , $item ) ;
            }
            catch( Exception $e )
            {
                return $this->formatError($response , '500' , [ 'patch(' . $id . ')' , $e->getMessage() ] , NULL , 500);
            }
        }
        else
        {
            $errors = [] ;

            $err  = $this->validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }

    }
}
