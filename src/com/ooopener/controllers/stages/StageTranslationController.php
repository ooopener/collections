<?php

namespace com\ooopener\controllers\stages ;

use com\ooopener\models\Collections;
use com\ooopener\things\Stage;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\TranslationController;
use com\ooopener\things\Thing;

use Slim\Container;

class StageTranslationController extends TranslationController
{
    /**
     * PlaceTranslationController constructor.
     *
     * @param Container $container
     * @param Collections $model
     * @param null $path
     * @param string $fields
     */
    public function __construct( Container $container , Collections $model = NULL , $path = NULL , $fields = 'description' )
    {
        parent::__construct( $container , $model , $path , $fields );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function alternativeHeadline( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Stage::FILTER_ALTERNATIVEHEADLINE ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function headline( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Stage::FILTER_HEADLINE ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function notes( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Stage::FILTER_NOTES ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function text( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Stage::FILTER_TEXT ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function description( Request $request , Response $response , array $args = [] )
    {
        $args['fields'] = Thing::FILTER_DESCRIPTION ;
        return $this->translation( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchAlternativeHeadline( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Stage::FILTER_ALTERNATIVEHEADLINE ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchHeadline( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Stage::FILTER_HEADLINE ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchNotes( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Stage::FILTER_NOTES ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchText( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Stage::FILTER_TEXT ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function patchDescription( Request $request , Response $response , array $args = [] )
    {
        return $this->patchElement( $request , $response , $args , Thing::FILTER_DESCRIPTION ) ;
    }
}