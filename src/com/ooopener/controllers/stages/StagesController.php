<?php

namespace com\ooopener\controllers\stages;

use com\ooopener\helpers\Status;
use com\ooopener\things\Stage;
use com\ooopener\things\Thing;
use com\ooopener\validations\StageValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\models\Collections;

use Exception ;
use Slim\Container;

class StagesController extends CollectionsController
{
    public function __construct(Container $container, Collections $model = NULL, string $path = NULL)
    {
        parent::__construct($container, $model, $path);
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="StageList",
     *     description="Stage",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/headlineText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="status",ref="#/components/schemas/Thesaurus"),
     *     @OA\Property(property="location",ref="#/components/schemas/PlaceList"),
     * )
     *
     * @OA\Schema(
     *     schema="Stage",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/StageList")},
     *     @OA\Property(property="activities",type="array",items=@OA\Items(ref="#/components/schemas/Thesaurus")),
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="discover",type="array",items=@OA\Items(ref="#/components/schemas/discovers")),
     *     @OA\Property(property="websites",type="array",items=@OA\Items(ref="#/components/schemas/Website"))
     * )
     *
     * @OA\Response(
     *     response="stageResponse",
     *     description="Result of the stage",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Stage")
     *     )
     * )
     *
     * @OA\Response(
     *     response="stageListResponse",
     *     description="Result list of stages",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/StageList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =

    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'audio'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'alternativeHeadline' => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'description'         => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'headline'            => [ 'filter' => Thing::FILTER_TRANSLATE   ] ,
        'location'            => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'status'              => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'numAudios'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'notes'               => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'compact' , 'list', 'normal' ] ] ,
        'text'                => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'compact' , 'list', 'normal' ] ] ,

        'activities'          => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'compact' ] ] ,
        'websites'            => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'compact' ] ] ,
        'audios'              => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'audios' ] ] ,
        'photos'              => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'photos' ] ] ,
        'videos'              => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'videos' ] ] ,

        'discover'             => [ 'filter' => Thing::FILTER_DEFAULT    , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,

        'articles'            => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'courses'             => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'events'              => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'conceptualObjects'   => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'organizations'       => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'people'              => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'places'              => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ] ,
        'stages'              => [ 'filter' => Thing::FILTER_EDGE        , 'skins' => [ 'full' , 'discover' , 'compact' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create($init = NULL, $lang = NULL, $skin = NULL, $params = NULL)
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Stage::FILTER_DISCOVER :
                    {

                        // regroup events, conceptualObjects, organizations, people, places, stages, courses
                        if( property_exists( $init , Stage::FILTER_DISCOVER ) )
                        {
                            $items = [] ;

                            if( property_exists( $init , Stage::FILTER_ARTICLES ) && is_array( $init->{ Stage::FILTER_ARTICLES } ) && count( $init->{ Stage::FILTER_ARTICLES } ) > 0 )
                            {
                                foreach( $init->{ Stage::FILTER_ARTICLES } as $item )
                                {
                                    array_push( $items , $this->container->articlesController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Stage::FILTER_ARTICLES } ) ;

                            if( property_exists( $init , Stage::FILTER_EVENTS ) && is_array( $init->{ Stage::FILTER_EVENTS } ) && count( $init->{ Stage::FILTER_EVENTS } ) > 0 )
                            {
                                foreach( $init->{ Stage::FILTER_EVENTS } as $item )
                                {
                                    array_push( $items , $this->container->eventsController->create( (object) $item ) ) ;
                                }

                            }

                            // remove property
                            unset( $init->{ Stage::FILTER_EVENTS } ) ;

                            if( property_exists( $init , Stage::FILTER_CONCEPTUAL_OBJECTS ) && is_array( $init->{ Stage::FILTER_CONCEPTUAL_OBJECTS } ) && count( $init->{ Stage::FILTER_CONCEPTUAL_OBJECTS } ) > 0 )
                            {
                                foreach( $init->{ Stage::FILTER_CONCEPTUAL_OBJECTS } as $item )
                                {
                                    array_push( $items , $this->container->conceptualObjectsController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Stage::FILTER_CONCEPTUAL_OBJECTS } ) ;

                            if( property_exists( $init , Stage::FILTER_ORGANIZATIONS ) && is_array( $init->{ Stage::FILTER_ORGANIZATIONS } ) && count( $init->{ Stage::FILTER_ORGANIZATIONS } ) > 0 )
                            {
                                foreach( $init->{ Stage::FILTER_ORGANIZATIONS } as $item )
                                {
                                    array_push( $items , $this->container->organizationsController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Stage::FILTER_ORGANIZATIONS } ) ;

                            if( property_exists( $init , Stage::FILTER_PEOPLE ) && is_array( $init->{ Stage::FILTER_PEOPLE } ) && count( $init->{ Stage::FILTER_PEOPLE } ) > 0 )
                            {
                                foreach( $init->{ Stage::FILTER_PEOPLE } as $item )
                                {
                                    array_push( $items , $this->container->peopleController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Stage::FILTER_PEOPLE } ) ;

                            if( property_exists( $init , Stage::FILTER_PLACES ) && is_array( $init->{ Stage::FILTER_PLACES } ) && count( $init->{ Stage::FILTER_PLACES } ) > 0 )
                            {
                                foreach( $init->{ Stage::FILTER_PLACES } as $item )
                                {
                                    array_push( $items , $this->container->placesController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Stage::FILTER_PLACES } ) ;

                            if( property_exists( $init , Stage::FILTER_STAGES ) && is_array( $init->{ Stage::FILTER_STAGES } ) && count( $init->{ Stage::FILTER_STAGES } ) > 0 )
                            {
                                foreach( $init->{ Stage::FILTER_STAGES } as $item )
                                {
                                    array_push( $items , $this->container->stagesController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Stage::FILTER_STAGES } ) ;

                            if( property_exists( $init , Stage::FILTER_COURSES ) && is_array( $init->{ Stage::FILTER_COURSES } ) && count( $init->{ Stage::FILTER_COURSES } ) > 0 )
                            {
                                foreach( $init->{ Stage::FILTER_COURSES } as $item )
                                {
                                    array_push( $items , $this->container->coursesController->create( (object) $item ) ) ;
                                }
                            }

                            // remove property
                            unset( $init->{ Stage::FILTER_COURSES } ) ;

                            // order result
                            $order = $init->{ Stage::FILTER_DISCOVER } ;

                            if( is_array( $order ) && count( $order ) > 0 )
                            {
                                $itemsOrdered = [] ;

                                // sorting
                                $position = 0;
                                foreach( $order as $ord )
                                {
                                    $found = NULL ;

                                    foreach( $items as $item )
                                    {
                                        if( $this->endsWith( $item->url , $ord ) )
                                        {
                                            $found = $item ;
                                            break ;
                                        }
                                    }

                                    if( $found !== NULL  )
                                    {
                                        $found->position = $position ;
                                        array_push( $itemsOrdered , $found );

                                        $position++;
                                    }
                                }

                                $init->{ $key } = $itemsOrdered ;
                            }
                            else
                            {
                                $init->{ $key } = $items ;
                            }
                        }
                        break ;
                    }
                }
            }
        }
        return $init;
    }

    ///////////////////////////

    public function delete(Request $request = NULL, Response $response = NULL, array $args = [])
    {
        extract(array_merge(self::ARGUMENTS_DELETE_DEFAULT, $args));

        if( !$this->model->exist( $id ) )
        {
            return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
        }

        //// remove all linked resources

        // remove status
        $status = $this->container->stageCoursesStatusController->deleteAll(NULL, NULL, ['owner' => $id]);

        // remove location
        $location = $this->container->stageLocationsController->deleteAll(NULL, NULL, ['owner' => $id]);

        // remove activities
        $activities = $this->container->stageActivitiesController->deleteAll(NULL, NULL, ['owner' => $id]);

        // remove websites
        $websites = $this->container->stageWebsitesController->deleteAll(NULL, NULL, ['owner' => $id]);

        // remove courses
        $courses = $this->container->stageCoursesController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove events
        $events = $this->container->stageEventsController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove conceptualObjects
        $objects = $this->container->stageConceptualObjectsController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove organizations
        $organizations = $this->container->stageOrganizationsController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove people
        $people = $this->container->stagePeopleController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove places
        $places = $this->container->stagePlacesController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove stages
        $stages = $this->container->stageStagesController->deleteAll(NULL, NULL, ['owner' => $id] ) ;

        // remove course discover
        if( isset( $this->container->courseDiscoverController ) )
        {
            $discover = $this->container->courseDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        // remove stage discover
        if( isset( $this->container->stageDiscoverController ) )
        {
            $discover = $this->container->stageDiscoverController->deleteReverse( NULL , NULL , [ 'owner' => $this->path . '/' . $id ] ) ;
        }

        return parent::delete($request, $response, $args);
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postStage",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="status",type="integer",description="The id of the stage status"),
     *             required={"name","location","status"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post(Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug($this . ' post()');
        }

        // check
        $params = $request->getParsedBody();

        $item = [];

        $item['active'] = 1;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = $this->path;

        if( isset($params['name']) )
        {
            $item['name'] = $params['name'];
        }

        // get open status
        $status = $this->container->coursesStatus->get( 'open' , [ 'key' => 'alternateName' , 'fields' => '_key' ] ) ;
        if( $status )
        {
            $status = $status->_key ;
        }

        $conditions =
        [
            'name'     => [ $params['name']     , 'required|min(2)|max(255)' ],
            'location' => [ $params['location'] , 'required|location'        ],
            'status'   => [ $status             , 'required|status'          ]
        ];

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new StageValidator($this->container);

        $validator->validate($conditions);

        if ($validator->passes())
        {
            //////

            try
            {
                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                // set empty array discover
                $item['discover'] = [] ;

                $result = $this->model->insert($item);

                // add location edge
                $po = $this->container->stageLocations;
                $poi = $po->insertEdge($po->from['name'] . '/' . $params['location'], $result->_id);

                // status edge
                if( $status )
                {
                    $edge = $this->container->stageCoursesStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // add edge
                    $edge->insertEdge( $idFrom , $result->_id ) ;
                }

                if( $result )
                {
                    return $this->success($response, $this->model->get($result->_key, ['queryFields' => $this->getFields()]));
                }
                else
                {
                    return $this->error($response, 'error');
                }
            }
            catch( Exception $e )
            {
                return $this->formatError($response, '500', ['post()', $e->getMessage()], NULL, 500);
            }
        }
        else
        {
            $errors = [];

            $err = $validator->errors();
            $keys = $err->keys();

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key);
            }

            return $this->error($response, $errors, "400");
        }
    }


    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putStage",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="location",type="integer",description="The place ID"),
     *             @OA\Property(property="status",type="integer",description="The id of the place status"),
     *             required={"name","location","status"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract(array_merge(self::ARGUMENTS_PUT_DEFAULT , $args));

        if( $response )
        {
            $this->logger->debug($this . ' put(' . $id . ')');
        }

        // check
        $params = $request->getParsedBody();

        $item = [];
        $location = null;
        $status   = null;

        if( isset($params[ 'name' ]) )
        {
            $item[ 'name' ] = $params[ 'name' ];
        }

        if( isset($params[ 'location' ]) )
        {
            $location = $params[ 'location' ];
        }

        if( isset( $params['status'] ) )
        {
            $status = $params['status'] ;
        }

        $conditions =
        [
            'name'     => [ $params['name'] , 'required|min(2)|max(255)' ],
            'location' => [ $location       , 'required|location'        ],
            'status'   => [ $status         , 'required|status'          ]
        ];

        ////// validator

        $validator = new StageValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                // update edges
                if( $location )
                {
                    $edge = $this->container->stageLocations ;

                    $idFrom = $edge->from['name'] . '/' . $location ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                if( $status )
                {
                    $edge = $this->container->stageCoursesStatus ;

                    $idFrom = $edge->from['name'] . '/' . $status ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    // update steps
                    $this->container->stepsController->updateSteps( $id ) ;

                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'list' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    private function endsWith( $haystack , $needle )
    {
        return substr( $haystack , -strlen( $needle ) ) === $needle ;
    }
}
