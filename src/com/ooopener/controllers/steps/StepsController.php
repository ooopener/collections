<?php

namespace com\ooopener\controllers\steps;

use com\ooopener\controllers\ThingsOrderController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Model;
use com\ooopener\things\Step;
use com\ooopener\validations\StepValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\models\Collections;
use com\ooopener\things\Thing;
use Slim\Container;

use Exception ;

class StepsController extends ThingsOrderController
{
    /**
     * Creates a new PlaceWebsitesController instance.
     *
     * @param Container $container
     * @param Model|NULL $model
     * @param Collections|NULL $owner
     * @param string|NULL $path
     */
    public function __construct( Container $container , Model $model = NULL , Collections $owner = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $owner , $path );

        $this->validator = new StepValidator( $container ) ;
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="StepList",
     *     description="Step",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/headlineText")
     *     },
     *     @OA\Property(property="audio",ref="#/components/schemas/AudioObject"),
     *     @OA\Property(property="image",ref="#/components/schemas/ImageObject"),
     *     @OA\Property(property="video",ref="#/components/schemas/VideoObject"),
     *     @OA\Property(property="position",type="integer"),
     * )
     *
     * @OA\Schema(
     *     schema="Step",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/StepList")},
     *     @OA\Property(property="audios",type="array",items=@OA\Items(ref="#/components/schemas/AudioObject")),
     *     @OA\Property(property="photos",type="array",items=@OA\Items(ref="#/components/schemas/ImageObject")),
     *     @OA\Property(property="videos",type="array",items=@OA\Items(ref="#/components/schemas/VideoObject")),
     *     @OA\Property(property="stage",ref="#/components/schemas/StageList")
     * )
     *
     * @OA\Response(
     *     response="stepResponse",
     *     description="Result of the step",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Step")
     *     )
     * )
     *
     * @OA\Response(
     *     response="stepListResponse",
     *     description="Result list of course steps",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/StepList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' => Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'audio'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'image'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,
        'video'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE ] ,

        'position'      => [ 'filter' => Thing::FILTER_UNIQUE_NAME , 'skins' => [ 'full' , 'normal' ] ] ,

        'alternativeHeadline' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'headline'            => [ 'filter' => Thing::FILTER_TRANSLATE ] ,

        'numAudios'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numPhotos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,
        'numVideos'                 => [ 'filter' => Thing::FILTER_JOIN_COUNT  ] ,

        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'compact' , 'normal' ] ] ,
        'notes'         => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'compact' , 'normal' ] ] ,
        'text'          => [ 'filter' => Thing::FILTER_TRANSLATE   , 'skins' => [ 'full' , 'compact' , 'normal' ] ] ,
        'audios'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'compact' , 'audios' ] ] ,
        'photos'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'compact' , 'photos' ] ] ,
        'videos'        => [ 'filter' => Thing::FILTER_JOIN_ARRAY  , 'skins' => [ 'full' , 'compact' , 'videos' ] ] ,
        //'markup'        => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' ] ] ,
        'stage'         => [ 'filter' => Thing::FILTER_EDGE_SINGLE , 'skins' => [ 'full' , 'compact' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Step::FILTER_STAGE :
                    {
                        if( property_exists( $init , Step::FILTER_STAGE ) && $init->{ Step::FILTER_STAGE } != NULL )
                        {
                            $init->{ $key } = $this->container->stagesController->create( (object) $init->{ Step::FILTER_STAGE } ) ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    ///////////////////////////

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        $owner = $this->getOwner( $id ) ;
        if( $owner )
        {
            $args['owner'] = (string)$owner->id ;
        }

        //// remove all linked resources

        // remove stages
        $stages = $this->container->stepStagesController->deleteAll(  NULL , NULL , [ 'owner' => $id ] ) ;

        return parent::delete( $request , $response , $args ) ;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response|object
     */
    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( $params['lang'] , $api['languages'] ) )
                {
                    $params['lang'] = $lang = $params['lang'] ;
                }
                else if( $params['lang'] == 'all' )
                {
                    $lang = NULL ;
                }

            }

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'], $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        try
        {
            if( !$this->model->exist( $id ) )
            {
                return $this->formatError( $response , '404', [ 'get(' . $id . ')' ] , NULL , 404 );
            }

            $extraQuery = '' ;

            $objectSkin = NULL ;
            if( $skin == 'extra' )
            {
                $skin = 'full' ;
                $objectSkin = 'full' ;
            }

            $fields = $this->getFields( $skin ) ;

            // get position
            if( array_key_exists( 'position' , $fields ) )
            {
                $extraQuery = 'LET ' . $fields['position']['unique'] . ' = ( ' .
                    'FOR doc_owner IN ' . $this->owner->table . ' ' .
                    'FILTER POSITION( doc_owner.' . $this->path . ' , TO_NUMBER( @value ) ) == true ' .
                    'RETURN POSITION( doc_owner.' . $this->path . ' , TO_NUMBER( @value ) , true ) ' .
                    ')[0]' ;
            }

            $result = $this->model->get
            (
                $id ,
                [
                    'extraQuery'  => $extraQuery ,
                    'queryFields' => $fields ,
                    'lang'        => $lang
                ]
            ) ;

            $result = $this->create( $result ) ;

            if( $response )
            {
                $options = [] ;

                // get owner
                $ownerItem = $this->getOwner( $id , $objectSkin ) ;
                if( $ownerItem )
                {
                    $options[ 'object' ] = $ownerItem ;
                }

                // add headers
                $response = $this->container->cache->withETag( $response , $result->modified );
                $response = $this->container->cache->withLastModified( $response , $result->modified );

                return $this->success( $response , $result , null , null , $options );
            }
            else
            {
                return $result ;
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postStep",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="stage",type="integer",description="The stage ID"),
     *             required={"name","stage"},
     *             @OA\Property(property="position",type="integer",description="The position of the step"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_POST_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' post(' . $id . ')' ) ;
        }

        // check
        $params = $request->getParsedBody();

        $stage = NULL ;

        $item = [];

        $item['active'] = 1 ;
        $item['withStatus'] = Status::DRAFT ;
        $item['path'] = 'courses/' . $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['stage'] ) )
        {
            $stage = $params['stage'] ;
        }

        if( isset( $params['headline'] ) )
        {
            $item['headline'] = $this->filterLanguages( $params['headline'] ) ;
        }

        $conditions =
        [
            'name'   => [ $params['name']  , 'required|max(70)' ] ,
            'stage'  => [ $stage           , 'required|stage'   ]
        ] ;

        if( isset( $params['position'] ) )
        {
            $position = (int) $params['position'] ;
            $conditions['position'] = [ $params['position'] , 'int|min(0,number)' ] ;
        }

        ////// validator

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            try
            {
                $o = $this->owner->get( $id , [ 'fields' => '_key,' . $this->path ]  ) ;
                if( !$o )
                {
                    return $this->formatError( $response , '404' , [ 'post(' . $id . ')' ] , NULL , 404 );
                }

                $item['audios'] = [] ;
                $item['photos'] = [] ;
                $item['videos'] = [] ;

                $new = $this->model->insert( $item );

                // stage edge
                if( $stage )
                {
                    $edge = $this->container->stepStages ;

                    $idFrom = $edge->from['name'] . '/' . $stage ;

                    // add edge
                    $edge->insertEdge( $idFrom , $new->_id ) ;
                }

                if( $new )
                {
                    // update owner
                    $this->insertInOwner( $new->_key , $o , $position ) ;

                    return $this->success( $response , $this->get( NULL , NULL , [ 'id' => $new->_key , 'skin' => 'full' ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $this->validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="putStep",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="stage",type="integer",description="The stage ID"),
     *             required={"name","stage"},
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' );
        }

        // check
        $params = $request->getParsedBody();

        $stage = NULL ;

        $item = [] ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['stage'] ) )
        {
            $stage = $params['stage'] ;
        }

        $conditions =
        [
            'name'   => [ $params['name']  , 'required|max(70)' ] ,
            'stage'  => [ $stage           , 'required|stage'   ]
        ] ;

        if( isset( $params['position'] ) )
        {
            $item['position'] = (int) $params['position'] ;
            $conditions['position'] = [ $params['position'] , 'int' ] ;
        }

        ////// validator

        $validator = new StepValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {

                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $idTo = $this->model->table . '/' . $id ;

                if( $stage )
                {
                    $edge = $this->container->stepStages ;

                    $idFrom = $edge->from['name'] . '/' . $stage ;

                    // check exists
                    if( !$edge->existEdge( $idFrom , $idTo ) )
                    {
                        // delete all edges to be sure
                        $edge->delete( $idTo , [ 'key' => '_to' ] ) ;
                        // add edge
                        $edge->insertEdge( $idFrom , $idTo ) ;
                    }
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    // update owner
                    $owner = $this->getOwner( $id ) ;
                    if( $owner )
                    {
                        $this->owner->updateDate( $owner->id ) ;
                    }

                    return $this->success( $response , $this->get( NULL , NULL , [ 'id' => $id , 'skin' => 'full' ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }


    //////////////////////////////////

    public function updateSteps( $stage )
    {
        // get steps from stage
        $steps = $this->container->stepStages->getEdge( (string) $stage ) ;
        if( $steps && property_exists( $steps , 'edge' ) && is_array( $steps->edge ) && count( $steps->edge ) > 0 )
        {
            $steps = $steps->edge ;

            foreach( $steps as $step )
            {
                $step = (object) $step ;

                $owner = $this->getOwner( $step->id ) ;

                // update model
                $this->model->updateDate( $step->id ) ;

                // update owner
                $this->owner->updateDate( $owner->id ) ;
            }
        }
    }
}
