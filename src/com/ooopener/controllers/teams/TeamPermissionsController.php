<?php

namespace com\ooopener\controllers\teams ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\Controller;
use com\ooopener\models\Model;
use Slim\Container;

use Exception ;

class TeamPermissionsController extends Controller
{
    public function __construct( Container $container , Model $owner )
    {
        parent::__construct( $container );
        $this->owner      = $owner ;
    }

    /**
     * The owner reference.
     */
    public $owner ;

    /**
     * The permission reference.
     */
    public $permission ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'name'  => NULL
    ] ;

    public function all( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all' ) ;
        }

        $items = NULL ;

        try
        {
            $team = $this->owner->get( $name , [ 'key' => 'name' , 'fields' => 'permissions' ] ) ;
            if( !$team )
            {
                return $this->formatError( $response , '404', [ 'all(' . $name . ')' ] , NULL , 404 );
            }

            $items = [] ;

            if( property_exists( $team , 'permissions' ) && count( $team->permissions ) > 0 )
            {
                foreach( $team->permissions as $permission )
                {
                    $resource = ( $permission['resource'] != "0" ) ? '/' . $permission['resource'] : '' ;
                    $path = $permission['module'] . $resource ;

                    switch( $permission['permission'] )
                    {
                        case 'A':
                            $items[ $path ] = 'admin' ;
                            break;
                        case 'W':
                            $items[ $path ] = 'write' ;
                            break;
                        case 'R':
                            $items[ $path ] = 'read' ;
                            break;
                    }
                }
            }


        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'all' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            return $this->success
            (
                $response,
                $items,
                $this->getFullPath()
            );
        }

        return $items ;
    }
}