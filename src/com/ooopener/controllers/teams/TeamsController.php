<?php

namespace com\ooopener\controllers\teams ;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\models\Teams;
use com\ooopener\things\Team;
use com\ooopener\things\Thing;
use com\ooopener\validations\TeamValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container;

use Exception ;

class TeamsController extends CollectionsController
{
    public function __construct( Container $container , Teams $model = null , $path = 'teams' )
    {
        parent::__construct( $container , $model , $path );
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="TeamList",
     *     description="A team",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="color",type="string"),
     *     @OA\Property(property="identifier",type="integer"),
     * )
     *
     * @OA\Schema(
     *     schema="Team",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/TeamList")}
     * )
     *
     * @OA\Response(
     *     response="teamResponse",
     *     description="Result of the team",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/Team")
     *     )
     * )
     *
     * @OA\Response(
     *     response="teamListResponse",
     *     description="Result list of teams",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/TeamList"))
     *     )
     * )
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' => Thing::FILTER_BOOL     ] ,
        'id'            => [ 'filter' => Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'color'         => [ 'filter' => Thing::FILTER_DEFAULT  ] ,
        'identifier'    => [ 'filter' => Thing::FILTER_INT      ] ,
        'created'       => [ 'filter' => Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' => Thing::FILTER_DATETIME ] ,

        'alternateName' => [ 'filter' => Thing::FILTER_TRANSLATE ] ,
        'description'   => [ 'filter' => Thing::FILTER_TRANSLATE ] ,

        'permissions'   => [ 'filter' => Team::FILTER_PERMISSIONS     , 'skins' => [ 'full' ] ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return mixed
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        // set url
        $init->url = $this->getBaseURL() . '/teams/' . $init->name ;
        return $init ;
    }

    ///////////////////////////


    public function delete( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        $this->logger->debug( $this . ' delete(' . $id . ')' ) ;

        try
        {
            // check exists
            $team = $this->model->get( $id , [ 'key' => 'name' ] ) ;
            if( !$team )
            {
                return $this->error( $response , null , '404' , null , 404 ) ;
            }

            // check not default teams
            if( in_array( $team->name , Team::DEFAULT ) )
            {
                return $this->error( $response , null , '401' , null , 401 ) ;
            }

            $auth = $this->container->auth ;
            $userTeam = $this->model->get( $auth->team->name , [ 'key' => 'name' ] ) ;

            // check right to delete
            if( $team->identifier <= $userTeam->identifier )
            {
                return $this->error( $response , null , '401' , null , 401 ) ;
            }

            // check if there is user in this team
            if( $this->container->users->exist( $id , [ 'key' => 'team' ] ) )
            {
                return $this->error( $response , null , '409' , null , 409 ) ;
            }

            $args['id'] = $team->_key ;
            return parent::delete( $request , $response , $args ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * Get the team
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     *
     */
    public function getTeam( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' getTeam(' . $id . ')' ) ;
        }

        // ------------ init

        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        if( $skin == 'main' || !in_array( $skin , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        // ----------------

        try
        {
            $team = $this->model->get( $id , [ 'key' => 'name' , 'active' => $active , 'queryFields' => $this->getFields( $skin )  ] ) ;

            if( $team )
            {
                $team = $this->create( $team ) ;

                if( $response )
                {
                    return $this->success( $response , $team ) ;
                }

                return $team ;
            }
            else
            {
                if( $response )
                {
                    return $this->error( $response , "no team" , '404' ) ;
                }

                return null ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ] , NULL , 500 );
        }

    }

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="postTeam",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="identifier",type="integer",description="The identifier"),
     *             required={"name","identifier"}
     *         )
     *     ),
     *     required=true
     * )
     */
    public function post( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' ) ;
        }

        // check
        $params = $request->getParsedBody() ;

        $item = [] ;

        $item['active'] = 1 ;
        $item['path'] = $this->path ;

        if( isset( $params['name'] ) )
        {
            $item['name'] = $params['name'] ;
        }

        if( isset( $params['identifier'] ) )
        {
            $item['identifier'] = (int)$params['identifier'] ;
        }

        $conditions =
        [
            'name'       => [ $params['name']       , 'required|uniqueName|min(2)|max(70)' ] ,
            'identifier' => [ $params['identifier'] , 'required|int|min(2,number)'         ]
        ] ;

        ////// validator

        $validator = new TeamValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {

                $result = $this->model->insert( $item );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ]) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'patch' methods options.
     */
    const ARGUMENTS_PATCH_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * @param Request|null $request
     * @param Response|null $response
     * @param array $args
     * @return Response
     *
     * @OA\RequestBody(
     *     request="patchTeam",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="name",type="string",description="The name of the resource"),
     *             @OA\Property(property="identifier",type="integer",description="The identifier"),
     *             @OA\Property(property="color",type="string",description="The color")
     *         )
     *     ),
     *     required=true
     * )
     */
    public function patch( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PATCH_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' patch(' . $id . ')' ) ;
        }

        $team = $this->model->get( $id , [ 'key' => 'name' , 'fields' => '_key,name' ] ) ;

        if( is_null($team) )
        {
            return $this->formatError( $response , '404', [ 'patch(' . $id . ')' ], NULL , 404 );
        }

        // check
        $params = $request->getParsedBody() ;

        $item = [];
        $conditions = [] ;

        if( isset( $params['color'] ) )
        {
            $item['color'] = $params['color'] ;
            $conditions['color'] = [ $params['color'] , 'min(3)|max(6)|isColor' ] ;
        }

        if( isset( $params['identifier'] ) && ( $team->name != Team::SUPER_ADMIN && $team->name != Team::ADMIN ) )
        {
            $item['identifier'] = (int)$params['identifier'] ;
            $conditions['identifier'] = [ $params['identifier'] , 'required|int|min(2,number)' ] ;
        }

        ////// validator

        $validator = new TeamValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {

                $result = $this->model->update( $item , $team->name , [ 'key' => 'name' ] );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $result->_key , [ 'queryFields' => $this->getFields() ]) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }


}
