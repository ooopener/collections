<?php

namespace com\ooopener\controllers\technicians;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\helpers\Status;
use com\ooopener\models\Collections;
use com\ooopener\things\Thing;
use com\ooopener\things\Technician;
use com\ooopener\validations\TechnicianValidator;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;

use Exception ;

class TechniciansController extends CollectionsController
{

    public function __construct( Container $container , Collections $model = NULL , string $path = NULL )
    {
        parent::__construct( $container , $model , $path );
    }

    ///////////////////////////

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     */
    const CREATE_PROPERTIES =
    [
        'active'        => [ 'filter' =>  Thing::FILTER_BOOL     ] ,
        'withStatus'    => [ 'filter' => Thing::FILTER_DEFAULT   ] ,
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_URL      ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'isBasedOn'     => [ 'filter' => Thing::FILTER_DEFAULT , 'skins' => [ 'full' ] ] ,

        'identifier'    => [ 'filter' =>  Thing::FILTER_DEFAULT ] ,

        'person'        => [ 'filter' =>  Thing::FILTER_JOIN ] ,

        'medicalSpecialties' => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'full' ]  ] ,
        'livestocks'         => [ 'filter' =>  Thing::FILTER_EDGE , 'skins' => [ 'full' ]  ]
    ];

    /**
     * Creates a new instance.
     *
     * @param object $init A generic object to create and populate the new thing.
     * @param string $lang The lang optional lang iso code.
     * @param string $skin The optional skin mode.
     * @param array $params The optional params object.
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        if( isset( $init ) )
        {
            foreach( self::CREATE_PROPERTIES as $key => $options )
            {
                switch( $key )
                {
                    case Technician::FILTER_LIVESTOCKS :
                    {
                        if( property_exists( $init , Technician::FILTER_LIVESTOCKS ) && is_array( $init->{ Technician::FILTER_LIVESTOCKS } ) && count( $init->{ Technician::FILTER_LIVESTOCKS } ) > 0 )
                        {
                            $sub = [] ;
                            foreach( $init->{ Technician::FILTER_LIVESTOCKS } as $item )
                            {
                                array_push( $sub , $this->container->livestocksController->create( (object) $item ) ) ;
                            }
                            $init->{ $key } = $sub ;
                        }
                        break ;
                    }
                    case Technician::FILTER_PERSON ;
                    {
                        if( property_exists( $init , Technician::FILTER_PERSON ) && $init->{ Technician::FILTER_PERSON } != NULL )
                        {
                            $init->{ $key } = $this->container->peopleController->create( (object) $init->{ Technician::FILTER_PERSON } ) ;
                        }
                        break ;
                    }
                }
            }
        }
        return $init ;
    }

    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        // remove children

        // remove medicalSpecialties
        $medical = $this->container->technicianMedicalSpecialtiesController->deleteAll( NULL , NULL , [ 'owner' => $id ] ) ;

        return parent::delete( $request , $response , $args ) ;
    }

    public function post( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        if( $response )
        {
            $this->logger->debug( $this . ' post()' );
        }

        // check
        $params = $request->getParsedBody();

        $organization = NULL ;
        $person       = NULL ;

        $item           = [];
        $item['active'] = 1;
        $item['withStatus'] = Status::DRAFT ;
        $item['path']   = $this->path;

        if( isset( $params['identifier'] ) )
        {
            $item['identifier'] = $params['identifier'];
        }

        $conditions = [] ;

        if( !isset( $params['person'] ) || $params['person'] == ''  )
        {
            return $this->error($response , [ 'person' => 'person is required' ] , "400");
        }

        $item['person'] = (int)$params['person'];
        $conditions['person'] = [ $params['person']   , 'required|person' ] ;

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }

        ////// validator

        $validator = new TechnicianValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                $new = $this->model->insert( $item );

                if( $new )
                {
                    return $this->success( $response , $this->model->get( $new->_key , [ 'queryFields' => $this->getFields() ] ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'post()' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'id' => NULL
    ] ;

    public function put( Request $request = NULL , Response $response = NULL , array $args = [])
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) );

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' );
        }

        // check
        $params = $request->getParsedBody();

        $item = [];

        if( isset( $params['identifier'] ) )
        {
            $item['identifier'] = $params['identifier'];
        }

        $conditions = [] ;

        ////// validator

        $validator = new TechnicianValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            //////

            try
            {
                if( !$this->model->exist( $id ) )
                {
                    return $this->formatError( $response , '404', [ 'put(' . $id . ')' ] , NULL , 404 );
                }

                $result = $this->model->update( $item , $id );

                if( $result )
                {
                    return $this->success( $response , $this->model->get( $id , [ 'queryFields' => $this->getFields( 'normal' ) ] ) );
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'put(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }
}
