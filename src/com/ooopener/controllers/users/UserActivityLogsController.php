<?php

namespace com\ooopener\controllers\users ;


use com\ooopener\controllers\CollectionsController;
use com\ooopener\things\Thing;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\models\Collections;
use Slim\Container;

class UserActivityLogsController extends CollectionsController
{
    /**
     * ActivityLogs constructor.
     *
     * @param Container $container
     * @param Collections|NULL $model
     * @param null $path
     */
    public function __construct( Container $container , Collections $model = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $path );
    }

    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Schema(
     *     schema="method",
     *     type="string",
     *     description="The request method",
     *     enum={
     *         "DELETE",
     *         "PATCH",
     *         "POST",
     *         "PUT"
     *     }
     * )
     *
     * @OA\Schema(
     *     schema="ActivityLog",
     *     description="The activity log",
     *     type="object",
     *     @OA\Property(property="id",type="string",description="Resource ID"),
     *     @OA\Property(property="url",type="string",description="The resource url"),
     *     @OA\Property(property="agent",type="string",description="The user agent"),
     *     @OA\Property(property="ip",type="string",description="The user ip"),
     *     @OA\Property(property="method",ref="#/components/schemas/method"),
     *     @OA\Property(property="resource",type="string",format="uri",description="The resource url logged"),
     *     @OA\Property(property="created",type="string",format="date-time",description="The creation date"),
     *     @OA\Property(property="user",ref="#/components/schemas/UserList"),
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'           => [ 'filter' => Thing::FILTER_ID         ] ,
        'url'          => [ 'filter' => Thing::FILTER_URL        ] ,
        'created'      => [ 'filter' => Thing::FILTER_DATETIME   ] ,

        'agent'        => [ 'filter' => Thing::FILTER_DEFAULT    ] ,
        'ip'           => [ 'filter' => Thing::FILTER_DEFAULT    ] ,
        'method'       => [ 'filter' => Thing::FILTER_DEFAULT    ] ,
        'resource'     => [ 'filter' => Thing::FILTER_URL_API    ] ,

        'user'         => [ 'filter' => Thing::FILTER_JOIN ]
    ];

    public function allForAccount( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args['facets'] = [ 'user' => $jwt->sub ] ;
        }

        $args['active'] = NULL ;

        return parent::all( $request , $response , $args ) ;
    }


    public function allByUser( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        if( array_key_exists( 'id' , $args ) )
        {
            $args['facets'] = [ 'user' => $args['id'] ] ;
            unset( $args['id'] );
        }

        $args['active'] = NULL ;

        return parent::all( $request , $response , $args ) ;
    }
}
