<?php

namespace com\ooopener\controllers\users ;

use com\ooopener\controllers\Controller;
use com\ooopener\models\Model;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container;

use Exception ;

class UserPermissionsController extends Controller
{
    /**
     * UserPermissions constructor.
     *
     * @param Container $container
     * @param Model|NULL $owner
     * @param Model|null $permission
     */
    public function __construct( Container $container , Model $owner = NULL , Model $permission = NULL )
    {
        parent::__construct( $container );
        $this->owner      = $owner ;
        $this->permission = $permission ;
    }

    /**
     * The model reference.
     */
    public $model ;

    /**
     * The owner reference.
     */
    public $owner ;

    /**
     * The permission reference.
     */
    public $permission ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'id'  => NULL
    ] ;

    public function all( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' all' ) ;
        }

        $items = NULL ;

        try
        {
            $user = $this->owner->get( $id , [ 'key' => 'uuid' ] ) ;
            if( !$user )
            {
                return $this->formatError( $response , '404', [ 'all(' . $id . ')' ] , NULL , 404 );
            }

            $permissions = $this->permission->all( [ "conditions" => [ "user='" . $id . "'" ] ] );

            $items = [] ;

            foreach( $permissions as $key => $value )
            {
                $resource = ( $value->resource != "0" ) ? '/' . $value->resource : '' ;
                $path = $value->module . $resource ;

                switch( $value->permission )
                {
                    case 'A':
                        $items[ $path ] = 'admin' ;
                        break;
                    case 'W':
                        $items[ $path ] = 'write' ;
                        break;
                    case 'R':
                        $items[ $path ] = 'read' ;
                        break;
                }
            }

        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'all' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            return $this->success
            (
                $response,
                $items,
                $this->getFullPath()
            );
        }

        return $items ;
    }
}