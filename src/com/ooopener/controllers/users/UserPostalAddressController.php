<?php

namespace com\ooopener\controllers\users ;

use com\ooopener\controllers\PostalAddressController;
use com\ooopener\models\Users;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Container;

class UserPostalAddressController extends PostalAddressController
{

    public function __construct( Container $container , Users $model = NULL )
    {
        parent::__construct( $container , $model );
    }

    public function getForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }

        return $this->get( $request , $response , $args ) ;
    }

    public function get( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $args['key'] = 'uuid' ;

        return parent::get( $request , $response , $args ) ;
    }

    public function patchForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }

        return $this->patch( $request , $response , $args ) ;
    }

    public function patch( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $args['key'] = 'uuid' ;

        return parent::patch( $request , $response , $args ) ;
    }
}