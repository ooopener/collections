<?php

namespace com\ooopener\controllers\users ;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\models\Collections;
use com\ooopener\things\Session;
use com\ooopener\things\Thing;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Slim\Container;

class UserSessionsController extends CollectionsController
{
    /**
     * UserSessionsController constructor.
     *
     * @param Container $container
     * @param Collections|NULL $model
     * @param null $path
     */
    public function __construct( Container $container , Collections $model = NULL , $path = NULL )
    {
        parent::__construct( $container , $model , $path );
    }


    /**
     * The enumeration of all properties to filtering when
     * we create a new instance.
     *
     * @OA\Parameter(
     *     name="activeSession",
     *     in="path",
     *     description="Active",
     *     required=true,
     *     @OA\Schema(type="boolean",default=true)
     * )
     *
     * @OA\Schema(
     *     schema="sessionType",
     *     type="string",
     *     description="The session type",
     *     enum={
     *         "access_token",
     *         "id_token"
     *     }
     * )
     *
     * @OA\Schema(
     *     schema="UserSession",
     *     description="An user session",
     *     type="object",
     *     @OA\Property(property="id",type="integer",description="The resource ID"),
     *     @OA\Property(property="type",ref="#/components/schemas/sessionType"),
     *     @OA\Property(property="created",type="string",format="date-time",description="The created date"),
     *     @OA\Property(property="modified",type="string",format="date-time",description="The modified date"),
     *     @OA\Property(property="agent",type="string",description="The user agent"),
     *     @OA\Property(property="ip",type="integer",description="The user ip"),
     *     @OA\Property(property="expired",type="string",format="date-time",description="The expired date"),
     *     @OA\Property(property="logout",type="string",format="date-time",description="The logout date"),
     *     @OA\Property(property="revoked",type="boolean",description="If session revoked"),
     * )
     */
    const CREATE_PROPERTIES =
    [
        'id'           => [ 'filter' => Thing::FILTER_ID         ] ,
        'type'         => [ 'filter' => Thing::FILTER_DEFAULT    ] ,
        'agent'        => [ 'filter' => Thing::FILTER_DEFAULT    ] ,
        'ip'           => [ 'filter' => Thing::FILTER_DEFAULT    ] ,
        'expired'      => [ 'filter' => Thing::FILTER_DATE       ] ,
        'created'      => [ 'filter' => Thing::FILTER_DATE       ] ,
        'logout'       => [ 'filter' => Thing::FILTER_DATE       ] ,
        'modified'     => [ 'filter' => Thing::FILTER_DATE       ] ,
        'revoked'      => [ 'filter' => Thing::FILTER_BOOL       ] ,

        'app'          => [ 'filter' => Thing::FILTER_JOIN   , 'skins' => [ 'full' ] ] ,
        'user'         => [ 'filter' => Session::FILTER_USER , 'skins' => [ 'full' ] ] ,

        // needed for check current session - removed in create
        'token_id'     => [ 'filter' => Thing::FILTER_DEFAULT    ] ,
    ];

    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        // set url
        $url = $this->getFullPath() ;
        if( $this->endsWith( $url , 'true' ) )
        {
            $url = substr( $url , 0, -strlen( 'true' )-1 ) ;
        }
        else if( $this->endsWith( $url , 'false' ) )
        {
            $url = substr( $url , 0 , -strlen( 'false' )-1 ) ;
        }
        $url = $url . '/' . $init->id ;
        $init->url = $url ;

        // check if current session
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->jti && $jwt->jti == $init->token_id )
        {
            $init->isCurrent = true ;
        }

        // remove token_id
        unset( $init->token_id ) ;

        return $init ;
    }

    ///////////////////////////

    public function allForAccount( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args['facets'] = [ 'user' => $jwt->sub ] ;
        }

        if( $args['active'] == 'true' || $args['active'] == 'TRUE' )
        {
            $args['conditions'] =
            [
                "doc.revoked == 0" ,
                "doc.logout == null" ,
                "doc.expired > DATE_ISO8601( DATE_NOW() )"
            ] ;
        }
        else
        {
            $args['conditions'] =
            [
                "( doc.revoked == 1 || doc.logout != null || doc.expired < DATE_ISO8601( DATE_NOW() ) )"
            ] ;
        }

        $args['active'] = NULL ;

        return parent::all( $request , $response , $args ) ;
    }

    public function allByUser( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        if( array_key_exists( 'id' , $args ) )
        {
            $args['facets'] = [ 'user' => $args['id'] ] ;
            unset( $args['id'] );
        }

        if( $args['active'] == 'true' || $args['active'] == 'TRUE' )
        {
            $args['conditions'] =
            [
                "doc.revoked == 0" ,
                "doc.logout == null" ,
                "doc.expired > DATE_ISO8601( DATE_NOW() )"
            ] ;
        }
        else
        {
            $args['conditions'] =
            [
                "( doc.revoked == 1 || doc.logout != null || doc.expired < DATE_ISO8601( DATE_NOW() ) )"
            ] ;
        }

        $args['active'] = NULL ;

        return parent::all( $request , $response , $args ) ;
    }

    public function revokeForAccount( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args[ 'id' ] = $jwt->sub ;
        }

        return $this->revoke( $request , $response , $args ) ;
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_REVOKE_DEFAULT =
    [
        'id'      => NULL ,
        'session' => NULL
    ] ;

    public function revoke( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_REVOKE_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' revoke(' . $id . ',' . $session . ')' ) ;
        }

        $user = $this->container->users->get( $id , [ 'key' => 'uuid' ] ) ;

        if( !$user )
        {
            return $this->formatError( $response , '404' , null , null , 404 ) ;
        }

        $user = $this->container->usersController->create( $user ) ;
        $auth = $this->container->auth ;

        // SECURITY - check if user team can delete selected user
        if( $auth->team->identifier > $user->team->identifier )
        {
            return $this->formatError( $response , '401' , null , null , 401 ) ;
        }

        try
        {
            if( !$this->container->sessions->exist( $session , [ "conditions" => [ "doc.user == '" . $id . "'" ] ] ) )
            {
                return $this->formatError( $response , '404', [ 'revoke(' . $id . ',' . $session . ')' ] , NULL , 404 );
            }

            $revoke = $this->container->sessions->revokeID( $session ) ;

            if( $revoke )
            {
                return $this->success( $response , $session ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'revoke' , $e->getMessage() ] , NULL , 500 );
        }

    }

    public function revokeAllForAccount( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args[ 'id' ] = $jwt->sub ;
        }
        return $this->revokeAll( $request , $response , $args ) ;
    }

    public function revokeAllListForAccount( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args[ 'id' ] = $jwt->sub ;
        }
        return $this->revokeAllList( $request , $response , $args ) ;
    }

    /**
     * The default 'put' methods options.
     */
    const ARGUMENTS_REVOKEALL_DEFAULT =
    [
        'id'    => NULL
    ] ;

    public function revokeAll( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_REVOKEALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' revokeAll(' . $id . ')' ) ;
        }

        try
        {
            // user exists
            $user = $this->container->users->get( $id , [ 'key' => 'uuid' ] ) ;
            if( !$user )
            {
                return $this->error( $response , null , '404' , null , 404 ) ;
            }

            $revoke = $this->model->revokeAll( $id ) ;

            if( $response )
            {
                return $this->success( $response , true ) ;
            }

            return true ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500' , [ 'revokeAll' , $e->getMessage() ] , NULL , 500 );
        }

    }

    public function revokeAllList( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_REVOKEALL_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' revokeAllList(' . $id . ')' ) ;
        }

        $params = $request->getParsedBody() ;

        if( $params )
        {
            if( isset( $params['list' ]) )
            {
                $list = $params[ 'list' ];

                $items = explode( ',' , $list ) ;

                // check user is superadmin or current user
                if( isset( $this->container->auth ) )
                {
                    if( $this->container->auth->team->name == "superadmin" || $this->container->auth->uuid == $id )
                    {
                        try
                        {
                            if( !$this->container->sessions->existAll( $items , [ "conditions" => [ "doc.user == '" . $id . "'" ] ] ) )
                            {
                                return $this->formatError( $response , '404', [ 'revokeAllList(' . $id . ')' ] , NULL , 404 );
                            }

                            $revoke = $this->container->sessions->revokeIDAll( $items ) ;

                            if( $revoke )
                            {
                                return $this->success( $response , $list ) ;
                            }
                        }
                        catch( Exception $e )
                        {
                            return $this->formatError( $response , '500' , [ 'revokeAllList' , $e->getMessage() ] , NULL , 500 );
                        }

                    }
                    else
                    {
                        return $this->formatError( $response , '401', [ 'revokeAllList(' . $id . ')' ] , NULL , 401 );
                    }
                }
                else
                {
                    return $this->formatError( $response , '400', [ 'revokeAllList(' . $id . ')' ] , NULL , 400 );
                }
            }
        }

        return $this->error( $response , 'no list' ) ;
    }


    /**
     * Outputs the elements.
     *
     * @param Request $request
     * @param Response $response
     * @param array $items
     * @param array $params
     * @param array $options
     *
     * @return mixed
     */
    protected function outputAll( Request $request = NULL, Response $response = NULL , $items = NULL , $params = NULL , array $options = NULL )
    {
        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        $skin = NULL ;
        $lang = NULL ;

        if( isset( $request ) )
        {
            $params = isset($params) ? $params : $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- skin

            if( !isset($skin) && array_key_exists( 'skin_default' ,$set ) )
            {
                if( array_key_exists( 'skin_all', $set)  )
                {
                    $skin = $set['skin_all'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }

            if( $skin == 'main' || !in_array( $skin , $set['skins'] ) )
            {
                $skin = NULL ;
            }
        }

        if( $items )
        {
            foreach( $items as $key => $value )
            {
                $items[$key] = $this->create( $value , $lang , $skin , $params ) ;
            }
        }

        if( $response )
        {
            return $this->success
            (
                $response,
                $items,
                $this->getFullPath( $params ) ,
                is_array($items) ? count($items) : NULL,
                $options
            );
        }

        return $items ;
    }

    private function endsWith( $haystack , $needle )
    {
        return substr( $haystack , -strlen( $needle ) ) === $needle ;
    }
}
