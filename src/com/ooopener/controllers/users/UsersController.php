<?php

namespace com\ooopener\controllers\users;

use com\ooopener\controllers\CollectionsController;
use com\ooopener\models\Users;

use com\ooopener\things\Thing;
use com\ooopener\validations\UserValidator;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;

use Exception ;


/**
 * The admin users controller.
 */
class UsersController extends CollectionsController
{
    /**
     * Creates a new UsersController instance.
     *
     * @param Container $container
     * @param Users $model
     * @param string $path
     *
     */
    public function __construct( Container $container , Users $model = NULL , $path = 'users' )
    {
        parent::__construct( $container , $model , $path );

        $this->validator = new UserValidator( $container ) ;
    }

    public $validator ;

    /**
     * The enumeration of all properties to filtering when we create a new instance.
     *
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="Resource UUID",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     *
     * @OA\Schema(
     *     schema="UserList",
     *     description="An user",
     *     type="object",
     *     allOf={
     *         @OA\Schema(ref="#/components/schemas/status"),
     *         @OA\Schema(ref="#/components/schemas/Thing"),
     *         @OA\Schema(ref="#/components/schemas/altText")
     *     },
     *     @OA\Property(property="uuid",type="string"),
     *
     *     @OA\Property(property="team",ref="#/components/schemas/TeamList"),
     *     @OA\Property(property="person",ref="#/components/schemas/PersonList"),
     *
     *     @OA\Property(property="givenName",type="string"),
     *     @OA\Property(property="familyName",type="string"),
     *
     *     @OA\Property(property="gender",ref="#/components/schemas/Thesaurus"),
     *
     *     @OA\Property(property="email",type="string"),
     *     @OA\Property(property="address",ref="#/components/schemas/PostalAddress"),
     *     @OA\Property(property="provider",type="string"),
     * )
     *
     * @OA\Schema(
     *     schema="User",
     *     type="object",
     *     allOf={@OA\Schema(ref="#/components/schemas/UserList")},
     * )
     *
     * @OA\Response(
     *     response="userResponse",
     *     description="Result of the user",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="result",ref="#/components/schemas/User")
     *     )
     * )
     *
     * @OA\Response(
     *     response="userListResponse",
     *     description="Result list of users",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="status", type="string",description="The request status",example="success"),
     *         @OA\Property(property="count",type="integer",description="Count of items"),
     *         @OA\Property(property="total",type="integer",description="Total of items"),
     *         @OA\Property(property="result",type="array",description="",items=@OA\Items(ref="#/components/schemas/UserList"))
     *     )
     * )
     *
     */
    const CREATE_PROPERTIES =
    [
        'id'            => [ 'filter' =>  Thing::FILTER_ID       ] ,
        'name'          => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'url'           => [ 'filter' =>  Thing::FILTER_DEFAULT  ] ,
        'created'       => [ 'filter' =>  Thing::FILTER_DATETIME ] ,
        'modified'      => [ 'filter' =>  Thing::FILTER_DATETIME ] ,

        'uuid'        => [ 'filter' =>  Thing::FILTER_DEFAULT    ] ,

        'team'        => [ 'filter' =>  Thing::FILTER_DEFAULT    ] ,
        'person'      => [ 'filter' =>  Thing::FILTER_JOIN       ] ,

        'permissions' => [ 'filter' =>  Thing::FILTER_DEFAULT   , 'skins' => [ 'full' ] ] ,

        'givenName'  => [ 'filter' =>  Thing::FILTER_DEFAULT    ] ,
        'familyName' => [ 'filter' =>  Thing::FILTER_DEFAULT    ] ,

        'gender'     => [ 'filter' =>  Thing::FILTER_JOIN       ] ,

        'favorites'    => [ 'filter' =>  Thing::FILTER_EDGE    , 'skins' => [ 'full' , 'favorites' ] ] ,

        'email'        => [ 'filter' =>  Thing::FILTER_DEFAULT    ] ,
        'address'      => [ 'filter' =>  Thing::FILTER_DEFAULT    ] ,
        'provider'     => [ 'filter' =>  Thing::FILTER_DEFAULT    ] ,
        'provider_uid' => [ 'filter' =>  Thing::FILTER_DEFAULT   , 'skins' => [ 'full' ] ] ,
        'image'        => [ 'filter' =>  Thing::FILTER_DEFAULT    ]
    ];

    /**
     * Create user
     *
     * @param null $init
     * @param null $lang
     * @param null $skin
     * @param null $params
     *
     * @return object
     */
    public function create( $init = NULL , $lang = NULL , $skin = NULL , $params = NULL )
    {
        if( isset( $init ) )
        {
            // set url
            $jwt = $this->container->jwt;
            if( $jwt && $jwt->sub && $jwt->sub == $init->uuid )
            {
                $init->url = $this->getBaseURL() . '/account';
            }
            else
            {
                $init->url = $this->getBaseURL() . '/users/' . $init->uuid;
            }

            $skinFull = $skin == 'full';

            $team = $this->container->teamsController->getTeam(NULL , NULL , [ 'id' => $init->team , 'lang' => $lang , 'skin' => $skinFull ? 'full' : '' ]);

            $init->team = $team;

            //////////////////////

            if( $skinFull )
            {

                // get default permission for account
                $scope = [ "account" => "admin" ];

                // get default permission for app
                $scope["app"] = "read" ;

                // get team permissions

                if( property_exists($team , 'permissions') && count($team->permissions) > 0 )
                {
                    foreach( $team->permissions as $permission )
                    {
                        $resource = ( $permission[ 'resource' ] != "0" ) ? '/' . $permission[ 'resource' ] : '';
                        $path = $permission[ 'module' ] . $resource;

                        switch( $permission[ 'permission' ] )
                        {
                            case 'A':
                                $scope[ $path ] = 'admin';
                                break;
                            case 'W':
                                $scope[ $path ] = 'write';
                                break;
                            case 'R':
                                $scope[ $path ] = 'read';
                                break;
                        }
                    }
                }


                // get user permissions

                if( property_exists($init , 'permissions') && is_countable($init->permissions) && count($init->permissions) > 0 )
                {
                    foreach( $init->permissions as $permission )
                    {
                        $resource = ( $permission[ 'resource' ] != "0" ) ? '/' . $permission[ 'resource' ] : '';
                        $path = $permission[ 'module' ] . $resource;

                        switch( $permission[ 'permission' ] )
                        {
                            case 'A':
                                $scope[ $path ] = 'admin';
                                break;
                            case 'W':
                                $scope[ $path ] = 'write';
                                break;
                            case 'R':
                                $scope[ $path ] = 'read';
                                break;
                        }
                    }
                }

                $init->scope = $scope;

                //unset($init->permissions);
            }
        }

        //////////////////////

        return $init ;
    }

    public function deleteFavorites( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug( $this . ' deleteFavorites()' ) ;
        }

        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $id = $jwt->sub;

            // check
            $params = $request->getParsedBody();

            $url = NULL;

            if( isset($params[ 'url' ]) )
            {
                $url = $params[ 'url' ];
            }

            $conditions = [ 'url' => [ $params[ 'url' ] , 'required|url' ] ];

            $this->validator->validate($conditions);

            if( $this->validator->passes() )
            {
                try
                {
                    $appUrl = $this->config[ 'app' ][ 'url' ];
                    // get url path
                    $urlPath = substr($url , strlen($appUrl));

                    // get user id
                    $init =
                    [
                        'key'    => 'uuid' ,
                        'fields' => '_id'
                    ] ;

                    $item = $this->model->get( $id , $init ) ;

                    if( $this->container->userFavorites->existEdge( $urlPath , $item->_id ) )
                    {
                        $this->container->userFavorites->deleteEdge( $urlPath , $item->_id ) ;
                    }

                    if( $response )
                    {
                        $items = $this->getFavorites() ;
                        return $this->success( $response , $items ) ;
                    }

                }
                catch( Exception $e )
                {
                    return $this->formatError($response , '500' , [ 'deleteFavorites()' , $e->getMessage() ] , NULL , 500);
                }

            }
            else
            {
                $errors = [];

                $err = $this->validator->errors();
                $keys = $err->keys();

                foreach( $keys as $key )
                {
                    $errors[ $key ] = $err->first($key);
                }

                return $this->error($response , $errors , "400");
            }
        }

        if( $response )
        {
            return $this->formatError( $response , '400' , NULL , NULL , 400 ) ;
        }

        return NULL ;
    }

    const ARGUMENTS_FAVORITES_DEFAULT =
    [
        'facets'  => NULL ,
        'lang'    => NULL ,
        'limit'   => 0 ,
        'offset'  => NULL ,
        'search'  => NULL ,
        'sort'    => NULL
    ] ;

    public function getFavorites( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_FAVORITES_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' getFavorites()' ) ;
        }

        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $id = $jwt->sub ;

            $api = $this->container->settings['api'] ;
            $set = $this->container->settings['users/favorites'] ;

            if( isset( $request ) )
            {
                $params = isset( $params ) ? $params : $request->getQueryParams();

                // ----- lang

                if( !empty($params['lang']) )
                {
                    if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                    {
                        $params['lang'] = $lang = strtolower($params['lang']) ;
                    }
                    else if( strtolower($params['lang']) == 'all' )
                    {
                        $lang = NULL ;
                    }

                }

                // ----- limit

                $limit = intval( isset( $limit ) ? $limit : $api[ 'limit_default' ] );
                if( isset( $params[ 'limit' ] ) )
                {
                    $limit             = filter_var
                    (
                        $params[ 'limit' ] ,
                        FILTER_VALIDATE_INT ,
                        [
                            'options' =>
                            [
                                "min_range" => intval( $api[ 'minlimit' ] ) ,
                                "max_range" => intval( $api[ 'maxlimit' ] )
                            ]
                        ]
                    );
                    $params[ 'limit' ] = intval( ( $limit !== FALSE ) ? $limit : $api[ 'limit_default' ] );
                }

                // ----- offset

                $offset = intval( isset( $offset ) ? $offset : $api[ 'offset_default' ] );
                if( isset( $params[ 'offset' ] ) )
                {
                    $offset             = filter_var
                    (
                        $params[ 'offset' ] ,
                        FILTER_VALIDATE_INT ,
                        [
                            'options' =>
                            [
                                "min_range" => intval( $api[ 'minlimit' ] ) ,
                                "max_range" => intval( $api[ 'maxlimit' ] )
                            ]
                        ]
                    );
                    $params[ 'offset' ] = intval( ( $offset !== FALSE ) ? $offset : $api[ 'offset_default' ] );
                }

                // ----- facets

                if( isset( $params[ 'facets' ] ) )
                {

                    try
                    {
                        if( is_string( $params['facets'] ) )
                        {
                            $facets = array_merge
                            (
                                json_decode( $params[ 'facets' ] , TRUE ) ,
                                isset( $facets ) ? $facets : []
                            );
                        }
                        else
                        {
                            $facets = $params['facets'] ;
                        }

                    }
                    catch( Exception $e )
                    {
                        $this->container->logger->warn( $this . ' all failed, the facets params failed to decode the json expression: ' . $params[ 'facets' ] );
                    }
                }

                // ----- sort

                if( isset( $params[ 'sort' ] ) )
                {
                    $sort = $params[ 'sort' ];
                }
                else if( is_null( $sort ) )
                {
                    $sort = $set[ 'sort_default' ];
                }

                // ----- search

                if( isset( $params[ 'search' ] ) )
                {
                    $search = $params[ 'search' ];
                }
            }

            try
            {
                $init =
                [
                    'facets'      => $facets ,
                    'key'         => 'uuid' ,
                    'lang'        => $lang ,
                    'limit'       => $limit ,
                    'offset'      => $offset ,
                    'search'      => $search ,
                    'sort'        => $sort
                ] ;

                $result = $this->model->getFavorites( $id , $init ) ;

                $items = $result->favorites ;
                $total = $result->total ;

                if( $response )
                {
                    $options = [ 'total' => $total ] ;

                    if( $limit != 0 )
                    {
                        $options['limit']  = $limit ;
                        $options['offset'] = $offset ;
                    }

                    return $this->success
                    (
                        $response ,
                        $items ,
                        $this->getFullPath( $params ) ,
                        is_array( $items ) ? count( $items ) : NULL ,
                        $options
                    ) ;
                }

                return $items ;
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'getFavorites()' , $e->getMessage() ] , NULL , 500 );
            }
        }

        if( $response )
        {
            return $this->formatError( $response , '400' , NULL , NULL , 400 ) ;
        }

        return NULL ;
    }

    public function getForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }

        $args['skin'] = 'full' ;

        return $this->getUuid( $request , $response , $args ) ;
    }

    /**
     * Get the current user
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     *
     */
    public function getUser( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            try
            {
                $user = $this->model->get( $jwt->sub , [ 'key' => 'uuid' , 'queryFields' => $this->getFields() ] ) ;

                if( $user )
                {
                    $user = $this->create( $user ) ;

                    return $this->success( $response , $user ) ;
                }
                else
                {
                    return $this->error( $response , "no user" , '404' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            return $this->error( $response , "error with token" , '400' ) ;
        }
    }

    /**
     * The default 'get methods options.
     */
    const ARGUMENTS_GET_USERNAME_DEFAULT =
    [
        'active'   => TRUE ,
        'id'       => NULL ,
        'item'     => NULL ,
        'lang'     => NULL ,
        'params'   => NULL ,
        'skin'     => NULL
    ] ;

    /**
     * Get a specific item reference.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return mixed
     */
    public function getUuid( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_USERNAME_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' get(' . $id . ')' ) ;
        }

        // ------------ init

        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        // ------------ Query Params

        if( isset( $request ) )
        {
            $params = $request->getQueryParams();

            // ----- lang

            if( !empty($params['lang']) )
            {
                if( in_array( strtolower($params['lang']) , $api['languages'] ) )
                {
                    $params['lang'] = $lang = strtolower($params['lang']) ;
                }
                else if( strtolower($params['lang']) == 'all' )
                {
                    $lang = NULL ;
                }
            }

            // ----- skin

            if( !isset($skin) )
            {
                if( array_key_exists( 'skin_get', $set)  )
                {
                    $skin = $set['skin_get'] ;
                }
                else if( array_key_exists( 'skin_default', $set)  )
                {
                    $skin = $set['skin_default'] ;
                }
            }

            if( !empty($params['skin']) )
            {
                if( in_array( $params['skin'] , $set['skins'] ) )
                {
                    $params['skin'] = $skin = $params['skin'] ;
                }
            }
        }

        if( $skin == 'main' || !in_array( $skin , $set['skins'] ) )
        {
            $skin = NULL ;
        }

        // ----------------

        try
        {
            $init =
            [
                'key'         => 'uuid' ,
                'active'      => $active ,
                'queryFields' => $this->getFields( $skin )
            ] ;

            $item = $this->model->get( $id , $init ) ;

            if( $item )
            {
                $item = $this->create( $item , $lang , $skin , $params ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }

        if( $response )
        {
            if( $item )
            {
                // add header
                $response = $this->container->cache->withLastModified( $response , $item->modified );

                return $this->success( $response, $item, $this->getFullPath( $params ) );
            }
            else
            {
                return $this->formatError( $response , '404' , NULL , NULL , 404 ) ;
            }
        }

        return $item ;
    }

    public function postFavorites( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        if( $response )
        {
            $this->logger->debug( $this . ' postFavorites()' );
        }

        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $id = $jwt->sub;

            // check
            $params = $request->getParsedBody();

            $url = NULL;

            if( isset($params[ 'url' ]) )
            {
                $url = $params[ 'url' ];
            }

            $conditions = [ 'url' => [ $params[ 'url' ] , 'required|url' ] ];

            $this->validator->validate($conditions);

            if( $this->validator->passes() )
            {
                try
                {
                    $appUrl = $this->config[ 'app' ][ 'url' ];
                    // get url path
                    $urlPath = substr($url , strlen($appUrl));

                    // get user id
                    $init =
                    [
                        'key'    => 'uuid' ,
                        'fields' => '_id'
                    ] ;

                    $item = $this->model->get( $id , $init ) ;

                    if( !$this->container->userFavorites->existEdge( $urlPath , $item->_id ) )
                    {
                        $this->container->userFavorites->insertEdge( $urlPath , $item->_id ) ;
                    }

                    if( $response )
                    {
                        $items = $this->getFavorites() ;
                        return $this->success( $response , $items ) ;
                    }

                }
                catch( Exception $e )
                {
                    return $this->formatError($response , '500' , [ 'postFavorites()' , $e->getMessage() ] , NULL , 500);
                }

            }
            else
            {
                $errors = [];

                $err = $this->validator->errors();
                $keys = $err->keys();

                foreach( $keys as $key )
                {
                    $errors[ $key ] = $err->first($key);
                }

                return $this->error($response , $errors , "400");
            }
        }

        if( $response )
        {
            return $this->formatError( $response , '400' , NULL , NULL , 400 ) ;
        }

        return NULL ;

    }

    public function putForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }

        return $this->put( $request , $response , $args ) ;
    }

    /**
     * The default 'get methods options.
     */
    const ARGUMENTS_PUT_DEFAULT =
    [
        'active'   => TRUE ,
        'id'       => NULL
    ] ;

    /**
     * Put user
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     *
     * @OA\RequestBody(
     *     request="putUser",
     *     @OA\MediaType(
     *         mediaType="application/x-www-form-urlencoded",
     *         @OA\Schema(
     *             @OA\Property(property="givenName",type="string",description="The name of the resource"),
     *             @OA\Property(property="familyName",type="string",description="The name of the resource"),
     *             required={"givenName","familyName"},
     *             @OA\Property(property="gender",type="integer",description="The id of the gender"),
     *             @OA\Property(property="person",type="integer",description="The id of the person"),
     *             @OA\Property(property="team",type="integer",description="The id of the team"),
     *         )
     *     ),
     *     required=true
     * )
     */
    public function put( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_PUT_DEFAULT , $args ) ) ;

        if( $response )
        {
            $this->logger->debug( $this . ' put(' . $id . ')' ) ;
        }

        // user exists
        $user = $this->model->get( $id , [ 'key' => 'uuid' ] ) ;
        if( !$user )
        {
            return $this->error( $response , null , '404' , null , 404 ) ;
        }

        $user = $this->create( $user ) ;
        $auth = $this->container->auth ;

        // SECURITY - check if user team can change selected user
        if( $auth->team->identifier > $user->team->identifier )
        {
            return $this->formatError( $response , '401' , null , null , 401 ) ;
        }

        // is you
        $isYou = false ;
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub && $jwt->sub == $id )
        {
            $isYou = true ;
        }

        // check
        $params = $request->getParsedBody() ;

        $item = [];

        if( isset( $params['givenName'] ) )
        {
            $item['givenName'] = $params['givenName'] ;
        }

        if( isset( $params['familyName'] ) )
        {
            $item['familyName'] = $params['familyName'] ;
        }

        if( isset( $params['gender'] ) )
        {
            $gender = $this->container->genders->exist( $params['gender'] ) ;
            if( $gender )
            {
                $item['gender'] = (int)$params['gender'] ;
            }
        }

        $conditions =
        [
            'givenName'  => [ $params['givenName']  , 'required|max(50)' ] ,
            'familyName' => [ $params['familyName'] , 'required|max(50)' ]
        ] ;

        $change_team = FALSE ;

        if( !$isYou )
        {
            // check team
            if( isset( $params['team'] ) && $params['team'] != '' )
            {
                if( $params['team'] != $user->team->name )
                {
                    $team = $this->container->teams->get( $params['team'] ) ;

                    // SECURITY - check if user auth can change team with selected team
                    if( $team )
                    {
                        $team = $this->container->teamsController->create( $team ) ;
                        if( $auth->team->identifier <= $team->identifier )
                        {
                            $item['team'] = $team->name ;

                            $change_team = TRUE ;
                        }

                    }
                }
            }

            // check person
            if( isset( $params['person'] ) )
            {
                if( $params['person'] != '' || $params['person'] != null )
                {
                    $item['person']       = (int)$params['person'] ;
                    $conditions['person'] = [ $params['person'] , 'person'    ] ;
                }
                else
                {
                    $item['person'] = null ;
                }
            }
            else
            {
                $item['person'] = null ;
            }
        }

        //////
        ////// security - remove sensible fields
        //////

        if( isset( $params['id'] ) )
        {
            unset( $params['id'] ) ;
        }
        if( isset( $params['uuid'] ) )
        {
            unset( $params['uuid'] ) ;
        }
        if( isset( $params['email'] ) )
        {
            unset( $params['email'] ) ;
        }
        if( isset( $params['image'] ) )
        {
            unset( $params['image'] ) ;
        }
        if( isset( $params['provider'] ) )
        {
            unset( $params['provider'] ) ;
        }

        ////// validator

        $this->validator->validate( $conditions ) ;

        if( $this->validator->passes() )
        {
            //////

            try
            {
                $result = $this->model->update( $item , $id , [ 'key' => 'uuid' , 'active' => $active ] );

                if( $result )
                {
                    if( $change_team === TRUE )
                    {
                        // revoke user sessions to apply new team immediately
                        $revoke = $this->container->sessions->revokeAll( $id ) ;
                    }
                    return $this->success( $response , $this->create( $this->model->get( $id , [ 'key' => 'uuid' , 'queryFields' => $this->getFields() ] ) ) ) ;
                }
                else
                {
                    return $this->error( $response , 'error' ) ;
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response , '500', [ 'get(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $this->validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }
    }

    public function deleteForAccount( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        // get current user
        $jwt = $this->container->jwt ;

        if( $jwt && $jwt->sub )
        {
            $args['id'] = $jwt->sub ;
        }

        return $this->delete( $request , $response , $args ) ;
    }

    /**
     * The default 'get methods options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'id' => NULL
    ] ;

    /**
     * Delete user
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function delete( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $args ) ) ;

        $this->logger->debug( $this . ' delete(' . $id . ')' ) ;

        $user = $this->container->users->get( $id , [ 'key' => 'uuid' ] ) ;

        if( !$user )
        {
            return $this->formatError( $response , '404' , null , null , 404 ) ;
        }

        $user = $this->create( $user ) ;
        $auth = $this->container->auth ;

        // SECURITY - check if user team can delete selected user
        if( $auth->team->identifier > $user->team->identifier )
        {
            return $this->formatError( $response , '401' , null , null , 401 ) ;
        }

        try
        {
            // remove user in firebase
            try
            {
                $this->container->firebase->getAuth()->deleteUser( $user->uuid ) ;
            }
            catch( Exception $e )
            {

            }

            $result = $this->model->delete( $id , [ 'key' => 'uuid' ] );

            if( $result )
            {
                // remove sessions
                $rt = $this->container->sessions->revokeAll( $id );

                return $this->success( $response , "ok" );
            }
            else
            {
                return $this->error( $response , 'not deleted' ) ;
            }
        }
        catch( Exception $e )
        {
            return $this->formatError( $response , '500', [ 'delete(' . $id . ')' , $e->getMessage() ] , NULL , 500 );
        }
    }

    /**
     * Delete all users
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function deleteAll( Request $request = NULL , Response $response = NULL , array $args = [] )
    {
        $this->logger->debug( $this . ' deleteAll()' ) ;

        $params = $request->getParsedBody() ;

        if( $params )
        {
            if( isset( $params['list' ]) )
            {
                $list = $params['list'] ;

                try
                {
                    $items = explode( ',' , $list ) ;

                    $nb = count( $items ) ;

                    // SECURITY - check if user can delete every user in the list
                    $auth = $this->container->auth ;

                    $count = $this->model->existAll( $items , 'uuid' , $auth->team->id ) ;

                    if( $nb != $count )
                    {
                        return $this->error( $response , 'error in list' ) ;
                    }

                    $result = $this->model->deleteAll( $items , [ 'key' => 'uuid' ] );

                    if( $result )
                    {
                        $this->container->sessions->revokeAllByIds( $items );

                        return $this->success( $response , "ok" );
                    }

                    return $this->error( $response , "not deleted" ) ;

                }
                catch( Exception $e )
                {
                    return $this->formatError( $response , '500', [ 'deleteAll()' , $e->getMessage() ] , NULL , 500 );
                }
            }
        }

        return $this->error( $response , 'no list' ) ;
    }

    /**
     * Output users
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param null $items
     * @param null $params
     * @param array|NULL $options
     *
     * @return null
     */
    protected function outputAll( Request $request = NULL , Response $response = NULL , $items = NULL , $params = NULL , array $options = NULL )
    {
        $api = $this->container->settings['api'] ;
        $set = $this->container->settings[$this->path] ;

        $format = $this->container['format'] ;
        $lang   = NULL ;
        $skin   = NULL ;

        $count  = NULL ;
        $limit  = NULL ;
        $offset = NULL ;
        $total  = NULL ;

        if( !isset($params) )
        {
            $params = isset( $request ) ? $request->getQueryParams() : []  ;
        }

        // ----- lang

        if( !empty($params['lang']) )
        {
            if( in_array( strtolower($params['lang']) , $api['languages'] ) )
            {
                $params['lang'] = $lang = strtolower($params['lang']) ;
            }
            else if( strtolower($params['lang']) == 'all' )
            {
                $lang = NULL ;
            }
            else
            {
                unset($params['lang']) ;
            }
        }

        // ----- skin

        if( !isset($skin) && array_key_exists( 'skin_default' ,$set ) )
        {
            if( array_key_exists( 'skin_all', $set)  )
            {
                $skin = $set['skin_all'] ;
            }
            else if( array_key_exists( 'skin_default', $set)  )
            {
                $skin = $set['skin_default'] ;
            }
        }

        if( !empty($params['skin']) )
        {
            if( in_array( $params['skin'] , $set['skins'] ) )
            {
                $params['skin'] = $skin = $params['skin'] ;
            }
            else
            {
                unset($params['skin']) ;
            }
        }


        if( $items )
        {
            foreach( $items as $key => $value )
            {
                $items[$key] = $this->create( $value , $lang , $skin , $params ) ;
            }
        }

        if( $response )
        {
            switch( $format )
            {
                case "html" :

                    if( is_array( $options ) )
                    {
                        if( array_key_exists( 'count' , $options ) )
                        {
                            $count = $options['count'] ;
                            unset( $options['count'] ) ;
                        }

                        if( array_key_exists( 'limit' , $options ) )
                        {
                            $limit = $options['limit'] ;
                            unset( $options['limit'] ) ;
                        }

                        if( array_key_exists( 'offset' , $options ) )
                        {
                            $offset = $options['offset'] ;
                            unset( $options['offset'] ) ;
                        }

                        if( array_key_exists( 'total' , $options ) )
                        {
                            $total = $options['total'] ;
                            unset( $options['total'] ) ;
                        }
                    }

                    return $this->render
                    (
                        $response ,
                        'users/all.twig' ,
                        [
                            //'api'    => $api['url'],
                            'limit'  => $limit,
                            'offset' => $offset,
                            'count'  => $count,
                            'total'  => $total,
                            'users'  => $items
                        ]
                    ) ;
                    break ;
                case "json" :
                default     :
                {
                    return $this->success
                    (
                        $response ,
                        $items ,
                        $this->getFullPath( $params ) ,
                        is_array($items) ? count($items) : NULL,
                        $options
                    );
                }
            }
        }

        return $items ;
    }
}


