<?php

namespace com\ooopener\data ;

interface Builder
{
    /**
     * Build a new object.
     *
     * @param object $init
     * @param array $options
     */
    public function build( $init = NULL , array $options = [] ) ;
}
