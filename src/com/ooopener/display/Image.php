<?php

namespace com\ooopener\display;

use Exception ;
use Imagick;
use ImagickException;
use ImagickDraw ;
use ImagickPixel ;

use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use graphics\display\Icon;
use graphics\geom\AspectRatio ;

/**
 * The image class.
 */
class Image
{
    /**
     * Creates a new Image instance.
     *
     * @param Container $container
     * @param object $init
     */
    public function __construct( Container $container , $init = NULL )
    {
        $this->container = $container ;
        if( isset( $init ) )
        {
            foreach( $init as $key => $value )
            {
                if( property_exists( $this , $key ) )
                {
                    $this->$key = $value ;
                }
            }
        }
    }

    // ---------------------------------------

    /**
     * The 'circle' overlay constant.
     */
    const OVERLAY_CIRCLE = 'circle' ;

    /**
     * The 'rounded' overlay constant.
     */
    const OVERLAY_ROUNDED = 'rounded' ;

    // ---------------------------------------

    /**
     * The container reference.
     */
    public $container ;

    /**
     * The default image compression of the output image.
     */
    public $compression = Imagick::COMPRESSION_JPEG ;

    /**
     * The logger reference.
     */
    public $logger ;

    /**
     * The default circle settings.
     */
    public $circleMask = '/assets/circle.svg' ;

    /**
     * The max height of the image.
     */
    public $maxheight = 1080 ;

    /**
     * The max width of the image.
     */
    public $maxwidth  = 1920 ;

    /**
     * The root path of the image (server side).
     */
    public $root ;

    // ---------------------------------------

    /**
     * Check the minimum resolution
     *
     * @param string $path
     * @param integer $w
     * @param integer $h
     * @param null|object $crop
     *
     * @return bool
     */
    public function checkResolution( $path , $w , $h , $crop = NULL )
    {
        if( isset( $crop ) )
        {
            if( $crop->width < $w || $crop->height < $h )
            {
                return false ;
            }
        }
        else
        {
            $img = new Imagick( $path ) ;

            if( $img->getImageWidth() < $w || $img->getImageHeight() < $h )
            {
                return false ;
            }
        }

        return true ;
    }

    // ---------------------------------------

    /**
     * Crop the image and resize if necessary
     *
     * @param string $path
     * @param object $crop
     * @param string $destination
     * @param int $w
     * @param int $h
     *
     * @return bool
     */
    public function crop( $path , $crop , $destination , $w = 0 , $h = 0 )
    {
        try
        {
            $img = new Imagick() ;

            $img->readImage( $path ) ;
            $img->cropImage( $crop->width , $crop->height , $crop->x , $crop->y ) ;

            // resize if necessary
            if( $w != 0 || $h != 0 )
            {
                $img->thumbnailImage( $w , $h , true ) ;
            }

            $img->setImageFormat( "png" ) ;
            $img->writeImage( $destination ) ;

            return true ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' crop() failed, throwing an error:' . $e->getMessage() ) ;
            return false ;
        }

    }

    // ---------------------------------------

    /**
     * Display a specific image.
     *
     * @param Response $response
     * @param Imagick $image
     * @param integer $quality
     * @param bool $gray
     * @param string $format
     * @param bool $strip
     *
     * @return mixed
     */
    public function display( Response $response = NULL , Imagick $image = NULL , $quality = 70 , $gray = FALSE , $format = 'jpg' , $strip = FALSE )
    {
        if( $image )
        {
            if( (bool) $gray )
            {
                $image->modulateImage(100,0,100);
            }

            if( !empty($format) )
            {
                $image->setImageFormat( $format )  ;
            }

            $image->setImageCompression( $this->compression );
            $image->setImageCompressionQuality( $quality );

            if( $strip )
            {
                $image->stripImage();
            }

            if( $response )
            {
                $body = $response->getBody() ;
                $body->write($image->getImageBlob()) ;
                return $response->withHeader( 'Content-Type' , 'image/' . $image->getImageFormat())
                                ->withHeader( 'Content-Length' , strlen( $image->__toString() ) )  ;
            }

            return $image ;
        }
    }

    // ---------------------------------------

    /**
     * The default resize method arguments.
     */
    const DEFAULT_GET_OPTIONS =
    [
        'format'  => 'jpg' ,
        'quality' => 70    ,
        'gray'    => FALSE ,
        'strip'   => TRUE
    ] ;

    /**
     * Output the image.
     *
     * @param Response $response
     * @param string $url
     * @param array $init
     *
     * @return mixed
     */
    public function get( Response $response = NULL , $url = NULL , array $init = [] )
    {
        extract( array_merge( self::DEFAULT_GET_OPTIONS , $init ) ) ;

        try
        {
            $image = new Imagick( $url ) ;

            return $this->display( $response , $image , $quality , $gray , $format , $strip ) ;
        }
        catch( ImagickException $e )
        {
            $this->container->logger->warn( $this . ' get failed, could not load the image with the url:[' . $url . '], the following ImagickException occured: ' . $e->getMessage() );
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' get failed, could not load the image with the url:[' . $url . '], the following Exception occured: ' . $e->getMessage() );
        }
    }

    // ---------------------------------------

    /**
     * The default icon method arguments.
     */
    const DEFAULT_ICON_OPTIONS =
    [
        'background' => NULL ,
        'bgcolor'    => NULL , // 'ffffff'
        'color'      => NULL , // '000000'
        'format'     => 'png' ,
        'quality'    => 70    ,
        'gray'       => FALSE ,
        'w'          => 30    ,
        'h'          => 30    ,
        'margin'     => 2     ,
        'render'     => NULL  , // 'map'
        'strip'      => FALSE ,
        'shadow'     => NULL    // ex: '60,2,20,20'
    ] ;

    /**
     * Display an icon with the specific svg file.
     * Ex: ../icon?render=map&bgcolor=ff0000&color=ffffff&margin=4&shadow=40,2,20,20
     *
     * @param Response $response
     * @param string $url
     * @param array $init
     *
     * @return mixed
     */
    public function icon( Response $response = NULL , $url = NULL , array $init = [] )
    {
        extract( array_merge( self::DEFAULT_ICON_OPTIONS , $init ) ) ;

        try
        {
            $icon = new Imagick() ;
            $icon->readImage( $url ) ;

            $icons = $this->container->settings['icons'] ;
            $map   = $this->container->settings['map'] ;

            if( $render == 'map' )
            {

                $background = Icon::createIcon
                (
                    $icons['root'] . $map['icon'] ,
                    $w , $h ,
                    !empty($bgcolor) ? $bgcolor : $icons['bgcolor']
                ) ;

                if( $margin > 0 )
                {
                    $w -= ($margin * 2) ;
                    $h -= ($margin * 2) ;
                }
            }

            $icon->thumbnailImage( $w , $h , true ) ;

            if( isset( $background ) )
            {
                $background->compositeImage
                (
                    $icon,
                    Imagick::COMPOSITE_DEFAULT,
                    ( $background->getImageWidth() - $icon->getImageWidth() ) / 2 ,
                    ( $background->getImageWidth() - $icon->getImageHeight() ) / 2
                );

                $background->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN ) ;
                $icon = $background ;
            }

            if( !empty($shadow) )
            {
                if( $shadow == 'true' )
                {
                    $shadow = $icons['shadow'] ; // default shadow
                }
                $shadow = explode( ',' , $shadow ) ;
                if( is_array( $shadow ) && ( count($shadow) == 4 ) )
                {
                    try
                    {
                        $shadowIcon = clone $icon ;
                        call_user_func_array(array( $shadowIcon , 'shadowImage' ) , $shadow ) ;
                        $shadowIcon->compositeImage( $icon, Imagick::COMPOSITE_DEFAULT, 0, 0 );
                        $icon = $shadowIcon ;
                    }
                    catch( Exception $e  )
                    {
                        $this->container->logger->warn( $this . ' icon(' . $url . ') failed, the following Exception occured: ' . $e->getMessage() );
                    }
                }
            }

            return $this->display
            (
                $response ,
                $icon,
                $quality , $gray , $format, $strip
            ) ;
        }
        catch( ImagickException $e )
        {
            $this->container->logger->warn( $this . ' icon(' . $url . ',' . $w . ',' . $h . ') failed, the following ImagickException occured: ' . $e->getMessage() );
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' icon(' . $url . ',' . $w . ',' . $h . ') failed, the following Exception occured: ' . $e->getMessage() );
        }
    }

    /**
     * The default icon method arguments.
     */
    const DEFAULT_PHOTO_OPTIONS =
    [
        'format'     => 'jpeg'   ,
        'gray'       => FALSE    ,
        'quality'    => 70       ,
        'mode'       => 'normal' ,
        'w'          => 128      ,
        'h'          => 128      ,
        'render'     => NULL     ,// 'map'
        'strip'      => FALSE
    ] ;

    public function photo( Response $response = NULL , $url = NULL , array $init = [] )
    {
        extract( array_merge( self::DEFAULT_ICON_OPTIONS , $init ) ) ;

        try
        {
            $image = new Imagick() ;
            $image->readImage( $url ) ;

            if( $mode == "thumb" )
            {
                $image->thumbnailImage( $w , $h , true ) ;
            }

            return $this->display
            (
                $response ,
                $image,
                $quality , $gray , $format , $strip
            ) ;
        }
        catch( ImagickException $e )
        {
            $this->container->logger->warn( $this . ' icon(' . $url . ',' . $w . ',' . $h . ') failed, the following ImagickException occured: ' . $e->getMessage() );
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' icon(' . $url . ',' . $w . ',' . $h . ') failed, the following Exception occured: ' . $e->getMessage() );
        }
    }

    /**
     * Display an icon with the specific svg file.
     * Ex: ../icon?render=map&bgcolor=ff0000&color=ffffff&margin=4&shadow=40,2,20,20
     *
     * @param Response $response
     * @param string $url
     * @param array $init
     *
     * @return mixed
     */
    public function old_icon( Response $response = NULL , $url = NULL , array $init = [] )
    {
        extract( array_merge( self::DEFAULT_ICON_OPTIONS , $init ) ) ;

        try
        {
            $icons = $this->container->settings['icons'] ;
            $map   = $this->container->settings['map'] ;

            if( $render == 'map' )
            {

                $background = Icon::createIcon
                (
                    $icons['root'] . $map['icon'] ,
                    $w , $h ,
                    !empty($bgcolor) ? $bgcolor : $icons['bgcolor']
                ) ;

                if( $margin > 0 )
                {
                    $w -= ($margin * 2) ;
                    $h -= ($margin * 2) ;
                }
            }

            $icon = Icon::createIcon( $url , $w , $h , $color ) ;

            if( isset( $background ) )
            {
                $background->compositeImage
                (
                    $icon,
                    Imagick::COMPOSITE_DEFAULT,
                    ( $background->getImageWidth() - $icon->getImageWidth() ) / 2 ,
                    ( $background->getImageWidth() - $icon->getImageHeight() ) / 2
                );

                $background->flattenImages();
                $icon = $background ;
            }

            if( !empty($shadow) )
            {
                if( $shadow == 'true' )
                {
                    $shadow = $icons['shadow'] ; // default shadow
                }
                $shadow = explode( ',' , $shadow ) ;
                if( is_array( $shadow ) && ( count($shadow) == 4 ) )
                {
                    try
                    {
                        $shadowIcon = clone $icon ;
                        call_user_func_array(array( $shadowIcon , 'shadowImage' ) , $shadow ) ;
                        $shadowIcon->compositeImage( $icon, Imagick::COMPOSITE_DEFAULT, 0, 0 );
                        $icon = $shadowIcon ;
                    }
                    catch( Exception $e  )
                    {
                        $this->container->logger->warn( $this . ' icon(' . $url . ') failed, the following Exception occured: ' . $e->getMessage() );
                    }
                }
            }

            return $this->display
            (
                $response ,
                $icon,
                $quality , $gray , $format, $strip
            ) ;
        }
        catch( ImagickException $e )
        {
            $this->container->logger->warn( $this . ' icon(' . $url . ',' . $w , ',' . $h . ') failed, the following ImagickException occured: ' . $e->getMessage() );
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' icon(' . $url . ',' . $w , ',' . $h . ') failed, the following Exception occured: ' . $e->getMessage() );
        }
    }

    // ---------------------------------------

    /**
     * The default resize method arguments.
     */
    const DEFAULT_RESIZE_OPTIONS =
    [
        'format'  => 'jpg' ,
        'quality' => 70    ,
        'gray'    => FALSE ,
        'h'       => NULL  ,
        'w'       => NULL  ,
        'strip'   => TRUE
    ] ;

    /**
     * Resize a specific image.
     *
     * @param Response $response
     * @param string $url
     * @param array $init
     *
     * @return mixed
     */
    public function resize( Response $response = NULL , $url = NULL , array $init = [] )
    {
        extract( array_merge( self::DEFAULT_RESIZE_OPTIONS , $init ) ) ;

        try
        {
            $image     = new Imagick( $url ) ;
            $geo       = $image->getImageGeometry() ;
            $width     = $geo['width'];
            $height    = $geo['height'];

            if( ( $width > $this->maxwidth ) || ( $height > $this->maxheight ) )
            {
                $image->resizeImage
                (
                    $this->maxwidth, $this->maxheight, Imagick::FILTER_LANCZOS, 1.1, TRUE
                ) ;
            }

            $checkW = is_int($w) && ( (int) $w > 0 ) ;
            $checkH = is_int($h) && ( (int) $h > 0 ) ;

            if( $checkW && $checkH )
            {
                $image->resizeImage( $w, $h, Imagick::FILTER_LANCZOS, 1.1, TRUE );
            }
            else if( $checkW || $checkH )
            {
                $ratio = new AspectRatio( $width , $height , TRUE ) ;

                if( $checkW > 0 )
                {
                    $ratio->setWidth( $w ) ;
                }
                else if( $checkH > 0 )
                {
                    $ratio->setHeight( $h ) ;
                }

                $image->resizeImage
                (
                    $ratio->getWidth(),
                    $ratio->getHeight(),
                    Imagick::FILTER_LANCZOS,
                    1.1,
                    TRUE
                );
            }

            return $this->display( $response , $image , $quality , $gray , $format , $strip ) ;
        }
        catch( ImagickException $e )
        {
            $this->container->logger->warn( $this . ' resize failed, could not load the image with the url:[' . $url . '], the following ImagickException occured: ' . $e->getMessage() );
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' resize failed, could not load the image with the url:[' . $url . '], the following Exception occured: ' . $e->getMessage() );
        }
    }

    // ---------------------------------------

    /**
     * The default thumb method arguments.
     */
    const DEFAULT_THUMB_OPTIONS =
    [
        'corners' => [ TRUE , TRUE , TRUE , TRUE ] , // array
        'h'       => 0 , // int
        'format'  => 'jpg' , // string
        'quality' => 70 , // int
        'gray'    => FALSE , // bool
        'overlay' => NULL , // string
        'padding' => 0 , // int
        'radius'  => 24 , // int
        'w'       => 0 , // int
        'strip'   => TRUE // bool
    ] ;

    /**
     * Creates the thumb of a specific image.
     *
     * @param Response $response
     * @param string $url
     * @param array $init
     *
     * @return mixed
     */
    public function thumb( Response $response = NULL , $url = NULL , array $init = [] )
    {
        extract( array_merge( self::DEFAULT_THUMB_OPTIONS , $init ) ) ;

        // -------

        switch( $overlay )
        {
            case self::OVERLAY_CIRCLE :
            case self::OVERLAY_ROUNDED :
            {
                break ; // do nothing
            }
            default :
            {
                $overlay = NULL ;
            }
        }

        if( !is_int($w) )       $w = 0 ;
        if( !is_int($h) )       $h = 0 ;
        if( !is_int($padding) ) $padding = 0 ;
        if( !is_int($radius)  ) $radius = 0 ;

        // -------

        try
        {
            $image  = new Imagick( $url ) ;
            $geo    = $image->getImageGeometry() ;
            $width  = $geo["width"];
            $height = $geo["height"];

            if( $w > 0 && $h > 0 )
            {
                $image->resizeImage( $w, $h, Imagick::FILTER_LANCZOS, 1.1, TRUE );
            }

            $mask = NULL ;

            $image->cropThumbnailImage( $w , $h );

            switch( $overlay )
            {
                case self::OVERLAY_CIRCLE : // ?w=300&h=300&overlay=circle
                {
                    $format   = 'png' ;
                    $maskFile = $this->root . $this->circleMask ;
                    if( file_exists( $maskFile ) )
                    {
                        $mask = Icon::createIcon( $maskFile , $w , $h ) ;
                    }
                    break ;
                }

                case self::OVERLAY_ROUNDED : // ?w=716&h=500&overlay=rounded&radius=42&corners=false,TRUE,false,false
                {
                    $format = 'png' ;

                    $mask = new Imagick();
                    $mask->newImage( $w , $h , new ImagickPixel('transparent') );

                    $draw = new ImagickDraw();
                    $draw->setFillColor('Black');

                    $padding = 0 ;

                    $draw->roundRectangle( $padding, $padding, $w+$padding, $h+$padding, $radius, $radius );

                    if( $corners[0] == FALSE ) // tl
                    {
                        $draw->rectangle( $padding, $padding , $radius + $padding , $radius + $padding ) ;
                    }

                    if( $corners[1] == FALSE ) // tr
                    {
                        $draw->rectangle( $w - $padding - $radius , $padding , $w - $padding , $radius - $padding ) ;
                    }

                    if( $corners[2] == FALSE ) // br
                    {
                        $draw->rectangle( $w - $padding - $radius , $h - $padding - $radius, $w - $padding , $h - $padding ) ;
                    }

                    if( $corners[3] == FALSE ) // bl
                    {
                        $draw->rectangle( $padding , $h - $padding - $radius, $radius + $padding , $h - $padding ) ;
                    }

                    $mask->drawImage( $draw );

                    break ;
                }
            }

            if( $mask )
            {
                $image->compositeImage( $mask , Imagick::COMPOSITE_DSTIN , 0, 0, Imagick::CHANNEL_ALPHA);
                $mask->clear() ;
            }

            return $this->display( $response , $image , $quality , $gray , $format , $strip ) ;
        }
        catch( ImagickException $e )
        {
            $this->container->logger->warn( $this . ' thumb failed, could not load the image with the url:[' . $url . '], the following ImagickException occured: ' . $e->getMessage() );
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' thumb failed, could not load the image with the url:[' . $url . '], the following Exception occured: ' . $e->getMessage() );
        }
    }

    /**
    * Returns a String representation of the object.
    * @return string A string representation of the object.
    */
    public function __toString() /*String*/
    {
        return '[' . get_class($this) . ']' ;
    }
}


