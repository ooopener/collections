<?php

namespace com\ooopener\handlers ;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use Slim\Container;
use Slim\Handlers\Error;

use Exception;

final class ErrorHandler extends Error
{
    /**
     * ErrorHandler constructor.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct();
        $this->container = $container ;
    }

    /**
     * @var Container
     */
    private $container;

    /**
     * @param Request $request
     * @param Response $response
     * @param Exception $exception
     *
     * @return Response
     */
    public function __invoke( Request $request, Response $response, Exception $exception)
    {
        $this->container->logger->critical( $exception->getMessage() );

        return parent::__invoke($request, $response, $exception);
    }
}
