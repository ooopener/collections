<?php

namespace com\ooopener\handlers ;


use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use Slim\Container;
use Slim\Handlers\NotFound ;

final class NotFoundHandler extends NotFound
{
    /**
     * NotFoundHandler constructor.
     *
     * @param Container $container
     * @param null $page
     */
    public function __construct( Container $container , $page = NULL )
    {
        $this->container = $container ;
        $this->page = $page ;
    }

    /**
     * The optional '404' template page view.
     */
    public $page ;

    /**
     * Invoked when a not found handler is catched.
     *
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function __invoke( Request $request, Response $response )
    {
        parent::__invoke( $request , $response ) ;

        $config  = $this->container->settings ;
        $logger  = $this->container->logger ;
        $tracker = $this->container->tracker ;

        $message = $config['errors']['404'] ;

        if( (bool) $config['useLogging'] && isset($logger) )
        {
            $logger->error( $message . ' - ' . $request->getUri() );
        }

        if( isset($tracker) )
        {
            $pre = (bool) $config['analytics']['useVersion']
                 ? '/' . $config['version']
                 : '' ;

            $tracker->pageview( $pre . '/errors/404' , $message . ' - ' . $request->getUri() );
        }

        if( isset($this->page) )
        {
            return $this->container->view->render
            (
                $response->withStatus(404) ,
                $this->page
            );
        }
        else
        {
            return $response->withJson
            (
                [
                    'status' => 'error' ,
                    'code'   => '404' ,
                    'message' => $message
                ]
                ,
                404 , $this->container->jsonOptions
            );
        }
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class($this) . ']' ;
    }
}
