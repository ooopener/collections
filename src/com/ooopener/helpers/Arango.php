<?php

namespace com\ooopener\helpers;

use ArangoDBClient\Collection as ArangoCollection;
use ArangoDBClient\CollectionHandler as ArangoCollectionHandler;
use ArangoDBClient\Connection as ArangoConnection;
use ArangoDBClient\ConnectionOptions as ArangoConnectionOptions;
use ArangoDBClient\DocumentHandler as ArangoDocumentHandler;
use ArangoDBClient\Document as ArangoDocument;
use ArangoDBClient\Exception as ArangoException;
use ArangoDBClient\ConnectException as ArangoConnectException;
use ArangoDBClient\ClientException as ArangoClientException;
use ArangoDBClient\ServerException as ArangoServerException;
use ArangoDBClient\Statement as ArangoStatement;
use ArangoDBClient\UpdatePolicy as ArangoUpdatePolicy;

use com\ooopener\controllers\Controller;
use Slim\Container;

class Arango extends Controller
{

    public $collection ;
    public $collectionHandler ;
    public $connection ;
    public $document ;
    public $documentHandler ;

    public $statement ;

    /**
     * Arango constructor.
     *
     * @param Container $container
     *
     * @throws ArangoException
     */
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $settings = $container['settings']['arangoDB'] ;

        $connectionOptions =
        [
            // database name
            ArangoConnectionOptions::OPTION_DATABASE => $settings['database'] ,
            // server endpoint to connect to
            ArangoConnectionOptions::OPTION_ENDPOINT => $settings['endpoint'] ,
            // authorization type to use (currently supported: 'Basic')
            ArangoConnectionOptions::OPTION_AUTH_TYPE => $settings['auth_type'] ,
            // user for basic authorization
            ArangoConnectionOptions::OPTION_AUTH_USER => $settings['auth_user'] ,
            // password for basic authorization
            ArangoConnectionOptions::OPTION_AUTH_PASSWD => $settings['auth_passwd'] ,
            // connection persistence on server. can use either 'Close' (one-time connections) or 'Keep-Alive' (re-used connections)
            ArangoConnectionOptions::OPTION_CONNECTION => $settings['connection'] ,
            // connect timeout in seconds
            ArangoConnectionOptions::OPTION_TIMEOUT => $settings['timeout'] ,
            // whether or not to reconnect when a keep-alive connection has timed out on server
            ArangoConnectionOptions::OPTION_RECONNECT => true ,
            // optionally create new collections when inserting documents
            ArangoConnectionOptions::OPTION_CREATE => true ,
            // optionally create new collections when inserting documents
            ArangoConnectionOptions::OPTION_UPDATE_POLICY => ArangoUpdatePolicy::LAST
        ];

        if( array_key_exists( 'debug' , $settings ) && $settings['debug'] == TRUE )
        {
            ArangoException::enableLogging() ;
        }

        try
        {
            $this->connection = new ArangoConnection( $connectionOptions ) ;

            $this->collectionHandler = new ArangoCollectionHandler( $this->connection ) ;
            $this->collection        = new ArangoCollection() ;

            $this->documentHandler = new ArangoDocumentHandler( $this->connection ) ;
            $this->document        = new ArangoDocument() ;

            $this->statement = new ArangoStatement( $this->connection , [] ) ;

            if( !$this->collectionHandler->has('rateLimit') )
            {
                // create rateLimit collection
                $this->collection->setName( 'rateLimit' ) ;
                $this->collectionHandler->create( $this->collection , [ 'isVolatile' => true ] ) ;
            }
        }
        catch( ArangoConnectException $e )
        {
            $this->logger->error( $this. ' Connection error: ' . $e->getMessage() . PHP_EOL ) ;
        }
        catch( ArangoClientException $e )
        {
            $this->logger->error( $this. ' Client error: ' . $e->getMessage() . PHP_EOL ) ;
        }
        catch( ArangoServerException $e )
        {
            $this->logger->error( $this.  'Server error: ' . $e->getServerCode() . ': ' . $e->getServerMessage() . ' - ' . $e->getMessage() . PHP_EOL ) ;
        }

        if( array_key_exists( 'batchSize' , $settings ) )
        {
            $this->batchSize = $settings['batchSize'] ;
        }
        else
        {
            $this->batchSize = 10000 ;
        }

    }

    private $cursor ;
    private $data ;

    private $batchSize;

    /**
     * Check if collection exists
     * @param $name
     * @return bool
     */
    public function collectionExists( $name )
    {
        try
        {
            return $this->collectionHandler->has( $name ) ;
        }
        catch( ArangoException $e )
        {
            return false ;
        }
    }

    /**
     * @throws ArangoException
     */
    public function execute()
    {
        $this->statement = new ArangoStatement
        (
            $this->connection ,
            $this->data
        ) ;

        $this->cursor = $this->statement->execute() ;
    }

    public function getAll()
    {
        $result = $this->getResult() ;

        if( $result && is_array( $result ) )
        {
            foreach( $result as $key => $value )
            {
                $result[$key] = (object) $value ;
            }
        }
        else
        {
            $result = [] ;
        }

        return $result ;
    }

    public function getFirstResult()
    {
        if( $this->getResult() )
        {
            return $this->getResult()[0] ;
        }
        else
        {
            return null ;
        }
    }

    public function getCursor()
    {
        return $this->cursor ;
    }

    public function getExtra()
    {
        return $this->cursor->getExtra() ;
    }

    public function getFoundRows()
    {
        return $this->cursor->getFullCount() ;
    }

    public function getObject()
    {
        if( $this->getFirstResult() )
        {
            return (object) $this->getFirstResult() ;
        }
        else
        {
            return null ;
        }
    }

    public function getResult()
    {
        $result = $this->cursor->getMetadata()['result'] ;

        if( $result && is_array( $result ) && count( $result ) > 0 )
        {
            return $result ;
        }
        else
        {
            return null ;
        }
    }

    public function lastInsertId()
    {
        return $this->getFirstResult() ;
    }

    /**
     * @param array $data
     */
    public function prepare( $data = [] )
    {
        $data['batchSize'] = $this->batchSize ;
        $this->data = $data ;
    }

}
