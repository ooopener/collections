<?php

namespace com\ooopener\helpers;

use Exception ;

/*
use admin\gds\helpers\DayOfWeek;

$dayOfWeek = new DayOfWeek() ;

echo $dayOfWeek->format(['Mo'] , true ) . '<br>';
echo $dayOfWeek->format(['Tu'] , true ) . '<br>';
echo $dayOfWeek->format(['We'] , true ) . '<br>';
echo $dayOfWeek->format(['Th'] , true ) . '<br>';
echo $dayOfWeek->format(['Fr'] , true ) . '<br>';
echo $dayOfWeek->format(['Sa'] , true ) . '<br>';
echo $dayOfWeek->format(['Su'] , true ) . '<br>';
echo $dayOfWeek->format(['Ph'] , true ) . '<br>';
echo $dayOfWeek->format(['Sh'] , true ) . '<br>';

echo '------ <br>';

echo $dayOfWeek->format(['Ph','Sh'] , true ) . '<br>';
echo $dayOfWeek->format(['Sh','Ph'] , true ) . '<br>';

echo '------ <br>';

echo $dayOfWeek->format(['Mo','Tu','We','Th','Fr'] , true ) . '<br>';
echo $dayOfWeek->format(['Sa','Su']                , true ) . '<br>';
echo $dayOfWeek->format(['Mo','Tu','We','Th','Fr','Sa','Su'] , true ) . '<br>';


echo $dayOfWeek->format(['Mo', 'Tu' , 'We','Sa'] ) . '<br>';
echo $dayOfWeek->format(['Mo', 'Tu' , 'We',  'Th' , 'Fr' , 'Sa','Su','Sh','Ph'] , true ) . '<br>';

echo '------ <br>';

echo json_encode($this->app->dayOfWeek->parse('Mo')) . '<br>';
echo json_encode($this->app->dayOfWeek->parse('Mo-We')) . '<br>';
echo json_encode($this->app->dayOfWeek->parse('Mo-We,Tu,Th,Fr,Sa-Su,Sh,Ph')) . '<br>';
echo json_encode
(
    $this->app->dayOfWeek->format
    (
        $this->app->dayOfWeek->parse('Mo-We,Th,Sa-Su,Sh,Ph')
    )
) . '<br>';

*/

/**
 * The DayOfWeek class.
 */
class DayOfWeek
{
    /**
     * Creates a new DayOfWeek instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        if( isset( $init ) )
        {
            foreach ($init as $key => $value)
            {
                if( property_exists( $this , $key ) )
                {
                    $this->{ $key } = $value ;
                }
            }
        }
    }

    /**
     * The days identifiers.
     */
    const days = [ 'Mo' , 'Tu' , 'We' , 'Th' , 'Fr' , 'Sa' , 'Su' ] ;

    /**
     * The special days identifiers.
     */
    const specials = [ 'Ph' , 'Sh' ] ;

    /**
     * The string day of weeks representations.
     */
    public $strings =
    [
        'all'  => 'Everyday' ,
        'days' =>
        [
            'Mo' => [ 'name' => 'Monday'          , 'short' => 'Mo' ] ,
            'Tu' => [ 'name' => 'Tuesday'         , 'short' => 'Tu' ] ,
            'We' => [ 'name' => 'Wednesday'       , 'short' => 'We' ] ,
            'Th' => [ 'name' => 'Thursday'        , 'short' => 'Th' ] ,
            'Fr' => [ 'name' => 'Friday'          , 'short' => 'Fr' ] ,
            'Sa' => [ 'name' => 'Saturday'        , 'short' => 'Sa' ] ,
            'Su' => [ 'name' => 'Sunday'          , 'short' => 'Su' ] ,
            'Ph' => [ 'name' => 'Public holidays' , 'short' => 'Ph' ] ,
            'Sh' => [ 'name' => 'School holidays' , 'short' => 'Sh' ]
        ]
        ,
        'none'     => '' ,
        'specials' => '%s / %s' ,
        'week'     => 'Working days' ,
        'weekend'  => 'Week end'
    ];

    /**
     * Format a collection (Array) of days.
     *
     * @param array $days
     * @param bool $humanReadble
     *
     * @return mixed
     */
    public function format( $days , $humanReadble = FALSE )
    {
        if( !$days || !is_array($days) || count($days) === 0 )
        {
            return ($humanReadble === true) ? $this->strings->none : '' ;
        }

        $exp  = '' ;
        $pos  = NULL ;
        $pre  = NULL ;
        $next = NULL ;
        $len  = count($days) ;

        for( $i = 0 ; $i < $len ; $i++ )
        {
            if( $pre )
            {
                $exp .= ',' ;
                $pre = NULL ;
            }

            $cur = $days[$i] ;
            $pos = array_search( $cur , self::days ) ;

            if( $pos !== FALSE )
            {
                $exp .= $cur ;

                if( array_key_exists( $i+1 , $days ) )
                {
                    $next = $days[$i+1] ;
                    if( $next && (array_search( $next , self::days ) == ($pos+1)) )
                    {
                        do
                        {
                            $cur = $next ;
                            $i++ ;
                            $pos++ ;
                            $next = array_key_exists( $i+1 , $days ) ? $days[$i+1] : NULL ;
                        }
                        while( $next && array_search( $next , self::days ) == ($pos+1) ) ;

                        $exp .= '-' . $cur ;
                    }
                }

                $pre = $cur ;
            }
            else if( array_search( $cur , self::specials ) !== FALSE )
            {
                $pos = array_search( $cur , self::specials )  ;
                $exp .= $cur ;
                $pre = $cur ;
            }
        }

        return $humanReadble ? $this->stringify($exp) : $exp ;
    }

    /**
     * Parse the passed-in expression.
     *
     * @param string $expression
     *
     * @return array
     */
    public function parse( $expression /*String*/ )
    {
        $collector = [] ;
        $days      = [] ;

        if( $expression && is_string($expression) && $expression != ''  )
        {
            $items = explode(',' , $expression ) ;

            $len = count($items) ;

            foreach( $items as $item )
            {
                if
                (
                    array_search( $item , self::days ) !== FALSE ||
                    array_search( $item , self::specials ) !== FALSE
                )
                {
                    $collector[$item] = TRUE ;
                }
                else if( strpos( $item , '-' ) !== FALSE )
                {
                    $elements = explode( '-', $item ) ;
                    if( count($elements) == 2 )
                    {
                        $first = array_search( $elements[0] , self::days ) ;
                        $last  = array_search( $elements[1] , self::days ) ;
                        if( ($first !== FALSE) && ($last !== FALSE) && ($last > $first) )
                        {
                            $subdays = array_slice( self::days , $first , $last+1-$first ) ;
                            foreach( $subdays as $d )
                            {
                                $collector[$d] = TRUE ;
                            }
                        }
                    }
                }
            }
        }

        foreach( self::days as $day )
        {
            if( array_key_exists( $day , $collector ) )
            {
                $days[] = $day ;
            }
        }

        foreach( self::specials as $day )
        {
            if( array_key_exists( $day , $collector ) )
            {
                $days[] = $day ;
            }
        }

        return $days ;
    }

    /**
     * Stringify the specific expression.
     * @see $strings
     *
     * @param string $exp
     *
     * @return string
     */
    public function stringify( $exp )
    {
        try
        {
            switch( $exp )
            {
                case '' :
                {
                    return $this->strings['none'] ;
                }
                case 'Mo-Fr' :
                {
                    return $this->strings['week'] ;
                }
                case 'Sa-Su' :
                {
                    return $this->strings['weekend'] ;
                }
                case 'Mo-Su' :
                {
                    return $this->strings['all'] ;
                }
                case 'Sh,Ph' :
                case 'Ph,Sh' :
                {
                    return sprintf
                    (
                        $this->strings['specials'],
                        $this->strings['days']['Ph']['name'],
                        $this->strings['days']['Sh']['name']
                    );
                }
                default :
                {
                    if( array_key_exists( $exp , $this->strings['days'] ) )
                    {
                        return $this->strings['days'][$exp]['name'] ;
                    }

                    foreach( self::days as $cur )
                    {
                        $exp = str_replace( $cur , $this->strings['days'][$cur]['short'] , $exp ) ;
                    }

                    foreach( self::specials as $cur )
                    {
                        $exp = str_replace( $cur , $this->strings['days'][$cur]['short'] , $exp  ) ;
                    }
                }
            }
        }
        catch (Exception $e)
        {
            //
        }

        return $exp ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class($this) . ']' ;
    }
}

