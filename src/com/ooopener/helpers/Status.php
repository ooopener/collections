<?php

namespace com\ooopener\helpers;

/**
 * Class Status
 * @package com\ooopener\helpers
 *
 */
class Status
{
    const ANONYMIZED = 'anonymized' ;

    const ARCHIVED = 'archived' ;

    const DRAFT = 'draft' ;

    const PUBLISHED = 'published' ;

    const REJECTED = 'rejected' ;

    const UNPUBLISHED = 'unpublished' ;

    const UNDER_REVIEW = 'under_review' ;
}
