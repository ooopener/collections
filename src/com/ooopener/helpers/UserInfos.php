<?php

namespace com\ooopener\helpers ;

use Slim\Container;

/**
 * Class UserInfos
 * @package com\ooopener\helpers
 */
class UserInfos
{
    /**
     * UserInfos constructor.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
    }

    /**
     * Get ip
     *
     * @return mixed
     */
    public function getIp()
    {
        // get user ip
        if ( isset( $_SERVER['HTTP_CF_CONNECTING_IP'] ) )
        {
            $ip = $_SERVER['HTTP_CF_CONNECTING_IP'] ;
        }
        else if( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) )
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'] ;
        }
        else if( isset( $_SERVER['HTTP_X_REAL_IP'] ) )
        {
            $ip = $_SERVER['HTTP_X_REAL_IP'] ;
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'] ;
        }

        //


        return $this->canonicalIP( $ip ) ;
    }

    /**
     * Get user agent
     *
     * @return mixed
     */
    public function getUserAgent()
    {
        return $_SERVER['HTTP_USER_AGENT'] ;
    }

    private function canonicalIP( $addr )
    {
        // Known prefix
        $v4mapped_prefix_hex = '00000000000000000000ffff';
        $v4mapped_prefix_bin = pack("H*", $v4mapped_prefix_hex);

        // Or more readable when using PHP >= 5.4
        # $v4mapped_prefix_bin = hex2bin($v4mapped_prefix_hex);

        // Parse
        $addr_bin = inet_pton( $addr );
        if( $addr_bin === FALSE )
        {
            // Unparsable? How did they connect?!?
            //die('Invalid IP address');
            return $addr ;
        }

        // Check prefix
        if( substr( $addr_bin , 0 , strlen( $v4mapped_prefix_bin ) ) == $v4mapped_prefix_bin )
        {
            // Strip prefix
            $addr_bin = substr( $addr_bin , strlen( $v4mapped_prefix_bin ) ) ;
        }

        // Convert back to printable address in canonical form
        return inet_ntop( $addr_bin ) ;
    }
}
