<?php

namespace com\ooopener\middlewares ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;
use Slim\Container;

class ActivityLog
{
    /**
     * ActivityLog constructor.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        $this->container = $container ;
    }

    /**
     * @var Container
     */
    private $container ;

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     *
     * @return Response
     */
    public function __invoke( Request $request , Response $response , $next )
    {
        $response = $next( $request , $response ) ;

        $code   = $response->getStatusCode() ;
        $method = $request->getMethod() ;

        $passthrough = $this->container->settings['token']['passthrough'];

        $path  = $request->getUri()->getPath() ;

        if( $code == 200 && !$this->checkPassthroughPath( '/' . $path , $passthrough ) && ( $method == "POST" || $method == "PUT" || $method == "PATCH" || $method == "DELETE" ) )
        {

            switch( $method )
            {
                case "DELETE" :
                    if( $request && $request->getParsedBody() && array_key_exists( 'list' , $request->getParsedBody() ) )
                    {
                        $param = $request->getParsedBody()['list'] ;
                        $list = explode( ',' , $param ) ;
                        if( $list && count( $list ) > 0 )
                        {
                            foreach( $list as $key => $value )
                            {
                                $this->saveActivityLog( $method , $path . '/' . $value ) ;
                            }
                        }
                    }
                    else
                    {
                        $this->saveActivityLog( $method , $path ) ;
                    }
                    break ;
                case "POST" :
                    $body = json_decode( $response->getBody() ) ;
                    if( $body && property_exists( $body , 'result' ) && property_exists( $body->result , 'id' ) )
                    {
                        $path .= '/' . $body->result->id ;
                    }
                    $this->saveActivityLog( $method , $path ) ;
                    break ;
                default :
                    $this->saveActivityLog( $method , $path ) ;
                    break ;
            }
        }

        return $response ;
    }

    private function checkPassthroughPath( string $uri , array $list )
    {
        foreach( $list as $passthrough )
        {
            $passthrough = rtrim($passthrough, "/") ;
            if( preg_match("@^{$passthrough}(/.*)?$@" , $uri ) )
            {
                return true ;
            }
        }
        return false ;
    }

    private function saveActivityLog( $method , $resource )
    {
        try
        {
            $ip    = $this->container->userInfos->getIp() ;
            $agent = $this->container->userInfos->getUserAgent() ;

            $item =
            [
                'active'   => 1,
                'path'     => 'activityLogs',
                'user'     => $this->container->jwt->sub ,
                'method'   => $method ,
                'ip'       => $ip ,
                'agent'    => $agent ,
                'resource' => $resource
            ];

            $this->container->activityLogs->insert( $item ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warning( " ActivityLogs => " . $e->getMessage() ) ;
        }
    }
}
