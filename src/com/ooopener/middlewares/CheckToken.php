<?php

namespace com\ooopener\middlewares ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use Slim\Container ;

use com\ooopener\controllers\Controller ;

/**
 * Class CheckToken
 *
 * @package com\ooopener\middlewares
 */
class CheckToken extends Controller
{
    /**
     * CheckToken constructor.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct($container);
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param $next
     *
     * @return Response
     */
    public function __invoke( Request $request , Response $response , $next )
    {
        // ----------- before

        // do nothing

        // ----------- now

        $jwt = $this->container->jwt ;

        if( $jwt )
        {
            try
            {
                $agent = $this->container->userInfos->getUserAgent() ;
                $check = $this->container->sessions->check( $jwt->jti , $agent ) ;

                if( !$check )
                {
                    // revoke token if necessary
                    $this->container->sessions->revoke( $jwt->jti ) ;

                    // check path
                    $path = $request->getUri()->getPath() ;
                    if( $path == "oauth/authorize" )
                    {
                        // save to redirect
                        $fullUrl = (string) $request->getUri() ;

                        $_SESSION[ "urlRedirect" ] = $fullUrl ;

                        // go to login
                        return $response->withRedirect( $this->container->router->pathFor( "api.logout" ) ) ;
                    }
                    else if( $path != "login" )
                    {
                        switch( $this->container['format'] )
                        {
                        case 'json' :
                            return $this->error($response, "token revoked", "401", null, 401 ) ;
                        default :
                            $fullUrl = (string) $request->getUri() ;

                            if( $path == "/" )
                            {
                                $fullUrl = substr( $fullUrl , 0 , -1 ) ;
                            }
                            $_SESSION[ "urlRedirect" ] = $fullUrl ;

                            // go to login
                            return $response->withRedirect( $this->container->router->pathFor("api.logout") ) ;
                        }

                    }

                }
            }
            catch( Exception $e )
            {
                return $this->formatError($response, "500", [ $this , $e->getMessage() ], false, 500);
            }
        }

        // ----------- after

        $response = $next( $request , $response ) ;

        // ----------- end

        return $response ;
    }

}
