<?php

namespace com\ooopener\middlewares ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\Controller;
use Slim\Container;

class Cors extends Controller
{
    public function __construct( Container $container )
    {
        parent::__construct( $container );
    }

    public function __invoke( Request $request , Response $response , $next )
    {
        $origin  = '*' ;
        $headers = 'X-Requested-With, Content-Type, Accept, Origin, Authorization, Expires, Last-Modified, If-Modified-Since' ;
        $maxAge  = 600 ;

        if( isset( $this->config['cors'] ) )
        {
            $cors = $this->config['cors'] ;
            if( isset( $cors['origin'] ) ) $origin = $cors['origin'] ;
            if( isset( $cors['headers'] ) ) $headers = $cors['headers'] ;
            if( isset( $cors['maxAge'] ) ) $maxAge = $cors['maxAge'] ;
        }

        $response = $response->withHeader( "Access-Control-Allow-Origin" , $origin ) ;

        $method = $request->getMethod() ;

        if( $method != 'OPTIONS' )
        {
            $response = $next($request, $response) ;
        }
        else
        {
            $route = $request->getAttribute("route") ;

            $methods = [] ;

            if ( !empty( $route ) )
            {
                $pattern = $route->getPattern() ;

                foreach ( $this->container->router->getRoutes() as $route )
                {
                    if ( $pattern === $route->getPattern() )
                    {
                        $methods = array_merge_recursive( $methods , $route->getMethods() ) ;
                    }
                }
                //Methods holds all of the HTTP Verbs that a particular route handles.
            }
            else
            {
                $methods[] = $request->getMethod() ;
            }

            $response = $response->withHeader( "Access-Control-Allow-Methods" , implode( "," , $methods ) ) ;
            $response = $response->withHeader( "Access-Control-Allow-Headers" , $headers ) ;
            $response = $response->withHeader( "Access-Control-Max-Age" , $maxAge ) ;
        }

        return $response ;
    }
}