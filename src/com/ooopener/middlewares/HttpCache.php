<?php

namespace com\ooopener\middlewares ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container;

class HttpCache
{
    /**
     * HttpCache constructor.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        $this->container = $container ;
    }

    /**
     * @var Container
     */
    private $container ;

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     *
     * @return Response
     */
    public function __invoke( Request $request , Response $response , $next )
    {
        $response = $next( $request , $response ) ;

        // add expires for png images
        if( $response->hasHeader( 'Content-Type' ) && ( $response->getHeader( 'Content-Type' )[0] == "image/png" ) )
        {
            $response = $response->withHeader( 'Expires' , gmdate('D, d M Y H:i:s T' , time() + 3600 ) ) ;
            $response = $response->withHeader( 'Vary' , 'Accept' ) ;
            $response = $response->withHeader( 'Cache-Control' , 'max-age=2592000' ) ;
        }

        return $response ;
    }
}