<?php

namespace com\ooopener\middlewares;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use com\ooopener\controllers\Controller ;

/**
 * The LogStatus class.
 */
class LogStatus extends Controller
{
    /**
     * Creates a new LogStatus instance.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct( $container );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     *
     * @return Response
     */
    public function __invoke( Request $request , Response $response , $next )
    {
        // ----------- before

        // do nothing

        // ----------- now

        $response = $next( $request , $response ) ;

        // ----------- after

        $uri  = $request->getUri() ;
        $code = $response->getStatusCode();
        $set  = $this->container->settings;

        if( (bool) $set['useLogging'] )
        {
            /*
            getScheme()
            getAuthority()
            getUserInfo()
            getHost()
            getPort()
            getPath()
            getBasePath()
            getQuery() (returns the full query string, e.g. a=1&b=2)
            getFragment()
            getBaseUrl()
            */
            $this->container->logger->info( 'HTTP ' . $code . ' ' . $uri->getPath() );
        }

        if( $request->isGet() && (bool) $set['useAnalytics'] )
        {
            $pre = (bool) $set['analytics']['useVersion'] ? '/' . $set['version'] : '' ;
            $this->container->tracker->pageview( $pre . '/status/' . $code , $uri );
        }

        // ----------- end

        return $response ;
    }
}

