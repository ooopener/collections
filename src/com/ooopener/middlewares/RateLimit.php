<?php

namespace com\ooopener\middlewares ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container;

use com\ooopener\controllers\Controller;

class RateLimit extends Controller
{
    public $expire ;
    public $maxRequests ;

    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->container = $container ;

        $settings = $this->config['rateLimit'] ;

        $this->expire = $settings['time'] ;
        $this->maxRequests = $settings['max_requests'] ;
    }

    public function __invoke( Request $request , Response $response , $next )
    {
        // ----------- before

        // check rate limit
        $method = $request->getMethod() ;
        $path = $request->getUri()->getPath() ;

        $passthrough = $this->config['token']['passthrough'] ;
        $passthroughAuthorized = $this->config['token']['passthroughAuthorized'] ;

        $passthrough = array_merge( $passthrough , $passthroughAuthorized ) ;

        if( $method == 'GET' && !in_array( '/' . $path , $passthrough ) )
        {
            $keys = $this->container->arango->getRateLimit( $this->expire ) ;
            $requestCount = count( $keys ) ;

            // reset time
            if( $requestCount > 0 )
            {
                $reset = round( $keys[0] / 1000 ) + ( $this->expire * 60 ) ;
            }
            else
            {
                $reset = time() + ( $this->expire * 60 ) ;
            }

            if( $path == "rate_limit" )
            {
                $data =
                [
                    "limit"     => $this->expire ,
                    "remaining" => $this->maxRequests - $requestCount ,
                    "reset"     => $reset
                ];

                return $response->withJson( $data , 200 , $this->container->jsonOptions );
            }

            if( $requestCount == $this->maxRequests )
            {
                return $this->error
                (
                    $response , "Rate Limit" , 429 , NULL , 429
                );
            }
        }

        // ----------- now

        $response = $next( $request , $response ) ;

        // ----------- after

        // update rate limit
        $code   = $response->getStatusCode() ;

        if( $method == 'GET' && $code == 200 && !in_array( '/' . $path , $passthrough ) )
        {

            $result = $this->container->arango->setRateLimit( $response ) ;

            if( $result == TRUE )
            {
                $response = $response->withHeader( 'X-RateLimit-Limit' , $this->expire ) ;
                $response = $response->withHeader( 'X-RateLimit-Reset' , $reset ) ;
                $response = $response->withHeader( 'X-RateLimit-Remaining' , $this->maxRequests - $requestCount - 1 ) ;
            }
            else if( $result == FALSE )
            {
                // error with token id
            }
            else
            {
                return $result ;
            }

        }

        // ----------- end

        return $response ;

    }
}