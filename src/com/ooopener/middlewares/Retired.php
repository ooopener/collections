<?php

namespace com\ooopener\middlewares;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Slim\Container ;

use com\ooopener\controllers\Controller ;

/**
 * Stops the application if is retired.
 */
class Retired extends Controller
{
    /**
     * Creates a new Retired instance.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct( $container );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     *
     * @return Response
     */
    public function __invoke( Request $request , Response $response , $next )
    {
        // ----------- before

        if( (bool) $this->config['retired'] )
        {
            return $this->formatError
            (
                $response , '301' ,
                [
                    $this->config[ 'version' ] ,
                    $this->config[ 'next'    ]
                ]
            );
        }

        // ----------- now

        $response = $next( $request , $response ) ;

        // ----------- after

        // do nothing

        // ----------- end

        return $response ;
    }
}

