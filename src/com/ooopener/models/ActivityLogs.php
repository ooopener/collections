<?php

namespace com\ooopener\models ;

use com\ooopener\things\Thing;

use Slim\Container;

class ActivityLogs extends Collections
{
    /**
     * ActivityLogs constructor.
     *
     * @param Container|NULL $container
     * @param null $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'            => Thing::FILTER_INT,
        'user'          => Thing::FILTER_DEFAULT,
        'method'        => Thing::FILTER_DEFAULT,
        'resource'      => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'ip'            => Thing::FILTER_DEFAULT,
        'agent'         => Thing::FILTER_DEFAULT
    ];

    ///////////////////////////
}