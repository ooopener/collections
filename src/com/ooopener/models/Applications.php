<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * This service to manage the applications table.
 */
class Applications extends Collections
{
    /**
     * Creates a new Applications instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'            => Thing::FILTER_INT,
        'name'          => Thing::FILTER_DEFAULT,

        'active'        => Thing::FILTER_INT,
        'path'          => Thing::FILTER_DEFAULT,

        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,
        'slogan'              => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,

        'version'             => Thing::FILTER_DEFAULT,

        'oAuth'         => Thing::FILTER_DEFAULT,
        'setting'       => Thing::FILTER_DEFAULT,

        'owner'         => Thing::FILTER_INT,

        'audios'        => Thing::FILTER_DEFAULT,
        'photos'        => Thing::FILTER_DEFAULT,
        'videos'        => Thing::FILTER_DEFAULT,

        'image'         => Thing::FILTER_INT
    ];

    ///////////////////////////

    /**
     * Returns the specific application representation.
     *
     * @param $client_id
     * @param $client_secret
     *
     * @return object The specific application representation.
     */
    public function getByClientCredentials( $client_id , $client_secret )
    {
        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.oAuth.client_id == @client_id && doc.oAuth.client_secret == @client_secret RETURN doc' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "client_id"     => $client_id ,
                "client_secret" => $client_secret
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Returns the specific application representation by property.
     *
     * @param string $name
     * @param mixed $value
     * @param string $filter
     *
     * @return object The specific application representation by property.
     */
    public function getByProperty( $name , $value , $filter = null  )
    {
        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $name . ' == @value RETURN doc' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "value" => $value
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Check if application is whitelisted
     *
     * @param string $client_id
     *
     * @return bool
     */
    public function isWhitelisted( $client_id )
    {
        $query = 'RETURN COUNT( FOR doc IN whitelist_applications FILTER doc.client_id == @value RETURN 1 )' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ "value" => $client_id ]
        ]);

        $this->container->arango->execute() ;

        return (bool) $this->container->arango->getFirstResult() ;
    }
}


