<?php

namespace com\ooopener\models;

use com\ooopener\helpers\Status;
use Slim\Container ;

/**
 * The collections model.
 */
class Collections extends Model
{
    /**
     * Creates a new Collections instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'      => TRUE,
        'conditions'  => [],
        'facets'      => NULL,
        'fetchStyle'  => NULL,
        'limit'       => 0,
        'offset'      => 0,
        'sort'        => NULL,
        'groupBy'     => NULL,
        'search'      => NULL,
        'fields'      => '*' ,
        'lang'        => NULL,
        'queryFields' => NULL ,
        'orders'      => NULL ,
        'groups'      => NULL,
        'values'      => NULL,
        'withStatus'  => Status::PUBLISHED,
        'words'       => NULL
    ] ;

    /**
     * Returns all elements.
     *
     * @param array $init
     *
     * @return array All elements.
     */
    public function all( $init = [] )
    {
        // -------- INIT
        $this->container->logger->debug( $this . ' all' ) ;

        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $init ) ) ;

        $binds = [] ;

        $query = 'FOR doc IN ' . $this->table ;

        // -------- WHERE

        $search = $this->getQuerySearch( $search , $this->searchable ) ;

        if( isset($search) )
        {
            extract( $search ) ;
        }

        if( !empty($search) )
        {
            $conditions[] = $search ;
        }

        if( isset( $words ) )
        {
            $i = 0 ;
            foreach( $words as $word )
            {
                $binds[ 'search' . $i++ ] = '%' . $word . '%' ;
            }
        }

        if( isset($active) && !is_null($active) )
        {
            $conditions[] = 'doc.active == ' . ((bool) $active ? '1' : '0') ;
        }

        extract( $this->getQueryFacets( $facets ) ) ; // $facets / values
        if( is_string($facets) && !empty($facets) )
        {
            $conditions[] = $facets ;
        }

        if( isset($values) && is_array($values) ) // see facets
        {
            foreach( $values as $key => $bindValue )
            {
                $binds[$key] = $bindValue['value'] ;
            }
        }

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- GROUP BY

        $groups = $this->getQueryGroups( $groupBy ) ;
        if( isset($groups) && count($groups) > 0 )
        {
            $query .= ' COLLECT ' ;

            foreach( $groups as $value )
            {
                $query .= $value ;
                $query .= ", " ;
            }

            $query = rtrim( $query , ', ' ) ;

            $query .= ' ' ;
        }

        // -------- ORDER BY

        $orders = $this->getQueryOrders( $sort ) ;
        if( isset($orders) && count($orders) > 0 )
        {
            $query .= ' SORT ' ;

            foreach( $orders as $value )
            {
                $query .= array_shift($orders) ;
                $query .= ", " ;
            }

            $query = rtrim( $query , ', ' ) ;

            $query .= ' ' ;
        }

        // -------- LIMIT / OFFSET

        if( $limit > 0 )
        {
            $query .= ' LIMIT @offset , @limit' ;

            $binds['offset'] = $offset ;
            $binds['limit'] = $limit ;
        }

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getAll() ;
    }

    /**
     * The default 'count' methods options.
     */
    const ARGUMENTS_COUNT_DEFAULT =
    [
        'active'     => TRUE,
        'conditions' => [],
        'facets'     => NULL,
        'search'     => NULL,
        'values'     => NULL,
        'withStatus' => Status::PUBLISHED,
        'words'      => NULL
    ] ;

    /**
     * Returns the number of users.
     *
     * @param array $init
     *
     * @return integer The number of users.
     */
    public function count( array $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_COUNT_DEFAULT , $init ) ) ;


        $query = 'RETURN COUNT ( FOR doc IN ' . $this->table . ' ' ;

        $binds = [] ;

        // -------- WHERE

        $search = $this->getQuerySearch( $search , $this->searchable ) ;

        if( isset($search) )
        {
            extract( $search ) ;
        }

        if( !empty($search) )
        {
            $conditions[] = $search ;
        }

        if( isset( $words ) )
        {
            $i = 0 ;
            foreach( $words as $word )
            {
                $binds[ 'search' . $i++ ] = '%' . $word . '%' ;
            }
        }

        if( isset($active) && !is_null($active) )
        {
            $conditions[] = 'doc.active == ' . ((bool) $active ? '1' : '0') ;
        }

        extract( $this->getQueryFacets( $facets ) ) ; // $facets / values
        if( is_string($facets) && !empty($facets) )
        {
            $conditions[] = $facets ;
        }

        if( isset($values) && is_array($values) ) // see facets
        {
            foreach( $values as $key => $bindValue )
            {
                $binds[$key] = $bindValue['value'] ;
            }
        }

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // end query

        $query .= ' RETURN 1 )' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getFirstResult() ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'active'      => NULL,
        'conditions'  => [],
        'extraQuery'  => '',
        'fields'      => '*' ,
        'key'         => '_key',
        'lang'        => NULL,
        'queryFields' => NULL,
        'withStatus'  => Status::PUBLISHED
    ] ;

    /**
     * Returns the specific item.
     *
     * @param mixed $value
     * @param array $init
     *
     * @return object The specific item.
     */
    public function get( $value , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $init ) ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ' ;

        if( !is_null($active) )
        {
            $query .= ' && doc.active == ' . ((bool) $active ? 1 : 0) ;
        }

        // extra query
        $query .= $extraQuery ;

        if( count($conditions) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "value" => $value
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    const ARGUMENTS_GET_ARRAY_ALL_DEFAULT =
    [
        'active'      => TRUE,
        'conditions'  => [],
        'facets'      => NULL,
        'fetchStyle'  => NULL,
        'key'         => '_key',
        'limit'       => 0,
        'offset'      => 0,
        'sort'        => NULL,
        'groupBy'     => NULL,
        'search'      => NULL,
        'field'       => 'array' ,
        'lang'        => NULL,
        'queryFields' => NULL ,
        'orders'      => NULL ,
        'groups'      => NULL,
        'values'      => NULL,
        'withStatus'  => Status::PUBLISHED,
        'words'       => NULL
    ] ;

    public function getArrayJoinAll( $value , $modelJoin , $joinFields , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_GET_ARRAY_ALL_DEFAULT , $init ) ) ;

        $binds = [ "value" => $value ] ;
        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ' ;

        if( !is_null($active) )
        {
            $query .= ' && doc.active == ' . ((bool) $active ? 1 : 0) ;
        }

        // -------- LIMIT / OFFSET

        $queryLimit = '' ;

        if( $limit > 0 )
        {
            $queryLimit = 'LIMIT @offset , @limit ' ;

            $binds['offset'] = $offset ;
            $binds['limit'] = $limit ;
        }

        ////

        $ref = 'ref_' . $field ;
        $docJoin = 'doc_' . $field ;

        // retrieve position name
        $position = $joinFields['position']['unique'] ;

        // num
        $num = 'num' . ucfirst( $field ) ;

        $filterJoinFields = $this->getFilterFields( $joinFields , $field , $lang ) ;

        $query .= ' LET array_join = ( ' .
            'FOR ' . $ref . ' IN doc.' . $field . ' ' .
            $queryLimit .
            'LET ' . $position . ' = POSITION( doc.' . $field . ' , ' . $ref . ' , true ) ' .
            'FOR ' . $docJoin . ' IN ' . $modelJoin . ' ' .
            'FILTER ' . $docJoin . '._key == TO_STRING( ' . $ref . ' ) ' .
            'RETURN { ' . $filterJoinFields . ' } ' .
            ' )' ;

        if( count($conditions) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- EDGES && FIELDS

        $query .= 'RETURN { ' . $field . ' : array_join , ' . $num . ': LENGTH( doc.' . $field . ' ) } ' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_UPDATE_DEFAULT =
    [
        'active' => NULL,
        'key'    => '_key'
    ] ;

    /**
     * Update the specific item.
     *
     * @param mixed $item
     * @param integer|string $keyValue
     * @param array $init
     *
     * @return bool
     */
    public function update( $item , $keyValue , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_UPDATE_DEFAULT , $init ) ) ;

        $fields = [] ;
        $binds  = [ 'key' => $keyValue ] ;

        foreach( $this->fillable as $property => $filter )
        {
            if
            (
                is_array( $item ) && array_key_exists( $property , $item )
            )
            {
                $fields[] = $property . ': @' . $property ;
                $binds[ $property ] = $item[$property] ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
                    ' UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'conditions' => [],
        'key'        => '_key'
    ] ;

    /**
     * Delete the specific item
     *
     * @param string $value
     * @param array $init
     *
     * @return mixed
     */
    public function delete( $value , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ';

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // remove
        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Delete items
     *
     * @param $items
     * @param array $init
     *
     * @return bool
     */
    public function deleteAll( $items , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ) ;

        $in = [] ;
        $binds = [] ;

        foreach( $items as $keyB => $value )
        {
            $in[] = 'doc.' . $key . ' == @i' . $keyB ;
            $binds['i' . $keyB] = $value ;
        }

        $query = 'FOR doc IN ' . $this->table . ' FILTER ( ' . implode(' OR ' , $in ) . ' ) ' ;

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        //
        $query .= ' REMOVE { _key : doc._key } IN ' . $this->table . ' RETURN OLD';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getAll() ;

    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_MULTI_FIELD_DEFAULT =
    [
        'field'  => NULL,
        'key'    => '_key',
        'num'    => NULL,
    ] ;

    public function deleteInMultiField( $id , $idField , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_MULTI_FIELD_DEFAULT , $init ) ) ;

        $fields = [] ;
        $binds =
        [
            'key' => $id ,
            $field => $idField
        ];

        $q = '';
        if( array_key_exists( $field , $this->fillable ) )
        {
            $q = 'LET newMultiField = REMOVE_VALUE( doc.' . $field . ' , @' . $field.' ) ' ;
            $fields[] = $field . ': newMultiField' ;
            if( array_key_exists( $num , $this->fillable ) )
            {
                $fields[] = $num . ': LENGTH( newMultiField )' ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
            $q .
            ' UPDATE doc WITH { ' . implode( ', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    public function deleteReverseInMultiField( $id , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_MULTI_FIELD_DEFAULT , $init ) ) ;

        $fields = [] ;
        $binds =
        [
            $field => $id
        ];

        $q = '';
        if( array_key_exists( $field , $this->fillable ) )
        {
            $q = 'LET newMultiField = REMOVE_VALUE( doc.' . $field . ' , @' . $field.' ) ' ;
            $fields[] = $field . ': newMultiField' ;
            if( array_key_exists( $num , $this->fillable ) )
            {
                $fields[] = $num . ': LENGTH( newMultiField )' ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER POSITION( doc.hasPart , @' . $field . ') ' .
            $q .
            ' UPDATE doc WITH { ' . implode( ', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;

    }

    /**
     * The default 'deleteListItem' method options.
     */
    const ARGUMENTS_DELETE_LIST_ITEM_DEFAULT =
    [
        'conditions' => [],
        'key'        => '_key'
    ] ;

    public function deleteListItem( $owner , $value , $keyList , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_LIST_ITEM_DEFAULT , $init ) ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @owner ' ;

        $fields = [] ;

        // update

        $fields[] = '@keyList: REMOVE_VALUE( doc.@keyList , @value )' ;
        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query .= ' UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'owner'   => $owner ,
                'value'   => $value ,
                'keyList' => $keyList
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    public function deleteListItemAll( $owner , $items , $keyList , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @owner ' ;

        $fields = [] ;

        // update

        $fields[] = '@keyList: REMOVE_VALUES( doc.@keyList , TO_ARRAY( @items ) )' ;
        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query .= ' UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'owner'   => $owner ,
                'items'   => $items ,
                'keyList' => $keyList
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_EXISTS_MULTI_FIELD_DEFAULT =
    [
        'field'  => NULL,
        'key'    => '_key'
    ] ;

    public function existsInMultiField( $value , $idField , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_EXISTS_MULTI_FIELD_DEFAULT , $init ) ) ;

        $query = 'RETURN COUNT( FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value && POSITION( doc.' . $field . ' , @field ) ' ;

        $query .= ' RETURN 1 )';


        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "value" => $value,
                "field" => $idField
            ]
        ]);

        $this->container->arango->execute() ;

        return (bool) $this->container->arango->getFirstResult() ;
    }

    /**
     * Insert a new item into the table
     *
     * @param array $init
     *
     * @return mixed
     */
    public function insert( $init )
    {
        if( $init && is_array( $init ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add dates
            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table . ' RETURN NEW';

            $this->container->arango->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->container->arango->execute() ;

            return $this->container->arango->getObject() ;
        }
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_INSERT_MULTI_FIELD_DEFAULT =
    [
        'active' => NULL,
        'field'  => NULL,
        'key'    => '_key',
        'num'    => NULL,
        'side'   => 'left'
    ] ;

    public function insertInMultiField( $id , $idField , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_INSERT_MULTI_FIELD_DEFAULT , $init ) ) ;

        $fields = [] ;
        $binds =
        [
            'key' => $id ,
            $field => $idField
        ];

        $q = '';
        if( array_key_exists( $field , $this->fillable ) )
        {
            if( $side == 'right' )
            {
                $s = 'PUSH' ;
            }
            else
            {
                $s = 'UNSHIFT' ;
            }

            $q = 'LET newMultiField = ' . $s . '( doc.' . $field . ' , @' . $field.' , true ) ' ;

            $fields[] = $field . ': newMultiField ' ;
            if( array_key_exists( $num , $this->fillable ) )
            {
                $fields[] = $num . ': LENGTH( newMultiField )' ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
            $q .
            ' UPDATE doc WITH { ' . implode( ', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * The default 'insertListItems' method options.
     */
    const ARGUMENTS_INSERT_LIST_ITEMS_DEFAULT =
    [
        'conditions' => [],
        'key'        => '_key'
    ] ;

    public function insertListItems( $owner , $items , $keyList , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_INSERT_LIST_ITEMS_DEFAULT , $init ) ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @owner ' ;

        $fields = [] ;

        // update

        $fields[] = '@keyList: APPEND( doc.@keyList , TO_ARRAY( @items ) , true )' ;
        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query .= ' UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'owner'   => $owner ,
                'items'   => $items ,
                'keyList' => $keyList
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_UPDATE_MULTI_FIELD_DEFAULT =
    [
        'field'    => NULL,
        'key'      => '_key',
        'num'      => NULL,
        'position' => 0
    ] ;

    public function updateDateParentMultiField( $id , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_INSERT_MULTI_FIELD_DEFAULT , $init ) ) ;

        $binds =
        [
            'value' => $id
        ] ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER POSITION( doc.' . $key . ' , @value ) ' .
            ' UPDATE doc WITH { modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    public function updateInMultiField( $id , $idField , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_INSERT_MULTI_FIELD_DEFAULT , $init ) ) ;

        $fields = [] ;
        $binds =
        [
            'key'      => $id ,
            $field     => $idField,
            'position' => $position
        ];

        $q = '';
        if( array_key_exists( $field , $this->fillable ) )
        {
            $q = ' LET newField = REMOVE_VALUE( doc.' . $field . ' , @' . $field.' ) '  .
                'LET startField = SLICE( newField , 0 , @position ) ' .
                'LET endField = SLICE( newField , @position ) ' .
                'LET newMultiField = UNION( PUSH( startField , @' . $field . ' , true ) , endField ) ' ;

            $fields[] = $field . ': newMultiField' ;
            if( array_key_exists( $num , $this->fillable ) )
            {
                $fields[] = $num . ': LENGTH( newMultiField )' ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
            $q .
            ' UPDATE doc WITH { ' . implode( ', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }
}


