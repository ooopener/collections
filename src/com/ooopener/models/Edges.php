<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;
use Slim\Container;

/**
 * This class is the generic Edge class.
 */
class Edges extends Model
{
    /**
     * Creates a new Edge instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    function __construct( Container $container , string $table = NULL , array $init = [] )
    {
        parent::__construct($container , $table , $init);
    }

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_EDGES_DEFAULT =
    [
        'active'      => TRUE,
        'conditions'  => [],
        'limit'       => 0,
        'offset'      => 0,
        'lang'        => NULL,
        'orders'      => NULL ,
        'skin'        => NULL,
        'skinFrom'    => NULL,
        'sort'        => NULL
    ] ;

    public function allEdges( $init = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_EDGES_DEFAULT , $init ) ) ;

        $binds = [] ;

        $query = 'FOR doc IN ' . $this->to['name'] ;

        if( isset($active) && !is_null($active) )
        {
            $conditions[] = 'doc.active == ' . ((bool) $active ? '1' : '0') ;
        }

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- ORDER BY

        $orders = $this->getQueryOrders( $sort ) ;
        if( isset($orders) && count($orders) > 0 )
        {
            $query .= ' SORT ' ;

            $query .= implode( ', ' , $orders ) ;

            $query .= ' ' ;
        }

        // -------- LIMIT / OFFSET

        if( $limit > 0 )
        {
            $query .= ' LIMIT @offset , @limit' ;

            $binds['offset'] = $offset ;
            $binds['limit'] = $limit ;
        }

        // query from
        $fieldsFrom = $this->container[$this->from['controller']]->getFields( $skinFrom ) ;

        $queryFrom = '';

        if( isset($active) && !is_null($active) )
        {
            $queryFrom .= ' FILTER doc_from.active == ' . ((bool) $active ? '1' : '0') ;
        }

        $orders = $this->getQueryOrders( $sort , 'doc_from' ) ;
        if( isset($orders) && count($orders) > 0 )
        {
            $queryFrom .= ' SORT ' ;

            $queryFrom .= implode( ', ' , $orders ) ;

            $queryFrom .= ' ' ;
        }

        $returnFrom = '' ;

        if( array_key_exists( 'model' , $this->from ) )
        {
            $queryFrom .= $this->container[$this->from['model']]->getFields( NULL , $fieldsFrom , $lang , 'from' ) ;
        }
        else
        {
            if( $fieldsFrom == '*' )
            {
                $returnFrom = ' RETURN doc_from' ;
            }
            else
            {
                $returnFrom = ' RETURN { ' . $this->getFilterFields( $fieldsFrom , 'from' , $lang ) . '}' ;
            }
        }

        $query .= ' LET from = ' .
                    '(' .
                    ' FOR doc_from IN INBOUND doc ' . $this->table . ' ' .
                    $queryFrom .
                    $returnFrom .
                    ' ) ';

        // query to
        $fieldsTo = $this->container[$this->to['controller']]->getFields(  ) ;

        if( $fieldsTo == '*' )
        {
            $returnTo = ' doc' ;
        }
        else
        {
            $returnTo = ' { ' . $this->getFilterFields( $fieldsTo , 'doc' , $lang ) . '}' ;
        }

        // return
        $query .= ' RETURN { from : from , to : ' . $returnTo . ' }';

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getAll() ;
    }

    public function insertEdge( $from , $to )
    {
        $query = 'INSERT { _from: @from , _to: @to , created: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table . ' RETURN NEW' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'from' => $from ,
                'to'   => $to
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'conditions' => [],
        'key'        => '_from'
    ] ;

    public function delete( $value , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ';

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // remove
        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    public function deleteEdge( $from , $to )
    {
        $query = 'FOR doc IN ' . $this->table ;

        $query .= ' FILTER doc._from == @from && doc._to == @to ' ;

        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "from" => $from ,
                "to"   => $to
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    public function deleteEdgeFrom( $from )
    {
        $query = 'FOR doc IN ' . $this->table ;

        $query .= ' FILTER doc._from == @from ' ;

        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "from" => $from
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    public function deleteEdgeTo( $to )
    {
        $query = 'FOR doc IN ' . $this->table ;

        $query .= ' FILTER doc._to == @to ' ;

        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "to"   => $to
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    public function existEdge( $from , $to )
    {
        $query = 'RETURN COUNT( FOR doc IN ' . $this->table . ' FILTER doc._from == @from && doc._to == @to RETURN 1 )' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "from" => $from ,
                "to"   => $to
            ]
        ]);

        $this->container->arango->execute() ;

        return (bool) $this->container->arango->getFirstResult() ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_EDGE_DEFAULT =
    [
        'lang' => NULL ,
        'skin' => NULL ,
        'sort' => NULL
    ] ;

    public function getEdge( $from = NULL , $to = NULL , $init = [] )
    {
        extract( array_merge( self::ARGUMENTS_GET_EDGE_DEFAULT , is_array($init) ? $init : [] ) ) ;

        $binds = [] ;

        if( $from ) $binds['from'] = $from ;
        if( $to ) $binds['to'] = $to ;

        if( !$from && $to )
        {
            /////// ONLY TO

            $query = 'FOR doc IN ' . $this->to['name'] ;

            // FILTER

            $query .= ' FILTER doc._key == @to' ;

            // query from
            $fieldsFrom = $this->container[$this->from['controller']]->getFields( $skin ) ;

            $returnFrom = '' ;

            // joins
            $fromJoins = $this->container[$this->from['controller']]->model->joins ;

            // edges
            $fromEdges = $this->container[$this->from['controller']]->model->edges ;

            foreach( $fieldsFrom as $key => $field )
            {
                switch( $field['filter'] )
                {
                    case Thing::FILTER_EDGE :
                    case Thing::FILTER_EDGE_SINGLE :
                        $currentEdge = null ;
                        foreach( $fromEdges as $edge )
                        {
                            if( $edge['name'] == $key )
                            {
                                $currentEdge = $edge ;
                                break ;
                            }
                        }
                        $currentEdge['unique'] = $field['unique'] ;
                        $returnFrom .= $this->getEdges( [ $currentEdge ] , 'doc_from' , $lang ) ;
                        break ;
                    case Thing::FILTER_EDGE_COUNT :
                        $currentEdge = null ;
                        foreach( $fromEdges as $edge )
                        {
                            if( 'num' . ucfirst( $edge['name'] ) == $key )
                            {
                                $currentEdge = $edge ;
                                break ;
                            }
                        }
                        $returnFrom .= $this->getEdgeCount( $field['unique'] , $currentEdge , 'doc_from' ) ;
                        break ;
                    case Thing::FILTER_JOIN :
                    case Thing::FILTER_JOIN_ARRAY :
                    case Thing::FILTER_JOIN_MULTIPLE :
                        $currentJoin = null ;
                        foreach( $fromJoins as $join )
                        {
                            if( $join['name'] == $key )
                            {
                                $currentJoin = $join ;
                                break ;
                            }
                        }
                        $currentJoin['unique'] = $field['unique'] ;
                        $returnFrom .= $this->getJoin( [ $currentJoin ] , 'doc_from' , $lang ) ;
                        break ;
                }
            }

            // return

            if( $fieldsFrom == '*' )
            {
                $returnFrom .= ' RETURN doc_from' ;
            }
            else
            {
                $returnFrom .= ' RETURN { ' . $this->getFilterFields( $fieldsFrom , 'from' , $lang ) . '}' ;
            }

            $sortQuery = '' ;
            if( $sort )
            {
                $sortQuery = ' SORT doc_from.' . $sort ;
            }

            $query .= ' LET from = ' .
                '(' .
                ' FOR doc_from IN INBOUND doc ' . $this->table . $sortQuery . ' ' .
                $returnFrom .
                ' ) ';

            // return
            $query .= ' RETURN { edge : from } ' ;
        }
        elseif( $from && !$to )
        {
            /////// ONLY FROM

            $query = 'FOR doc IN ' . $this->from['name'] ;

            // FILTER

            $query .= ' FILTER doc._key == @from' ;

            // query to
            $fieldsTo = $this->container[$this->to['controller']]->getFields( $skin ) ;

            $returnTo = '' ;

            // joins
            $toJoins = $this->container[$this->to['controller']]->model->joins ;

            // edges
            $toEdges = $this->container[$this->to['controller']]->model->edges ;

            foreach( $fieldsTo as $key => $field )
            {
                switch( $field['filter'] )
                {
                    case Thing::FILTER_EDGE :
                    case Thing::FILTER_EDGE_SINGLE :
                        $currentEdge = null ;
                        foreach( $toEdges as $edge )
                        {
                            if( $edge['name'] == $key )
                            {
                                $currentEdge = $edge ;
                                break ;
                            }
                        }
                        $currentEdge['unique'] = $field['unique'] ;
                        $returnTo .= $this->getEdges( [ $currentEdge ] , 'doc_to' , $lang ) ;
                        break ;
                    case Thing::FILTER_EDGE_COUNT :
                        $currentEdge = null ;
                        foreach( $toEdges as $edge )
                        {
                            if( 'num' . ucfirst( $edge['name'] ) == $key )
                            {
                                $currentEdge = $edge ;
                                break ;
                            }
                        }
                        $returnTo .= $this->getEdgeCount( $field['unique'] , $currentEdge , 'doc_to' ) ;
                        break ;
                    case Thing::FILTER_JOIN :
                    case Thing::FILTER_JOIN_ARRAY :
                    case Thing::FILTER_JOIN_MULTIPLE :
                        $currentJoin = null ;
                        foreach( $toJoins as $join )
                        {
                            if( $join['name'] == $key )
                            {
                                $currentJoin = $join ;
                                break ;
                            }
                        }
                        $currentJoin['unique'] = $field['unique'] ;
                        $returnTo .= $this->getJoin( [ $currentJoin ] , 'doc_to' , $lang ) ;
                        break ;
                }
            }

            if( $fieldsTo == '*' )
            {
                $returnTo .= ' RETURN doc_to' ;
            }
            else
            {
                $returnTo .= ' RETURN { ' . $this->getFilterFields( $fieldsTo , 'to' , $lang ) . '}' ;
            }

            $sortQuery = '' ;
            if( $sort )
            {
                $sortQuery = ' SORT doc_from.' . $sort ;
            }

            $query .= ' LET to = ' .
                '(' .
                ' FOR doc_to IN OUTBOUND doc ' . $this->table . $sortQuery . ' ' .
                $returnTo .
                ' ) ';


            // return
            $query .= ' RETURN { edge : to }' ;
        }

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;

    }
}
