<?php

namespace com\ooopener\models ;

use com\ooopener\things\Thing;
use Slim\Container;

class Emails extends Model
{
    public function __construct( Container $container , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'             => Thing::FILTER_INT,
        'name'           => Thing::FILTER_DEFAULT,
        'alternateName'  => Thing::FILTER_DEFAULT,
        'value'          => Thing::FILTER_DEFAULT,
        'additionalType' => Thing::FILTER_INT
    ];

}