<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Events model.
 */
class Events extends Collections
{
    /**
     * Creates a new Events instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'name'        => Thing::FILTER_DEFAULT,
        'path'        => Thing::FILTER_DEFAULT,

        'active'      => Thing::FILTER_INT,
        'withStatus'  => Thing::FILTER_DEFAULT,

        'endDate'     => Thing::FILTER_DEFAULT,
        'eventStatus' => Thing::FILTER_DEFAULT,
        'startDate'   => Thing::FILTER_DEFAULT,

        'about'         => Thing::FILTER_INT,

        'capacity'          => Thing::FILTER_INT,
        'numAttendee'       => Thing::FILTER_INT,
        'remainingAttendee' => Thing::FILTER_INT,

        'alternateName'       => Thing::FILTER_DEFAULT,
        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,

        'audios'        => Thing::FILTER_DEFAULT,
        'photos'        => Thing::FILTER_DEFAULT,
        'videos'        => Thing::FILTER_DEFAULT,

        'isBasedOn'     => Thing::FILTER_DEFAULT
    ];

    /**
     * The default 'countFrom' methods options.
     */
    const ARGUMENTS_COUNT_FROM_DEFAULT =
    [
        'active'     => NULL,
        'conditions' => [],
        'facets'     => NULL,
        'from'       => NULL,
        'to'         => NULL,
        'interval'   => 5,
        'search'     => NULL,
        'values'     => NULL,
        'words'      => NULL
    ] ;

    /**
     * Returns the number of users.
     *
     * @param array $init
     *
     * @return integer The number of users.
     */
    public function countFrom( array $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_COUNT_FROM_DEFAULT , $init ) ) ;


        $query = 'RETURN COUNT ( FOR doc IN ' . $this->table . ' ' ;

        $query .= ' LET endDate = ( IS_DATESTRING( doc.endDate ) ? doc.endDate : DATE_ADD( DATE_NOW() , 10 , "y" ) ) ' ;
        $query .= ' LET duration = ( DATE_DIFF( doc.startDate , endDate , "d" ) + 1 ) ' ;
        $query .= ' LET interval = ( DATE_ADD( @start , @interval , "d" ) ) ' ;

        $binds = [] ;

        // dates
        $binds['start'] = $from ;
        //$binds['end']   = $to ;

        $binds['interval']   = $interval ;

        $conditions[] = ' ( doc.startDate <= endDate ) '
                    . 'AND ( doc.startDate <= interval ) '
                    . 'AND ( @start <= endDate ) ' ;

        // -------- WHERE

        $search = $this->getQuerySearch( $search , $this->searchable ) ;
        if( isset($search) )
        {
            extract( $search ) ;
        }

        if( !empty($search) )
        {
            $conditions[] = $search ;
        }

        if( isset( $words ) )
        {
            $i = 0 ;
            foreach( $words as $word )
            {
                $binds[ 'search' . $i++ ] = '%' . $word . '%' ;
            }
        }

        if( isset($active) && !is_null($active) )
        {
            $conditions[] = 'doc.active == ' . ((bool) $active ? '1' : '0') ;
        }

        extract( $this->getQueryFacets( $facets ) ) ; // $facets / values
        if( is_string($facets) && !empty($facets) )
        {
            $conditions[] = $facets ;
        }

        if( isset($values) && is_array($values) ) // see facets
        {
            foreach( $values as $key => $bindValue )
            {
                $binds[$key] = $bindValue['value'] ;
            }
        }

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // end query

        $query .= ' RETURN 1 )' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getFirstResult() ;
    }

    /**
     * The default 'from' method options.
     */
    const ARGUMENTS_FROM_DEFAULT =
    [
        'active'      => TRUE,
        'conditions'  => [],
        'facets'      => NULL,
        'fetchStyle'  => NULL,
        'from'        => 'now',
        'groupBy'     => NULL,
        'interval'    => 5,
        'lang'        => NULL,
        'limit'       => 0,
        'offset'      => 0,
        'queryFields' => NULL ,
        'orders'      => NULL ,
        'groups'      => NULL,
        'search'      => NULL,
        'sort'        => NULL,
        'fields'      => '*' ,
        'values'      => NULL,
        'words'       => NULL
    ] ;

    /**
     * Returns all elements from a specific date.
     *
     * @param array $init
     *
     * @return array all elements from a specific date.
     */
    public function from( $init = [] )
    {
        // -------- INIT

        extract( array_merge( self::ARGUMENTS_FROM_DEFAULT , $init ) ) ;

        $binds = [] ;

        $query = 'FOR doc IN ' . $this->table ;

        $query .= ' LET endDate = ( IS_DATESTRING( doc.endDate ) ? doc.endDate : DATE_ADD( DATE_NOW() , 10 , "y" ) ) ' ;
        $query .= ' LET duration = ( DATE_DIFF( doc.startDate , endDate , "d" ) + 1 ) ' ;
        $query .= ' LET interval = ( DATE_ADD( @start , @interval , "d" ) ) ' ;

        // -------- WHERE

        // dates
        $binds['start'] = $from ;

        $binds['interval']   = $interval ;

        $conditions[] = ' ( doc.startDate <= endDate ) '
                        . 'AND ( doc.startDate <= interval ) '
                        . 'AND ( @start <= endDate ) ' ;


        $search = $this->getQuerySearch( $search , $this->searchable ) ;

        if( isset($search) )
        {
            extract( $search ) ;
        }

        if( !empty($search) )
        {
            $conditions[] = $search ;
        }

        if( isset( $words ) )
        {
            $i = 0 ;
            foreach( $words as $word )
            {
                $binds[ 'search' . $i++ ] = '%' . $word . '%' ;
            }
        }

        if( isset($active) && !is_null($active) )
        {
            $conditions[] = 'doc.active == ' . ((bool) $active ? '1' : '0') ;
        }

        extract( $this->getQueryFacets( $facets ) ) ; // $facets / values
        if( is_string($facets) && !empty($facets) )
        {
            $conditions[] = $facets ;
        }

        if( isset($values) && is_array($values) ) // see facets
        {
            foreach( $values as $key => $bindValue )
            {
                $binds[$key] = $bindValue['value'] ;
            }
        }

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- GROUP BY

        $groups = $this->getQueryGroups( $groupBy ) ;
        if( isset($groups) && count($groups) > 0 )
        {
            $query .= ' COLLECT ' ;

            foreach( $groups as $value )
            {
                $query .= $value ;
                $query .= ", " ;
            }

            $query = rtrim( $query , ', ' ) ;

            $query .= ' ' ;
        }

        // -------- ORDER BY

        $orders = $this->getQueryOrders( $sort ) ;
        if( isset($orders) && count($orders) > 0 )
        {
            $query .= ' SORT ' ;

            foreach( $orders as $value )
            {
                $query .= array_shift($orders) ;
                $query .= ", " ;
            }

            $query = rtrim( $query , ', ' ) ;

            $query .= ' ' ;
        }
        else
        {
            $query .= ' SORT duration, doc.startDate, endDate ASC ' ;
        }

        // -------- LIMIT / OFFSET

        if( $limit > 0 )
        {
            $query .= ' LIMIT @offset , @limit' ;

            $binds['offset'] = $offset ;
            $binds['limit'] = $limit ;
        }

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getAll() ;
    }
}


