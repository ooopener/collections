<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Games model.
 */
class Games extends Collections
{
    /**
     * Creates a new Games instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct(Container $container = NULL, $table = NULL, array $init = [])
    {
        parent::__construct($container, $table, $init);
    }
    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        // courses
        'name'   => Thing::FILTER_DEFAULT,
        'active' => Thing::FILTER_INT,
        'withStatus'  => Thing::FILTER_DEFAULT,

        'image'   => Thing::FILTER_DEFAULT,
        'path'    => Thing::FILTER_DEFAULT,

        'about'          => Thing::FILTER_INT,
        'additionalType' => Thing::FILTER_INT,

        'item'           => Thing::FILTER_DEFAULT,

        'quest'          => Thing::FILTER_DEFAULT,

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT

    ];

    ///////////////////////////

    /**
     * Indicates if the item exist.
     *
     * @param integer $about
     * @param string $id
     * @param string $edge
     *
     * @return bool
     */
    public function existAbout( $about , $id , $edge )
    {

        $query = 'FOR doc IN ' . $this->table .
                ' FILTER doc._key == @id ' .
                ' RETURN COUNT( FOR doc_quest IN INBOUND doc @@edge ' .
                ' OPTIONS { bfs : true , uniqueVertices : \'global\' } ' .
                ' FILTER doc_quest.about == @about ' .
                ' RETURN 1 )';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "about" => $about ,
                "@edge" => $edge,
                "id"    => $id
            ]
        ]);

        $this->container->arango->execute() ;

        return (bool) $this->container->arango->getFirstResult() ;
    }
}
