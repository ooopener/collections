<?php

namespace com\ooopener\models ;

use com\ooopener\things\Thing;
use Slim\Container;

class Keywords extends Things
{
    public function __construct( Container $container , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'name'          => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'href'          => Thing::FILTER_DEFAULT
    ];

}