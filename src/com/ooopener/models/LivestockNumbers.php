<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The LivestocksNumbers model.
 */
class LivestockNumbers extends Model
{
    /**
     * Creates a new Livestocks instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'             => Thing::FILTER_INT,
        'additionalType' => Thing::FILTER_INT,
        'name'           => Thing::FILTER_DEFAULT,
        'value'          => Thing::FILTER_DEFAULT
    ];


}


