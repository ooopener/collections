<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Livestocks model.
 */
class Livestocks extends Collections
{
    /**
     * Creates a new Livestocks instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'active'       => Thing::FILTER_INT,
        'withStatus'   => Thing::FILTER_DEFAULT,
        'path'         => Thing::FILTER_DEFAULT,

        'numbers'      => Thing::FILTER_DEFAULT,
        'workshops'    => Thing::FILTER_DEFAULT,

        'isBasedOn'     => Thing::FILTER_DEFAULT
    ];


    public function all( $init = [] )
    {
        // -------- INIT
        $this->container->logger->debug( $this . ' all' ) ;

        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $init ) ) ;

        $binds = [] ;

        $query = 'FOR doc IN ' . $this->table ;



        // -------- GROUP BY

        $groups = $this->getQueryGroups( $groupBy ) ;
        if( isset($groups) && count($groups) > 0 )
        {
            $query .= ' COLLECT ' ;

            foreach( $groups as $value )
            {
                $query .= $value ;
                $query .= ", " ;
            }

            $query = rtrim( $query , ', ' ) ;

            $query .= ' ' ;
        }

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang , 'doc' , TRUE ) ;

        // -------- WHERE

        $search = $this->getQuerySearch( $search , $this->searchable , 'result' ) ;

        if( isset($search) )
        {
            extract( $search ) ;
        }

        if( !empty($search) )
        {
            $conditions[] = $search ;
        }

        if( isset( $words ) )
        {
            $i = 0 ;
            foreach( $words as $word )
            {
                $binds[ 'search' . $i++ ] = '%' . $word . '%' ;
            }
        }

        if( isset($active) && !is_null($active) )
        {
            $conditions[] = 'doc.active == ' . ((bool) $active ? '1' : '0') ;
        }

        extract( $this->getQueryFacets( $facets ) ) ; // $facets / values
        if( is_string($facets) && !empty($facets) )
        {
            $conditions[] = $facets ;
        }

        if( isset($values) && is_array($values) ) // see facets
        {
            foreach( $values as $key => $bindValue )
            {
                $binds[$key] = $bindValue['value'] ;
            }
        }

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- ORDER BY

        $orders = $this->getQueryOrders( $sort , 'result' ) ;
        if( isset($orders) && count($orders) > 0 )
        {
            $query .= ' SORT ' ;

            foreach( $orders as $value )
            {
                $query .= array_shift($orders) ;
                $query .= ", " ;
            }

            $query = rtrim( $query , ', ' ) ;

            $query .= ' ' ;
        }

        // -------- LIMIT / OFFSET

        if( $limit > 0 )
        {
            $query .= ' LIMIT @offset , @limit' ;

            $binds['offset'] = $offset ;
            $binds['limit'] = $limit ;
        }

        $query .= ' RETURN result ' ;

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getAll() ;
    }
}


