<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

use Exception ;

/**
 * This class is the generic Model class.
 */
class Model
{
    /**
     * Creates a new Model instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    function __construct( Container $container , $table = NULL , $init = [] )
    {
        extract( array_merge( self::ARGUMENTS_CONSTRUCTOR_DEFAULT , $init ) ) ;

        $this->container  = $container  ;
        $this->table      = $table      ;

        $this->edges      = $edges  ;
        $this->facetable  = $facetable  ;
        $this->from       = $from  ;
        $this->joins      = $joins  ;
        $this->searchable = $searchable ;
        $this->sortable   = $sortable   ;
        $this->to         = $to   ;
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'     => Thing::FILTER_INT,
        'name'   => Thing::FILTER_DEFAULT
    ];

    ///////////////////////////


    /**
     * The default 'constructor' default arguments.
     */
    const ARGUMENTS_CONSTRUCTOR_DEFAULT =
    [
        'edges'      => NULL,
        'facetable'  => NULL,
        'from'       => NULL,
        'joins'      => NULL,
        'searchable' => NULL,
        'sortable'   => NULL,
        'to'         => NULL
    ] ;

    /**
     * The container reference.
     */
    public $container ;

    /**
     * The edges settings.
     */
    public $edges ;

    /**
     * The facet settings.
     */
    public $facetable ;

    /**
     * The from settings.
     */
    public $from ;

    /**
     * The joins settings.
     */
    public $joins ;

    /**
     * The searchable fields.
     */
    public $searchable ;

    /**
     * The map of all the sortabled field of this model.
     */
    public $sortable ;

    /**
     * The default table name.
     */
    public $table ;

    /**
     * The to settings.
     */
    public $to ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'conditions'  => [],
        'fields'      => '*',
        'lang'        => NULL,
        'queryFields' => NULL
    ] ;

    public function all( $init = [] )
    {
        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $init ) ) ;

        $binds = [] ;

        $query = 'FOR doc IN ' . $this->table ;

        // -------- WHERE

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getAll() ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_COUNT_DEFAULT =
    [
        'active'     => NULL,
        'conditions' => [],
        'facets'     => NULL,
        'search'     => NULL,
        'values'     => NULL,
        'words'      => NULL
    ] ;

    /**
     * Returns the number of elements registered in the specific table.
     *
     * @param array $init
     *
     * @return integer The number of elements registered in the specific table.
     */
    public function count( array $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_COUNT_DEFAULT , $init ) ) ;


        $query = 'RETURN COUNT ( FOR doc IN ' . $this->table . ' ' ;

        $binds = [] ;

        // -------- WHERE

        $search = $this->getQuerySearch( $search , $this->searchable ) ;

        if( isset($search) )
        {
            extract( $search ) ;
        }

        if( !empty($search) )
        {
            $conditions[] = $search ;
        }

        if( isset( $words ) )
        {
            $i = 0 ;
            foreach( $words as $word )
            {
                $binds[ 'search' . $i++ ] = '%' . $word . '%' ;
            }
        }

        if( isset($active) && !is_null($active) )
        {
            $conditions[] = 'doc.active == ' . ((bool) $active ? '1' : '0') ;
        }

        extract( $this->getQueryFacets( $facets ) ) ; // $facets / values
        if( is_string($facets) && !empty($facets) )
        {
            $conditions[] = $facets ;
        }

        if( isset($values) && is_array($values) ) // see facets
        {
            foreach( $values as $key => $bindValue )
            {
                $binds[$key] = $bindValue['value'] ;
            }
        }

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // end query

        $query .= ' RETURN 1 )' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getFirstResult() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'key'    => '_key'
    ] ;

    /**
     * Delete the specific item
     *
     * @param string|integer $value
     * @param array $init
     *
     * @return mixed
     */
    public function delete( $value , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ';

        // remove
        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * The default 'exist' method options.
     */
    const ARGUMENTS_EXIST_DEFAULT =
    [
        'conditions' => [],
        'key'        => '_key'
    ] ;

    /**
     * Indicates if the item exist.
     *
     * @param mixed $value
     * @param array $init
     *
     * @return bool
     */
    public function exist( $value , $init = []  )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_EXIST_DEFAULT , $init ) ) ;

        $query = 'RETURN COUNT( FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ' ;

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        //
        $query .= ' RETURN 1 )';


        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ "value" => $value ]
        ]);

        $this->container->arango->execute() ;

        return (bool) $this->container->arango->getFirstResult() ;
    }

    /**
     * Indicates if the list of items exist (by id or the custom field parameter).
     *
     * @param array $items
     * @param array $init
     *
     * @return integer
     */
    public function existAll( $items , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_EXIST_DEFAULT , $init ) ) ;


        $in = [] ;
        $binds = [] ;

        foreach( $items as $keyI => $value )
        {
            $in[] = 'doc.' . $key . ' == @i' . $keyI ;
            $binds['i' . $keyI] = $value ;
        }

        $query = 'RETURN COUNT( FOR doc IN ' . $this->table . ' FILTER ( ' . implode(' OR ' , $in ) . ' ) ' ;

        if( count( $conditions ) > 0 )
        {
            $query .= ' && ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        //
        $query .= ' RETURN 1 )';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getFirstResult() ;
    }

    /**
     * For a SELECT with a LIMIT clause, returns the number of rows that would be returned were there no LIMIT clause.
     *
     * @return array
     */
    public function foundRows()
    {
        return $this->container->arango->getFoundRows() ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'fields'      => '*' ,
        'key'         => '_key',
        'lang'        => NULL,
        'queryFields' => NULL
    ] ;

    /**
     * Returns the specific item.
     *
     * @param mixed $value
     * @param array $init
     *
     * @return object the specific item.
     */
    public function get( $value , $init = [] )
    {
        // ---- variables

        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , is_array($init) ? $init : [] ) ) ;

        // ----

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ' ;

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "value" => $value
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    // ------------ query helpers

    public function getEdgeCount( $name , $edge , $docRef = 'doc' )
    {
        $table = $this->container[$edge['edgeController']]->edge->table ;

        return ' LET ' . $name . ' = ' .
            '( ' .
            'LENGTH( FOR v IN OUTBOUND ' . $docRef . ' ' . $table . ' RETURN v ) ' .
            ') ' ;
    }

    /**
     * Return edges query
     *
     * @param array $edges
     * @param string $docRef
     * @param string $lang
     *
     * @return string
     */
    public function getEdges( $edges , $docRef = 'doc' , $lang = NULL )
    {
        $edges_query = '' ;

        foreach( $edges as $edge )
        {
            $fields = $this->container[$edge['controller']]->getFields( $edge['skin'] ) ;

            $name = array_key_exists( 'unique' , $edge ) ? $edge['unique'] : $edge['name'] ;

            $return = '' ;

            $table = $this->container[$edge['edgeController']]->edge->table ;
            $doc = $edge['name'] . '_' . $table ;

            foreach( $fields as $key => $field )
            {
                switch( $field['filter'] )
                {
                    case Thing::FILTER_EDGE :
                    case Thing::FILTER_EDGE_SINGLE :
                        if( array_key_exists( 'edges' , $edge ) && count( $edge['edges'] ) > 0 )
                        {
                            $currentEdge = null;
                            foreach( $edge[ 'edges' ] as $ed )
                            {
                                if( $ed[ 'name' ] == $key )
                                {
                                    $currentEdge = $ed;
                                    break;
                                }
                            }
                            $currentEdge[ 'unique' ] = $field[ 'unique' ];
                            $return .= self::getEdges( [ $currentEdge ] , 'doc_' . $doc , $lang);
                        }
                        break ;
                    case Thing::FILTER_EDGE_COUNT :
                        if( array_key_exists( 'edges' , $edge ) && count( $edge['edges'] ) > 0 )
                        {
                            $currentEdge = null;
                            foreach( $edge[ 'edges' ] as $ed )
                            {
                                if( 'num' . ucfirst($ed[ 'name' ]) == $key )
                                {
                                    $currentEdge = $ed;
                                    break;
                                }
                            }
                            $return .= self::getEdgeCount( $field[ 'unique' ] , $currentEdge , 'doc_' . $doc);
                        }
                        break ;
                    case Thing::FILTER_JOIN :
                    case Thing::FILTER_JOIN_ARRAY :
                    case Thing::FILTER_JOIN_MULTIPLE :
                        if( array_key_exists( 'joins' , $edge ) && count( $edge['joins'] ) > 0 )
                        {
                            $currentJoin = null;
                            foreach( $edge[ 'joins' ] as $jo )
                            {
                                if( $jo[ 'name' ] == $key )
                                {
                                    $currentJoin = $jo ;
                                    break;
                                }
                            }
                            $currentJoin[ 'unique' ] = $field[ 'unique' ];
                            $return .= self::getJoin([ $currentJoin ] , 'doc_' . $doc , $lang);
                        }
                        break ;
                }
            }

            $return .= ' RETURN { ' . $this->getFilterFields( $fields , $doc , $lang ) . ' } ' ;


            $order = array_key_exists( 'order' , $edge ) ? ' SORT doc_' . $doc . '.' . $edge['order'] . ' ' : ' SORT edge_' . $doc . '.created DESC ' ;
            $direction = (array_key_exists( 'direction' , $edge ) && $edge['direction'] == 'reverse') ? 'OUTBOUND' : 'INBOUND' ;
            $edges_query .= ' LET ' . $name . ' = ' .
                '( ' .
                'FOR doc_' . $doc . ' , edge_' . $doc . ' IN ' . $direction . ' ' . $docRef . ' ' . $table . ' ' .
                'OPTIONS { bfs : true , uniqueVertices : \'global\' } ' . $order .
                $return .
                ') ';
        }

        return $edges_query ;
    }

    public function getFields( $fields , $queryFields , $lang = NULL , $docRef = 'doc' , $result = FALSE )
    {
        $query = '' ;

        $edges_fields = NULL ;

        if( $fields == '*' && $queryFields == NULL )
        {
            // joins
            if( $this->joins && count( $this->joins ) > 0 )
            {
                foreach( $this->joins as $join )
                {
                    $query .= $this->getJoin( [ $join ] , $docRef , $lang ) ;
                }
            }

            // edges
            if( $this->edges && count( $this->edges ) > 0 )
            {
                $query .= $this->getEdges( $this->edges , $docRef , $lang ) ;

                $edges_fields = [] ;
                foreach( $this->edges as $edge )
                {
                    array_push( $edges_fields , '{ ' . $edge['name'] . ' : ' . $edge['name'] . ' }' ) ;
                }
            }

            if( $edges_fields )
            {
                $query .= ' RETURN MERGE ( doc , ' . implode( ',' , $edges_fields ) . ' )' ;
            }
            else
            {
                $query .= ' RETURN doc' ;
            }
        }
        else
        {
            if( $queryFields == NULL )
            {
                $r = explode( ',' , $fields );

                $fields = [] ;
                foreach( $r as $value )
                {
                    array_push( $fields , $value . ': doc.' . $value ) ;
                }
                $fields = implode( ' , ' , $fields ) ;

            }
            else
            {
                $doc = $docRef ;
                if( $docRef != 'doc' )
                {
                    $doc = 'doc_' . $docRef ;
                }

                foreach( $queryFields as $key => $field )
                {
                    switch( $field['filter'] )
                    {
                        case Thing::FILTER_EDGE :
                        case Thing::FILTER_EDGE_SINGLE :
                            $currentEdge = null ;
                            foreach( $this->edges as $edge )
                            {
                                if( $edge['name'] == $key )
                                {
                                    $currentEdge = $edge ;
                                    break ;
                                }
                            }
                            $currentEdge['unique'] = $field['unique'] ;
                            $query .= $this->getEdges( [ $currentEdge ] , $doc , $lang ) ;
                            break ;
                        case Thing::FILTER_EDGE_COUNT :
                            $currentEdge = null ;
                            foreach( $this->edges as $edge )
                            {
                                if( 'num' . ucfirst( $edge['name'] ) == $key )
                                {
                                    $currentEdge = $edge ;
                                    break ;
                                }
                            }
                            $query .= $this->getEdgeCount( $field['unique'] , $currentEdge , $doc ) ;
                            break ;
                        case Thing::FILTER_JOIN :
                        case Thing::FILTER_JOIN_ARRAY :
                        case Thing::FILTER_JOIN_MULTIPLE :
                            $currentJoin = null ;
                            foreach( $this->joins as $join )
                            {
                                if( $join['name'] == $key )
                                {
                                    $currentJoin = $join ;
                                    break ;
                                }
                            }
                            $currentJoin['unique'] = $field['unique'] ;
                            $query .= $this->getJoin( [ $currentJoin ] , $doc , $lang ) ;
                            break ;
                    }
                }

                $fields = $this->getFilterFields( $queryFields , $docRef , $lang ) ;
            }

            if( $result === FALSE )
            {
                $query .= ' RETURN { ' . $fields . ' }' ;
            }
            else
            {
                $query .= ' LET result = { ' . $fields . ' }' ;
            }
        }

        return $query ;
    }

    public function getFilterFields( $queryFields , $doc = 'doc' , $lang = NULL )
    {
        $url      = $this->container->settings['app']['url'] ;
        $mediaUrl = $this->container->settings['app']['mediaUrl'] ;

        $fields = [] ;
        if( $doc != 'doc' )
        {
            $doc = 'doc_' . $doc ;
        }
        foreach( $queryFields as $key => $options )
        {
            $filter = array_key_exists( 'filter' , $options ) ? $options['filter'] : NULL ;
            $name = array_key_exists( 'unique' , $options ) ? $options['unique'] : $key ;
            $type = array_key_exists( 'type' , $options ) ? $options['type'] : NULL ;

            switch( $filter )
            {
                case Thing::FILTER_BOOL :
                    $field = $key . ': TO_BOOL( ' . $doc . '.' . $key . ' )' ;
                    break ;
                case Thing::FILTER_ID :
                    $field = $key . ': TO_NUMBER( ' . $doc . '._key )' ;
                    break ;
                case Thing::FILTER_IMAGE :
                    $field = $key . ': ' . $doc . '.image ? CONCAT( "' . $url . 'media" , "/" , ' . $doc . '.image ) : null ' ;
                    break ;
                case Thing::FILTER_INT :
                    $field = $key . ': TO_NUMBER( ' . $doc . '.' . $key . ' )' ;
                    break ;
                case Thing::FILTER_MEDIA_SOURCE :
                    $field = $key . ': IS_ARRAY( ' . $doc . '.source ) ? ( FOR s IN IS_ARRAY( ' . $doc . '.source ) ? ' . $doc . '.source : [] RETURN MERGE( s , { contentUrl : CONCAT( "' . $mediaUrl . '" , s.contentUrl ) } ) ) : null ' ;
                    break;
                case Thing::FILTER_MEDIA_THUMBNAIL :
                    $field = $key . ': IS_OBJECT( ' . $doc . '.thumbnail ) ? MERGE( ' . $doc . '.thumbnail , { contentUrl : CONCAT( "' . $mediaUrl . '" , ' . $doc . '.thumbnail.contentUrl ) } ) : null ' ;
                    break;
                case Thing::FILTER_MEDIA_URL :
                    $field = $key . ': ' . $doc . '.contentUrl ? CONCAT( "' . $mediaUrl . '" , ' . $doc . '.contentUrl ) : null ' ;
                    break ;
                case Thing::FILTER_URL :
                    $field = $key . ': CONCAT( "' . $url . '" , ' . $doc . '.path , "/" , ' . $doc . '._key )' ;
                    break ;
                case Thing::FILTER_URL_API :
                    $field = $key . ': CONCAT( "' . $url . '" , ' . $doc . '.' . $key . ')' ;
                    break ;
                case Thing::FILTER_THESAURUS_URL :
                    $field = $key . ': CONCAT( "' . $url . '" , ' . $doc . '.path )' ;
                    break ;
                case Thing::FILTER_THESAURUS_IMAGE :
                    $field = $key . ': TO_BOOL( ' . $doc . '.' . $key . ' ) == true ? CONCAT( "' . $url . '" , ' . $doc . '.path , "/" , ' . $doc . '._key , "/image" ) : null ' ;
                    break ;
                case Thing::FILTER_DATETIME :
                    $field = $key . ': IS_DATESTRING(' . $doc . '.' . $key . ') ? DATE_FORMAT( ' . $doc . '.' . $key . ' ? : 0 , "%yyyy-%mm-%ddT%hh:%ii:%ssZ" ) : null' ;
                    break ;
                case Thing::FILTER_DISTANCE :
                    $field = $key . ': distance' ;
                    break ;
                case Thing::FILTER_EDGE_SINGLE :
                case Thing::FILTER_JOIN :
                    $field = $key . ': IS_ARRAY( ' . $name . ' ) ? FIRST( ' . $name . ' ? : [] ) : null';
                    break ;
                case Thing::FILTER_JOIN_COUNT :
                    // get property
                    $prop = lcfirst( substr( $name , strlen( 'num' ) ) ) ;
                    $field = $key . ': IS_ARRAY( ' . $doc . '.' . $prop . ' ) ? LENGTH( ' . $doc . '.' . $prop . ' ) : 0';
                    break ;
                case Thing::FILTER_EDGE_COUNT :
                case Thing::FILTER_EDGE :
                case Thing::FILTER_JOIN_ARRAY :
                case Thing::FILTER_JOIN_MULTIPLE :
                case Thing::FILTER_UNIQUE_NAME :
                    $field = $key . ': ' . $name ;
                    break ;
                case Thing::FILTER_TRANSLATE :
                    if( $lang != NULL )
                    {
                        $field = $key . ': TRANSLATE( "' . $lang . '" , ' . $doc . '.' . $key . ' , "" ) ' ;
                        break ;
                    }
                case Thing::FILTER_DEFAULT :
                default :
                    $field = $key . ': ' . $doc . '.' . $key ;
                    break ;
            }
            array_push( $fields , $field ) ;
        }

        return implode( ' , ' , $fields ) ;
    }

    public function getJoin( $joins , $docRef = 'doc' , $lang = NULL )
    {
        $joins_query = '' ;

        foreach( $joins as $join )
        {
            $fields = $this->container[$join['controller']]->getFields( $join['skin'] ) ;

            $name = (array_key_exists( 'unique' , $join ) && ($join['unique'] != null || $join['unique'] != '' )) ? $join['unique'] : $join['name'] ;

            $joinKey = '_key' ;

            if( array_key_exists( 'key' , $join ) )
            {
                $joinKey = $join['key'] ;
            }

            $return = '' ;

            foreach( $fields as $key => $field )
            {
                switch( $field['filter'] )
                {
                    case Thing::FILTER_EDGE :
                    case Thing::FILTER_EDGE_SINGLE :
                        if( array_key_exists( 'edges' , $join ) && count( $join['edges'] ) > 0 )
                        {
                            $currentEdge = null;
                            foreach( $join[ 'edges' ] as $ed )
                            {
                                if( $ed[ 'name' ] == $key )
                                {
                                    $currentEdge = $ed;
                                    break;
                                }
                            }
                            $currentEdge[ 'unique' ] = $field[ 'unique' ];
                            $return .= self::getEdges( [ $currentEdge ] , 'doc_' . $join['name'] , $lang);
                        }
                        break ;
                    case Thing::FILTER_EDGE_COUNT :
                        if( array_key_exists( 'edges' , $edge ) && count( $edge['edges'] ) > 0 )
                        {
                        $currentEdge = null;
                        foreach( $join[ 'edges' ] as $ed )
                        {
                            if( 'num' . ucfirst($ed[ 'name' ]) == $key )
                            {
                                $currentEdge = $ed;
                                break;
                            }
                        }
                        $return .= self::getEdgeCount( $field[ 'unique' ] , $currentEdge , 'doc_' . $join['name'] );
                        }
                        break ;
                    case Thing::FILTER_JOIN :
                    case Thing::FILTER_JOIN_ARRAY :
                    case Thing::FILTER_JOIN_MULTIPLE :
                        if( array_key_exists( 'joins' , $join ) && count( $join['joins'] ) > 0 )
                        {
                            $currentJoin = null;
                            foreach( $join[ 'joins' ] as $jo )
                            {
                                if( $jo[ 'name' ] == $key )
                                {
                                    $currentJoin = $jo;
                                    break;
                                }
                            }
                            $currentJoin[ 'unique' ] = $field[ 'unique' ];
                            $return .= self::getJoin([ $currentJoin ] , 'doc_' . $join['name'] , $lang);
                        }
                        break ;
                }
            }

            if( $fields == '*' )
            {
                $return .= ' RETURN doc_' . $join['name'] . ' ' ;
            }
            else
            {
                $return .= ' RETURN { ' . $this->getFilterFields( $fields , $join['name'] , $lang ) . ' } ' ;
            }

            if( array_key_exists( 'multiple' , $join ) && is_array( $join['multiple'] ) )
            {
                // tables

                // request
                if( array_key_exists( 'reverse' , $join ) && $join['reverse'] != '' )
                {
                    $request = 'FOR doc_' . $join['name'] . '_multiple IN [' . implode( ',' , $join['multiple'] ) . '] ' .
                        'FOR doc_' . $join['name'] . ' IN doc_' . $join['name'] . '_multiple ' .
                        'FILTER doc_' . $join['name'] . '.' . $join['reverse'] . ' ANY == ' . $docRef . '._id ' ;
                }
                else
                {
                    $request = 'FOR doc_' . $join['name'] . '_index IN ' . $docRef . '.' . $join['name'] . ' ' .
                        'LET doc_' . $join['name'] . '_multiple = UNION( ' . implode(',' , $join['multiple'] ) . ' ) ' .
                        'LET doc_' . $join['name'] . ' = NTH( doc_' . $join['name'] . '_multiple , POSITION( doc_' . $join['name'] . '_multiple[*]._id , doc_' . $join['name'] . '_index , true ) ) ';
                }

            }
            else if( array_key_exists( 'array' , $join ) && $join['array'] == true )
            {
                // table
                $table = $this->container[$join['controller']]->model->table ;

                $refArray = 'ref_' . $join['name'] ;

                // position
                $position = '' ;
                if( array_key_exists( 'position' , $fields ) )
                {
                    $position = 'LET ' . $fields['position']['unique'] . ' = POSITION( ' . $docRef . '.' . $join['name'] . ' , ' . $refArray . ' , true ) ' ;
                }

                // request
                $request = 'FOR ' . $refArray . ' IN ' . $docRef . '.' . $join['name'] . ' ' .
                    $position .
                    'FOR doc_' . $join['name'] . ' IN ' . $table . ' ' .
                    'FILTER doc_' . $join['name'] . '.' . $joinKey . ' == TO_STRING( ' . $refArray . ' ) ' ;

                // inject position in return query
            }
            else
            {
                // table
                $table = $this->container[$join['controller']]->model->table ;

                // request
                $request = 'FOR doc_' . $join['name'] . ' IN ' . $table . ' ' .
                    // remove 'LIMIT 1' to avoid bug with fullCount with arangodb
                    //'FILTER doc_' . $join['name'] . '.' . $joinKey . ' == TO_STRING( ' . $docRef . '.' . $join['name'] . ' ) LIMIT 1';
                    'FILTER doc_' . $join['name'] . '.' . $joinKey . ' == TO_STRING( ' . $docRef . '.' . $join['name'] . ' ) ';

            }

            $joins_query .= ' LET ' . $name . ' = ' .
                '( ' .
                $request .
                $return .
                ') ';
        }

        return $joins_query ;
    }

    /**
     * Returns the collection of all wanted sortable fields (ASC and DESC).
     *
     * @param array $facets
     *
     * @return array
     */
    public function getQueryFacets( $facets , $doc = 'doc' )
    {
        $condition = '' ;
        $values    = [] ;

        if
        (
              isset($this->facetable) && is_array($this->facetable)
           && isset($facets)    && is_array($facets)
        )
        {
            foreach( $facets as $key => $value )
            {
                if( array_key_exists( $key , $this->facetable ) )
                {
                    try
                    {
                        $set = $this->facetable[$key] ;
                        $id  = reset( $set ) ;
                        $keyTable = key( $set ) ;

                        switch( $id )
                        {
                            case 'edge' :
                            {
                                $items  = NULL ;
                                $fields = array_key_exists( 'fields' , $set ) ? $set['fields'] : '_key' ;
                                $edge   = array_key_exists( 'edge' , $set ) ? $set['edge'] : NULL ;

                                $condition = ' LENGTH( '.
                                            ' FOR doc_' . $key . ' IN INBOUND ' . $doc . ' ' . $edge . ' ' .
                                            ' FILTER doc_' . $key . '.' . $fields . ' == @' . $key .
                                            ' RETURN doc_' . $key . '._key ' .
                                            ') > 0';

                                $values[$key] = [ 'value' => (string) $value ] ;

                                break ;
                            }
                            case 'edgeComplex' :
                            {
                                $items  = NULL ;
                                $edge   = array_key_exists( 'edge' , $set ) ? $set['edge'] : NULL ;

                                $filter = [] ;

                                foreach( $value as $keyM => $s )
                                {
                                    $values[$key.$keyM] = [ 'value' => $s ] ;

                                    array_push( $filter , 'doc_' . $key . '.' . $keyM . ' == @' . $key . $keyM ) ;
                                }

                                $condition = ' LENGTH( '.
                                    ' FOR doc_' . $key . ' IN INBOUND ' . $doc . ' ' . $edge . ' ' .
                                    ' FILTER ' . implode( ' && ' , $filter ) .
                                    ' RETURN doc_' . $key . '._key ' .
                                    ') > 0';

                                break ;
                            }
                            case 'thesaurus' :
                            {
                                $items  = NULL ;
                                $fields = array_key_exists( 'fields' , $set ) ? $set['fields'] : '_key,name,alternateName' ;
                                $edge   = array_key_exists( 'edge' , $set ) ? $set['edge'] : NULL ;

                                $filter = [] ;


                                foreach( explode( ',' , $value ) as $keyM => $s )
                                {
                                    if( $s[0] == '-' )
                                    {
                                        $s = ltrim( $s , '-' );
                                        $contains = ' !CONTAINS' ;
                                        $glue = ' && ' ;
                                    }
                                    else
                                    {
                                        $contains = ' CONTAINS' ;
                                        $glue = ' || ' ;
                                    }

                                    $values[$key.$keyM] = [ 'value' => (string) $s ] ;

                                    $search = [] ;

                                    foreach( explode( ',' , $fields ) as $field )
                                    {
                                        array_push( $search , $contains . '( doc_' . $key . '.' . $field .' , @' . $key . $keyM . ' ) ' ) ;
                                    }
                                    array_push( $filter , '(' . implode( $glue , $search ) . ')' ) ;
                                }

                                $condition = ' LENGTH( '.
                                    ' FOR doc_' . $key . ' IN INBOUND ' . $doc . ' ' . $edge . ' ' .
                                    ' FILTER ( ' . implode( ' && ' , $filter ) . ' ) ' .
                                    ' RETURN doc_' . $key . '._key ' .
                                    ') > 0';


                                break ;
                            }

                            case 'list' :

                                if( strlen($condition) > 0 )
                                {
                                    $condition .= ' && ' ;
                                }

                                if( is_array( $value ) && count( $value ) == 1 && array_key_exists( 'length' , $value ) )
                                {
                                    $condition .= ' LENGTH( FOR doc_length IN ' . $this->table . ' FILTER ' . $doc . '._id IN doc_length.' . $keyTable . ' LIMIT 1 RETURN 1 ) == ' . $value['length'] ;
                                }
                                else
                                {
                                    $multiple = explode( ',' , $value ) ;

                                    $arr = [] ;
                                    foreach( $multiple as $keyM => $valueM )
                                    {
                                        $values[$key.$keyM] = [ 'value' => $valueM ] ;
                                        $arr[] = "@" . $key . $keyM ;
                                    }

                                    $condition .= ' TO_ARRAY( [ ' . implode( ',' , $arr ) . ' ] ) ANY IN ' . $doc . '.' . $keyTable ;
                                }

                                break ;

                            case 'listField' :

                                if( strlen($condition) > 0 )
                                {
                                    $condition .= ' && ' ;
                                }

                                $multiple = explode( ',' , $value ) ;

                                $arr = [] ;
                                foreach( $multiple as $keyM => $valueM )
                                {
                                    $values[$key.$keyM] = [ 'value' => $valueM ] ;
                                    $arr[] = "@" . $key . $keyM ;
                                }

                                $condition .= ' TO_ARRAY( [ ' . implode( ',' , $arr ) . ' ] ) ANY == ' . $doc . '.' . $keyTable ;

                                break ;

                            case 'listFieldSorted' :

                                if( strlen($condition) > 0 )
                                {
                                    $condition .= ' && ' ;
                                }

                                $multiple = explode( ',' , $value ) ;

                                $arr = [] ;
                                foreach( $multiple as $keyM => $valueM )
                                {
                                    $values[$key.$keyM] = [ 'value' => $valueM ] ;
                                    $arr[] = "@" . $key . $keyM ;
                                }

                                $condition .= ' TO_ARRAY( [ ' . implode( ',' , $arr ) . ' ] ) ANY == ' . $doc . '.' . $keyTable ;
                                $condition .= ' SORT POSITION( [ ' . implode( ',' , $arr ) . ' ] , ' . $doc . '.' . $keyTable . ' , true ) ';

                                break ;

                            default :
                            case 'field' :
                            {
                                if( strlen($condition) > 0 )
                                {
                                    $condition .= ' && ' ;
                                }

                                $multiple = explode( ',' , $value ) ;

                                foreach( $multiple as $keyM => $valueM )
                                {
                                    if( (!empty($valueM) && strlen($valueM) > 1 && $valueM[0] == '-' ) )
                                    {
                                        $valueM = ltrim($valueM,'-') ;
                                        $eq = ' !' ;

                                        if( $keyM > 0 )
                                        {
                                            $condition .= ' && ' ;
                                        }
                                    }
                                    else
                                    {
                                        $eq = ' =' ;

                                        if( $keyM > 0 )
                                        {
                                            $condition .= ' || ' ;
                                        }
                                    }

                                    $condition .= $doc . '.' . $keyTable . $eq . '~ @' . $key . $keyM ;

                                    $values[$key.$keyM] = [ 'value' => $valueM ] ;

                                }

                                if( strlen($condition) > 0 )
                                {
                                    $condition = ' ( ' . $condition . ' ) ' ;
                                }

                                break ;
                            }
                        }
                    }
                    catch( Exception $e )
                    {
                        $this->container->logger->warn( $this . " all failed, the '" . $key . "' facet failed, " . $e->getMessage() ) ;
                    }
                }
            }
        }

        return [ 'facets' => $condition , 'values' => $values ] ;
    }

    /**
     * Returns the collection of all wanted groupable fields.
     *
     * @param string $expression
     *
     * @return NULL|array
     */
    public function getQueryGroups( $expression )
    {
        $groups = NULL ;

        if( empty($expression) )
        {
            return $groups ;
        }

        $gr = explode( ',' , $expression ) ;
        if( count($gr) > 0 )
        {
            foreach( $gr as $value )
            {
                if( array_key_exists( $value , $this->sortable ) )
                {
                    $groups[] = $value . ' = doc.' . $value ;
                }
            }
        }

        return $groups ;
    }

    /**
     * Returns the collection of all wanted sortable fields (ASC and DESC).
     *
     * @param string $expression
     * @param string $doc
     *
     * @return NULL|array
     */
    public function getQueryOrders( $expression , $doc = 'doc' )
    {
        $orders = NULL ;

        if( empty($expression) )
        {
            return $orders ;
        }

        $sort = explode( ',' , $expression ) ;

        if( count($sort) > 0 )
        {
            $orders = [] ;
            foreach( $sort as $value )
            {
                if( !empty($value) )
                {
                    $first = $value[ 0 ];

                    if( $first == "-" )
                    {
                        $order = 'DESC';
                        $value = ltrim($value , "-");
                    }
                    else
                    {
                        $order = 'ASC';
                    }

                    if( array_key_exists($value , $this->sortable) )
                    {
                        $value = $this->sortable[ $value ];

                        $expression = '';
                        $expression .= $doc . '.' . $value;
                        $expression .= ' ' . $order;

                        $orders[] = $expression;
                    }
                }
            }
        }

        return $orders ;
    }

    /**
     * Returns the collection of all wanted sortable fields (ASC and DESC).
     *
     * @param string $expression
     * @param array $fields
     * @param string $doc
     *
     * @return NULL|array
     */
    public function getQuerySearch( $expression , array $fields = [ 'name' ] , $doc = 'doc' )
    {
        if
        (
            isset( $expression ) && !empty($expression) &&
            is_array( $fields )  && !empty($fields)
        )
        {
            $condition = '' ;
            $words = explode ( ' ' , $expression ) ;
            $i     = 0 ;
            if( count($words) > 0 )
            {
                foreach( $words as $word )
                {
                    $condition .= (($i == 0) ? '' : ' && ' ) ;
                    $j = 0 ;
                    foreach( $fields as $field )
                    {
                        if( $j > 0 )
                        {
                            $condition .= ' || ' ;
                        }
                        $condition .= "LIKE( $doc.$field , @search" . $i . ' , true ) ' ;
                        $j++ ;
                    }
                    $i++ ;
                }
            }

            return [ 'search' => ' ( ' . $condition . ' ) ' , 'words' => $words ] ;
        }

        return NULL ;
    }

    /**
     * Insert a new item into the table
     *
     * @param array $init
     *
     * @return mixed
     */
    public function insert( $init )
    {
        if( $init && is_array( $init ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add dates
            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table . ' RETURN NEW';

            $this->container->arango->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->container->arango->execute() ;

            return $this->container->arango->getObject() ;
        }
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_UPDATE_DEFAULT =
    [
        'active' => NULL,
        'key'    => '_key'
    ] ;

    /**
     * Update the specific item.
     *
     * @param mixed $item
     * @param string|integer $keyValue
     * @param array $init
     *
     * @return bool
     */
    public function update( $item , $keyValue , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_UPDATE_DEFAULT , $init ) ) ;

        $fields = [] ;
        $binds  = [ 'key' => $keyValue ] ;

        foreach( $this->fillable as $property => $filter )
        {
            if
            (
                is_array( $item ) && array_key_exists( $property , $item )
            )
            {
                $fields[] = $property . ': @' . $property ;
                $binds[ $property ] = $item[$property] ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
            ' UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * The default 'update date' method options.
     */
    const ARGUMENTS_UPDATE_DATE_DEFAULT =
    [
        'key'    => '_key'
    ] ;

    public function updateDate( $value , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_UPDATE_DATE_DEFAULT , $init ) ) ;

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ' .
            ' UPDATE doc WITH { modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'value' => $value
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    // ------------

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class($this) . ']' ;
    }
}


