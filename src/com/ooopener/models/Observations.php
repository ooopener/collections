<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;
use Slim\Container;

class Observations extends Collections
{
    public function __construct( Container $container = NULL , string $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'name'        => Thing::FILTER_DEFAULT,
        'path'        => Thing::FILTER_DEFAULT,
        'active'      => Thing::FILTER_INT,
        'withStatus'  => Thing::FILTER_DEFAULT,

        'endDate'     => Thing::FILTER_DEFAULT,
        'eventStatus' => Thing::FILTER_DEFAULT,
        'startDate'   => Thing::FILTER_DEFAULT,

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,

        'review'      => Thing::FILTER_DEFAULT,

        'image'       => Thing::FILTER_INT,

        'firstOwner'  => Thing::FILTER_INT,

        'isBasedOn'   => Thing::FILTER_DEFAULT
    ];
}
