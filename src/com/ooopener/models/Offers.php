<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Offers model.
 */
class Offers extends Things
{
    /**
     * Creates a new Offers instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'            => Thing::FILTER_INT,
        'name'          => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'category'      => Thing::FILTER_DEFAULT,
        'price'         => Thing::FILTER_DEFAULT,
        'priceCurrency' => Thing::FILTER_DEFAULT,
        'validFrom'     => Thing::FILTER_DEFAULT,
        'validThrough'  => Thing::FILTER_DEFAULT
    ];


}


