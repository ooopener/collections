<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The OpeningHoursSpecifications model.
 */
class OpeningHoursSpecifications extends Things
{
    /**
     * Creates a new OpeningHoursSpecifications instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'            => Thing::FILTER_INT,
        'name'          => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'dayOfWeek'     => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'opens'         => Thing::FILTER_DEFAULT,
        'closes'        => Thing::FILTER_DEFAULT,
        'validFrom'     => Thing::FILTER_DEFAULT,
        'validThrough'  => Thing::FILTER_DEFAULT
    ];

}


