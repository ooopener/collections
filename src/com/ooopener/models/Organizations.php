<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Organizations model.
 */
class Organizations extends Collections
{
    /**
     * Creates a new Organizations instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        // place

        'id'         => Thing::FILTER_INT,
        'identifier' => Thing::FILTER_INT,

        'path'       => Thing::FILTER_DEFAULT,

        'name'       => Thing::FILTER_DEFAULT,
        'legalName'  => Thing::FILTER_DEFAULT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,

        // dates
        'dissolutionDate' => Thing::FILTER_DEFAULT,
        'foundingDate'    => Thing::FILTER_DEFAULT,

        // legal
        'taxID' => Thing::FILTER_DEFAULT,
        'vatID' => Thing::FILTER_DEFAULT,

        // postal address

        'address'    => Thing::FILTER_DEFAULT,

        // texts

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'slogan'        => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,

        'audios'        => Thing::FILTER_DEFAULT,
        'photos'        => Thing::FILTER_DEFAULT,
        'videos'        => Thing::FILTER_DEFAULT,

        'isBasedOn'     => Thing::FILTER_DEFAULT
    ];

}

