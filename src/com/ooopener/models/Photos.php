<?php

namespace com\ooopener\models ;

use com\ooopener\things\Thing;

use Slim\Container;

class Photos extends Things
{
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'owner' => Thing::FILTER_INT,
        'media' => Thing::FILTER_INT,
    ];
}