<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Places model.
 */
class Places extends Collections
{
    /**
     * Creates a new Places instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        // place

        'id'     => Thing::FILTER_INT,
        'name'   => Thing::FILTER_DEFAULT,
        'active' => Thing::FILTER_INT,
        'withStatus'  => Thing::FILTER_DEFAULT,

        'path'   => Thing::FILTER_DEFAULT,

        'status' => Thing::FILTER_DEFAULT,

        // geo

        'geo'        => Thing::FILTER_DEFAULT,
        'elevation'  => Thing::FILTER_DEFAULT,

        // address

        'address'    => Thing::FILTER_DEFAULT,

        // services

        'services' => Thing::FILTER_DEFAULT,

        'capacity'          => Thing::FILTER_INT,
        'numAttendee'       => Thing::FILTER_INT,
        'remainingAttendee' => Thing::FILTER_INT,
        'publicAccess'      => Thing::FILTER_INT,

        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'slogan'        => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,
        'notes'         => Thing::FILTER_DEFAULT,

        'audios'        => Thing::FILTER_DEFAULT,
        'photos'        => Thing::FILTER_DEFAULT,
        'videos'        => Thing::FILTER_DEFAULT,

        'isBasedOn'     => Thing::FILTER_DEFAULT
    ];

    ///////////////////////////

    /**
     * The default 'near' method options.
     */
    const ARGUMENTS_NEAR_DEFAULT =
    [
        'active'      => TRUE,
        'conditions'  => [],
        'distance'    => 0,
        'facets'      => NULL,
        'fetchStyle'  => NULL,
        'lang'        => NULL,
        'latitude'    => 0,
        'longitude'   => 0,
        'limit'       => 0,
        'order'       => 'ASC',
        'offset'      => 0,
        'queryFields' => NULL ,
        'search'      => NULL,
        'fields'      => '*' ,
        'values'      => NULL,
        'words'       => NULL
    ] ;

    /**
     * Returns all elements from a specific date.
     *
     * @param array $init
     *
     * @return array all elements from a specific date.
     */
    public function near( $init = [] )
    {
        // -------- INIT

        extract( array_merge( self::ARGUMENTS_NEAR_DEFAULT , $init ) ) ;

        $binds = [] ;

        $binds['latitude']  = $latitude ;
        $binds['longitude'] = $longitude ;

        // -------- SELECT

        $query = 'FOR doc IN ' . $this->table ;

        $query .= ' LET distance = DISTANCE( doc.geo.latitude , doc.geo.longitude , @latitude , @longitude ) ' ;

        if( $distance > 0 )
        {
            $binds['radius']    = $distance ;

            $query .= ' FILTER distance <= @radius ' ;
        }

        $query .= ' SORT distance ASC ' ;

        // add distance query field
        if( $queryFields && is_array($queryFields) )
        {
            $queryFields[Thing::FILTER_DISTANCE] = [ 'filter' => Thing::FILTER_DISTANCE ];
        }
        /*else
        {
            $fields .= ',distance' ;
        }*/


        // -------- LIMIT / OFFSET

        if( $limit > 0 )
        {
            $query .= ' LIMIT @offset , @limit' ;

            $binds['offset'] = $offset ;
            $binds['limit'] = $limit ;
        }

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getAll() ;
    }

    /**
     * The default 'sameGeoExists' method options.
     */
    const ARGUMENTS_SAME_GEO_EXISTS_DEFAULT =
    [
        'active'      => TRUE,
        'lang'        => NULL,
        'latitude'    => 0,
        'longitude'   => 0,
        'queryFields' => NULL ,
        'fields'      => '*' ,
        'values'      => NULL,
        'words'       => NULL
    ] ;

    public function sameGeoExists( $init = [] )
    {
        // -------- INIT

        extract( array_merge( self::ARGUMENTS_SAME_GEO_EXISTS_DEFAULT , $init ) ) ;

        $binds = [] ;

        $binds['latitude']  = $latitude ;
        $binds['longitude'] = $longitude ;

        // -------- SELECT

        $query = 'FOR doc IN ' . $this->table ;

        $query .= ' FILTER doc.geo.latitude == @latitude && doc.geo.longitude == @longitude ' ;

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }
}

