<?php

namespace com\ooopener\models;

use com\ooopener\things\Session;
use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * This service to manage the sessions table.
 */
class Sessions extends Collections
{
    /**
     * Creates a new Sessions instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'type'       => Thing::FILTER_DEFAULT ,
        'user'       => Thing::FILTER_DEFAULT ,
        'client_id'  => Thing::FILTER_DEFAULT ,
        'app'        => Thing::FILTER_DEFAULT ,
        'token_id'   => Thing::FILTER_DEFAULT ,
        'refresh'    => Thing::FILTER_DEFAULT ,
        'ip'         => Thing::FILTER_DEFAULT ,
        'path'        => Thing::FILTER_DEFAULT ,
        'agent'      => Thing::FILTER_DEFAULT ,
        'expired'    => Thing::FILTER_DEFAULT ,
        'logout'     => Thing::FILTER_DEFAULT
    ];

    /**
     * The enumeration of all the fillable fields.
     */
    public $sortable =
    [
        'id'        => '_key',
        'type'      => 'type' ,
        'user'      => 'user' ,
        'client_id' => 'client_id' ,
        'created'   => 'created'
    ];

    ///////////////////////////

    /**
     * check if token exist and not revoked
     *
     * @param $token_id string
     * @param $agent string
     *
     * @return bool
     */
    public function check( $token_id , $agent )
    {
        $query = 'RETURN COUNT( FOR doc IN ' . $this->table .
            ' FILTER doc.token_id == @token_id && doc.agent == @agent && doc.revoked == 0 ' .
            ' RETURN 1 )' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "agent"    => $agent ,
                "token_id" => $token_id
            ]
        ]);

        $this->container->arango->execute() ;

        return (bool) $this->container->arango->getFirstResult() ;
    }

    /**
     * check if refresh token exist and not revoked
     *
     * @param $client_id string
     * @param $refresh string
     *
     * @return bool
     */
    public function checkRefresh( $client_id , $refresh )
    {
        $query = 'FOR doc IN ' . $this->table .
            ' FILTER doc.client_id == @client_id && doc.refresh == @refresh && doc.revoked == 0 RETURN doc' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "client_id"     => $client_id ,
                "refresh"    => $refresh
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Returns the specific token representation by property.
     *
     * @param string $name
     * @param mixed $value
     * @param string $filter
     *
     * @return object The specific token representation by property.
     */
    public function getByProperty( $name , $value , $filter = null  )
    {
        $query = 'FOR doc IN ' . $this->table .
            ' FILTER doc.' . $name . ' == @name RETURN doc' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "name" => $value
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Insert a new item into the table.
     *
     * @param array $init
     *
     * @return NULL|integer
     */
    public function insert( $init )
    {
        if( $init && ( $init instanceof Session || is_array( $init ) ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( ( $init instanceof Session ) && property_exists( $init , $property ) )
                {
                    $binds[ $property ] = $init->{$property} ;
                    $values[] = $property . ':@' . $property ;
                }
                else if( ( is_array( $init ) && array_key_exists( $property , $init ) ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add dates
            $values[] = 'path: "sessions"' ;
            $values[] = 'revoked: 0' ;
            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table ;

            $this->container->arango->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->container->arango->execute() ;

            return TRUE ;
        }

        return null ;
    }

    /**
     * Update token
     *
     * @param string $refresh
     * @param Session $token
     *
     * @return bool
     */
    public function updateToken( $refresh , $token )
    {
        $query = 'FOR doc IN ' . $this->table .
            ' FILTER doc.refresh == @refresh ' .
            ' UPDATE doc WITH { token_id: @token_id , expired: @expired ,modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "token_id"   => $token->token_id ,
                "expired"    => $token->expired ,
                "refresh"    => $refresh
            ]
        ]);

        $this->container->arango->execute() ;

        return TRUE ;
    }

    /**
     * @param $user
     * @param $client
     * @param $ip
     * @param $agent
     *
     * @return bool
     */
    public function logoutAccessToken( $user , $client , $ip , $agent )
    {
        $query = 'FOR doc IN ' . $this->table ;

        // filter
        $query .= ' FILTER doc.type == @type && doc.user == @user && doc.client_id == @client_id && doc.ip == @ip && doc.agent == @agent' .
            ' && doc.revoked == 0 && doc.logout == null && doc.expired > DATE_ISO8601( DATE_NOW() )' ;

        // update
        $query .= ' UPDATE doc WITH { revoked: @revoked , logout: DATE_ISO8601( DATE_NOW() ) , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "revoked"    => 1 ,
                "type"       => "access_token" ,
                "user"       => $user ,
                "client_id"  => $client ,
                "ip"         => $ip ,
                "agent"      => $agent
            ]
        ]);

        $this->container->arango->execute() ;

        $result = $this->container->arango->getResult() ;

        return $result ;
    }

    /**
     * Logout user
     *
     * @param $user string
     * @param $ip string
     * @param $agent string
     *
     * @return bool
     */
    public function logoutIDToken( $user , $ip , $agent )
    {
        $query = 'FOR doc IN ' . $this->table ;

        // filter
        $query .= ' FILTER doc.type == @type && doc.user == @user && doc.ip == @ip && doc.agent == @agent' .
            ' && doc.revoked == 0 && doc.logout == null && doc.expired > DATE_ISO8601( DATE_NOW() )' ;

        // update
        $query .= ' UPDATE doc WITH { revoked: @revoked , logout: DATE_ISO8601( DATE_NOW() ) , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "revoked"    => 1 ,
                "type"       => "id_token" ,
                "user"       => $user ,
                "ip"         => $ip ,
                "agent"      => $agent
            ]
        ]);

        $this->container->arango->execute() ;

        $result = $this->container->arango->getResult() ;

        return $result ;
    }

    /**
     * Revoke token
     *
     * @param $token_id
     *
     * @return bool
     */
    public function revoke( $token_id )
    {
        $query = 'FOR doc IN '  . $this->table .
            ' FILTER doc.token_id == @token_id && doc.revoked == 0 ' .
            ' UPDATE doc WITH { revoked: @revoked , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table .
            ' RETURN NEW' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'token_id' => $token_id ,
                'revoked'  => 1
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Revoke token
     *
     * @param $id
     *
     * @return bool
     */
    public function revokeID( $id )
    {
        $query = 'FOR doc IN '  . $this->table . ' FILTER doc._key == @id' .
            ' UPDATE doc WITH { revoked: @revoked , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table . ' RETURN NEW' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'id'      => $id ,
                'revoked' => 1
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_REVOKE_DEFAULT =
    [
        'key'    => '_key'
    ] ;

    public function revokeIDAll( $items , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_REVOKE_DEFAULT , $init ) ) ;

        $in = [] ;
        $binds = [] ;

        foreach( $items as $keyB => $value )
        {
            $in[] = 'doc.' . $key . ' == @i' . $keyB ;
            $binds['i' . $keyB] = $value ;
        }

        $query = 'FOR doc IN ' . $this->table . ' FILTER ( ' . implode(' OR ' , $in ) . ' ) ' ;

        //
        $query .= ' UPDATE doc WITH { revoked: @revoked , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table ;

        $binds['revoked'] = 1 ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return TRUE ;
    }


    /**
     * Revoke all sessions from the users list
     *
     * @param array $list
     */
    public function revokeAllByIds( $list )
    {
        foreach( $list as $user )
        {
            $this->revokeAll( $user ) ;
        }
    }

    /**
     * Revoke all sessions for the user
     *
     * @param string $user
     *
     * @return bool
     */
    public function revokeAll( $user )
    {
        $query = 'FOR doc IN '  . $this->table .
            ' FILTER doc.user == @user && doc.revoked == 0 ' .
            ' UPDATE doc WITH { revoked: @revoked , modified: DATE_ISO8601( DATE_NOW() ) } IN ' . $this->table .
            ' RETURN NEW' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                'user'     => $user ,
                'revoked'  => 1
            ]
        ]);

        $this->container->arango->execute() ;

        return TRUE ;
    }
}


