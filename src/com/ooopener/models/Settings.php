<?php

namespace com\ooopener\models;

use Slim\Container;

class Settings extends Edges
{
    /**
     * Settings constructor.
     *
     * @param Container $container
     * @param string|NULL $table
     * @param array $init
     */
    public function __construct( Container $container , string $table = NULL , array $init = [] )
    {
        parent::__construct
        (
            $container ,
            $table ,
            array_merge( self::ARGUMENTS_CONSTRUCTOR_DEFAULT , is_array($init) ? $init : [] )
        );
    }

    /**
     * The default 'constructor' default arguments.
     */
    const ARGUMENTS_CONSTRUCTOR_DEFAULT =
    [
        'facetable'  => NULL,
        'searchable' => [ 'fr', 'en' , 'it' , 'es' , 'de' , 'alternateName' ],
        'sortable'   =>
        [
            'id'            => 'CAST(id as SIGNED INTEGER)',
            'fr'            => 'name.fr',
            'en'            => 'name.en',
            'it'            => 'name.it',
            'es'            => 'name.es',
            'de'            => 'name.de',
            'alternateName' => 'alternateName',
            'active'        => 'CAST(active as SIGNED INTEGER)',
            'created'       => 'created',
            'modified'      => 'modified'
        ]
    ] ;
}