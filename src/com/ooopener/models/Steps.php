<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Steps model.
 */
class Steps extends Collections
{
    /**
     * Creates a new Steps instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct(Container $container = NULL, $table = NULL, array $init = [])
    {
        parent::__construct($container, $table, $init);
    }
    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        // courses

        'name'   => Thing::FILTER_DEFAULT,
        'active' => Thing::FILTER_INT,
        'withStatus'  => Thing::FILTER_DEFAULT,

        'position' => Thing::FILTER_INT,

        'path'    => Thing::FILTER_DEFAULT,

        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,

        'audios'        => Thing::FILTER_DEFAULT,
        'photos'        => Thing::FILTER_DEFAULT,
        'videos'        => Thing::FILTER_DEFAULT,

        'isBasedOn'     => Thing::FILTER_DEFAULT

    ];

    ///////////////////////////

}
