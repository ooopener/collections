<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The TeamPermissions model.
 */
class TeamPermissions extends Model
{
    /**
     * Creates a new TeamPermissions instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'team'       => Thing::FILTER_INT,
        'module'     => Thing::FILTER_DEFAULT,
        'resource'   => Thing::FILTER_INT,
        'permission' => Thing::FILTER_DEFAULT,
        'visible'    => Thing::FILTER_BOOL
    ];

    ///////////////////////////

}


