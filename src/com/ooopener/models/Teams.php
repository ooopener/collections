<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Teams model.
 */
class Teams extends Collections
{
    /**
     * Creates a new Teams instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'active'        => Thing::FILTER_INT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'color'         => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'identifier'    => Thing::FILTER_INT,
        'name'          => Thing::FILTER_DEFAULT,
        'path'          => Thing::FILTER_DEFAULT,
        'permissions'   => Thing::FILTER_DEFAULT
    ];


}


