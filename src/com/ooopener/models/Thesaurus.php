<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The thesaurus model.
 */
class Thesaurus extends Model
{
    /**
     * Creates a new Thesaurus instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , $init = [] )
    {
        parent::__construct
        (
            $container ,
            $table ,
            array_merge( self::ARGUMENTS_CONSTRUCTOR_DEFAULT , is_array($init) ? $init : [] )
        );
    }

    /**
     * The default 'constructor' default arguments.
     */
    const ARGUMENTS_CONSTRUCTOR_DEFAULT =
    [
        'facetable'  =>
        [
            'alternateName' =>
            [
                'alternateName' => 'field' ,
                'type'          => Thing::FILTER_DEFAULT
            ],
            'name' =>
            [
                'name' => 'field' ,
                'type' => Thing::FILTER_DEFAULT
            ]
        ],
        'searchable' => [ 'name.fr', 'name.en' , 'name.it' , 'name.es' , 'name.de' , 'alternateName' ],
        'sortable'   =>
        [
            'id'            => 'CAST(id as SIGNED INTEGER)',
            'fr'            => 'name.fr',
            'en'            => 'name.en',
            'it'            => 'name.it',
            'es'            => 'name.es',
            'de'            => 'name.de',
            'alternateName' => 'alternateName',
            'active'        => 'CAST(active as SIGNED INTEGER)',
            'created'       => 'created',
            'modified'      => 'modified'
        ]
    ] ;

    // -----

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'            => Thing::FILTER_INT,
        'active'        => Thing::FILTER_BOOL,
        'image'         => Thing::FILTER_BOOL,
        'path'          => Thing::FILTER_DEFAULT,
        'name'          => Thing::FILTER_DEFAULT,
        'pattern'       => Thing::FILTER_DEFAULT,
        'format'        => Thing::FILTER_DEFAULT,
        'validator'     => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'color'         => Thing::FILTER_DEFAULT,
        'bgcolor'       => Thing::FILTER_DEFAULT
    ];

    // -----

    /**
     * The default 'all' methods options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'      => TRUE,
        'conditions'  => [],
        'facets'      => NULL,
        'limit'       => 0,
        'offset'      => 0,
        'sort'        => NULL,
        'groupBy'     => NULL,
        'search'      => NULL,
        'fields'      => '*' ,
        'queryFields' => NULL ,
        'lang'        => NULL,
        'orders'      => NULL ,
        'groups'      => NULL ,
        'words'       => NULL
    ] ;

    /**
     * Returns all services elements.
     *
     * @param array $init
     *
     * @return array all services elements.
     */
    public function all( $init = [] )
    {
        // -------- INIT
        $this->container->logger->debug( $this . ' all' ) ;

        extract( array_merge( self::ARGUMENTS_ALL_DEFAULT , $init ) ) ;

        $binds = [] ;

        $query = 'FOR doc IN ' . $this->table ;

        // -------- WHERE

        $search = $this->getQuerySearch( $search , $this->searchable ) ;

        if( isset($search) )
        {
            extract( $search ) ;
        }

        if( !empty($search) )
        {
            $conditions[] = $search ;
        }

        if( isset( $words ) )
        {
            $i = 0 ;
            foreach( $words as $word )
            {
                $binds[ 'search' . $i++ ] = '%' . $word . '%' ;
            }
        }

        if( isset($active) && !is_null($active) )
        {
            $conditions[] = 'doc.active == ' . ((bool) $active ? '1' : '0') ;
        }

        extract( $this->getQueryFacets( $facets ) ) ; // $facets / values
        if( is_string($facets) && !empty($facets) )
        {
            $conditions[] = $facets ;
        }

        if( isset($values) && is_array($values) ) // see facets
        {
            foreach( $values as $key => $bindValue )
            {
                $binds[$key] = $bindValue['value'] ;
            }
        }

        if( count($conditions) > 0 )
        {
            $query .= ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // -------- GROUP BY

        $groups = $this->getQueryGroups( $groupBy ) ;
        if( isset($groups) && count($groups) > 0 )
        {
            $query .= ' COLLECT ' ;

            foreach( $groups as $value )
            {
                $query .= $value ;
                $query .= ", " ;
            }

            $query = rtrim( $query , ', ' ) ;

            $query .= ' ' ;
        }

        // -------- ORDER BY

        $orders = $this->getQueryOrders( $sort ) ;
        if( isset($orders) && count($orders) > 0 )
        {
            $query .= ' SORT ' ;

            $query .= implode( ', ' , $orders ) ;

            $query .= ' ' ;
        }

        // -------- LIMIT / OFFSET

        if( $limit > 0 )
        {
            $query .= ' LIMIT @offset , @limit' ;

            $binds['offset'] = $offset ;
            $binds['limit'] = $limit ;
        }

        // -------- EDGES && FIELDS

        $query .= $this->getFields( $fields , $queryFields , $lang ) ;

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getAll() ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'key'    => '_key'
    ] ;

    /**
     * Delete the specific item
     *
     * @param string|integer $value
     * @param array $init
     *
     * @return mixed
     */
    public function delete( $value , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ';

        // remove
        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Delete items
     *
     * @param $items
     * @param array $init
     *
     * @return bool
     */
    public function deleteAll( $items , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ) ;

        $in = [] ;
        $binds = [] ;

        foreach( $items as $keyB => $value )
        {
            $in[] = 'doc.' . $key . ' == @i' . $keyB ;
            $binds['i' . $keyB] = $value ;
        }

        $query = 'FOR doc IN ' . $this->table . ' FILTER ( ' . implode(' OR ' , $in ) . ' ) ' ;

        //
        $query .= ' REMOVE { _key : doc._key } IN ' . $this->table . ' RETURN OLD';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getAll() ;
    }


    /**
     * Indicates if the specific item exist (by alternateName only).
     *
     * @param string $value
     *
     * @return bool
     */
    public function existByAlternateName( $value )
    {
        return $this->exist( $value , [ 'key' => 'alternateName' ] ) ;
    }

    /**
     * Indicates if the specific item exist (by id only).
     *
     * @param string $value
     *
     * @return bool
     */
    public function existByID( $value )
    {
        return $this->exist( $value , 'id' ) ;
    }

    /**
     * Indicates if the specific item exist (by name only).
     *
     * @param string $value
     *
     * @return bool
     */
    public function existByName( $value )
    {
        return $this->exist( $value , 'fr,en,it,es,de' ) ;
    }

    /**
     * The default 'get' method options.
     */
    const ARGUMENTS_GET_DEFAULT =
    [
        'fields' => '*' ,
        'key'    => 'id'
    ] ;

    /**
     * Find the specific service by the alternateName.
     *
     * @param string $value
     *
     * @return object the specific service by the alternateName.
     */
    public function getByAlternateName( $value )
    {
        return $this->get( $value , [ 'key' => 'alternateName' ] ) ;
    }

    /**
     * Find the specific item by id or name.
     *
     * @param string $value
     *
     * @return object
     */
    protected function getByIDorNameInTable( $value )
    {
        return $this->getByValue( $value , 'id,fr,en,es,it,de' ) ;
    }

    /**
     * Insert a new item into the table
     *
     * @param array $init
     *
     * @return mixed
     */
    public function insert( $init )
    {
        if( $init && is_array( $init ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add dates
            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table . ' RETURN NEW';

            $this->container->arango->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->container->arango->execute() ;

            return $this->container->arango->getObject() ;
        }
    }



    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_UPDATE_DEFAULT =
    [
        'active' => NULL,
        'key'    => '_key'
    ] ;

    /**
     * Update the specific item.
     *
     * @param mixed $item
     * @param string|integer $keyValue
     * @param array $init
     *
     * @return bool
     */
    public function update( $item , $keyValue , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_UPDATE_DEFAULT , $init ) ) ;

        $fields = [] ;
        $binds  = [ 'key' => $keyValue ] ;

        foreach( $this->fillable as $property => $filter )
        {
            if
            (
                is_array( $item ) && array_key_exists( $property , $item )
            )
            {
                $fields[] = $property . ': @' . $property ;
                $binds[ $property ] = $item[$property] ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
            ' UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }
}


