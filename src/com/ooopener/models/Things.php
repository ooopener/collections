<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Things model.
 */
class Things extends Model
{
    /**
     * Creates a new Things instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , $init = [] )
    {
        extract( array_merge( self::ARGUMENTS_CONSTRUCTOR_DEFAULT , is_array($init) ? $init : [] ) ) ;
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'     => Thing::FILTER_INT,
        'owner'  => Thing::FILTER_INT,
        'name'   => Thing::FILTER_DEFAULT
    ];

    ///////////////////////////

    /**
     * The default 'constructor' default arguments.
     */
    const ARGUMENTS_CONSTRUCTOR_DEFAULT =
    [
        'facetable'  => NULL,
        'searchable' => NULL,
        'sortable'   =>
        [
            'id'       => 'CAST(id as SIGNED INTEGER)',
            'name'     => 'UPPER(name)',
            'created'  => 'created',
            'modified' => 'modified'
        ]
    ] ;

    ///////////////////////////

    /**
     * The owner identifier key.
     */
    public $ownerKey = 'owner' ;

    /**
     * The default 'all' method options.
     */
    const ARGUMENTS_ALL_DEFAULT =
    [
        'active'     => TRUE,
        'conditions' => [],
        'facets'     => NULL,
        'limit'      => 0,
        'offset'     => 0,
        'sort'       => NULL,
        'fields'     => '*' ,
        'orders'     => NULL
    ] ;

    /**
     * Returns the collection of all the things.
     *
     * @param integer|string $owner an optional owner identifier.
     * @param array $init
     *
     * @return array the collection of all the things.
     */
    public function all( $owner = NULL , $init = [] )
    {
        $init['conditions'] = [ 'doc.owner == ' . $owner ] ;
        return parent::all( $init ) ;
    }

    /**
     * The default 'delete' method options.
     */
    const ARGUMENTS_DELETE_DEFAULT =
    [
        'key'    => '_key'
    ] ;

    /**
     * Delete the specific item
     *
     * @param string|integer $value
     * @param array $init
     *
     * @return mixed
     */
    public function delete( $value , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_DELETE_DEFAULT , $init ) ) ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ';

        // remove
        $query .= ' REMOVE { _key: doc._key } IN ' . $this->table . ' RETURN OLD' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ 'value' => $value ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Indicates if the specific owner exist.
     *
     * @param integer|string $id
     *
     * @return bool
     */
    public function existOwner( $id )
    {
        return $this->exist( $id , $this->ownerKey ) ;
    }

    /**
     * The default 'update' method options.
     */
    const ARGUMENTS_UPDATE_DEFAULT =
    [
        'active' => NULL,
        'key'    => '_key'
    ] ;

    /**
     * Update the specific item.
     *
     * @param mixed $item
     * @param string|integer $keyValue
     * @param array $init
     *
     * @return bool
     */
    public function update( $item , $keyValue , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_UPDATE_DEFAULT , $init ) ) ;

        $fields = [] ;
        $binds  = [ 'key' => $keyValue ] ;

        foreach( $this->fillable as $property => $filter )
        {
            if
            (
                is_array( $item ) && array_key_exists( $property , $item )
            )
            {
                $fields[] = $property . ': @' . $property ;
                $binds[ $property ] = $item[$property] ;
            }
        }

        $fields[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @key ' .
            ' UPDATE doc WITH { ' . implode(', ' , $fields ) . ' } IN ' . $this->table . ' RETURN NEW';

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }
}


