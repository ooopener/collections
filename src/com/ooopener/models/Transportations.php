<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;
use Slim\Container;

class Transportations extends Things
{
    public function __construct( Container $container = NULL , string $table = NULL , array $init = [] )
    {
        parent::__construct($container , $table , $init);
    }

    public $fillable =
    [
        'additionalType' => Thing::FILTER_INT,
        'alternateName'  => Thing::FILTER_DEFAULT,
        'name'           => Thing::FILTER_DEFAULT,
        'duration'       => Thing::FILTER_DEFAULT,
        'comment'        => Thing::FILTER_DEFAULT
    ];
}