<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The UserPermissions model.
 */
class UserPermissions extends Model
{
    /**
     * Creates a new UserPermissions instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'user'       => Thing::FILTER_DEFAULT,
        'module'     => Thing::FILTER_DEFAULT,
        'resource'   => Thing::FILTER_INT,
        'permission' => Thing::FILTER_DEFAULT
    ];

    ///////////////////////////

}


