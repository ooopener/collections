<?php

namespace com\ooopener\models;

use com\ooopener\helpers\Status;
use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Users model.
 */
class Users extends Collections
{
    /**
     * Creates a new Users instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'            => Thing::FILTER_DEFAULT,
        'name'          => Thing::FILTER_DEFAULT,
        'uuid'          => Thing::FILTER_DEFAULT,

        'person'        => Thing::FILTER_DEFAULT,
        'team'          => Thing::FILTER_DEFAULT,

        'gender'        => Thing::FILTER_DEFAULT,
        'givenName'     => Thing::FILTER_DEFAULT,
        'familyName'    => Thing::FILTER_DEFAULT,

        'active'        => Thing::FILTER_DEFAULT,
        'image'         => Thing::FILTER_DEFAULT,
        'provider'      => Thing::FILTER_DEFAULT,
        'provider_uid'  => Thing::FILTER_DEFAULT,

        'email'         => Thing::FILTER_DEFAULT,
        'faxNumber'     => Thing::FILTER_DEFAULT,
        'telephone'     => Thing::FILTER_DEFAULT,

        // postal address

        'address'    => Thing::FILTER_DEFAULT
    ];

    ///////////////////////////

    const ARGUMENTS_GET_DEFAULT =
    [
        'conditions'  => [],
        'facets'      => NULL,
        'key'         => '_key',
        'lang'        => NULL,
        'limit'       => 0,
        'offset'      => 0,
        'sort'        => NULL,
        'search'      => NULL
    ] ;

    public function getFavorites( $value , $init = [] )
    {
        // ---- INIT

        extract( array_merge( self::ARGUMENTS_GET_DEFAULT , $init ) ) ;

        $model = $this->container->things ;

        $binds = [ "value" => $value ] ;



        // -------- WHERE

        $search = $this->getQuerySearch( $search , $model->searchable , 'doc_favorites' ) ;

        if( isset($search) )
        {
            extract( $search ) ;
        }

        if( !empty($search) )
        {
            $conditions[] = $search ;
        }

        if( isset( $words ) )
        {
            $i = 0 ;
            foreach( $words as $word )
            {
                $binds[ 'search' . $i++ ] = '%' . $word . '%' ;
            }
        }

        extract( $model->getQueryFacets( $facets , 'doc_favorites' ) ) ; // $facets / values
        if( is_string($facets) && !empty($facets) )
        {
            $conditions[] = $facets ;
        }

        if( isset($values) && is_array($values) ) // see facets
        {
            foreach( $values as $k => $bindValue )
            {
                $binds[$k] = $bindValue['value'] ;
            }
        }

        $filter = '' ;

        if( count($conditions) > 0 )
        {
            $filter = ' FILTER ' . implode( ' && ' , $conditions ) . ' ' ;
        }

        // ---- QUERY

        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.' . $key . ' == @value ' .
            'LET total = LENGTH( FOR doc_favorites IN INBOUND doc users_favorites ' . $filter . ' RETURN 1 ) ' .
            'LET favorites = ' .
            '( ' .
            'FOR doc_favorites , edge_favorites IN INBOUND doc users_favorites ' .
            'OPTIONS { bfs : true , uniqueVertices : "global" } ' .
            $filter
        ;

        // -------- ORDER BY

        if( $sort )
        {
            $orders = $this->getQueryOrders( $sort , 'doc_favorites' ) ;
            if( isset($orders) && count($orders) > 0 )
            {
                $query .= ' SORT ' ;

                foreach( $orders as $value )
                {
                    $query .= array_shift($orders) ;
                    $query .= ", " ;
                }

                $query = rtrim( $query , ', ' ) ;

                $query .= ' ' ;
            }
        }
        else
        {
            $query .= 'SORT edge_favorites.created DESC ' ;
        }

        // -------- LIMIT / OFFSET

        if( $limit > 0 )
        {
            $query .= ' LIMIT @offset , @limit' ;

            $binds['offset'] = $offset ;
            $binds['limit'] = $limit ;
        }

        $thingFields = $this->container->thingsController->getFields() ;
        $returnThing = $model->getFields( NULL , $thingFields , $lang , 'favorites' ) ;

        $query .= $returnThing . ' ' .
            ') RETURN { favorites : favorites , total : total }' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => $binds ,
            'fullCount' => (bool) $limit
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Insert a new item into the table.
     *
     * @param array $init
     *
     * @return NULL|integer
     */
    public function insert( $init )
    {
        if( $init && is_array( $init ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add default team and dates
            if( !array_key_exists( 'team' , $init ) )
            {
                $values[] = 'team: "guest"' ;
            }
            $values[] = 'modified: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table ;

            $this->container->arango->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->container->arango->execute() ;

            return TRUE ;

        }

        return null ;
    }


}


