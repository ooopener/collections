<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * This service to manage the users_auth_apps table.
 */
class UsersAuthApps extends Model
{
    /**
     * Creates a new UsersAuthApps instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'user_id'    => Thing::FILTER_INT,
        'client_id'  => Thing::FILTER_INT,
        'scope'      => Thing::FILTER_DEFAULT

    ];

    /**
     * The enumeration of all the fillable fields.
     */
    public $sortable =
    [
        'user_id'           => 'CAST(user_id as SIGNED INTEGER)',
        'client_id'         => 'CAST(client_id as SIGNED INTEGER)',

        'created'           => 'created',
        'modified'          => 'modified'
    ];

    ///////////////////////////

    /**
     * Returns the specific user representation.
     *
     * @param integer $user_id
     * @param integer $client_id
     * @param string $fields
     *
     * @return object the specific user representation.
     */
    public function getUserApp( $user_id , $client_id ,$fields = '*' )
    {
        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.user_id == @user && doc.client_id == @client_id RETURN doc' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' =>
            [
                "user"      => $user_id ,
                "client_id" => $client_id
            ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }
}


