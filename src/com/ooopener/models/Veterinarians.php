<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;
use Slim\Container;

class Veterinarians extends Collections
{
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'path'         => Thing::FILTER_DEFAULT,
        'identifier'   => Thing::FILTER_DEFAULT,
        'address'      => Thing::FILTER_DEFAULT,
        'active'       => Thing::FILTER_INT,
        'withStatus'   => Thing::FILTER_DEFAULT,

        'isBasedOn'     => Thing::FILTER_DEFAULT
    ];
}
