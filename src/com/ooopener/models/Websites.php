<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Websites model.
 */
class Websites extends Things
{
    /**
     * Creates a new Websites instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'             => Thing::FILTER_INT,
        'additionalType' => Thing::FILTER_INT,
        'alternateName'  => Thing::FILTER_DEFAULT,
        'name'           => Thing::FILTER_DEFAULT,
        'href'           => Thing::FILTER_DEFAULT,
        'order'          => Thing::FILTER_INT
    ];


}


