<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * The Workplaces model.
 */
class Workplaces extends Collections
{
    /**
     * Creates a new Workplaces instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'name'       => Thing::FILTER_DEFAULT,
        'path'       => Thing::FILTER_DEFAULT,
        'identifier' => Thing::FILTER_INT,
        'active'     => Thing::FILTER_INT,
        'withStatus' => Thing::FILTER_DEFAULT,

        'capacity'          => Thing::FILTER_INT,
        'numAttendee'       => Thing::FILTER_INT,
        'remainingAttendee' => Thing::FILTER_INT,

        'isBasedOn'     => Thing::FILTER_DEFAULT
    ];


}


