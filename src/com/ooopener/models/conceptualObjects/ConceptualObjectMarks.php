<?php

namespace com\ooopener\models\conceptualObjects ;

use com\ooopener\models\Things;
use com\ooopener\things\Thing;

use Slim\Container;

class ConceptualObjectMarks extends Things
{
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'name'          => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'position'      => Thing::FILTER_DEFAULT,
        'text'          => Thing::FILTER_DEFAULT,

        'additionalType' => Thing::FILTER_INT,
        'language'       => Thing::FILTER_INT,
        'technique'      => Thing::FILTER_INT
    ];
}
