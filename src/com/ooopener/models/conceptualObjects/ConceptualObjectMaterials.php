<?php

namespace com\ooopener\models\conceptualObjects ;

use com\ooopener\models\Things;
use com\ooopener\things\Thing;

use Slim\Container;

class ConceptualObjectMaterials extends Things
{
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'name'          => Thing::FILTER_DEFAULT,
        'alternateName' => Thing::FILTER_DEFAULT,
        'description'   => Thing::FILTER_DEFAULT,
        'material'      => Thing::FILTER_INT,
        'technique'     => Thing::FILTER_INT
    ];
}
