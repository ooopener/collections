<?php

namespace com\ooopener\models\conceptualObjects ;

use com\ooopener\models\Things;
use com\ooopener\things\Thing;

use Slim\Container;

class ConceptualObjectNumbers extends Things
{
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'name'           => Thing::FILTER_DEFAULT,
        'additionalType' => Thing::FILTER_INT,
        'alternateName'  => Thing::FILTER_DEFAULT,
        'date'           => Thing::FILTER_DEFAULT,
        'value'          => Thing::FILTER_DEFAULT
    ];
}
