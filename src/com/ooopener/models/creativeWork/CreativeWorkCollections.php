<?php

namespace com\ooopener\models\creativeWork;

use com\ooopener\models\Collections;
use com\ooopener\things\Thing;
use Slim\Container;

class CreativeWorkCollections extends Collections
{
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'active'      => Thing::FILTER_INT,
        'withStatus'  => Thing::FILTER_DEFAULT,

        'name'        => Thing::FILTER_DEFAULT,
        'path'        => Thing::FILTER_DEFAULT,
        'image'       => Thing::FILTER_INT,

        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,

        'endDate'     => Thing::FILTER_DEFAULT,
        'startDate'   => Thing::FILTER_DEFAULT
    ];
}
