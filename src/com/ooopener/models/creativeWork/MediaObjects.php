<?php

namespace com\ooopener\models\creativeWork ;

use com\ooopener\models\Collections;
use com\ooopener\things\Thing;

use Slim\Container;

class MediaObjects extends Collections
{
    public function __construct( Container $container = NULL , $table = NULL , $edgeAudio = NULL , $edgeAudios = NULL , $edgeImage = NULL , $edgeLogo = NULL , $edgePhotos = NULL , $edgeVideo = NULL , $edgeVideos = NULL , $tablesThings = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );

        $this->edgeAudio  = $edgeAudio ;
        $this->edgeAudios = $edgeAudios ;
        $this->edgeImage  = $edgeImage ;
        $this->edgeLogo   = $edgeLogo ;
        $this->edgePhotos = $edgePhotos ;
        $this->edgeVideo  = $edgeVideo ;
        $this->edgeVideos = $edgeVideos ;

        $this->tablesThings = $tablesThings ;
    }

    public $edgeAudio ;

    public $edgeAudios ;

    public $edgeImage ;

    public $edgeLogo ;

    public $edgePhotos ;

    public $edgeVideo ;

    public $edgeVideos ;

    public $tablesThings ;

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'active'              => Thing::FILTER_DEFAULT,
        'withStatus'          => Thing::FILTER_DEFAULT,
        'path'                => Thing::FILTER_DEFAULT,

        'owner'               => Thing::FILTER_DEFAULT,

        'author'              => Thing::FILTER_DEFAULT,
        'alternativeHeadline' => Thing::FILTER_DEFAULT,
        'bitrate'             => Thing::FILTER_INT,
        'contentSize'         => Thing::FILTER_INT,
        'contentUrl'          => Thing::FILTER_DEFAULT,
        'description'         => Thing::FILTER_DEFAULT,
        'duration'            => Thing::FILTER_DEFAULT,
        'editor'              => Thing::FILTER_DEFAULT,
        'embedUrl'            => Thing::FILTER_DEFAULT,
        'encoding'            => Thing::FILTER_DEFAULT,
        'encodingFormat'      => Thing::FILTER_DEFAULT,
        'headline'            => Thing::FILTER_DEFAULT,
        'height'              => Thing::FILTER_INT,
        'inLanguage'          => Thing::FILTER_DEFAULT,
        'keywords'            => Thing::FILTER_DEFAULT,
        'license'             => Thing::FILTER_DEFAULT,
        'mentions'            => Thing::FILTER_DEFAULT,
        'name'                => Thing::FILTER_DEFAULT,
        'notes'               => Thing::FILTER_DEFAULT,
        'playerType'          => Thing::FILTER_DEFAULT,
        'publisher'           => Thing::FILTER_DEFAULT,
        'review'              => Thing::FILTER_DEFAULT,
        'source'              => Thing::FILTER_DEFAULT,
        'text'                => Thing::FILTER_DEFAULT,
        'thumbnailUrl'        => Thing::FILTER_DEFAULT,
        'width'               => Thing::FILTER_INT,

        'actor'                => Thing::FILTER_DEFAULT,
        'caption'              => Thing::FILTER_DEFAULT,
        'director'             => Thing::FILTER_DEFAULT,
        'exifData'             => Thing::FILTER_DEFAULT,
        'musicBy'              => Thing::FILTER_DEFAULT,
        'representativeOfPage' => Thing::FILTER_DEFAULT,
        'thumbnail'            => Thing::FILTER_DEFAULT,
        'transcript'           => Thing::FILTER_DEFAULT,
        'videoFrameSize'       => Thing::FILTER_DEFAULT,
        'videoQuality'         => Thing::FILTER_DEFAULT,

        'audioCodec'           => Thing::FILTER_DEFAULT,
        'bitsPerSample'        => Thing::FILTER_DEFAULT,
        'channels'             => Thing::FILTER_DEFAULT,
        'sampleRate'           => Thing::FILTER_DEFAULT,
        'videoCodec'           => Thing::FILTER_DEFAULT,

    ];

    public function deleteAudio( $id )
    {
        return $this->deleteThings( $id , $this->edgeAudio ) ;
    }

    public function deleteAudios( $id )
    {
        return $this->deleteThings( $id , $this->edgeAudios , 'audios' ) ;
    }

    public function deleteImage( $id )
    {
        return $this->deleteThings( $id , $this->edgeImage ) ;
    }

    public function deleteLogo( $id )
    {
        return $this->deleteThings( $id , $this->edgeLogo ) ;
    }

    public function deletePhotos( $id )
    {
        return $this->deleteThings( $id , $this->edgePhotos , 'photos' ) ;
    }

    public function deleteVideo( $id )
    {
        return $this->deleteThings( $id , $this->edgeVideo ) ;
    }

    public function deleteVideos( $id )
    {
        return $this->deleteThings( $id , $this->edgeVideos , 'videos' ) ;
    }

    public function updateAllThings( $id , $type )
    {
        switch( $type )
        {
            case 'audio' :
                $audio  = $this->updateThings( $id , $this->edgeAudio ) ;
                $audios = $this->updateThings( $id , $this->edgeAudios ) ;
                break ;
            case 'image' :
                $image  = $this->updateThings( $id , $this->edgeImage ) ;
                $logo   = $this->updateThings( $id , $this->edgeLogo ) ;
                $photos = $this->updateThings( $id , $this->edgePhotos ) ;
                break ;
            case 'video' :
                $video  = $this->updateThings( $id , $this->edgeVideo ) ;
                $videos = $this->updateThings( $id , $this->edgeVideos ) ;
                break ;
        }

        return true ;
    }

    private function deleteThings( $id , $edge , $type = NULL )
    {
        $binds = [ 'from' => $this->table . '/' . $id  ] ;

        $query = 'FOR doc IN ' . $edge ;

        $query .= ' FILTER doc._from == @from ' ;

        if( is_array( $this->tablesThings ) && count( $this->tablesThings ) > 0 )
        {
            $query .= 'LET key = PARSE_IDENTIFIER( doc._to ).key ' ;

            foreach( $this->tablesThings as $thing )
            {
                // check collection exists
                if( $this->container->arango->collectionExists( $thing ) )
                {
                    $binds['@'.$thing] = $thing ;

                    $field = '' ;

                    if( $type )
                    {
                        $binds['mediaRef'] = (int) $id ;
                        $field = $type . ': REMOVE_VALUE( thing.' . $type . ' , @mediaRef ) , ' ;
                    }

                    $query .= 'LET updateThing_' . $thing . ' = ' .
                                '( ' .
                                'FOR thing IN @@' . $thing . ' ' .
                                'FILTER thing._key == key ' .
                                'UPDATE thing._key WITH { '. $field .' modified: DATE_ISO8601( DATE_NOW() ) } IN @@' . $thing . ' OPTIONS { ignoreErrors: true } RETURN NEW ' .
                                ') ' ;
                }

            }
        }

        $query .= ' REMOVE { _key: doc._key } IN ' . $edge . ' RETURN OLD' ;

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    private function updateThings( $id , $edge )
    {
        $binds = [ 'from' => $this->table . '/' . $id  ] ;

        $query = 'FOR doc IN ' . $edge ;

        $query .= ' FILTER doc._from == @from ' ;

        if( is_array( $this->tablesThings ) && count( $this->tablesThings ) > 0 )
        {
            $query .= 'LET key = PARSE_IDENTIFIER( doc._to ).key ' ;

            foreach( $this->tablesThings as $thing )
            {
                // check collection exists
                if( $this->container->arango->collectionExists( $thing ) )
                {
                    $binds['@'.$thing] = $thing ;

                    $query .= 'LET updateThing_' . $thing . ' = ' .
                        '(' .
                        'UPDATE key WITH { modified: DATE_ISO8601( DATE_NOW() ) } IN @@' . $thing . ' OPTIONS { ignoreErrors: true } RETURN NEW ' .
                        ') ' ;
                }
            }
        }

        $query .= ' RETURN 1' ;

        $this->container->arango->prepare
        ([
            'query'     => $query ,
            'bindVars'  => $binds
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }
}
