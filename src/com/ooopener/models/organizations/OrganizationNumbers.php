<?php

namespace com\ooopener\models\organizations ;

use com\ooopener\models\Things;
use com\ooopener\things\Thing;

use Slim\Container;

class OrganizationNumbers extends Things
{
    public function __construct( Container $container = NULL , $table = NULL , array $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'             => Thing::FILTER_INT,
        'name'           => Thing::FILTER_DEFAULT,
        'additionalType' => Thing::FILTER_INT,
        'value'          => Thing::FILTER_DEFAULT
    ];
}
