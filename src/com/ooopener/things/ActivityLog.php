<?php

namespace com\ooopener\things ;

use core\Objects ;

class ActivityLog extends Thing
{
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    public $agent ;

    public $ip ;

    public $method ;

    public $resource ;

    public $user ;

    ///////////////////////////

    const FILTER_USER       = 'user' ;

    ///////////////////////////

    /**
     * The serializable attributes.
     */
    const serialiableAttributes =
    [
        'id'          ,
        'agent'       ,
        'created'     ,
        'description' ,
        'ip'          ,
        'method'      ,
        'resource'    ,
        'user'
    ] ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = [] ;
        foreach( self::serialiableAttributes as $key )
        {
            $object[$key] = $this->{$key} ;
        }
        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }
}