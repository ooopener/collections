<?php

namespace com\ooopener\things;

use core\Objects;

/**
 * The value object of the place.
 */
class ConceptualObject extends CreativeWork
{
    /**
     * Creates a new ConceptualObject instance.
     * @param object $init a generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * Indicates if the object is active.
     * @var bool
     */
    public $active ;

    /**
     * The category of this object.
     * @var int
     */
    public $category ;

    /**
     * The location of the resource.
     */
    public $location ;

    /**
     * Marks and inscriptions on the resource
     * @var array
     */
    public $marks ;

    /**
     * Material and technique informations of the resource.
     * @var array
     */
    public $materials ;

    /**
     * The measurement informations of the resource.
     * @var array
     */
    public $measurements ;

    /**
     * The artistic movement of the resource.
     */
    public $movement ;

    /**
     * Without an Object number it is not possible either to uniquely identify an object or to link an object with its documentation. The Object number should be marked on, or otherwise physically associated with the object.
     */
    public $numbers ;

    /**
     * Photographs of this resource.
     * @var array
     */
    public $photos ;

    /**
     * The resource production informations.
     * It can be applied to collections management areas as well as object history and description areas.
     * It supports the documentation of the production of man made objects. As such it is required for all types of collection disciplines.
     */
    public $productions ;

    /**
     * The remarks about the resource.
     * @var array
     */
    public $remarks ;

    /**
     * The temporal characteristics of the resource.
     */
    public $temporal ;

    ///////////////////////////

    const FILTER_CATEGORY     = 'category' ;
    const FILTER_LOCATION     = 'location' ;
    const FILTER_MARKS        = 'marks' ;
    const FILTER_MATERIALS    = 'materials' ;
    const FILTER_MEASUREMENTS = 'measurements' ;
    const FILTER_NUMBERS      = 'numbers' ;
    const FILTER_MOVEMENT     = 'movement' ;
    const FILTER_PHOTOS       = 'photos' ;
    const FILTER_PRODUCTIONS  = 'productions' ;
    const FILTER_TEMPORAL     = 'temporal' ;
    const FILTER_REMARKS      = 'remarks' ;

    ///////////////////////////

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'           => $this->id,
            'name'         => $this->name,
            'url'          => $this->url,
            'image'        => $this->image,
            'created'      => $this->created,
            'modified'     => $this->modified,

            'active'        => $this->active,
            'alternateName' => $this->alternateName,
            'category'      => $this->category,
            'location'      => $this->location,
            'description'   => $this->description,
            'remarks'       => $this->remarks,
            'movement'      => $this->movement,
            'temporal'      => $this->temporal,
            'materials'     => $this->materials,
            'measurements'  => $this->measurements,
            'numbers'       => $this->numbers,
            'photos'        => $this->photos,
            'productions'   => $this->productions,
            'keywords'      => $this->keywords
        ];

        return Objects::compress( $object ) ;
    }
}


