<?php

namespace com\ooopener\things;

/**
 * A course.
 */
class Course extends Thing
{
    /**
     * Creates a new Course instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_ALTERNATIVEHEADLINE = 'alternativeHeadline' ;
    const FILTER_CAUTIONS            = 'cautions' ;
    const FILTER_HEADLINE            = 'headline' ;
    const FILTER_PHOTOS              = 'photos' ;
    const FILTER_STEPS               = 'steps' ;

    const FILTER_DISCOVER            = 'discover' ;

    const FILTER_ARTICLES            = 'articles' ;
    const FILTER_CONCEPTUAL_OBJECTS  = 'conceptualObjects' ;
    const FILTER_COURSES             = 'courses' ;
    const FILTER_EVENTS              = 'events' ;
    const FILTER_ORGANIZATIONS       = 'organizations' ;
    const FILTER_PEOPLE              = 'people' ;
    const FILTER_PLACES              = 'places' ;
    const FILTER_STAGES              = 'stages' ;

    ///////////////////////////


    /**
     * Indicates if the Course is active.
     */
    public $active ;

    /**
     * An alternative headline of the item.
     */
    public $alternativeHeadline ;

    /**
     * The audience associated with this course
     */
    public $audience ;

    /**
     * The important info of the course.
     * @var string
     */
    public $cautions ;

    /**
     * The distance of the course.
     * @var string
     */
    public $distance ;

    /**
     * The elevation of the course.
     * @var string
     */
    public $elevation ;

    /**
     * An headline of the item.
     */
    public $headline ;

    /**
     * The level of the courses.
     * @var String
     */
    public $level ;

    /**
     * The opening hours of the course.
     * @var array
     */
    public $openingHoursSpecification ;

    /**
     * The path associated with this course
     */
    public $path ;

    /**
     * photos associated with this course.
     * @var array
     */
    public $photos ;

    /**
     * The status of the item.
     */
    public $status ;

    /**
     * steps of this course.
     * @var array
     */
    public $steps ;

    /**
     * A text of the item.
     */
    public $text ;

    /**
     * The transportations of the course.
     * @var array
     */
    public $transportations ;


}


