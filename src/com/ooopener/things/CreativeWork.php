<?php

namespace com\ooopener\things;

use core\Objects;

/**
 * A creative work representation.
 */
class CreativeWork extends Thing
{
    /**
     * Creates a new CreativeWork instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    // ------------ filters

    const FILTER_ALTERNATIVEHEADLINE = 'alternativeHeadline' ;
    const FILTER_AUDIENCE            = 'audience' ;
    const FILTER_AUDIO               = 'audio' ;
    const FILTER_AUTHOR              = 'author' ;
    const FILTER_CONTRIBUTOR         = 'contributor' ;
    const FILTER_EDITOR              = 'editor' ;
    const FILTER_HAS_PART            = 'hasPart' ;
    const FILTER_HEADLINE            = 'headline' ;
    const FILTER_IS_BASED_ON         = 'isBasedOn' ;
    const FILTER_IS_PART_OF          = 'isPartOf' ;
    const FILTER_KEYWORDS            = 'keywords' ;
    const FILTER_MAIN_ENTITY         = 'mainEntity' ;
    const FILTER_MENTIONS            = 'mentions' ;
    const FILTER_OFFERS              = 'offers' ;
    const FILTER_PRODUCER            = 'producer' ;
    const FILTER_PROVIDER            = 'provider' ;
    const FILTER_PUBLISHER           = 'publisher' ;
    const FILTER_SPONSOR             = 'sponsor' ;
    const FILTER_TRANSLATOR          = 'translator' ;
    const FILTER_VIDEO               = 'video' ;

    // ------------

    /**
     * The alternative headline of this content.
     * @var string
     */
    public $alternativeHeadline ;

    /**
     * An intended audience, i.e. a group for whom something was created.
     */
    public $audience ;

    /**
     * An embedded audio object.
     */
    public $audio ;

    /**
     * The author of this content.
     */
    public $author ;

    /**
     * The location depicted or described in the content. For example, the location in a photograph or painting.
     */
    public $contentLocation ;

    /**
     * Official rating of a piece of content—for example,'MPAA PG-13'.
     */
    public $contentRating ;

    /**
     * A secondary contributor to the CreativeWork or Event.
     */
    public $contributor ;

    /**
     * The party holding the legal copyright to the CreativeWork.
     */
    public $copyrightHolder ;

    /**
     * The year during which the claimed copyright for the CreativeWork was first asserted.
     */
    public $copyrightYear ;

    /**
     * The date on which the CreativeWork was created or the item was added to a DataFeed.
     */
    public $dateCreated ;

    /**
     * The date on which the CreativeWork was most recently modified or when the item's entry was modified within a DataFeed.
     */
    public $dateModified ;

    /**
     * Date of first broadcast/publication.
     */
    public $datePublished ;

    /**
     * Specifies the Person who edited the CreativeWork.
     */
    public $editor ;

    /**
     * The encoding of this content.
     * @var string
     */
    public $encoding ;

    /**
     * Media type typically expressed using a MIME format (see IANA site and MDN reference) e.g. application/zip for a SoftwareApplication binary, audio/mpeg for .mp3 etc.).
     * @var string
     */
    public $encodingFormat ;

    /**
     * Date the content expires and is no longer useful or available. For example a VideoObject or NewsArticle whose availability or relevance is time-limited, or a ClaimReview fact check whose publisher wants to indicate that it may no longer be relevant (or helpful to highlight) after some date.
     */
    public $expires ;

    /**
     * Indicates an item or CreativeWork that is part of this item, or CreativeWork (in some sense).
     */
    public $hasPart ;

    /**
     * The headline of this content.
     * @var string
     */
    public $headline ;

    /**
     * The language of the content or performance or used in an action.
     * @var string
     */
    public $inLanguage ;

    /**
     * A flag to signal that the item, event, or place is accessible for free.
     * @var bool
     */
    public $isAccessibleForFree ;

    /**
     * A resource that was used in the creation of this resource. This term can be repeated for multiple sources.
     */
    public $isBasedOn ;

    /**
     * Indicates an item or CreativeWork that this item, or CreativeWork (in some sense), is part of.
     */
    public $isPartOf ;

    /**
     * The keywords of this content.
     * @var string
     */
    public $keywords ;

    /**
     * A license document that applies to this content, typically indicated by URL.
     * @var string
     */
    public $license ;

    /**
     * The location where the CreativeWork was created, which may not be the same as the location depicted in the CreativeWork.
     */
    public $locationCreated ;

    /**
     * Indicates the primary entity described in some page or other CreativeWork.
     */
    public $mainEntity ;

    /**
     * A material that something is made from, e.g. leather, wool, cotton, paper.
     */
    public $material ;

    /**
     * The mentions of this content.
     * @var string
     */
    public $mentions ;

    /**
     * An offer to provide this item—for example, an offer to sell a product, rent the DVD of a movie, perform a service, or give away tickets to an event.
     */
    public $offers ;

    /**
     * The position of an item in a series or sequence of items.
     */
    public $position ;

    /**
     * The person or organization who produced the work.
     * @var array
     */
    public $producer ;

    /**
     * The service provider, service operator, or service performer; the goods producer. Another party (a seller) may offer those services or goods on behalf of the provider. A provider may also serve as the seller.
     */
    public $provider ;

    /**
     * The publisher of the creative work.
     * @var array
     */
    public $publisher ;

    /**
     * The Event where the CreativeWork was recorded. The CreativeWork may capture all or part of the event.
     */
    public $recordedAt ;

    /**
     * The place and time the release was issued, expressed as a PublicationEvent.
     */
    public $releaseEvent ;

    /**
     * The review of the creative work.
     * @var string
     */
    public $review ;

    /**
     * The Organization on whose behalf the creator was working.
     */
    public $sourceOrganization ;

    /**
     * A person or organization that supports a thing through a pledge, promise, or financial contribution. e.g. a sponsor of a Medical Study or a corporate sponsor of an event.
     * @var array
     */
    public $sponsor ;

    /**
     * The "temporal" property can be used in cases where more specific properties (e.g. temporalCoverage, dateCreated, dateModified, datePublished) are not known to be appropriate.
     */
    public $temporal ;

    /**
     * The temporalCoverage of a CreativeWork indicates the period that the content applies to, i.e. that it describes, either as a DateTime or as a textual string indicating a time period in ISO 8601 time interval format. In the case of a Dataset it will typically indicate the relevant time period in a precise notation (e.g. for a 2011 census dataset, the year 2011 would be written "2011/2012"). Other forms of content e.g. ScholarlyArticle, Book, TVSeries or TVEpisode may indicate their temporalCoverage in broader terms - textually or via well-known URL. Written works such as books may sometimes have precise temporal coverage too, e.g. a work set in 1939 - 1945 can be indicated in ISO 8601 interval format format via "1939/1945".
     * Open-ended date ranges can be written with ".." in place of the end date. For example, "2015-11/.." indicates a range beginning in November 2015 and with no specified final date. This is tentative and might be updated in future when ISO 8601 is officially updated. Supersedes datasetTimeInterval.
     */
    public $temporalCoverage ;

    /**
     * A thumbnail image relevant to the Thing.
     * @var string
     */
    public $thumbnailUrl ;

    /**
     * The text of the creative work.
     * @var string
     */
    public $text ;

    /**
     * Approximate or typical time it takes to work with or through this learning resource for the typical intended target audience, e.g. 'PT30M', 'PT1H25M'.
     */
    public $timeRequired ;

    /**
     * Organization or person who adapts a creative work to different languages, regional differences and technical requirements of a target market, or that translates during some event.
     */
    public $translator ;

    /**
     * The typical expected age range, e.g. '7-9', '11-'.
     */
    public $typicalAgeRange ;

    /**
     * The version of the CreativeWork embodied by a specified resource.
     */
    public $version ;

    /**
     * An embedded video object.
     */
    public $video ;

    /**
     * The enumeration of all object properties.
     */
    public static $properties =
    [
        'id'           => self::FILTER_ID ,
        'name'         => self::FILTER_DEFAULT ,
        'url'          => self::FILTER_URL ,
        'created'      => self::FILTER_DATETIME ,
        'modified'     => self::FILTER_DATETIME ,
        'description'  => self::FILTER_TRANSLATE ,

        'author'              => self::FILTER_DEFAULT ,
        'alternativeHeadline' => self::FILTER_TRANSLATE ,
        'encoding'            => self::FILTER_DEFAULT ,
        'encodingFormat'      => self::FILTER_DEFAULT ,
        'headline'            => self::FILTER_TRANSLATE ,
        'inLanguage'          => self::FILTER_DEFAULT ,
        'keywords'            => self::FILTER_DEFAULT ,
        'license'             => self::FILTER_DEFAULT ,
        'mentions'            => self::FILTER_TRANSLATE ,
        'publisher'           => self::FILTER_DEFAULT ,
        'review'              => self::FILTER_TRANSLATE ,
        'text'                => self::FILTER_TRANSLATE ,
        'thumbnailUrl'        => self::FILTER_DEFAULT
    ];

    /**
     * Creates an instance and initialize it.
     *
     * @param object $init
     * @param $iso8601
     * @param string $url
     *
     * @return CreativeWork
     */
    public static function create( $init = NULL , $iso8601 = NULL , $url = NULL , $lang = NULL )
    {
        $creative = NULL ;

        if( isset( $init ) )
        {
            $creative = new CreativeWork() ;

            foreach( self::$properties as $property => $filter )
            {
                switch( $filter )
                {
                    case self::FILTER_DATETIME :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $creative->{ $property } = isset( $iso8601 )
                                ? $iso8601->formatTimeToISO8601($init->{ $property })
                                : $init->{ $property } ;
                        }
                        break ;
                    }
                    case self::FILTER_URL :
                    {
                        $creative->{ $property } = $url ;
                        break ;
                    }
                    case self::FILTER_ID :
                    {
                        $creative->{ $property } = (int) $init->{ '_key' } ;
                        break ;
                    }
                    case self::FILTER_TRANSLATE :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            if( is_array( $init->{$property} ) )
                            {
                                if( $lang != NULL )
                                {
                                    if( array_key_exists( $lang , $init->{ $property } ) )
                                    {
                                        $creative->{ $property } = $init->{ $property }[$lang] ;
                                    }
                                    else
                                    {
                                        $creative->{ $property } = "" ;
                                    }
                                }
                                else
                                {
                                    $creative->{ $property } = $init->{ $property } ;
                                }
                            }
                            else if( is_string( $init->{ $property } ) )
                            {
                                $creative->{ $property } = $init->{ $property } ;
                            }
                        }
                        break ;
                    }
                    case self::FILTER_DEFAULT :
                    default :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $creative->{ $property } = $init->{ $property } ;
                        }
                    }
                }
            }
        }

        return $creative ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = array
        (
            'id'          => $this->id,
            'name'        => $this->name,
            'url'         => $this->url,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'description' => $this->description,

            'author'              => $this->author,
            'alternativeHeadline' => $this->alternativeHeadline,
            'encoding'            => $this->encoding,
            'encodingFormat'      => $this->encodingFormat,
            'headline'            => $this->headline,
            'inLanguage'          => $this->inLanguage,
            'keywords'            => $this->keywords,
            'license'             => $this->license,
            'mentions'            => $this->mentions,
            'publisher'           => $this->publisher,
            'review'              => $this->review,
            'text'                => $this->text,
            'thumbnailUrl'        => $this->thumbnailUrl
        );

        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }
}


