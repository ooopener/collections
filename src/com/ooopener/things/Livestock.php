<?php

namespace com\ooopener\things;

use core\Objects;

class Livestock extends Thing
{
    /**
     * Creates a new Livestock instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    public $organization ;

    public $numbers ;

    public $worshops ;

    ///////////////////////////

    const FILTER_ORGANIZATION  = 'organization' ;
    const FILTER_TECHNICIANS   = 'technicians' ;
    const FILTER_VETERINARIANS = 'veterinarians' ;

    ///////////////////////////

    /**
     * The serializable attributes.
     */
    const serialiableAttributes =
    [
        'id'           ,
        'url'          ,
        'created'      ,
        'modified'     ,
        'organization' ,
        'numbers'      ,
        'workshops'
    ];

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = [] ;
        foreach( self::serialiableAttributes as $key )
        {
            $object[$key] = $this->{$key} ;
        }
        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }
}