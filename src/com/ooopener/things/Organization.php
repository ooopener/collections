<?php

namespace com\ooopener\things;

use core\Objects;

/**
 * Entities that have a somewhat fixed, physical extension.
 * @see http://schema.org/Organization
 */
class Organization extends Thing
{
    /**
     * Creates a new Organization instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_ADDITIONAL          = 'additional' ;
    const FILTER_ADDRESS             = 'address' ;
    const FILTER_EMPLOYEES           = 'employees' ;
    const FILTER_FOUNDER             = 'founder' ;
    const FILTER_FOUNDING_LOCATION   = 'foundingLocation' ;
    const FILTER_KEYWORDS            = 'keywords' ;
    const FILTER_LOCATION            = 'location' ;
    const FILTER_LOGO                = 'logo' ;
    const FILTER_MEMBER_OF           = 'memberOf' ;
    const FILTER_MEMBERS             = 'members' ;
    const FILTER_OWNS                = 'owns' ;
    const FILTER_PHOTOS              = 'photos' ;
    const FILTER_PROVIDERS           = 'providers' ;
    const FILTER_PARENT_ORGANIZATION = 'parentOrganization' ;
    const FILTER_SUB_ORGANIZATION    = 'subOrganization' ;
    const FILTER_SPONSOR             = 'sponsor' ;

    const FILTER_SLOGAN = 'slogan' ;

    ///////////////////////////

    /**
     * Indicates if the thing is active.
     */
    public $active ;

    /**
     * The additional description of the organization.
     * Note : this property is a custom attributeof the original Organization class defined in http://schema.org/Organization.
     * @var string
     */
    public $additional ;

    /**
     * Physical address of the item (PostalAddress or any object to describe it).
     */
    public $address ;

    /**
     * The area served by the organization (GeoShape)
     */
    public $areaServed ;

    /**
     * The employees of the organization
     * @var array
     */
    public $employees ;

    /**
     * Upcoming or past events associated with this organization (legacy spelling; see singular form, event).
     * @var array
     */
    public $events ;

    /**
     * Photographs of this organization (legacy spelling; see singular form, photo).
     * @var array
     */
    public $images ;

    /**
     * The keywords collection of this object.
     * @var array
     */
    public $keywords ;

    /**
     * The legal name of the organization
     * @var string
     */
    public $legalName ;

    /**
     * The location of the organization
     * @var PostalAddress or Place
     */
    public $location ;

    /**
     * An associated logo
     */
    public $logo ;

    /**
     * The members of the organization
     * @var array
     */
    public $members ;

    /**
     * Photographs of this organization (legacy spelling; see singular form, photo).
     * @var array
     */
    public $photos ;

    /**
     * The providers of the organization
     * @var array
     */
    public $providers ;

    /**
     * A relationship between two organizations where the first includes the second, e.g., as a subsidiary.
     * @var array or Organization
     */
    public $subOrganization ;

    /**
     * A person or organization that supports a thing through a pledge, promise, or financial contribution. e.g. a sponsor of a Medical Study or a corporate sponsor of an event.
     * @var array|Person|Organization
     */
    public $sponsor ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'          => $this->id,
            'identifier'  => $this->identifier,
            'name'        => $this->name,
            'legalName'   => $this->legalName,
            'url'         => $this->url,
            'active'      => $this->active,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'image'       => $this->image,
            'licence'     => $this->licence,
            'description' => $this->description,
            'publisher'   => $this->publisher,

            'about'                     => $this->about,
            'additional'                => $this->additional,
            'additionalType'            => $this->additionalType,
            'alternateName'             => $this->alternateName,
            'address'                   => $this->address,
            'areServed'                 => $this->areaServed,
            'employees'                 => $this->employees,
            'events'                    => $this->events,
            'images'                    => $this->images,
            'keywords'                  => $this->keywords,
            'location'                  => $this->location,
            'logo'                      => $this->logo,
            'members'                   => $this->members,
            'photos'                    => $this->photos,
            'providers'                 => $this->providers,
            'subOrganization'           => $this->subOrganization,
            'sponsor'                   => $this->sponsor
        ];

        return Objects::compress( $object ) ;
    }
}


