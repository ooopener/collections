<?php

namespace com\ooopener\things;


/**
 * A step.
 */
class Step extends Thing
{
    /**
     * Creates a new Step instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_PHOTOS = 'photos' ;
    const FILTER_STAGE  = 'stage' ;

    ///////////////////////////

    /**
     * Indicates if the step is active.
     */
    public $active ;

    /**
     * the position of this step
     */
    public $position ;

    /**
     * the stage associated with this step
     */
    public $stage ;
}


