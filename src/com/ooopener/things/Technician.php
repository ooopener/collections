<?php

namespace com\ooopener\things;

class Technician extends Thing
{
    public function __construct( object $init = NULL )
    {
        parent::__construct( $init );
    }

    ///////////////////////////

    const FILTER_LIVESTOCKS          = 'livestocks' ;
    const FILTER_MEDICAL_SPECIALTIES = 'medicalSpecialties' ;
    const FILTER_PERSON              = 'person' ;

    ///////////////////////////
}