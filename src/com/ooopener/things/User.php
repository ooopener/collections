<?php

namespace com\ooopener\things;

use core\Objects;

/**
 * An event happening at a certain time and location, such as a concert, lecture, or festival. Repeated events may be structured as separate Event objects.
 */
class User extends Person
{
    /**
     * Creates a new User instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The favorites.
     * @var array
     */
    public $favorites ;

    /**
     * The provider.
     * @var string
     */
    public $provider ;

    /**
     * The provider uid.
     * @var string
     */
    public $provider_uid ;

    /**
     * The uuid of the user.
     * @var string
     */
    public $uuid ;

    /**
     * The team to which the user belongs
     * @var array|object
     */
    public $team ;

    /**
     * The permissions of the user
     * @var array|object
     */
    public $permissions ;

    /**
     * The person linked
     * @var integer
     */
    public $person ;

    /**
     * The scope of the user
     * @var array
     */
    public $scope ;

    /**
     * The gravatar path pattern expression.
     */
    const GRAVATAR_PATH = 'https://www.gravatar.com/avatar/%s?s=%u&d=%s' ;

    /**
     * The enumeration of all object properties.
     */
    public static $properties =
    [
        'id'           => self::FILTER_ID ,
        'name'         => self::FILTER_DEFAULT ,
        'url'          => self::FILTER_URL ,
        'created'      => self::FILTER_DATE ,
        'modified'     => self::FILTER_DATE ,
        'uuid'         => self::FILTER_DEFAULT ,
        'favorites'    => self::FILTER_DEFAULT ,
        'team'         => self::FILTER_DEFAULT ,
        'person'       => self::FILTER_DEFAULT ,
        'permissions'  => self::FILTER_DEFAULT ,
        'givenName'    => self::FILTER_DEFAULT ,
        'familyName'   => self::FILTER_DEFAULT ,
        'gender'       => self::FILTER_DEFAULT ,
        'email'        => self::FILTER_DEFAULT ,
        'address'      => self::FILTER_ADDRESS ,
        'provider'     => self::FILTER_DEFAULT ,
        'provider_uid' => self::FILTER_DEFAULT ,
        'image'        => self::FILTER_IMAGE
    ];

    /**
     * Creates a new User instance.
     * @param object $init An object to initialize a new Museum instance.
     * @param $iso8601
     * @param $url
     *
     * @return User $user
     */
    public static function create( $init = NULL , $iso8601 = NULL , $url = NULL )
    {
        $user = NULL ;

        if( isset( $init ) )
        {
            $user = new User() ;

            foreach( self::$properties as $property => $filter )
            {
                switch( $filter )
                {
                    case self::FILTER_DEFAULT :
                    default :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $user->{ $property } = $init->{ $property } ;
                        }
                    }
                }
            }
        }

        return $user ;
    }

    /**
     * Returns the avatar url expression of the current user.
     *
     * @param array $options An setting object.
     * @param string $defaultIcon The default icon value ('mm'), we can use the values : 'blank', 'mm', 'identicon', 'monsterid', 'wavatar', 'retro', '404'.
     * @param integer $defaultSize The default size value (45).
     *
     * @return string
     */
    public function getAvatarUrl( $options = [] , $defaultIcon = 'mm' , $defaultSize = 45 )
    {
        $icon = isset( $options['icon'] )  ? $options['icon'] : $defaultIcon ;
        $size = isset( $options['size']  ) ? $options['size'] : $defaultSize  ;
        if( isset( $this->image ) )
        {
            switch( $this->provider )
            {
                case 'google.com':
                    $url = $this->image . '?sz=' . $size ;
                    break;
                case 'facebook.com':
                    $sizeparam = '';

                    if( $size >= 200 )
                    {
                        $sizeparam = 'large' ;
                    }
                    if( $size >= 90 && $size < 200 ) {
                        $sizeparam = 'normal' ;
                    }
                    if( $size >= 50 && $size < 90 ) {
                        $sizeparam = 'small' ;
                    }
                    if( $size < 50 ) {
                        $sizeparam = 'square' ;
                    }

                    $url = 'https://graph.facebook.com/' . $this->provider_uid . '/picture?type=' . $sizeparam ;
                    break;
            }
            return $url ;
        }
        else
        {
            return sprintf( self::GRAVATAR_PATH ,md5( $this->email ), $size , $icon ) ;
        }
    }

    public function getFullName()
    {
        if( !$this->givenName || !$this->familyName )
        {
            return null ;
        }

        return $this->givenName . " " . $this->familyName ;
    }

    public function getFullNameOrUsername()
    {
        if(
            ( $this->givenName  &&  $this->givenName != '' )
            && ( $this->familyName && $this->familyName != '' )
        )
        {
            return $this->givenName . " " . $this->familyName ;
        }

        return $this->name ;
    }

    public function isAdmin()
    {
        return $this->team == "superadmin" || $this->team == "admin" ;
    }

    public function isAdminOnly()
    {
        return $this->team == "admin" ;
    }

    public function isSuperadmin()
    {
        return $this->team == "superadmin";
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'           => $this->id,
            'name'         => $this->name,
            'url'          => $this->url,
            'created'      => $this->created,
            'modified'     => $this->modified,
            'uuid'         => $this->uuid,
            'favorites'    => $this->favorites,
            'team'         => $this->team,
            'permissions'  => $this->permissions,
            'person'       => $this->person,
            'active'       => $this->active,
            'email'        => $this->email,
            'gender'       => $this->gender,
            'givenName'    => $this->givenName,
            'familyName'   => $this->familyName,
            'address'      => $this->address,
            'memberOf'     => $this->memberOf,
            'provider'     => $this->provider,
            'provider_uid' => $this->provider_uid,
            'image'        => $this->image,
            'scope'        => $this->scope
        ];

        return Objects::compress( $object ) ;
    }
}


