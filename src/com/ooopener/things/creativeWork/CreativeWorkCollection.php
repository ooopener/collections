<?php

namespace com\ooopener\things\creativeWork;

use com\ooopener\things\CreativeWork;

/**
 * A creative work collection representation.
 */
class CreativeWorkCollection extends CreativeWork
{
    /**
     * Creates a new CreativeWorkCollection instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * A director of e.g. tv, radio, movie, video gaming etc. content, or of an event. Directors can be associated with individual items or with a series, episode, clip.
     */
    public $director ;

    /**
     * The end date and time of the item (in ISO 8601 date format).
     */
    public $endDate ;

    /**
     * Photographs of this creativeWorkCollection (legacy spelling; see singular form, photo).
     * @var array
     */
    public $photos ;

    /**
     * The start date and time of the item (in ISO 8601 date format).
     */
    public $startDate ;

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }

    ///////////////////////////

    const FILTER_PHOTOS = 'photos' ;

    ///////////////////////////
}
