<?php

namespace com\ooopener\things\creativeWork ;

use com\ooopener\things\CreativeWork;
use core\Objects;

/**
 * An media object file representation.
 */
class MediaObject extends CreativeWork
{
    /**
     * Creates a new MediaObject instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        parent::__construct( $init );
    }

    /**
     * The bitrate of the item.
     * @var int
     */
    public $bitrate ;

    /**
     * The contentSize of the item.
     * @var int
     */
    public $contentSize ;

    /**
     * The contentUrl of the item.
     * @var string
     */
    public $contentUrl ;

    /**
     * The duration of the item.
     * @var int
     */
    public $duration ;

    /**
     * The editor of the item.
     * @var string
     */
    public $editor ;

    /**
     * The embedUrl of the item.
     * @var string
     */
    public $embedUrl ;

    /**
     * The height of the item.
     * @var int
     */
    public $height ;

    /**
     * The playerType of the item.
     * @var string
     */
    public $playerType ;

    /**
     * When multiple image appear in an entry, indicates which is primary. At most one image may be primary. Default value is false.
     * @var boolean
     */
    public $primary ;

    /**
     * The source available for this item
     * @var array
     */
    public $source ;

    /**
     * The width of the item.
     * @var int
     */
    public $width ;

    /**
     * The enumeration of all object properties.
     */
    public static $properties =
    [
        'id'           => self::FILTER_ID ,
        'name'         => self::FILTER_DEFAULT ,
        'url'          => self::FILTER_URL ,
        'created'      => self::FILTER_DATETIME ,
        'modified'     => self::FILTER_DATETIME ,
        'description'  => self::FILTER_TRANSLATE ,

        'author'              => self::FILTER_DEFAULT ,
        'alternativeHeadline' => self::FILTER_TRANSLATE ,
        'encoding'            => self::FILTER_DEFAULT ,
        'encodingFormat'      => self::FILTER_DEFAULT ,
        'headline'            => self::FILTER_TRANSLATE ,
        'inLanguage'          => self::FILTER_DEFAULT ,
        'keywords'            => self::FILTER_DEFAULT ,
        'license'             => self::FILTER_DEFAULT ,
        'mentions'            => self::FILTER_TRANSLATE ,
        'publisher'           => self::FILTER_DEFAULT ,
        'review'              => self::FILTER_TRANSLATE ,
        'source'              => self::FILTER_DEFAULT ,
        'text'                => self::FILTER_TRANSLATE ,
        'thumbnailUrl'        => self::FILTER_DEFAULT ,

        'bitrate'        => self::FILTER_INT,
        'contentSize'    => self::FILTER_INT ,
        'contentUrl'     => self::FILTER_DEFAULT ,
        'duration'       => self::FILTER_DEFAULT ,
        'editor'         => self::FILTER_DEFAULT ,
        'embedUrl'       => self::FILTER_DEFAULT ,
        'playerType'     => self::FILTER_DEFAULT ,
        'width'          => self::FILTER_INT ,
        'height'         => self::FILTER_INT
    ];


    /**
     * Creates an instance and initialize it.
     *
     * @param object $init
     * @param $iso8601
     * @param string $url
     *
     * @return MediaObject
     */
    public static function create( $init = NULL , $iso8601 = NULL , $url = NULL , $lang = NULL )
    {
        $media = NULL ;

        if( isset( $init ) )
        {
            $media = new MediaObject() ;

            foreach( self::$properties as $property => $filter )
            {
                switch( $filter )
                {
                    case self::FILTER_DATETIME :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $media->{ $property } = isset( $iso8601 )
                                ? $iso8601->formatTimeToISO8601($init->{ $property })
                                : $init->{ $property } ;
                        }
                        break ;
                    }
                    case self::FILTER_URL :
                    {
                        $media->{ $property } = $url ;
                        break ;
                    }
                    case self::FILTER_ID :
                    {
                        $media->{ $property } = (int) $init->{ '_key' } ;
                        break ;
                    }
                    case self::FILTER_TRANSLATE :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            if( is_array( $init->{$property} ) )
                            {
                                if( $lang != NULL )
                                {
                                    if( array_key_exists( $lang , $init->{ $property } ) )
                                    {
                                        $media->{ $property } = $init->{ $property }[$lang] ;
                                    }
                                    else
                                    {
                                        $media->{ $property } = "" ;
                                    }
                                }
                                else
                                {
                                    $media->{ $property } = $init->{ $property } ;
                                }
                            }
                            else if( is_string( $init->{ $property } ) )
                            {
                                $media->{ $property } = $init->{ $property } ;
                            }
                        }
                        break ;
                    }
                    case self::FILTER_DEFAULT :
                    default :
                    {
                        if( property_exists( $init , $property ) )
                        {
                            $media->{ $property } = $init->{ $property } ;
                        }
                    }
                }
            }
        }

        return $media ;
    }

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object = array
        (
            'id'          => $this->id,
            'name'        => $this->name,
            'url'         => $this->url,
            'created'     => $this->created,
            'modified'    => $this->modified,
            'description' => $this->description,

            'author'              => $this->author,
            'alternativeHeadline' => $this->alternativeHeadline,
            'encoding'            => $this->encoding,
            'encodingFormat'      => $this->encodingFormat,
            'headline'            => $this->headline,
            'inLanguage'          => $this->inLanguage,
            'keywords'            => $this->keywords,
            'license'             => $this->license,
            'mentions'            => $this->mentions,
            'publisher'           => $this->publisher,
            'review'              => $this->review,
            'source'              => $this->source,
            'text'                => $this->text,
            'thumbnailUrl'        => $this->thumbnailUrl,

            'bitrate'        => $this->bitrate,
            'contentSize'    => $this->contentSize,
            'contentUrl'     => $this->contentUrl,
            'duration'       => $this->duration,
            'editor'         => $this->editor,
            'embedUrl'       => $this->embedUrl,
            'playerType'     => $this->playerType,
            'width'          => $this->width,
            'height'         => $this->height
        );

        return Objects::compress( $object ) ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return $this->formatToString( get_class($this) , 'id' ) ;
    }
}


