<?php

namespace com\ooopener\validations;

use com\ooopener\helpers\Version;
use Slim\Container;

use Exception ;

class ApplicationValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ]
            ,
            'url_redirect' =>
            [
                'redirect' => 'Invalid URL in string'
            ],
            'version' =>
            [
                'version' => 'The version is invalid'
            ]
        ]);
    }

    /**
     * Validates if the type exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->applicationsTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_type failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the url redirect value is string of URL.
     */
    public function validate_redirect( $value , $input , $args )
    {
        try
        {
            if( $value != '' )
            {
                $list = explode( ',' , $value ) ;

                foreach( $list as $url )
                {
                    if( filter_var( $url , FILTER_VALIDATE_URL ) === false )
                    {
                        return FALSE ;
                    }
                }

                return TRUE ;
            }

            return TRUE ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' validate_redirect failed, ' . $e->getMessage() );
            return FALSE ;
        }
    }

    public function validate_version( $value , $input , $args )
    {
        $v = Version::fromString( $value ) ;

        if( $v == NULL )
        {
            return FALSE ;
        }

        return TRUE ;
    }
}
