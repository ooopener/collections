<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

class BadgeItemValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ],
            'bgcolor' =>
            [
                'isColor' => 'The color expression is invalid, ex: FF0000.'
            ]
            ,
            'color' =>
            [
                'isColor' => 'The color expression is invalid, ex: FF0000.'
            ]
        ]);
    }

    /**
     * Validates if the specific type exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->badgeItemsTypes->exist( (string)$value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_type failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if it is a color.
     */
    public function validate_isColor( $value , $input , $args )
    {
        return (bool) preg_match( '/^[a-f0-9]{6}$/i' , $value ) ;
    }
}