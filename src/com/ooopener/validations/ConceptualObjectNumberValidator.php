<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

class ConceptualObjectNumberValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'date' =>
            [
                'simpleDate' => 'The date is not valid.'
            ]
            ,
            'additionalType' =>
            [
                'additionalType' => 'The additionalType is not valid.'
            ]
        ]);
    }

    /**
     * Validates if the number additionalType exist.
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->conceptualObjectNumberTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_type failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

}


