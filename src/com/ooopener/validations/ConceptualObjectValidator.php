<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

class ConceptualObjectValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'category' =>
            [
                'category' => 'The category is not valid.'
            ]
            ,
            'location' =>
            [
                'location' => 'The location don\'t exist.'
            ]
            ,
            'movement' =>
            [
                'category' => 'The movement is not valid.'
            ]
            ,
            'temporal' =>
            [
                'category' => 'The temporal entitt is not valid.'
            ]
        ]);
    }

    /**
     * Validates if the category exist.
     */
    public function validate_category( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->conceptualObjectCategories->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_location failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the art movement exist.
     */
    public function validate_movement( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->artMovements->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_movement failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }
        return FALSE ;
    }

    /**
     * Validates if the art movement exist.
     */
    public function validate_temporal( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->artTemporalEntities->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_temporal failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }
}


