<?php

namespace com\ooopener\validations;

use Slim\Container;
use Violin\Violin;

use Exception ;

class CourseValidator extends Violin
{
    public function __construct( Container $container )
    {
        $this->container = $container ;

        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ],
            'audience' =>
            [
                'audience' => 'The audience doesn\'t exist'
            ],
            'level' =>
            [
                'level' => 'The level doesn\'t exist'
            ],
            'path' =>
            [
                'path' => 'The path doesn\'t exist'
            ],
            'status' =>
            [
                'status' => 'The status doesn\'t exist'
            ],
            'url' =>
            [
                'url' => 'The URL is not a valid API resource URL'
            ]
        ]);
    }

    protected $container ;

    /**
     * Validates if the course additionalType exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->coursesTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_type failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the audience exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_audience( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->coursesAudiences->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_audience failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the level exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_level( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->coursesLevels->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_level failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the path exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_path( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->coursesPaths->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_path failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the status exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_status( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->coursesStatus->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_status failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    public function validate_url( $value , $input , $args )
    {
        $appUrl = $this->container->settings['app']['url'] ;
        $urlPath = substr( $value , strlen( $appUrl ) ) ;
        if( $this->startsWith( $value , $appUrl ) &&
            preg_match( "/^[a-zA-Z]+\/[0-9]+$/" , $urlPath ) === 1 )
        {
            $split = explode( '/' , $urlPath ) ;

            switch( $split[0] )
            {
                case 'articles' :
                    return $this->container->articles->exist( $split[1] ) ;
                case 'conceptualObjects' :
                    return $this->container->conceptualObjects->exist( $split[1] ) ;
                case 'courses' :
                    return $this->container->courses->exist( $split[1] ) ;
                case 'events' :
                    return $this->container->events->exist( $split[1] ) ;
                case 'organizations' :
                    return $this->container->organizations->exist( $split[1] ) ;
                case 'people' :
                    return $this->container->people->exist( $split[1] ) ;
                case 'places' :
                    return $this->container->places->exist( $split[1] ) ;
                case 'stages' :
                    return $this->container->stages->exist( $split[1] ) ;
            }
        }

        return false ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class( $this ) . ']' ;
    }

    private function startsWith( $haystack , $needle )
    {
        return $haystack[0] === $needle[0]
            ? strncmp( $haystack, $needle, strlen( $needle ) ) === 0
            : false;
    }
}
