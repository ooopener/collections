<?php

namespace com\ooopener\validations;

use Slim\Container;

use Exception;

class DiseaseAnalysisSamplingValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the specific type exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->samplings->exist( (string)$value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_type failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }
}