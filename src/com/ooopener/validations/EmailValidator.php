<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

class EmailValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'value' =>
            [
                'required' => 'The email is required.'
            ],
            'additionalType' =>
            [
                'required'  => 'The additionalType is required.',
                'emailType' => 'The additionalType don\'t exist.'
            ]

        ]);
    }

    /**
     * Validates if the email type value is valid.
     */
    public function validate_emailType( $value , $input , $args )
    {
        try
        {
            return $this->container->emailsTypes->exist( (string)$value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' validate_emailType failed, ' . $e->getMessage() );
            return FALSE ;
        }
    }
}


