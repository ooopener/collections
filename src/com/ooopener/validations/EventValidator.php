<?php

namespace com\ooopener\validations;

use Slim\Container;

use Exception ;

class EventValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'after' =>
            [
                'true' => 'The end date is not after the start date.'
            ]
            ,
            'endDate' =>
            [
                'datetime' => 'The end date is invalid.'
            ]
            ,
            'eventStatus' =>
            [
                'eventStatus' => 'The event status is invalid.'
            ]
            ,
            'location' =>
            [
                'location' => 'The location don\'t exist.'
            ]
            ,
            'startDate' =>
            [
                'datetime' => 'The start date is invalid.'
            ]
            ,
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the event status exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_eventStatus( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->eventsStatusTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_eventStatus failed, value:' . json_encode($value)
                                                    . ' args:' . json_encode($args)
                                                   . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the event name is unique in the users table.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_uniqueEventStatusName( $value , $input , $args )
    {
        return !$this->container->users->existName( $value ) ;
    }

    /**
     * Validates if the event additionalType exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->eventsTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_type failed, value:' . json_encode($value)
                                             . ' args:' . json_encode($args)
                                            . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }
}


