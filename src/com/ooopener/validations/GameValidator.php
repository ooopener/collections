<?php

namespace com\ooopener\validations;

use Exception ;
use Slim\Container;

class GameValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'about' =>
            [
                'course'       => 'The course doesn\'t exist',
                'step'         => 'The step doesn\'t exist',
                'uniqueCourse' => 'The course already taken',
                'uniqueStep'   => 'The step already taken'
            ]
        ]);
    }

    /**
     * Validates if the course exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_course( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->courses->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_course failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the step exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_step( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->steps->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_step failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the course is unique.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_uniqueCourse( $value , $input , $args )
    {

        try
        {
            if( $args && count($args) == 2 )
            {
                return !$this->container->applicationsGames->existAbout( (int)$value , (string)$args[0] , (string)$args[1] ) ;
            }
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_uniqueCourse failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the step is unique.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_uniqueStep( $value , $input , $args )
    {

        try
        {
            if( $args && count($args) == 2 )
            {
                return !$this->container->coursesGames->existAbout( (int)$value , (string)$args[0] , (string)$args[1] ) ;
            }
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_uniqueStep failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }
}