<?php

namespace com\ooopener\validations;

use Imagick ;

use Slim\Container;

class ImageObjectValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addRuleMessages
        ([
            'required' => 'Le champ {field} est requis.',
            'max'      => 'Le champ {field} nécessite {$0} caractère(s) max.',
            'min'      => 'Le champ {field} nécessite {$0} caractère(s) min.'
        ]);

        $this->addFieldMessages
        ([
            'height' =>
            [
                'min' => 'La hauteur de l\'image est trop petite !'
            ]
            ,
            'width' =>
            [
                'min' => 'La largeur de l\'image est trop petite !'
            ]
            ,
            'image' =>
            [
                'image' => "Une image au format jpeg ou png est requise."
            ]
        ]);
    }

    /**
     * Validates if a image is valid.
     */
    public function validate_image( $value , $input , $args )
    {
        return ($value != NULL) && ($value instanceof Imagick) ;
    }
}


