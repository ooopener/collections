<?php

namespace com\ooopener\validations;

use Slim\Container;

class KeywordValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'name' =>
            [
                'required' => 'Un nom est requis.'
            ]
            ,
            'url' =>
            [
                'required' => 'Une url valide est requise.',
                'url'      => 'Une url valide est requise.'
            ]
        ]);
    }
}


