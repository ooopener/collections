<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

use Violin\Violin;

class LivestockNumberValidator extends Violin
{
    public function __construct( Container $container )
    {
        $this->container = $container ;

        $this->addFieldMessages
        ([
            'value' =>
            [
                'value' => 'The value is not valid.'
            ]
            ,
            'type' =>
            [
                'type' => 'The type is not valid.'
            ]
        ]);
    }

    protected $container ;

    /**
     * Validates if the value is valid with the pattern .
     */
    public function validate_value( $value , $input , $args )
    {
        if( empty($value) )
        {
            return $args[0] == 'true' || $args[0] === 'TRUE' ;
        }

        return true ;
    }

    /**
     * Validates if the number type exist.
     */
    public function validate_type( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->livestocksNumbersTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_type failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class( $this ) . ']' ;
    }
}


