<?php

namespace com\ooopener\validations;

use Exception ;
use Slim\Container;

use Violin\Violin;

class LivestockValidator extends Violin
{
    public function __construct( Container $container )
    {
        $this->container = $container ;

        $this->addFieldMessages
        ([
            'organization' =>
            [
                'organization' => 'The organization don\'t exist.'
            ]
        ]);
    }

    protected $container ;

    /**
     * Validates if the specific organization exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_organization( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->organizations->exist( (string)$value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_organization failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class( $this ) . ']' ;
    }
}


