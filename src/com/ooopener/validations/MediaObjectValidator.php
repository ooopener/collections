<?php

namespace com\ooopener\validations;

use Imagick ;

use Slim\Container;

class MediaObjectValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addRuleMessages
        ([
            'required' => 'The field {field} is required.'
        ]);

        $this->addFieldMessages
        ([
            'height' =>
            [
                'min' => 'The height is too low!'
            ]
            ,
            'width' =>
            [
                'min' => 'The width is too low!'
            ]
            ,
            'image' =>
            [
                'image' => "Need a correct mime type."
            ]
        ]);
    }

    /**
     * Validates if a image is valid.
     */
    public function validate_image( $value , $input , $args )
    {
        return ($value != NULL) && ($value instanceof Imagick) ;
    }
}


