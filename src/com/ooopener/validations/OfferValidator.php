<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

class OfferValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'category' =>
            [
                'category' => 'The category is not valid.'
            ]
            ,
            'price' =>
            [
                'price' => 'The price is not valid.'
            ]
            ,
            'validFrom' =>
            [
                'date' => 'The \'validFrom\' date is invalid.'
            ]
            ,
            'validPeriod' =>
            [
                'true' => 'The validFrom and validThrough fields are not valids.'
            ]
            ,
            'validThrough' =>
            [
                'date' => 'The \'validThrough\' date is invalid.'
            ]
        ]);
    }

    /**
     * Validates the category name (if exist in the data table).
     */
    public function validate_category( $value , $input , $args )
    {
        try
        {
            return $this->container->offersCategories->exist( (string)$value ) === TRUE ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn( $this . ' validate_category failed, value:' . json_encode($value) ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the value is a valid price.
     */
    public function validate_price( $value , $input , $args )
    {
        return ($value === '0') || (is_numeric($value) && (($value + 0) > 0)) ;
    }
}


