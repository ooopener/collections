<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

use Violin\Violin;

class OrganizationNumberValidator extends Violin
{
    public function __construct( Container $container )
    {
        $this->container = $container ;

        $this->addFieldMessages
        ([
            'value' =>
            [
                'value' => 'The value is not valid.'
            ]
            ,
            'type' =>
            [
                'type' => 'The type is not valid.'
            ]
        ]);
    }

    protected $container ;

    /**
     * Validates if the value is valid with the pattern .
     */
    public function validate_value( $value , $input , $args )
    {
        if( empty($value) )
        {
            return $args[0] == 'true' || $args[0] === 'TRUE' ;
        }

        // check pattern if set
        if( array_key_exists( 'type' , $input ) && $input['type'] != '' )
        {
            $word = $this->container->organizationsNumbersTypes->get( (string)$input['type'] ) ;

            if( $word )
            {
                if( property_exists( $word , 'pattern' ) && $word->pattern !== null && $word->pattern !== '' )
                {
                    // check pattern
                    if( preg_match( '/' . $word->pattern  . '/' , $value ) === 1  )
                    {
                        if(
                            property_exists( $word , 'validator' ) &&
                            $word->validator !== null && $word->validator !== '' &&
                            method_exists( $this , $word->validator )
                        )
                        {
                            // check method
                            $callFunction = $word->validator ;
                            return $this->{$callFunction}( $value ) ;
                        }
                        else
                        {
                            return true ;
                        }
                    }
                    else
                    {
                        return false ;
                    }
                }
            }
        }

        return true ;
    }

    public function luhn(string $number): bool
    {
        $sum  = 0 ;
        $flag = 0 ;

        for( $i = strlen($number) - 1 ; $i >= 0 ; $i-- )
        {
            $add = $flag++ & 1 ? $number[$i] * 2 : $number[$i] ;
            $sum += $add > 9 ? $add - 9 : $add ;
        }

        return $sum % 10 === 0 ;
    }

    /**
     * Validates if the number type exist.
     */
    public function validate_type( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->organizationsNumbersTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_type failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class( $this ) . ']' ;
    }
}


