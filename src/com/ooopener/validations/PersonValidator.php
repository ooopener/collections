<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

class PersonValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'birthDate' =>
            [
                'simpleDate' => 'The birth date is invalid.'
            ]
            ,
            'deathDate' =>
            [
                'simpleDate' => 'The death date is invalid.'
            ],
            'prefix' =>
            [
                'prefix' => 'The prefix don\'t exist.'
            ]
        ]);
    }

    /**
     * Validates if the specific prefix exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_prefix( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->honorificPrefix->exist( (string)$value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_prefix failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }
}


