<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

class PhoneNumberValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'value' =>
            [
                'required' => 'The phoneNumber is required.'
            ],
            'additionalType' =>
            [
                'required'        => 'The additionalType is required.',
                'phoneNumberType' => 'The additionalType don\'t exist.'
            ]

        ]);
    }

    /**
     * Validates if the phone number type value is valid.
     */
    public function validate_phoneNumberType( $value , $input , $args )
    {
        try
        {
            return $this->container->phoneNumbersTypes->exist( (string)$value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' validate_phoneNumberType failed, ' . $e->getMessage() );
            return FALSE ;
        }
    }
}


