<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

class PlaceValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'addressLocality' =>
            [
                'commune' => "Invalid addressLocality, it's 'commune' don't exist."
            ]
            ,
            'postalCode' =>
            [
                'postalCode' => 'Invalid postal code.'
            ]
            ,
            'telephone' =>
            [
                'telephone' => 'Invalid phone number.'
            ]
            ,
            'additionalType' =>
            [
                'additionalType' => 'The additionalType don\'t exist.'
            ],
            'status' =>
            [
                'status' => 'The place status is invalid.'
            ]
        ]);
    }

    /**
     * Validates if a name is unique in the users table.
     */
    public function validate_postalCode( $value , $input , $args )
    {
        return preg_match('/^0[1-9]|[1-8][0-9]|9[0-8]|2A|2B[0-9]{3}$/',$value) ;
    }

    /**
     * Validates if the event additionalType exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->placesTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_type failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the place status exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_status( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->placesStatusTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_status failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }
}


