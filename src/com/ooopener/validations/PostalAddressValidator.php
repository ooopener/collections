<?php

namespace com\ooopener\validations;

use Slim\Container;

class PostalAddressValidator extends Validation
{
    public function __construct( Container $container , $strict = FALSE )
    {
        parent::__construct( $container ) ;

        $this->strict    = $strict ;

        $this->addFieldMessages
        ([
            'addressLocality' =>
            [
                'commune' => "Invalid addressLocality, it's 'commune' don't exist."
            ]
            ,
            'postalCode' =>
            [
                'postalCode' => 'Invalid postal code.'
            ]
        ]);
    }

    /**
     * Indicates if the validation is strict.
     */
    public $strict ;

    /**
     * Validates if a name is unique in the users table.
     */
    public function validate_commune( $value , $input , $args )
    {
        if( $this->strict )
        {
            return $this->container->insee->existCommune( $value ) ;
        }
        else
        {
            return ($value == NULL)
                || ($value == '')
                || $this->container->insee->existCommune( $value ) ;
        }
    }

    /**
     * Validates if a name is unique in the users table.
     */
    public function validate_postalCode( $value , $input , $args )
    {
        if( $this->strict )
        {
            return preg_match('/^0[1-9]|[1-8][0-9]|9[0-8]|2A|2B[0-9]{3}$/',$value) ;
        }
        else
        {
            return ($value == NULL)
                || ($value == '')
                || preg_match('/^0[1-9]|[1-8][0-9]|9[0-8]|2A|2B[0-9]{3}$/',$value) ;
        }
    }
}


