<?php

namespace com\ooopener\validations;

use Exception ;
use Slim\Container;

use Violin\Violin;

class SectorValidator extends Violin
{
    public function __construct( Container $container )
    {
        $this->container = $container ;

        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'additionalType' => 'The type don\'t exist.'
            ]
            ,
            'location' =>
            [
                'location' => 'The location don\'t exist.'
            ]
        ]);
    }

    protected $container ;

    /**
     * Validates if the specific location exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_location( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->places->exist( $value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_location failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the sector type exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_additionalType( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->sectorsTypes->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_additionalType failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class( $this ) . ']' ;
    }
}


