<?php

namespace com\ooopener\validations;

use Slim\Container;
use Violin\Violin;

use Exception ;

class StepValidator extends Violin
{
    public function __construct( Container $container )
    {
        $this->container = $container ;

        $this->addFieldMessages
        ([
            'stage' =>
            [
                'stage' => 'The stage doesn\'t exist'
            ]
        ]);
    }

    protected $container ;

    /**
     * Validates if the stage exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_stage( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->stages->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_stage failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class( $this ) . ']' ;
    }
}