<?php

namespace com\ooopener\validations;

use Slim\Container;

use Exception ;

class TeamValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'color' =>
            [
                'isColor' => 'The color expression is invalid, ex: FF0000.'
            ],
            'name' =>
            [
                'uniqueName' => 'The name must be unique.'
            ]
        ]);
    }

    /**
     * Validates if it is a color.
     */
    public function validate_isColor( $value , $input , $args )
    {
        return (bool) preg_match( '/^[a-f0-9]{6}$/i' , $value ) ;
    }

    /**
     * Validates if a name is unique in the table.
     */
    public function validate_uniqueName( $value , $input , $args )
    {
        $this->container->logger->info( $this . ' validate_uniqueName, value:' . json_encode($value) . ' args:' . json_encode($args) ) ;
        try
        {
            if( $args && count($args) > 0 )
            {
                if
                (
                    $this->container->teams->exist( $args[0] , [ 'key' => 'name' ] )
                    && ($args[0] == $value)
                )
                {
                    return TRUE ;
                }
            }

            return $this->container->teams->exist( $value , [ 'key' => 'name' ] ) === FALSE ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn( $this . ' validate_uniqueName failed, value:' . json_encode($value) . ' args:' . json_encode($args) . ' error:' . $e->getMessage() ) ;
        }

        return FALSE ;
    }
}