<?php

namespace com\ooopener\validations;

use com\ooopener\models\Thesaurus;

use Slim\Container;

use Exception ;

class ThesaurusValidator extends Validation
{
    /**
     * ThesaurusValidator constructor.
     *
     * @param Container $container
     * @param Thesaurus $model
     */
    public function __construct( Container $container , Thesaurus $model = NULL )
    {
        parent::__construct( $container ) ;

        $this->model     = $model ;

        $this->addFieldMessages
        ([
            'bgcolor' =>
            [
                'isColor' => 'The color expression is invalid, ex: FF0000.'
            ]
            ,
            'color' =>
            [
                'isColor' => 'The color expression is invalid, ex: FF0000.'
            ]
            ,
            'alternateName' =>
            [
                'alnumSlash'          => 'The alternate name must be alphanumeric with slashes, dashes and underscores permitted.' ,
                'notEmpty'            => 'That alternate name is not empty.' ,
                'uniqueAlternateName' => 'That alternate name is already in use.'
            ]
        ]);
    }

    protected $model ;

    /**
     * Validates if a context name is unique.
     */
    public function validate_alnumSlash( $value , $input , $args )
    {
        return (bool) preg_match( '/^[\pL\pM\pN\/]+$/u' , $value );
    }

    /**
     * Validates if it is a color.
     */
    public function validate_isColor( $value , $input , $args )
    {
        return (bool) preg_match( '/^[a-f0-9]{6}$/i' , $value ) ;
    }

    /**
     * Validates if a name is unique in the thesaurus table.
     */
    public function validate_notEmpty( $value , $input , $args )
    {
        return isset($value) && !empty($value) ;
    }

    /**
     * Validates if a name is unique in the thesaurus table.
     */
    public function validate_uniqueAlternateName( $value , $input , $args )
    {
        $this->container->logger->info( $this . ' validate_uniqueName, value:' . json_encode($value) . ' args:' . json_encode($args) ) ;
        try
        {
            if( $args && count($args) > 0 )
            {
                if
                (
                    $this->model->existByAlternateName( $args[0] )
                    && ($args[0] == $value)
                )
                {
                    return TRUE ;
                }
            }

            return $this->model->existByAlternateName( $value ) === FALSE ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn( $this . ' validate_uniqueName failed, value:' . json_encode($value) . ' args:' . json_encode($args) . ' error:' . $e->getMessage() ) ;
        }

        return FALSE ;
    }
}


