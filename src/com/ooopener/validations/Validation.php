<?php

namespace com\ooopener\validations;

use DateTime ;
use Exception ;

use Slim\Container;

use Violin\Violin;

/**
 * Class Validator
 * @package com\ooopener\validations
 */
class Validation extends Violin
{
    /**
     * Validator constructor.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        $this->container = $container ;
    }

    /**
     * @var Container
     */
    protected $container ;

    /**
     * Validates if the date is valid with the pattern 'yyyy-mm-dd'.
     */
    public function validate_date( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        if( $this->validateDate( $value ) )
        {
            return TRUE ;
        }

        return FALSE  ;
    }

    /**
     * Validates if the date is valid .
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_datetime( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        return $this->validateISO8601Date( $value )  ;
    }

    /**
     * Validates if the specific location exist.
     *
     * @param $value
     * @param $input
     * @param $args
     *
     * @return bool
     */
    public function validate_location( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->places->exist( (string)$value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_location failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the date is valid with the pattern 'yyy-mm-dd'.
     *
     * @param string $value
     * @param string $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_simpleDate( $value , $input , $args )
    {
        if( empty($value) )
        {
            return $args[0] == 'true' || $args[0] === 'TRUE' ;
        }

        $d1 = DateTime::createFromFormat( 'Y-m-d' , $value );
        $d2 = DateTime::createFromFormat( 'Y-m'   , $value );
        $d3 = DateTime::createFromFormat( 'Y'     , $value );

        $b1 = $d1 && $d1->format('Y-m-d') === $value ;
        $b2 = $d2 && $d2->format('Y-m')   === $value ;
        $b3 = $d3 && $d3->format('Y')     === $value ;

        return $b1 || $b2 || $b3 ;
    }

    /**
     * Validates the team.
     *
     * @param string $value
     * @param string $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_team( $value , $input , $args )
    {
        $valids = ['admin', 'guest'] ;

        if( $this->container )
        {
            $auth = $this->container->auth ;

            if( $auth->team->name == 'superadmin' )
            {
                $valids[] = 'superadmin' ;
            }
        }

        return in_array( $value , $valids ) ;
    }

    /**
     * Validates if the hours are valid 'hh:mm,hh:mm,....'.
     */
    public function validate_times( $value , $input , $args )
    {
        if( is_string($value) )
        {
            $times = explode( ',' , $value ) ;
            foreach( $times as $time )
            {
                if( !$this->validateDate( $time , 'H:i' ) )
                {
                    return FALSE ;
                }
            }
            return TRUE ;
        }

        return FALSE  ;
    }

    /**
     * Validates if true'.
     */
    public function validate_true( $value , $input , $args )
    {
        return $value === TRUE ;
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class( $this ) . ']' ;
    }

    private function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    private function validateISO8601Date($dateStr)
    {
        if( preg_match('/^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/', $dateStr) > 0 )
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}


