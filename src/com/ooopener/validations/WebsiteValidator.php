<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

class WebsiteValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'additionalType' =>
            [
                'required'    => 'A valid website type is required.',
                'websiteType' => 'A valid website type is required.'
            ],
            'url' =>
            [
                'required' => 'A valid url is required.',
                'url'      => 'A valid url is required.'
            ]
            ,
            'rel' =>
            [
                'required'    => 'A valid website type is required.',
                'websiteType' => 'A valid website type is required.'
            ]
        ]);
    }

    /**
     * Validates if the alternateName value is unique.
     */
    public function validate_websiteType( $value , $input , $args )
    {
        try
        {
            return $this->container->websitesTypes->exist( (string)$value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn( $this . ' validate_websiteType failed, ' . $e->getMessage() );
            return FALSE ;
        }
    }
}


