<?php

namespace com\ooopener\validations;

use Exception ;

use Slim\Container;

use Violin\Violin;

class WorkplaceValidator extends Violin
{
    public function __construct( Container $container )
    {
        $this->container = $container ;

        $this->addFieldMessages
        ([
            'identifier' =>
            [
                'identifier' => 'The identifier is not valid.'
            ]
        ]);
    }

    protected $container ;

    /**
     * Validates if the identifier exist.
     */
    public function validate_identifier( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->livestockNumbers->exist( (string)$value ) ;
        }
        catch (Exception $e)
        {
            $this->container->logger->warn
            (
                $this . ' validate_identifier failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }


    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class( $this ) . ']' ;
    }
}


