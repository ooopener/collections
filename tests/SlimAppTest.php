<?php

use PHPUnit\Framework\TestCase ;

use Slim\App;
use Slim\Container ;
use Slim\Http\Environment ;
use Slim\Http\Request ;
use Slim\Http\RequestBody ;
use Slim\Http\Uri ;
use Slim\Http\Headers ;
use Slim\Http\UploadedFile ;

// ------------------------

define ( '__APP__'  , __DIR__ . "/../" ) ;
define ( '__MODE__' , 'dev' ) ;


// ---------------------------------- session

session_cache_limiter(FALSE);
session_start() ;

// ---------------------------------- errors

error_reporting(-1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
date_default_timezone_set('UTC');

// ---------------------------------- autoloader

require __APP__ . 'vendor/autoload.php';


// ------------------------


class SlimAppTest extends TestCase
{
    public $container;
    public $application;

    public function setUp()
    {
        // ---------------------------------- config

        $config = array_merge
        (
            parse_ini_file( __APP__ . 'config/commons.ini' , TRUE ) ,
            parse_ini_file( __APP__ . 'config/config.' . __MODE__ . '.ini' , TRUE ) ,
            parse_ini_file( __APP__ . 'config/errors.ini' , TRUE )
        ) ;

        $config['useLogging'] = FALSE ;

        ini_set( 'display_errors'         , $config['display_errors'] ) ;
        ini_set( 'display_startup_errors' , $config['display_startup_errors']  ) ;

        // ---------------------------------- firebase config

        $firebase = json_decode( file_get_contents( __APP__ . 'config/google-services.json' ) , true );


        $container   = new Container( [ 'settings' => $config , 'firebase' => $firebase ] ) ;
        $application = new App( $container );

        // ------------------------

        require __APP__ . 'dependencies.php' ;
        require __APP__ . 'middlewares.php' ;

        require __APP__ . 'models.php' ;
        require __APP__ . 'controllers.php' ;

        require __APP__ . 'routes.php' ;

        $this->container   = $container ;
        $this->application = $application ;

        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $_SERVER['HTTP_USER_AGENT'] = 'Slim Framework';
    }

    protected function getRequest( string $method, string $path, array $query = null , array $data = null , array $cookies = null , string $token = null )
    {
        $method = strtoupper( $method ) ;

        $version = $this->container->settings['version'];

        $headers =
        [
            'USER_AGENT'      => 'Slim Framework' ,
            'X_FORWARDED_FOR' => '127.0.0.1' ,
            'REQUEST_METHOD'  => $method ,
            'REQUEST_URI'     => '/' . $version . $path ,
            'SCRIPT_NAME'     => '/' . $version . '/index.php'
        ];

        $form = FALSE ;

        if( isset( $query ) && !empty( $query ) )
        {
            $headers['QUERY_STRING'] = http_build_query( $query ) ;
        }

        if( isset( $data ) && !empty( $data ) )
        {
            if( $method == 'POST' || $method == 'PUT' )
            {
                $form = TRUE ;
                $headers['CONTENT_TYPE'] = 'application/x-www-form-urlencoded' ;
            }
        }

        if( isset( $token ) && !empty( $token ) )
        {
            $headers['HTTP_AUTHORIZATION'] = 'Bearer ' . $token ;
        }

        if( !isset( $cookies ) )
        {
            $cookies = [] ;
        }

        $environment   = Environment::mock( $headers ) ;
        $uri           = Uri::createFromEnvironment( $environment ) ;
        $headers       = Headers::createFromEnvironment( $environment ) ;
        $serverParams  = $environment->all() ;
        $body          = new RequestBody() ;
        $uploadedFiles = UploadedFile::createFromEnvironment( $environment ) ;

        $request = new Request( $method , $uri , $headers , $cookies , $serverParams , $body , $uploadedFiles ) ;

        if( $form == TRUE && in_array($request->getMediaType(), ['application/x-www-form-urlencoded', 'multipart/form-data'] ) )
        {
            $request = $request->withParsedBody( $data ) ;
        }

        return $request ;
    }
}
