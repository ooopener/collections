<?php

use Slim\Http\Response ;

class ApplicationControllerTest extends SlimAppTest
{
    public function testVersion()
    {
        $request = $this->getRequest( 'GET' , '/version' ) ;

        $response = new Response() ;
        $response = $this->container->applicationController->version( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) ) ;
        $this->assertRegExp( '/^application\/json/' , $response->getHeader( 'Content-Type' )[0] ) ;
    }
}