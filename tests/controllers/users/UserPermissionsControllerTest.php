<?php

use Slim\Http\Response ;

use com\ooopener\models\Model ;

class UserPermissionsControllerTest extends SlimAppTest
{
    public $owner ;

    public $permission ;

    public function setUp()
    {
        parent::setUp();

        $this->owner      = $this->createMock( Model::class ) ;
        $this->permission = $this->createMock( Model::class ) ;

        $this->container->userPermissionsController->owner      = $this->owner ;
        $this->container->userPermissionsController->permission = $this->permission ;
    }

    public function testTeamPermissionsUnknownTeam()
    {
        // fake db result
        $this->container->userPermissionsController->owner->method( 'get' )
            ->will( $this->returnValue( false ) ) ;

        $request = $this->getRequest( 'GET' , '/user/bazinga/permissions' ) ;

        $response = new Response() ;
        $response = $this->container->userPermissionsController->all( $request , $response ) ;

        $this->assertEquals( 404 , $response->getStatusCode() ) ;
    }

    public function testTeamPermissionsResult()
    {
        // fake db results
        $rowDBPermissions =
        [
            (object)
            [
                'module'     => 'events' ,
                'resource'   => '0' ,
                'permission' => 'A'
            ],
            (object)
            [
                'module'     => 'objects' ,
                'resource'   => '0' ,
                'permission' => 'W'
            ],
            (object)
            [
                'module'     => 'organizations' ,
                'resource'   => '0' ,
                'permission' => 'R'
            ],
            (object)
            [
                'module'     => 'people' ,
                'resource'   => '0' ,
                'permission' => 'D'
            ],
            (object)
            [
                'module'     => 'places' ,
                'resource'   => '1' ,
                'permission' => 'R'
            ],
            (object)
            [
                'module'     => 'thesaurus' ,
                'resource'   => '0' ,
                'permission' => 'E'
            ],
        ];

        $this->container->userPermissionsController->owner->method( 'get' )
            ->will( $this->returnValue( true ) ) ;

        $this->container->userPermissionsController->permission->method( 'all' )
            ->will( $this->returnValue( $rowDBPermissions ) ) ;

        $request = $this->getRequest( 'GET' , '/user/bazinga/permissions' ) ;

        $response = new Response() ;
        $response = $this->container->userPermissionsController->all( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
        $this->assertTrue( $response->hasHeader( 'Content-Type' ) ) ;
        $this->assertRegExp( '/^application\/json/' , $response->getHeader( 'Content-Type' )[0] ) ;

        $body = json_decode( $response->getBody( true ) ) ;

        $this->assertEquals( 'success' , $body->status ) ;

        $expectedResult = (object)
        [
            'events'        => 'admin' ,
            'objects'       => 'write' ,
            'organizations' => 'read' ,
            'places/1'      => 'read'
        ];

        $this->assertEquals( $expectedResult , $body->result ) ;

    }
}