<?php

use Slim\Http\Response ;

use com\ooopener\models\Tokens ;
use com\ooopener\models\Users ;

class UsersControllerTest extends SlimAppTest
{
    public $model ;

    public $tokens ;

    public function setUp()
    {
        parent::setUp();

        $this->model  = $this->createMock( Users::class ) ;
        $this->tokens = $this->createMock( Tokens::class ) ;

        $this->container->usersController->model = $this->model ;
        $this->container->tokens = $this->tokens ;
    }

    public function testUsersPutEmpty()
    {
        // fake db result
        $this->container->usersController->model->method( 'getByProperty' )
            ->will( $this->returnValue( false ) ) ;

        $request = $this->getRequest( 'PUT' , '/user/bazinga' ) ;

        $response = new Response() ;
        $response = $this->container->usersController->put( $request , $response ) ;

        $this->assertEquals( 404 , $response->getStatusCode() ) ;
    }

    public function testUsersPutNotUpdate()
    {
        // fake db results
        $this->container->usersController->model->method( 'getByProperty' )
            ->will( $this->returnValue( true ) ) ;

        $this->container->usersController->model->method( 'update' )
            ->will( $this->returnValue( false ) ) ;


        $data_request =
        [
            'givenName'  => 'test' ,
            'familyName' => 'unit' ,
            'team'       => 'superadmin'
        ];

        $auth = (object)
        [
            'team' => 'superadmin'
        ];

        $this->container->auth = $auth ;

        $request = $this->getRequest( 'PUT' , '/user/bazinga' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->usersController->put( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testUsersPutRevoke()
    {
        // fake db result
        $this->container->usersController->model->method( 'getByProperty' )
            ->will( $this->returnValue( true ) ) ;

        $this->container->usersController->model->method( 'update' )
            ->will( $this->returnValue( true ) ) ;

        $this->container->tokens->method( 'revokeAll' )
            ->will( $this->returnValue( true ) ) ;

        $data_request =
        [
            'givenName'  => 'test' ,
            'familyName' => 'unit' ,
            'team'       => 'superadmin'
        ];

        $auth = (object)
        [
            'team' => 'superadmin'
        ];

        $this->container->auth = $auth ;

        $request = $this->getRequest( 'PUT' , '/user/bazinga' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->usersController->put( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
    }
}