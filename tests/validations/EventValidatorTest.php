<?php

use com\ooopener\validations\EventValidator ;

class EventValidatorTest extends SlimAppTest
{
    public $event ;

    public function setUp()
    {
        parent::setUp() ;
        $this->event = new EventValidator( $this->container ) ;
    }

    public function testValidateSimpleDate()
    {
        $value = "" ;
        $args = [ 'required' ] ;

        $this->assertFalse( $this->event->validate_simpleDate( $value , null , $args ) ) ;

        $value = "2017-01-01" ;
        $this->assertTrue( $this->event->validate_simpleDate( $value ) ) ;

        $value = "2017-21-01" ;
        $this->assertFalse( $this->event->validate_simpleDate( $value ) ) ;

        $value = "2017-01-32" ;
        $this->assertFalse( $this->event->validate_simpleDate( $value ) ) ;

        $value = "2017-02-30" ;
        $this->assertFalse( $this->event->validate_simpleDate( $value ) ) ;
    }
}