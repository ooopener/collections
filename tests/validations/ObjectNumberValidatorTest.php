<?php

use com\ooopener\validations\ObjectNumberValidator ;

class ObjectNumberValidatorTest extends SlimAppTest
{
    public $object ;

    public function setUp()
    {
        parent::setUp() ;
        $this->object = new ObjectNumberValidator( $this->container ) ;
    }

    public function testValidateSimpleDate()
    {
        $value = "" ;
        $args = [ 'required' ] ;

        $this->assertFalse( $this->object->validate_simpleDate( $value , null , $args ) ) ;

        $value = "2017-01-01" ;
        $this->assertTrue( $this->object->validate_simpleDate( $value ) ) ;

        $value = "2017-21-01" ;
        $this->assertFalse( $this->object->validate_simpleDate( $value ) ) ;

        $value = "2017-01-32" ;
        $this->assertFalse( $this->object->validate_simpleDate( $value ) ) ;

        $value = "2017-02-30" ;
        $this->assertFalse( $this->object->validate_simpleDate( $value ) ) ;
    }
}