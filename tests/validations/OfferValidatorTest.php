<?php

use com\ooopener\validations\OfferValidator ;

class OfferValidatorTest extends SlimAppTest
{
    public $offer ;

    public function setUp()
    {
        parent::setUp() ;
        $this->offer = new OfferValidator( $this->container ) ;
    }

    public function testValidateymdDate()
    {
        $value = "" ;
        $args = [ 'required' ] ;

        $this->assertFalse( $this->offer->validate_ymdDate( $value , null , $args ) ) ;

        $value = "2017-01-01" ;
        $this->assertTrue( $this->offer->validate_ymdDate( $value ) ) ;

        $value = "2017-21-01" ;
        $this->assertFalse( $this->offer->validate_ymdDate( $value ) ) ;

        $value = "2017-01-32" ;
        $this->assertFalse( $this->offer->validate_ymdDate( $value ) ) ;

        $value = "2017-02-30" ;
        $this->assertFalse( $this->offer->validate_ymdDate( $value ) ) ;

        $value = "2017-5-5" ;
        $this->assertFalse( $this->offer->validate_ymdDate( $value ) ) ;
    }

    public function testValidatePrice()
    {
        $value = "" ;

        $this->assertFalse( $this->offer->validate_price( $value ) ) ;

        $value = "0" ;
        $this->assertTrue( $this->offer->validate_price( $value ) ) ;

        $value = "10" ;
        $this->assertTrue( $this->offer->validate_price( $value ) ) ;

        $value = "-0" ;
        $this->assertFalse( $this->offer->validate_price( $value ) ) ;

        $value = "-20" ;
        $this->assertFalse( $this->offer->validate_price( $value ) ) ;
    }
}