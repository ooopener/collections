<?php

use com\ooopener\validations\OpeningHoursSpecificationValidator ;

class OpeningHoursSpecificationValidatorTest extends SlimAppTest
{
    public $opening ;

    public function setUp()
    {
        parent::setUp() ;
        $this->opening = new OpeningHoursSpecificationValidator( $this->container ) ;
    }

    public function testValidateSimpleDate()
    {
        $value = "" ;
        $args = [ 'required' ] ;

        $this->assertFalse( $this->opening->validate_isValidDate( $value , null , $args ) ) ;

        $value = "2017-01-01" ;
        $this->assertTrue( $this->opening->validate_isValidDate( $value ) ) ;

        $value = "2017-21-01" ;
        $this->assertFalse( $this->opening->validate_isValidDate( $value ) ) ;

        $value = "2017-01-32" ;
        $this->assertFalse( $this->opening->validate_isValidDate( $value ) ) ;

        $value = "2017-02-30" ;
        $this->assertFalse( $this->opening->validate_isValidDate( $value ) ) ;
    }

    public function testValidateTimes()
    {
        $value = "" ;

        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "blabla" ;
        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "1:2" ;
        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "95:52" ;
        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "24:10" ;
        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "23:60" ;
        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "-95:52" ;
        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "95:-52" ;
        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "-05:52" ;
        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "05:-52" ;
        $this->assertFalse( $this->opening->validate_times( $value ) ) ;

        $value = "00:00" ;
        $this->assertTrue( $this->opening->validate_times( $value ) ) ;

        $value = "23:59" ;
        $this->assertTrue( $this->opening->validate_times( $value ) ) ;

        $value = "05:52" ;
        $this->assertTrue( $this->opening->validate_times( $value ) ) ;
    }
}