<?php

use com\ooopener\validations\ThesaurusValidator ;

class ThesaurusValidatorTest extends SlimAppTest
{
    public $thesaurus ;

    public function setUp()
    {
        parent::setUp() ;
        $this->thesaurus = new ThesaurusValidator( $this->container ) ;
    }

    public function testValidateAlnumSlash()
    {
        $value = "" ;

        $this->assertFalse( $this->thesaurus->validate_alnumSlash( $value ) ) ;

        $value = "!sf" ;
        $this->assertFalse( $this->thesaurus->validate_alnumSlash( $value ) ) ;

        $value = "\sf" ;
        $this->assertFalse( $this->thesaurus->validate_alnumSlash( $value ) ) ;

        $value = "bazinga" ;
        $this->assertTrue( $this->thesaurus->validate_alnumSlash( $value ) ) ;

        $value = "1245" ;
        $this->assertTrue( $this->thesaurus->validate_alnumSlash( $value ) ) ;

        $value = "bazinga/yep" ;
        $this->assertTrue( $this->thesaurus->validate_alnumSlash( $value ) ) ;

        $value = "bazinga/yep/1254" ;
        $this->assertTrue( $this->thesaurus->validate_alnumSlash( $value ) ) ;
    }

    public function testValidateIsColor()
    {
        $value = "" ;

        $this->assertFalse( $this->thesaurus->validate_isColor( $value ) ) ;

        $value = "00" ;
        $this->assertFalse( $this->thesaurus->validate_isColor( $value ) ) ;

        $value = "0000000" ;
        $this->assertFalse( $this->thesaurus->validate_isColor( $value ) ) ;

        $value = "azerty" ;
        $this->assertFalse( $this->thesaurus->validate_isColor( $value ) ) ;

        $value = "000000" ;
        $this->assertTrue( $this->thesaurus->validate_isColor( $value ) ) ;

        $value = "FFFFFF" ;
        $this->assertTrue( $this->thesaurus->validate_isColor( $value ) ) ;

        $value = "FF19AC" ;
        $this->assertTrue( $this->thesaurus->validate_isColor( $value ) ) ;
    }
}