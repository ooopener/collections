<?php

use com\ooopener\validations\UserValidator ;

class UserValidatorTest extends SlimAppTest
{
    public $user ;

    public function setUp()
    {
        parent::setUp() ;
        $this->user = new UserValidator( $this->container ) ;
    }

    public function testValidateTeam()
    {
        $value = "" ;
        $args = [ 'required' ] ;

        $this->assertTrue( $this->user->validate_team( $value ) ) ;
        $this->assertFalse( $this->user->validate_team( $value ,  null , $args ) ) ;

        $value = "superadmin" ;
        $this->assertTrue( $this->user->validate_team( $value ) ) ;

        $value = "bazinga" ;
        $this->assertFalse( $this->user->validate_team( $value ) ) ;
    }
}