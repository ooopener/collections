<?php

use com\ooopener\validations\Validation ;

class ValidationTest extends SlimAppTest
{
    public $validation ;

    public function setUp()
    {
        parent::setUp() ;
        $this->validation = new Validation( $this->container ) ;
    }

    public function testValidateTeam()
    {
        $value = "admin" ;
        $this->assertTrue( $this->validation->validate_team( $value ) ) ;

        $value = "guest" ;
        $this->assertTrue( $this->validation->validate_team( $value ) ) ;

        $value = "superadmin" ;
        $this->assertFalse( $this->validation->validate_team( $value ) ) ;
    }

    public function testValidateSimpleDate()
    {
        $value = "admin" ;
        $this->assertFalse( $this->validation->validate_simpleDate( $value ) ) ;

        $value = "2017-21-01" ;
        $this->assertFalse( $this->validation->validate_simpleDate( $value ) ) ;

        //$value = "2017-02-30" ;
        //$this->assertFalse( $this->validation->validate_simpleDate( $value ) ) ;

        $value = "2017-01-32" ;
        $this->assertFalse( $this->validation->validate_simpleDate( $value ) ) ;

        $value = "2017-01-01" ;
        $this->assertTrue( $this->validation->validate_simpleDate( $value ) ) ;
    }
}